;+
; $id: geometry_index.pro, v 1.1 2012/09/26 14:00:00 xxu
;
; Purpose:
;   - Calculate the geometry index for UNL-VRTM output of STOKES parameter
;
; Inputs:
;   - n_sza = number of solar zenith angles
;   - n_vza = ......... viewing zenith angles
;   - n_raz = ......... relative azimuthal angles
;   - i_sza = subscripts (index) of the specified solar zenith angle 
;   - i_vza = ................................... viewing zenith angle
;   - i_raz = ................................... relative azimuthal angle
;
;    Note that i_sza, (i_vza, and i_raz) should be a number between 0 
;    and n_sza-1, (n_vza-1, and n_raz-1), following IDL array's 
;    indixing rule.
;
; Returns:
;    - i_geom = index for the geometry dimension of variable STOKES.
;    - optional argument, n_geom = total number of geometry combinations

  function geometry_index, n_sza, n_vza, n_raz, $
                           i_sza, i_vza, i_raz, $
                           n_geom = n_geom

  ; total number of geometry combinations
  n_geom = n_sza * n_vza * n_raz

  ; vza offset
  vza_offset = i_sza * n_vza + i_vza

  ; geometry index
  i_geom = n_raz * vza_offset + i_raz

  ; return to the calling routine
  return, i_geom

  ; End of function
  end
