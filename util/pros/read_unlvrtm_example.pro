;+
; $id:read_unlvrtm_example.pro, v 1.0, 2012/09/24 10:39:02 CST by xxu
;
; Purpose:
;   - an example code reading the UNL-VRTM output, a NetCDF file
;
; Required routines:
;   - ncdf_read.pro (from gamap library)

; specify the file name 
  filename = 'cntl_LAMDA.0001.nc'

; read variables from the file, variables' name are needed. 
; The variables named can find from the diaginfo file, which is generated 
; by UNL-VRTM together with the NetCDF file.
; It should be noted that the returned 'data' is a structrue, which contains
; all the specified variables.
  ncdf_read, data, filename=filename, $
             variables=['LAMDA','R_SFC','TAUAER','STOKES', 'LPAR_NAMES', 'JCOBIANS_COLUMN']

; to check the returned data
  help, data, /str

; retrieve varaibles, make sure you know their dimensions 
  lamda = data.lamda
  r_sfc = data.r_sfc
  tau   = data.tauaer
  stoks = data.stokes 
  lpars = strtrim( string(data.lpar_names), 2 )  ; get the linearization parameter names

; end of program
  end

