#!/bin/sh

# dirs
bashdir=/work/jwang7/xxu/ART_RUNS/run4xiong/runbash/
namelistdir=/work/jwang7/xxu/ART_RUNS/run4xiong/namelists/
rootrun=/work/jwang7/xxu/ART_RUNS/run4xiong/



# echo start time
echo Start time is `date`

for band     in o2aband # co2stro capi380 capi674 capi870 capi1375 capi1640
do 
for surface  in water grass
do 
for atmos    in ray so4 bb so4bb # wc ic wcic
do

casename=${band}_${surface}_${atmos}

echo $casename

rundir=${rootrun}${casename}
namelistfile=${namelistdir}namelist.ini.${casename}


# Check if the run packages are there
if [ -d $rundir ]; then
   echo ' - Run packages are already there ...'
else
   echo ' - First time of this job, create the run package now ...'
   mkdir $rundir
fi

cd $rundir
echo Run directory is `pwd`

# copy files
cp -fv $bashdir/unlvrtm.exe ./
cp -fv $namelistfile ./namelist.ini

echo '#!/bin/sh' > run.sh
echo '#PBS -N '$casename >> run.sh
echo '#PBS -l select=1' >> run.sh
echo '#PBS -l walltime=20:00:00' >> run.sh
echo '#PBS -o '${casename}.o >> run.sh
echo '#PBS -e '${casename}.e >> run.sh

echo 'cd ' $rundir >> run.sh
echo './unlvrtm.exe > ' ${casename}.log >> run.sh

chmod u+x run.sh

# run the model
qsub run.sh

done
done
done

# echo end time
echo End time is `date`

