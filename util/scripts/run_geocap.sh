#!/bin/sh

# define directories
root=/work/jwang7/xxu/ART_RUNS/v1.1.2/run-geocape/

# define geometries & surface
for sza_case in sza1 sza2 sza3;          do
for vza_case in vza1 vza2 vza3;          do
for surface  in wat  gra;                do
for lamda    in 340 380 470 640 670 860; do
#for lamda    in 640;                     do          

  # solar zenith angles
  case "${sza_case}" in
    'sza1') sza='  0.5  3.37  6.76 10.16 13.61 17.10' ;;
    'sza2') sza='20.67 24.32 28.07 31.97 36.03 40.32' ;;
    'sza3') sza='44.90 49.88 55.44 61.93 70.25 80.50' ;;
  esac

  # view zenith angles
  case "${vza_case}" in
    'vza1') vza=' 2.57  7.71 12.86 18.00 23.14 28.29' ;;
    'vza2') vza='33.43 38.57 43.71 48.86 54.00 59.14' ;;
    'vza3') vza='64.29 69.43 74.57 79.71 84.86 88.50' ;;
  esac

  # surface
  case "$surface" in
  'wat') sfc='2' ;;
  'gra') sfc='3' ;;
  esac

  # wavelength
  case "${lamda}" in
  '340') refr01='1.384      8.970E-09'
         refr02='1.592      3.318E-03' 
         ;;
  '380') refr01='1.378      5.957E-09'
         refr02='1.584      2.729E-03'
         ;;
  '440') refr01='1.372      4.331E-09'
         refr02='1.574      1.986E-03'
         ;;
  '470') refr01='1.370      4.218E-09'
         refr02='1.569      1.686E-03'
         ;;
  '640') refr01='1.366      1.566E-08'
         refr02='1.551      6.786E-04'
         ;;
  '670') refr01='1.366      2.145E-08'
         refr02='1.549      6.732E-04'
         ;;
  '870') refr01='1.363      2.866E-07'
         refr02='1.540      2.000E-03'
         ;;
  esac

  # define case name
  casename=${surface}_${lamda}_${sza_case}_${vza_case}
  echo $casename
  rundir=${root}runs/${casename}

  # Check if the run packages are there
  if [ -d $rundir ]; then
     echo ' - Run packages are already there ...'
  else
     echo ' - First time of this job, create the run package now ...'
     mkdir $rundir
  fi
  cd $rundir
  echo Run directory is `pwd`

  # copy files
  cp -fv ${root}unlvrtm.exe ./
  cp -fv ${root}NAMELIST.INI ./namelist.ini

  # edit the namelist file for each individual case
  sed "s/XX_LAMDA/$lamda/g" namelist.ini > namelist.ini.temp
  mv namelist.ini.temp namelist.ini
  sed "s/XX_SZA/$sza/g" namelist.ini > namelist.ini.temp
  mv namelist.ini.temp namelist.ini
  sed "s/XX_VZA/$vza/g" namelist.ini > namelist.ini.temp
  mv namelist.ini.temp namelist.ini
  sed "s/XX_SFC/$sfc/g" namelist.ini > namelist.ini.temp
  mv namelist.ini.temp namelist.ini
  sed "s/XX_REFR01/$refr01/g" namelist.ini > namelist.ini.temp
  mv namelist.ini.temp namelist.ini
  sed "s/XX_REFR02/$refr02/g" namelist.ini > namelist.ini.temp
  mv namelist.ini.temp namelist.ini

  # generate a run script for submitting job
  echo '#!/bin/sh' > run.sh
  echo '#PBS -N '$casename >> run.sh
  echo '#PBS -l select=1' >> run.sh
  echo '#PBS -l walltime=01:00:00' >> run.sh
  echo '#PBS -o '${casename}.o >> run.sh
  echo '#PBS -e '${casename}.e >> run.sh
  echo ' ' >> run.sh
  echo 'cd ' $rundir >> run.sh
  echo 'time ./unlvrtm.exe > ' ${casename}.log >> run.sh
  echo 'rm unlvrtm.exe' >> run.sh

  chmod u+x run.sh

  # run the model
  qsub run.sh

done
done
done
done

exit
