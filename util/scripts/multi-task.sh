#!/bin/sh

# define directories
root=/work/jwang7/xxu/ART_RUNS/v1.3/aeronet/bj110215_0230/

# define cases
for case2 in 0 N L NL; do
for case1 in A AR APR; do

  cd ${root}
  
  this_case=${case1}${case2}
  rundir=${root}run-${this_case}
  echo ${rundir}
  mkdir ${rundir}
  cd ${rundir}
  cp ../namelist/namelist.ini . 
  cp ../namelist/inv_namelist.ini-${this_case} inv_namelist.ini
  cp ../spectra.dat .
  cp ../unlvrtm-aeronet-inv.exe .

  # generate a run script for submitting job 
  echo '#!/bin/sh' > run.sh
  echo '#SBATCH --job-name=r.'${this_case} >> run.sh
  echo '#SBATCH --time=10:00:00' >> run.sh
  echo '#SBATCH --partition=jwang7' >> run.sh
  echo '#SBATCH --error='${rundir}'/job.e' >> run.sh
  echo '#SBATCH --output='${rundir}'/job.o' >> run.sh
  echo ' ' >> run.sh
  echo 'ulimit -s unlimited' >> run.sh
  echo 'export KMP_STACKSIZE=209715200' >> run.sh
  echo ' ' >> run.sh
  echo 'cd ' $rundir >> run.sh
  echo 'mkdir forward' >> run.sh
  echo 'time ./unlvrtm-aeronet-inv.exe > log' >> run.sh
  echo 'rm ./unlvrtm-aeronet-inv.exe' >> run.sh

  chmod u+x run.sh

  # submit the job
  sbatch run.sh

done
done

exit
