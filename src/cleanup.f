!  $Id: CLEANUP.f, v1.1 2010/01/12 11:43:51 xxu Exp $ 
      SUBROUTINE CLEANUP
!
!******************************************************************************
!  Subroutine CLEANUP deallocates the memory assigned to dynamic allocatable 
!  arrays 
!******************************************************************************
!
      ! References to F90 modules
      USE ATMPROF_MOD,        ONLY : CLEANUP_ATMOS
      USE AIROPTIC_MOD,       ONLY : CLEANUP_AIROPTIC
      USE SPECTRA_MOD,        ONLY : CLEANUP_SPECTRA
      USE GAS_MOD,            ONLY : CLEANUP_GAS
      USE RAYLEIGH_MOD,       ONLY : CLEANUP_RAYLEIGH
      USE AEROSOL_MOD,        ONLY : CLEANUP_AEROSOL
      USE IOP_MOD,            ONLY : CLEANUP_IOP
      USE VLIDORT_ARRAY_MOD,  ONLY : CLEANUP_VLIDORT_ARRAY
      USE LAMB_ALBEDO_MOD,    ONLY : CLEANUP_LAMB_ALBEDO
      USE BRDF_MOD,           ONLY : CLEANUP_VBRDF

      IMPLICIT NONE

      !================================================================
      ! CLEANUP begins here!
      !================================================================ 

      CALL CLEANUP_ATMOS
      CALL CLEANUP_AIROPTIC
      CALL CLEANUP_SPECTRA
      CALL CLEANUP_LAMB_ALBEDO
      CALL CLEANUP_GAS
      CALL CLEANUP_RAYLEIGH
      CALL CLEANUP_AEROSOL
      CALL CLEANUP_IOP
      CALL CLEANUP_VBRDF
      CALL CLEANUP_VLIDORT_ARRAY

      !================================================================
      ! Print to screen
      !================================================================
      WRITE( 6, '(/a )' ) REPEAT( '=', 79 )
      WRITE( 6, 100     )
100   FORMAT( '  - CLEANUP: deallocating all retrieval arrays now...' )

      ! Return to the calling routine 
      END SUBROUTINE CLEANUP
