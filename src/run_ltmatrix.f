! $Id: run_ltmatrix.f, v 1.1 2012/07/23 14:13:41 xxu $Exp
      SUBROUTINE RUN_LTMATRIX( LAMDA, IMODE )
!
!******************************************************************************
!  Subourtine RUN_LTMATRIX runs the linearized T-Matirx code with the 
!   input from reading namelist.ini. The results are saved in the 
!   module variables from AEROSOL_MOD (xxu, 07/23/12)
!   
!  Followings are the original comments from SpurrR's code:
!  Flag inputs
!  -----------
!
!      Do_Expcoeffs      - Boolean flag for computing Expansion
!                          Coefficients
!      Do_Fmatrix        - Boolean flag for computing F-matrix at
!                          equal-angles
!      Do_Monodisperse   - Boolean flag for Doing a Monodisperse
!                          calculation
!                          If set, the PSD stuff will be turned off
!                          internally
!      Do_EqSaSphere     - Boolean flag for specifying particle size in
!                          terms of the  equal-surface-area-sphere
!                          radius
!      Do_psd_OldStyle   - Boolean flag for using original PSD
!                          specifications
!      Do_LinearRef      - Boolean Flag for doing Refractive Index
!                          linearization
!      Do_LinearEps      - Boolean Flag for doing (Aspect ratio)
!                          linearization
!      Do_LinearPSD      - Boolean Flag for doing PSD linearization
!
!  General inputs
!  --------------
!      NKMAX.LE.988 is such that NKMAX+2 is the                        
!           number of Gaussian quadrature points used in               
!           integrating over the size distribution for particles
!
!      NDGS - parameter controlling the number of division points      
!             in computing integrals over the particle surface.        
!             For compact particles, the recommended value is 2.       
!             For highly aspherical particles larger values (3, 4,...) 
!             may be necessary to obtain convergence.                  
!             The code does not check convergence over this parameter. 
!             Therefore, control comparisons of results obtained with  
!             different NDGS-values are recommended.
!
!      NPNA - number of equidistant scattering angles (from 0      
!             to 180 deg) for which the scattering matrix is           
!             calculated.                                              
!
!      EPS and NP - specify the shape of the particles.                
!             For spheroids NP=-1 and EPS is the ratio of the          
!                 horizontal to rotational axes.  EPS is larger than   
!                 1 for oblate spheroids and smaller than 1 for       
!                 prolate spheroids.                                   
!             For cylinders NP=-2 and EPS is the ratio of the          
!                 diameter to the length.                              
!             For Chebyshev particles NP must be positive and 
!                 is the degree of the Chebyshev polynomial, while     
!                 EPS is the deformation parameter                     
!
!      Accuracy       - accuracy of the computations
!
!  optical inputs
!  --------------
!
!      LAMBDA         - wavelength of light (microns)
!      N_REAL, N_IMAG - real and imaginary parts, refractive index
!      (N-i.GE.0)   
!
!  PSD inputs
!  ----------
!
!      psd_Index      - Index for particle size distribution of spheres
!      psd_pars       - Parameters characterizing PSD (up to 3 allowed)
!
!      Monoradius     - Monodisperse radius size (Microns)
!
!      R1, R2         - Minimum and Maximum radii (Microns)
!      FixR1R2        - Boolean flag for allowing internal calculation
!      of R1/R2
!
!  Module referenced by run_ltmatrix.f
!  ============================================================================
!  (1 ) namelist_array_mod.f
!  (2 ) lmie_array_mod.f
!  (3 ) RTSMIE_parameters_m.f90
!
!  NOTES:
! (1 ) The MIE_BULK is the old version (2009) of LMIE code has 4 elements, 
!      inlcuding extinction efficiency (Qext), scattering efficiency (Qsca), 
!      omega, and assymmetric factor. Now in the new version (2011), 
!      assymmetric factor is separated, and the first two elements are 
!      extinction (k_ext) and scattering coefficients (k_sca). They are related
!      by Qext = k_ext / G, where G is the geometric cross section. Hence,
!      in this code, what we need is Qext and its derivatives, G need to be 
!      applied. (xxu, 12/16/11)
!******************************************************************************
!
      ! References to F90 modules
      USE TMAT_PARAMETERS,     ONLY : FPK=>TMAT_FPKIND,  NPL1, MAXNPA 
      USE TMAT_MASTER_PLUS_M,  ONLY : TMAT_MASTER_PLUS
      USE NAMELIST_ARRAY_MOD,  ONLY : LMONODIS,       MONOR
      USE NAMELIST_ARRAY_MOD,  ONLY : IDSHAPE,        SHAPE_PAR
      USE NAMELIST_ARRAY_MOD,  ONLY : IDIST,          DISTPAR
      USE NAMELIST_ARRAY_MOD,  ONLY : SIZERANGE
      USE NAMELIST_ARRAY_MOD,  ONLY : RMRI,           N_MOMENTS
      USE NAMELIST_ARRAY_MOD,  ONLY : LPRT,           LDEBUG_MIE_CALC
      USE NAMELIST_ARRAY_MOD,  ONLY : LJACOB_REFR,    LJACOB_SIZE
      USE NAMELIST_ARRAY_MOD,  ONLY : LJACOB_EPS
      USE ERROR_MOD,           ONLY : ERROR_STOP
      USE AEROSOL_MOD,         ONLY : NPANGLE,        ASYMM
      USE AEROSOL_MOD,         ONLY : NCOEFFS,        BULK_AOP      
      USE AEROSOL_MOD,         ONLY : EXPCOEFFS,      FMATRIX
      USE AEROSOL_MOD,         ONLY : DIST  
      USE AEROSOL_MOD,         ONLY : LPSD_BULK,      LPSD_ASYMM
      USE AEROSOL_MOD,         ONLY : LPSD_EXPCOEF,   LPSD_FMATRIX
      USE AEROSOL_MOD,         ONLY : LPSD_DIST,      LRFE_BULK 
      USE AEROSOL_MOD,         ONLY : LRFE_ASYMM,     LRFE_EXPCOEF
      USE AEROSOL_MOD,         ONLY : LRFE_FMATRIX 


      ! all vriables are implicitly defined
      IMPLICIT NONE

      ! Arguments
      REAL*8,  INTENT(IN)   :: LAMDA
      INTEGER, INTENT(IN)   :: IMODE

      !================================================================
      ! Varibles used for calling the Linear T-Matrix routine
      !================================================================

      ! Boolean Input arguments
 
      ! Flags for Expansion Coefficient and Fmatrix calculations
      LOGICAL               :: DO_EXPCOEFFS
      LOGICAL               :: DO_FMATRIX

      ! Logical flag for using equal-surface-area sepcification
      LOGICAL               :: DO_EQSASPHERE  

      ! Logical flag for Monodisperse calculation
      LOGICAL               :: DO_MONODISPERSE  

      ! linearization of other quantitites
      LOGICAL               :: DO_LINEARREF
      LOGICAL               :: DO_LINEARPSD
      LOGICAL               :: DO_LINEAREPS

      ! Style flag
      !  * This is checked and re-set (if required) for Monodisperse case
      LOGICAL               :: DO_PSD_OLDSTYLE
 
      ! General input argumments
      INTEGER               :: NP, NDGS, NPNA
      INTEGER               :: NKMAX 
      
      ! Accuracy and aspect ratio
      REAL(FPK)             :: ACCURACY, EPS 

      ! Optical: Wavelength, refractive index
      REAL(FPK)             :: WAVELENGTH, N_REAL, N_IMAG

      ! Flag for making an internal Fix of R1 and R2 (if set)
      LOGICAL               :: FIXR1R2
       
      ! Monodisperse radius (input)
      REAL(FPK)             :: MONORADIUS 

      ! R1 and R2
      REAL(FPK)             :: R1, R2

      ! PSD index and parameters
      INTEGER               :: PSD_INDEX
      REAL(FPK)             :: PSD_PARS(3)

      ! Output arguments

      ! Bulk scattering parameter
      REAL(FPK)             :: TMAT_BULK(3)

      ! linearizations w.r.t. PSD parameters
      REAL(FPK)             :: LPSD_TMAT_BULK(3,3)

      ! linearizations w.r.t. RefIdx/Eps parameters
      REAL(FPK)             :: LRFE_TMAT_BULK(3,3)

      ! Expansion coefficients and Asymmetry parameter
      INTEGER               :: TMAT_NCOEFFS
      REAL(FPK)             :: TMAT_EXPCOEFFS(NPL1,6)
      REAL(FPK)             :: TMAT_ASYMM

      ! linearizations w.r.t. PSD parameters
      REAL(FPK)             :: LPSD_TMAT_EXPCOEFFS (NPL1,6,3)
      REAL(FPK)             :: LPSD_TMAT_ASYMM(3)

      ! linearizations w.r.t. RefIdx/Eps parameters
      REAL(FPK)             :: LRFE_TMAT_EXPCOEFFS (NPL1,6,3)
      REAL(FPK)             :: LRFE_TMAT_ASYMM(3)

      ! F-Matrix
      REAL(FPK)             :: TMAT_FMATRIX (MAXNPA,6)
      
      ! Linearizations of F-matrix
      REAL(FPK)             :: LPSD_TMAT_FMATRIX (MAXNPA,6,3)
      REAL(FPK)             :: LRFE_TMAT_FMATRIX (MAXNPA,6,3)

      ! Distribution parameters
      !    1 = Normalization
      !    2 = Cross-section
      !    3 = Volume
      !    4 = REFF
      !    5 = VEFF
      REAL(FPK)             :: TMAT_DIST (5)
      REAL(FPK)             :: LPSD_TMAT_DIST (5,3)

      ! Number of RFE parameters
      INTEGER               :: NLIN

      ! diagnostic outputs
      LOGICAL               :: FAIL
      INTEGER               :: ISTATUS
      CHARACTER(LEN=255)    :: MESSAGE, TRACE, TRACE_2, ERRMSG

      !================================================================
      ! Additional local varibles
      !================================================================
      INTEGER               :: I, J, K, II, IU, N
      INTEGER               :: MIN_N
      REAL*8                :: MF1, MF2
       
      !================================================================
      ! RUN_LTMATRIX begins here!
      !================================================================
      WRITE(6, '(A)' ) ' RUN_LTMATRIX: calculate the T-Matrix property'
       
      !================================================================
      ! Prepare inputs for the linearized T-Matrix code
      !================================================================ 

      ! Initializations... logicals
      DO_EXPCOEFFS    = .TRUE.
      DO_FMATRIX      = .TRUE.
      DO_EQSASPHERE   = .TRUE.
      DO_LINEARREF    = LJACOB_REFR      ! Ture for derivatives wrt refract
      DO_LINEAREPS    = LJACOB_EPS       ! Ture for derivatives wrt aspect ratio
      DO_LINEARPSD    = LJACOB_SIZE      ! True for derivatives wrt size pars
      FIXR1R2         = .FALSE.          ! False for using the namelisted R1 R2
      DO_PSD_OLDSTYLE = .FALSE. 

      ! Initializations... Some constants & dimensions 
      NKMAX           = 20
      NDGS            = 2
      ACCURACY        = 1.0D-3

      ! Initializations... Fmatrix angles are regular here between 0 and 180.
      NPNA            = NPANGLE

      ! Assign variables read from namelist.ini
      EPS             = SHAPE_PAR(IMODE)
      NP              = IDSHAPE(IMODE)
      DO_MONODISPERSE = LMONODIS(IMODE)
      MONORADIUS      = MONOR(IMODE)
      PSD_INDEX       = IDIST(IMODE)
      PSD_PARS(1:3)   = DISTPAR(1:3,IMODE)
      R1              = SIZERANGE(1,IMODE)
      R2              = SIZERANGE(2,IMODE)
      WAVELENGTH      = LAMDA * 1d-3
      N_REAL          = RMRI(1,IMODE)
      N_IMAG          = ABS( RMRI(2,IMODE) )

      ! Screen print debugging
      IF ( LPRT ) THEN
         WRITE(6,100) 'IMODE     =', IMODE
         WRITE(6,100) 'PSD_INDEX =', PSD_INDEX
         WRITE(6,200) 'PSD_PARS  =', PSD_PARS
         WRITE(6,200) 'R1 & R2   =', R1, R2
         WRITE(6,200) 'Wave      =', WAVELENGTH
         WRITE(6,200) 'Refractive=', N_REAL, N_IMAG
         WRITE(6,100) 'N_Shape   =', NP
         WRITE(6,200) 'Aspact rat=', EPS
         WRITE(6,100) 'N_F-ANGLES=', NPNA
      ENDIF
     
      !================================================================
      ! Call the T-Matrix routine
      !================================================================
      CALL TMAT_MASTER_PLUS
     &   ( DO_EXPCOEFFS, DO_FMATRIX,                            ! I
     &     DO_MONODISPERSE, DO_EQSASPHERE,                      ! I 
     &     DO_LINEARREF, DO_LINEAREPS, DO_LINEARPSD,            ! I
     &     DO_PSD_OLDSTYLE, PSD_INDEX, PSD_PARS,                ! I
     &     MONORADIUS, R1, R2,  FIXR1R2,                        ! I
     &     NP, NKMAX, NPNA, NDGS, EPS, ACCURACY,                ! I
     &     WAVELENGTH, N_REAL, N_IMAG,                          ! I
     &     TMAT_BULK, TMAT_ASYMM, TMAT_NCOEFFS,                 ! O
     &     TMAT_EXPCOEFFS, TMAT_FMATRIX,                        ! O
     &     LPSD_TMAT_BULK, LPSD_TMAT_ASYMM,                     ! O
     &     LPSD_TMAT_EXPCOEFFS, LPSD_TMAT_FMATRIX,              ! O
     &     LRFE_TMAT_BULK, LRFE_TMAT_ASYMM,                     ! O
     &     LRFE_TMAT_EXPCOEFFS, LRFE_TMAT_FMATRIX,              ! O
     &     NLIN, TMAT_DIST, LPSD_TMAT_DIST,                     ! )
     &     FAIL, ISTATUS, MESSAGE, TRACE, TRACE_2 )             ! O
 
      ! Check the run status
      IF ( FAIL ) THEN
         WRITE(6,'(a)') TRIM( ADJUSTL(MESSAGE) )
         WRITE(6,'(a)') TRIM( ADJUSTL(TRACE  ) )
         WRITE(6,'(a)') TRIM( ADJUSTL(TRACE_2) ) 
         ERRMSG = 'LTMATRIX calculation failed ' 
         CALL ERROR_STOP( ERRMSG, 'run_ltmatrix.f' )
      ENDIF

      ! Screen print for debug
      IF ( LPRT ) THEN
         WRITE(6,200) 'TMAT_BULK =', TMAT_BULK
         WRITE(6,200) 'TMAT_DIST =', TMAT_DIST
         WRITE(6,200) 'R1 & R2   =', R1, R2
      ENDIF  

      !================================================================  
      ! Now save the output to the module varaibles in AEROSOL_MOD
      !================================================================

      ! Bulk properties
      MIN_N               = MIN( TMAT_NCOEFFS, N_MOMENTS )
      NCOEFFS(IMODE)      = MIN_N

      ! Ext Xsec, Scat Xsec, and single scattering albedo 
      BULK_AOP(1:3,IMODE) = TMAT_BULK(1:3)

      ! Convert extinction coefficients to efficiency factor
      ! Qext = kext / G
      BULK_AOP(4:5,IMODE) = TMAT_BULK(1:2) / TMAT_DIST(2) 

      ! Size and distribution paramters
      DIST(1:5,IMODE)       = TMAT_DIST(1:5)

      ! Assymetric factor and phase function expansions
      IF ( DO_EXPCOEFFS ) THEN 
         ASYMM(IMODE)     = TMAT_ASYMM
         DO N = 0, MIN_N-1
            EXPCOEFFS(:,N,IMODE) = TMAT_EXPCOEFFS(N+1,:)
         ENDDO
      ENDIF

      ! F-Matrix 
      IF ( DO_FMATRIX ) THEN
         DO N = 1, NPANGLE
            FMATRIX(1:6,N,IMODE) = TMAT_FMATRIX(N,1:6)
         ENDDO
      ENDIF 

      ! Linearizations ... wrt size parameters
      IF ( DO_LINEARPSD ) THEN

         LPSD_BULK(1:3,1:3,IMODE) = LPSD_TMAT_BULK(1:3,1:3)
         LPSD_DIST(1:5,1:3,IMODE) = LPSD_TMAT_DIST(1:5,1:3)

         !-------------------------------------------------------------
         ! Convert d_kext/d_rg to d_Qext/d_rg (xxu, 12/15/11):
         ! Here Qext = kext / G,
         ! then d_Qext / d_rg = ( d_kext/d_rg * G - d_G/d_rg * kext )
         !                                / G^2
         !-------------------------------------------------------------
         LPSD_BULK(4,1:3,IMODE) = ( LPSD_TMAT_BULK(1,1:3) * TMAT_DIST(2)
     &                          - LPSD_TMAT_DIST(2,1:3) * TMAT_BULK(1) )
     &                          / TMAT_DIST(2) / TMAT_DIST(2)  
         LPSD_BULK(5,1:3,IMODE) = ( LPSD_TMAT_BULK(2,1:3) * TMAT_DIST(2)
     &                          - LPSD_TMAT_DIST(2,1:3) * TMAT_BULK(2))
     &                          / TMAT_DIST(2) / TMAT_DIST(2)

         IF ( DO_EXPCOEFFS ) THEN
            LPSD_ASYMM(1:3,IMODE) = LPSD_TMAT_ASYMM(1:3)
            DO J = 1, 3
            DO N = 0, MIN_N-1
               LPSD_EXPCOEF(:,N,J,IMODE) = LPSD_TMAT_EXPCOEFFS(N+1,:,J)
            ENDDO
            ENDDO
         ENDIF

         IF ( DO_FMATRIX ) THEN
            DO J = 1, 3
            DO N = 1, NPANGLE
            LPSD_FMATRIX(:,N,J,IMODE)
     &                          = LPSD_TMAT_FMATRIX(N,:,J)
            ENDDO
            ENDDO
         ENDIF


      ENDIF

      ! Linearizations ... wrt refractive index
      IF ( DO_LINEARREF ) THEN

         LRFE_BULK(1:3,1:3,IMODE) = LRFE_TMAT_BULK(1:3,1:3)
         LRFE_BULK(4:5,1:3,IMODE) = LRFE_TMAT_BULK(1:2,1:3) 
     &                            / TMAT_DIST(2)

         IF ( DO_EXPCOEFFS ) THEN 
            LRFE_ASYMM(1:3,IMODE)  = LRFE_TMAT_ASYMM(1:3)
            DO J = 1, 3 
            DO N = 0, MIN_N-1
               LRFE_EXPCOEF(:,N,J,IMODE) = LRFE_TMAT_EXPCOEFFS(N+1,:,J)
            ENDDO 
            ENDDO
         ENDIF

         IF ( DO_FMATRIX ) THEN
            DO J = 1, 3
            DO N = 1, NPANGLE
            LRFE_FMATRIX(:,N,J,IMODE)
     &                          = LRFE_TMAT_FMATRIX(N,:,J)
            ENDDO
            ENDDO
         ENDIF

      ENDIF

      !================================================================
      ! End of LMIE model calculation,
      !================================================================

      ! Return to the calling routine if no debugging print
      IF ( .NOT. LDEBUG_MIE_CALC )  RETURN

      !================================================================
      ! Debug output of mie calculation
      !================================================================
      IU = 6 
      WRITE(IU,*) ' '
      WRITE(IU,*) '**************************************************'
      WRITE(IU,*) '****** L I N E A R I Z E D   T M A T R I X *******'
      WRITE(IU,*) '**************************************************'
      WRITE(IU,*)  '   MODE# = ', IMODE
      WRITE(IU,110)'  ==> Ext Xsec     :', BULK_AOP(1,IMODE)
      WRITE(IU,110)'  ==> Sca Xsec     :', BULK_AOP(2,IMODE)
      WRITE(IU,110)'  ==> SS Albedo    :', BULK_AOP(3,IMODE)
      WRITE(IU,110)'  ==> Ext Coeff.   :', BULK_AOP(4,IMODE)
      WRITE(IU,110)'  ==> Sca Coeff.   :', BULK_AOP(5,IMODE)
      WRITE(IU,110)'  ==> Asym factor  :', ASYMM(IMODE)
      WRITE(IU,110)'  ==> Total Density:', DIST(1,IMODE)
      WRITE(IU,110)'  ==> Geomet XSec  :', DIST(2,IMODE)
      WRITE(IU,110)'  ==> Dist. Volume :', DIST(3,IMODE)
      WRITE(IU,110)'  ==> Eff Radius   :', DIST(4,IMODE)
      WRITE(IU,110)'  ==> Eff Variance :', DIST(5,IMODE)
      WRITE(IU,130)'  ==> LRFE_BULK(1) :', LRFE_BULK(1,:,IMODE)
      WRITE(IU,130)'  ==> LRFE_BULK(2) :', LRFE_BULK(2,:,IMODE)
      WRITE(IU,130)'  ==> LRFE_BULK(3) :', LRFE_BULK(3,:,IMODE)
      WRITE(IU,130)'  ==> LRFE_BULK(4) :', LRFE_BULK(4,:,IMODE)
      WRITE(IU,130)'  ==> LRFE_BULK(5) :', LRFE_BULK(5,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_BULK(1) :', LPSD_BULK(1,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_BULK(2) :', LPSD_BULK(2,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_BULK(3) :', LPSD_BULK(3,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_BULK(4) :', LPSD_BULK(4,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_BULK(5) :', LPSD_BULK(5,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_DIST(1) :', LPSD_DIST(1,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_DIST(2) :', LPSD_DIST(2,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_DIST(3) :', LPSD_DIST(3,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_DIST(4) :', LPSD_DIST(4,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_DIST(5) :', LPSD_DIST(5,:,IMODE)
      WRITE(IU,*) '********** EXPANSION COEFFICIENTS ***********'
      DO II = 0, NCOEFFS(IMODE) - 1
         WRITE(IU,160) II, EXPCOEFFS(:,II,IMODE)
      ENDDO

 100  FORMAT(A, I12  )
 110  FORMAT(A, F12.7)
 120  FORMAT(A, 2F12.7)
 130  FORMAT(A, 1P3E11.3)
 150  FORMAT(A, 1P5E11.3)
 160  FORMAT(I4, 6F11.7)
 170  FORMAT(I4, 5F11.7)
 180  FORMAT(I4, 6F12.5)  
 200  FORMAT(A,1P5E11.3) 

      ! Return the calling routine
      END SUBROUTINE RUN_LTMATRIX
