! $Id: airoptic_mod.f, v 1.1 2010/08/25 14:13 xxu Exp $
      MODULE AIROPTIC_MOD
!
!******************************************************************************
!  Module AIROPTIC_MOD contains variables related to atmospheric components
!  ( aerosol, air molecualr, etc), used for VLIDORT optical property 
!  preparation.
!  (xxu, 8/25/10, 1/11/11)
!  
!  Module Variables:
!  ============================================================================
!  (1 ) TAUAER   ( REAL*8  ) : Aerosol optical depth                [ none ]
!  (2 ) SSAAER   ( REAL*8  ) : Aerosol single scattering albedo     [ none ]
!  (3 ) PHAAER   ( REAL*8  ) : Aerosol scattering phase function    [ none ]
!  (4 ) TAURAY   ( REAL*8  ) : Rayleigh optical depth               [ none ]
!  (5 ) PHARAY   ( REAL*8  ) : Rayleigh scattering phase function   [ none ]
!  (6 ) TAUGAS   ( REAL*8  ) : Total gas absorption optical depth   [ none ]
!  (7 ) TAUGAS_A ( REAL*8  ) : Absorption optical depth for all gas [ none ]
!
!  Module Routines
!  ============================================================================
!  (1 ) INIT_AIR             : Allocate module arrays
!  (2 ) CLEANUP_AIR          : Deallocate module arrays
!******************************************************************************
!  
      IMPLICIT NONE

      ! Make everything PRIVATE ...
      PRIVATE

      ! ... except these routines
      PUBLIC :: INIT_AIROPTIC
      PUBLIC :: CLEANUP_AIROPTIC
      PUBLIC :: PRINT_AIROPTIC 

      ! ... and these variables
      PUBLIC :: TAUAER, SSAAER, PHAAER
      PUBLIC :: TAURAY, PHARAY
      PUBLIC :: TAUGAS, TAUGAS_A

      !=================================================================
      ! MODULE VARIABLES
      !=================================================================
      REAL*8,ALLOCATABLE  :: TAUAER(:,:)
      REAL*8,ALLOCATABLE  :: SSAAER(:,:)
      REAL*8,ALLOCATABLE  :: PHAAER(:,:,:,:)
      REAL*8,ALLOCATABLE  :: TAURAY(:)
      REAL*8,ALLOCATABLE  :: PHARAY(:,:,:)
      REAL*8,ALLOCATABLE  :: TAUGAS(:)
      REAL*8,ALLOCATABLE  :: TAUGAS_A(:,:)

      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !=================================================================       
      CONTAINS
!
!----------------------------------------------------------------------
!
      SUBROUTINE INIT_AIROPTIC

      ! References to F90 modules 
      USE ERROR_MOD,          ONLY : ALLOC_ERR
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS, N_MOMENTS
      USE NAMELIST_ARRAY_MOD, ONLY : NMODE
      !USE ATMPROF_MOD,        ONLY : MAXMOL
      USE NAMELIST_ARRAY_MOD, ONLY : NGAS

      ! Local variables
      INTEGER            :: AS
      REAL*8             :: PS
      LOGICAL, SAVE      :: IS_INIT = .FALSE.

      !=================================================================
      ! INIT_AIROPTIC begins here!
      !=================================================================
      
      ! Return if we have already initialized
      IF ( IS_INIT ) RETURN

      ! For aerosols 
      ALLOCATE( TAUAER(N_LAYERS,NMODE), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'TAUAER' )
      ALLOCATE( SSAAER(N_LAYERS,NMODE), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'SSAAER' )
      ALLOCATE( PHAAER(N_LAYERS,0:N_MOMENTS-1,6,NMODE), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'PHAAER' )

      TAUAER = 0d0
      SSAAER = 0d0
      PHAAER = 0d0

      ! For Rayleigh
      ALLOCATE( TAURAY(N_LAYERS), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'TAURAY' )
      ALLOCATE( PHARAY(N_LAYERS,0:N_MOMENTS-1,6), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'PHARAY' )

      TAURAY = 0d0
      PHARAY = 0d0

      ! For Trace gas
      ALLOCATE( TAUGAS(N_LAYERS), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'TAUGAS' )
      TAUGAS = 0d0

      ALLOCATE( TAUGAS_A(N_LAYERS,NGAS), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'TAUGAS_A' )
      TAUGAS_A = 0d0

      ! Reset IS_INIT so we do not allocate arrays again
      IS_INIT = .TRUE.

      WRITE(6,100)
 100  FORMAT(" - INIT_AIROPTIC: initialize air optic arrays" )
      RETURN

      END SUBROUTINE INIT_AIROPTIC
 
!------------------------------------------------------------------------------
 
      SUBROUTINE CLEANUP_AIROPTIC

      ! Deallocates all module arrays
      IF ( ALLOCATED( TAUAER )     ) DEALLOCATE( TAUAER     )
      IF ( ALLOCATED( SSAAER )     ) DEALLOCATE( SSAAER     )
      IF ( ALLOCATED( PHAAER )     ) DEALLOCATE( PHAAER     )
      IF ( ALLOCATED( TAURAY )     ) DEALLOCATE( TAURAY     )
      IF ( ALLOCATED( PHARAY )     ) DEALLOCATE( PHARAY     )
      IF ( ALLOCATED( TAUGAS )     ) DEALLOCATE( TAUGAS     )
      IF ( ALLOCATED( TAUGAS_A )   ) DEALLOCATE( TAUGAS_A   )

      END SUBROUTINE CLEANUP_AIROPTIC

!------------------------------------------------------------------------------

      SUBROUTINE PRINT_AIROPTIC

      ! Referenced to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS, N_MOMENTS
      USE NAMELIST_ARRAY_MOD, ONLY : LDEBUG_AOD_CALC
      USE NAMELIST_ARRAY_MOD, ONLY : LDEBUG_RAYLEIGH_CALC
      USE NAMELIST_ARRAY_MOD, ONLY : LDEBUG_TRACEGAS_CALC
      USE ATMPROF_MOD,        ONLY : ATMZ, ATMP

      ! Local variables
      INTEGER  :: L

      !================================================================
      ! PRINT_AIROPTIC begins here!
      !================================================================
     
      IF ( LDEBUG_AOD_CALC .OR. LDEBUG_RAYLEIGH_CALC .OR. 
     &     LDEBUG_TRACEGAS_CALC ) THEN
      WRITE( 6,'(/a)') 'PRINT_AIROPTIC: Optical properties'
      WRITE( 6, '(a)') REPEAT( '-', 79 )
      WRITE( 6, 100) 'Layer', 'PRS', 'HGT', 'RAY', 'GAS', 'AOD', 'SSA'
      WRITE( 6, '(a)') REPEAT( '-', 79 )
      WRITE(6, 101) 0, ATMP(1), ATMZ(1)
      DO L = 1, N_LAYERS
         WRITE(6, 200) L, ATMP(L+1), ATMZ(L+1), TAURAY(L), TAUGAS(L),
     &                 SUM(TAUAER(L,:)), 
     &                 SUM(SSAAER(L,:) * TAUAER(L,:)) / 
     &                   MAX( SUM(TAUAER(L,:)), 1d-20)
      ENDDO
      WRITE( 6, '(a)') REPEAT( '-', 79 )
      WRITE(6, 300) SUM(TAURAY(:)), SUM(TAUGAS(:)), SUM(TAUAER(:,:))

      ENDIF
 
      IF ( LDEBUG_TRACEGAS_CALC ) THEN 
         WRITE( 6,'(/a)') 'PRINT_AIROPTIC: Each trace gases'
         WRITE( 6, 350 ) 'H2O', 'CO2', 'O3', 'N2O', 'CO', 'CH4', 'O2',
     &                   'SO2', 'NO2', 'NH3', 'OH', 'Total'
         WRITE( 6, 400 ) SUM(TAUGAS_A(:,1)),  SUM(TAUGAS_A(:,2)), 
     &                   SUM(TAUGAS_A(:,3)),  SUM(TAUGAS_A(:,4)),
     &                   SUM(TAUGAS_A(:,5)),  SUM(TAUGAS_A(:,6)),
     &                   SUM(TAUGAS_A(:,7)),  SUM(TAUGAS_A(:,9)),
     &                   SUM(TAUGAS_A(:,10)), SUM(TAUGAS_A(:,11)),
     &                   SUM(TAUGAS_A(:,13)), SUM(TAUGAS(:))
      ENDIF 

 100  FORMAT(A5, 6A10)
 101  FORMAT(I5,F11.4, F9.3)
 200  FORMAT(I5,F11.4, F9.3, 1P4E11.4)
 300  FORMAT('Column:', 20x, 1P3E11.4)
 350  FORMAT( 4X, 12A10) 
 400  FORMAT(' TGas: ',1P12E10.3)

      END SUBROUTINE PRINT_AIROPTIC
 
!------------------------------------------------------------------------------

      END MODULE AIROPTIC_MOD
