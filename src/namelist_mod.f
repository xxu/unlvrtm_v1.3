! $Id: namelist_mod.f, v1.1 2010/11/26 12:01:01 xxu
      MODULE NAMELIST_MOD
!
!******************************************************************************
!  Module NANMELIST_MOD reads the namelist input file at the start run and
!  passes the information to several other modules, such as 'DIRECTORY_MOD'
!  and 'LOGICAL_MOD'. (xxu, 2010/11/26, 2011/05/23)
!  
!  Module Variables
!  ============================================================================
!  (1 ) VERBOSE   (LOGICAL ) : Turns on echo-back of lines
!  (2 ) FIRSTCOL  (INTEGER ) : First column of the input file (default=26)
!  (3 ) MAXDIM    (INTEGER ) : Maximum number of substrings to read in
!  (4 ) FILENAME  (CHAR*255) : Namelist input file name
!  (5 ) IU_NAME   (INTEGER ) : Unit # for namelist input file
!
!  Module Routines
!  ============================================================================
!  (1 ) READ_NAMELIST        : Main subroutine to read the file namelist.ini
!  (2 ) READ_ONE_LINE        : Read one line of the file namelist.ini
!  (3 ) SPLIT_ONE_LINE       : Split one line in to a set
!  (4 ) READ_CONTROL_MENU    : Read the control menu 
!  (5 ) READ_GEOMETRY_MENU   : Read the geometry menu 
!  (6 ) READ_RTM_MENU        : Read the rtm menu
!  (7 ) READ_JACOBIAN_MENU   : Read the Jacobian menu 
!  (8 ) READ_AEROSOL_MENU    : Read the aerosol menu 
!  (9 ) READ_GAS_MENU        : Read the gas menu
!  (10) READ_RAYLEIGH_MENU   : Read the Rayleigh menu
!  (11) READ_DIAGNOSTIC_MENU : Read the diagnostic menu
!  (12) READ_DEBUG_MENU      : Read the debug menu
!
!  NOTES:
!  (1 ) This code is based on the input_mod.f for the GEOS-Chem model.
!  (2 ) Now code is for version v1.1.1. (xxu, 10/1/12)
!******************************************************************************
!
      IMPLICIT NONE

      !=================================================================
      ! MODULE PRIVATE DECLARATIONS -- keep certain internal variables 
      ! and routines from being seen outside "namelist_mod.f"
      !=================================================================

      ! Make everything PRIVATE ...
      PRIVATE

      ! ... except these routines
      PUBLIC :: READ_NAMELIST
      PUBLIC :: READ_ONE_LINE
      PUBLIC :: SPLIT_ONE_LINE

      ! ... and these variables
      PUBLIC :: IU_NAME
      PUBLIC :: MAXDIM

      !=================================================================
      ! MODULE VARIABLES 
      !=================================================================
      LOGICAL            :: VERBOSE  = .FALSE.
      INTEGER, PARAMETER :: FIRSTCOL = 26
      INTEGER, PARAMETER :: MAXDIM   = 255
      !CHARACTER(LEN=255) :: FILENAME = 'namelist.ini'
      INTEGER, PARAMETER :: IU_NAME  = 11

      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !=================================================================
      CONTAINS

!------------------------------------------------------------------------------

      SUBROUTINE READ_NAMELIST
!
!******************************************************************************
!  Subroutine READ_NAMELIST is the driver program for reading the NAMELIST
!  input file "namelist.ini" from disk. (xxu, 11/10/10)
!******************************************************************************
!
      ! References to F90 modules
      USE CHARPAK_MOD, ONLY : STRREPL
      USE ERROR_MOD,   ONLY : ERROR_STOP

      ! Local variables
      CHARACTER(LEN=255) :: FILENAME = 'namelist.ini'
      CHARACTER(LEN=255) :: TOPTITLE
      LOGICAL            :: EOF
      INTEGER            :: IOS
      CHARACTER(LEN=1)   :: TAB   = ACHAR(9)
      CHARACTER(LEN=1)   :: SPACE = ' '
      CHARACTER(LEN=255) :: LINE
      CHARACTER(LEN=255) :: MESSAGE 

      !=================================================================
      ! READ_INPUT_FILE begins here!
      !=================================================================

      ! Screen print
      WRITE( 6, '(a  )' ) REPEAT( '=', 79 )
      WRITE( 6, 100   ) TRIM( FILENAME )
 100  FORMAT( ' - READ_NAMELIST: Reading ', a )

      ! Open file
      OPEN( IU_NAME, FILE=TRIM( FILENAME ), STATUS='OLD', IOSTAT=IOS )
      IF ( IOS /= 0 ) THEN
         MESSAGE = 'Fail to open file: '//TRIM( FILENAME )
         CALL ERROR_STOP( MESSAGE, 'namelist_mod.f' )
      ENDIF

      ! Read TOPTITLE for binary punch file
      TOPTITLE = READ_ONE_LINE( EOF  )
      IF ( EOF ) RETURN

      ! Loop until EOF
      DO

         ! Read a line from the file, exit if EOF
         LINE = READ_ONE_LINE( EOF )
         IF ( EOF ) EXIT

         ! Replace tab characters in LINE (if any) w/ spaces
         CALL STRREPL( LINE, TAB, SPACE )

         IF      ( INDEX( LINE, 'CONTROL MENU'   ) > 0 ) THEN
            CALL READ_CONTROL_MENU

         ELSE IF ( INDEX( LINE, 'GEOMETRY MENU'  ) > 0 ) THEN
            CALL READ_GEOMETRY_MENU

         ELSE IF ( INDEX( LINE, 'RTM MENU'       ) > 0 ) THEN
            CALL READ_RTM_MENU

         ELSE IF ( INDEX( LINE, 'JACOBIAN MENU'  ) > 0 ) THEN
            CALL READ_JACOBIAN_MENU

         ELSE IF ( INDEX( LINE, 'SURFACE MENU'   ) > 0 ) THEN
            CALL READ_SURFACE_MENU  

         ELSE IF ( INDEX( LINE, 'AEROSOL MENU'   ) > 0 ) THEN
            CALL READ_AEROSOL_MENU

         ELSE IF ( INDEX( LINE, 'TRACE-GAS MENU' ) > 0 ) THEN
            CALL READ_GAS_MENU

         ELSE IF ( INDEX( LINE, 'RAYLEIGH MENU'  ) > 0 ) THEN
            CALL READ_RAYLEIGH_MENU

         ELSE IF ( INDEX( LINE, 'DIAGNOSTIC MENU') > 0 ) THEN
            CALL READ_DIAGNOSTIC_MENU

         ELSE IF ( INDEX( LINE, 'DEBUG MENU'     ) > 0 ) THEN
            CALL READ_DEBUG_MENU

         ELSE IF ( INDEX( LINE, 'END OF FILE'    ) > 0 ) THEN
            EXIT

         ENDIF

      ENDDO

      ! Close input file
      CLOSE( IU_NAME )

      WRITE(6,110)
 110  FORMAT(" - READ_NAMELIST: complete reading the namelist ", /)

      END SUBROUTINE READ_NAMELIST

!------------------------------------------------------------------------------

      FUNCTION READ_ONE_LINE( EOF, LOCATION ) RESULT( LINE )
!
!******************************************************************************
!  Subroutine READ_ONE_LINE reads a line from the input file.  If the global 
!  variable VERBOSE is set, the line will be printed to stdout.  READ_ONE_LINE
!  can trap an unexpected EOF if LOCATION is passed.  Otherwise, it will pass
!  a logical flag back to the calling routine, where the error trapping will
!  be done. (bmy, 7/20/04)
! 
!  Arguments as Output:
!  ===========================================================================
!  (1 ) EOF      (CHARACTER) : Logical flag denoting EOF condition
!  (2 ) LOCATION (CHARACTER) : Name of calling routine; traps premature EOF
!
!  Function value:
!  ===========================================================================
!  (1 ) LINE     (CHARACTER) : A line of text as read from the file
!
!  NOTES:
!******************************************************************************
!      
      ! References to F90 modules
      USE ERROR_MOD, ONLY : ERROR_STOP

      ! Arguments
      LOGICAL,          INTENT(OUT)          :: EOF
      CHARACTER(LEN=*), INTENT(IN), OPTIONAL :: LOCATION

      ! Local variables
      INTEGER                                :: IOS
      CHARACTER(LEN=255)                     :: LINE, MSG

      !=================================================================
      ! READ_ONE_LINE begins here!
      !=================================================================

      ! Initialize
      EOF = .FALSE.

      ! Read a line from the file
      READ( IU_NAME, '(a)', IOSTAT=IOS ) LINE

      ! IO Status < 0: EOF condition
      IF ( IOS < 0 ) THEN
         EOF = .TRUE.

         ! Trap unexpected EOF -- stop w/ error msg if LOCATION is passed
         ! Otherwise, return EOF to the calling program
         IF ( PRESENT( LOCATION ) ) THEN
            MSG = 'READ_ONE_LINE: error at: ' // TRIM( LOCATION )
            WRITE( 6, '(a)' ) MSG
            WRITE( 6, '(a)' ) 'Unexpected end of file encountered!'
            WRITE( 6, '(a)' ) 'STOP in READ_ONE_LINE (namelist_mod.f)'
            WRITE( 6, '(a)' ) REPEAT( '=', 79 )
            STOP
         ELSE
            RETURN
         ENDIF
      ENDIF

      ! IO Status > 0: true I/O error condition
      IF ( IOS > 0 ) CALL ERROR_STOP( 'Error when reading', 
     &                                'read_one_line:1' )

      ! Print the line (if necessary)
      IF ( VERBOSE ) WRITE( 6, '(a)' ) TRIM( LINE )

      ! Return to calling program
      END FUNCTION READ_ONE_LINE

!------------------------------------------------------------------------------

      SUBROUTINE SPLIT_ONE_LINE( SUBSTRS, N_SUBSTRS, N_EXP, LOCATION )
!
!******************************************************************************
!  Subroutine SPLIT_ONE_LINE reads a line from the input file (via routine 
!  READ_ONE_LINE), and separates it into substrings. (bmy, 7/20/04)
!
!  SPLIT_ONE_LINE also checks to see if the number of substrings found is 
!  equal to the number of substrings that we expected to find.  However, if
!  you don't know a-priori how many substrings to expect a-priori, 
!  you can skip the error check.
! 
!  Arguments as Input:
!  ===========================================================================
!  (3 ) N_EXP     (INTEGER  ) : Number of substrings we expect to find
!                               (N_EXP < 0 will skip the error check!)
!  (4 ) LOCATION  (CHARACTER) : Name of routine that called SPLIT_ONE_LINE
!
!  Arguments as Output:
!  ===========================================================================
!  (1 ) SUBSTRS   (CHARACTER) : Array of substrings (separated by " ")
!  (2 ) N_SUBSTRS (INTEGER  ) : Number of substrings actually found
!
!  NOTES:
!******************************************************************************
!
      ! References to F90 modules
      USE CHARPAK_MOD, ONLY: STRSPLIT

      ! Arguments
      CHARACTER(LEN=255), INTENT(OUT) :: SUBSTRS(MAXDIM)
      INTEGER,            INTENT(OUT) :: N_SUBSTRS
      INTEGER,            INTENT(IN)  :: N_EXP
      CHARACTER(LEN=*),   INTENT(IN)  :: LOCATION

      ! Local varaibles
      LOGICAL                         :: EOF
      CHARACTER(LEN=255)              :: LINE, MSG

      !=================================================================
      ! SPLIT_ONE_LINE begins here!
      !=================================================================  

      ! Create error msg
      MSG = 'SPLIT_ONE_LINE: error at ' // TRIM( LOCATION )

      !=================================================================
      ! Read a line from disk
      !=================================================================
      LINE = READ_ONE_LINE( EOF )

      ! STOP on End-of-File w/ error msg
      IF ( EOF ) THEN
         WRITE( 6, '(a)' ) TRIM( MSG )
         WRITE( 6, '(a)' ) 'End of file encountered!'
         WRITE( 6, '(a)' ) 'STOP in SPLIT_ONE_LINE (namelist_mod.f)!'
         WRITE( 6, '(a)' ) REPEAT( '=', 79 )
         STOP
      ENDIF

      !=================================================================
      ! Split the lines between spaces -- start at column FIRSTCOL
      !=================================================================
      CALL STRSPLIT( LINE(FIRSTCOL:), ' ', SUBSTRS, N_SUBSTRS )

      ! Sometimes we don't know how many substrings to expect,
      ! if N_EXP is greater than MAXDIM, then skip the error check
      IF ( N_EXP < 0 ) RETURN

      ! Stop if we found the wrong 
      IF ( N_EXP /= N_SUBSTRS ) THEN
         WRITE( 6, '(a)' ) TRIM( MSG )
         WRITE( 6, 100   ) N_EXP, N_SUBSTRS
         WRITE( 6, '(a)' ) 'STOP in SPLIT_ONE_LINE (namelist_mod.f)!'
         WRITE( 6, '(a)' ) REPEAT( '=', 79 )
         STOP
 100     FORMAT( 'Expected ',i2, ' substrs but found ',i3 )
      ENDIF

      ! Return to calling program
      END SUBROUTINE SPLIT_ONE_LINE

!------------------------------------------------------------------------------

      SUBROUTINE READ_CONTROL_MENU

      ! References to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : LAMDA
      USE NAMELIST_ARRAY_MOD, ONLY : LAMDAb,       LAMDAe
      USE NAMELIST_ARRAY_MOD, ONLY : NLAMDA_NMLST, LAMDAS_NMLST
      USE NAMELIST_ARRAY_MOD, ONLY : SPECTRA_STEP
      USE NAMELIST_ARRAY_MOD, ONLY : IATMOS,       N_LAYERS
      USE NAMELIST_ARRAY_MOD, ONLY : DIR_RUN
      USE NAMELIST_ARRAY_MOD, ONLY : DIR_DATA,     DIR_OUTPUT
      USE NAMELIST_ARRAY_MOD, ONLY : LATMOS_M1,    LATMOS_M2
      USE NAMELIST_ARRAY_MOD, ONLY : LATMOS_M3
      USE ERROR_MOD,          ONLY : ERROR_STOP

      ! Local variables
      INTEGER            :: N, N_LAYERS_IN
      CHARACTER(LEN=255) :: SUBSTRS(MAXDIM) 
      CHARACTER(LEN=255) :: ERRMSG

      !=================================================================
      ! READ_CONTROL_MENU begins here!
      !=================================================================

      ! Start and end Wavelengths
      CALL SPLIT_ONE_LINE( SUBSTRS, N, -1, 'read_control_menu:1' )
 
      ! The wavelengths input here is quite flexible:
      !   N=1, Mono-spectrum
      !   N=2, Multi-spectra with regular intervals
      !   N>2, Direct inputs of Multi-spectra
      ! For detail see the below commented lines 
      NLAMDA_NMLST = MIN( N, 99 )
      READ( SUBSTRS(1:NLAMDA_NMLST), * ) LAMDAS_NMLST(1:NLAMDA_NMLST)
      LAMDAb = LAMDAS_NMLST(1)
      LAMDAe = LAMDAS_NMLST(NLAMDA_NMLST)
      LAMDA  = LAMDAb

      !!!--------------------------------------------------------------
      !!! Below is commented out but still leave here for references
      !!! (xxu, 11/5/12)
      !!! Mono-spectrum
      !IF ( N == 1 ) THEN
      !   NLAMDA_NMLST = 1
      !   READ( SUBSTRS(1), * ) LAMDAS_NMLST(1)
      !   LAMDAe       = LAMDAb
      !   LAMDA        = LAMDAb
      !   NLAMDA_NMLST = 1
      !!! Multi-spectra with regular intervals
      !ELSE IF ( N == 2 ) THEN 
      ! 
      !   READ( SUBSTRS(1), * ) LAMDAb
      !   READ( SUBSTRS(2), * ) LAMDAe
      !   LAMDA        = LAMDAb
      !   NLAMDA_NMLST = 2
      !
      !!! Direct inputs of Multi-spectra 
      !ELSE IF ( N > 2 ) THEN 
      !   NLAMDA_NMLST = MIN( N, 100 )
      !   READ( SUBSTRS(1:NLAMDA_NMLST), * ) LAMDAS_NMLST(1:NLAMDA_NMLST)
      !ENDIF
      !!!--------------------------------------------------------------

      ! Spectral step
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_control_menu:1a' )
      READ( SUBSTRS(1), * ) SPECTRA_STEP 

      ! Index of atmospheric type
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_control_menu:2' )
      READ( SUBSTRS(1:N), * ) IATMOS

      ! N_LAYERS
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_control_menu:3' )
      READ( SUBSTRS(1:N), * ) N_LAYERS_IN

      ! Run dir
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_control_menu:5' )
      READ( SUBSTRS(1:N), '(a)' ) DIR_RUN

      ! Data dir
      ! Run dir
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_control_menu:6' )
      READ( SUBSTRS(1:N), '(a)' ) DIR_DATA

      ! Output dir
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_control_menu:7' )
      READ( SUBSTRS(1:N), '(a)' ) DIR_OUTPUT

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_control_menu:8' )


      ! Screen print
      WRITE( 6, '(/,a)') 'CONTROL MENU:'
      WRITE( 6, '(  a)') REPEAT( '-', 48 )
      WRITE( 6, 100 ) 'Start & end lamda [nm]  : ', LAMDAb, LAMDAe
      WRITE( 6, 100 ) 'Spectra step[nm or #/cm]: ', SPECTRA_STEP
      WRITE( 6, 110 ) 'Atmos. Type [1-6,or -1] : ', IATMOS
      WRITE( 6, 110 ) '# of atmos. layers      : ', N_LAYERS_IN
      WRITE( 6, 120 ) 'Run subdirectory        : ', TRIM(DIR_RUN    )
      WRITE( 6, 120 ) 'Data subdirectory       : ', TRIM(DIR_DATA   )
      WRITE( 6, 120 ) 'Output subdirectory     : ', TRIM(DIR_OUTPUT )
 100  FORMAT(  A, 2F8.2 )     
 110  FORMAT(  A, I5    )
 120  FORMAT(  A, A     )

      !================================================================
      ! Check the inputs
      !================================================================

      ! Those checks moved to spectra_mod.f: 
      !! Give a warning if the wavelength is out of range
      !IF ( LAMDAb < 200 .OR. LAMDAe > 4000 ) THEN
      !   PRINT*, LAMDAb, LAMDAe
      !   ERRMSG = "Wavelenth out of range [200-4000nm]"
      !   CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
      !ENDIF 
      !
      !! Ending lamda must be larger or equal to the start one
      !IF ( LAMDAb > LAMDAe ) THEN
      !   PRINT*, LAMDAb, LAMDAe 
      !   ERRMSG = "Starting lamda is larger than ending one"
      !   CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
      !ENDIF

      ! Check the profile type input
      IF ( IATMOS /= 1 .AND. IATMOS /= 2 .AND. IATMOS /= 3 .AND. 
     &     IATMOS /= 4 .AND. IATMOS /= 5 .AND. IATMOS /= 6 .AND. 
     &     IATMOS /= -1 ) THEN 
         ERRMSG = 'Stoped, please check IATM in namelist.ini'
         CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
      ENDIF

      ! Check # of atmos layers
      LATMOS_M1 = .FALSE.
      LATMOS_M2 = .FALSE.
      IF ( N_LAYERS_IN == -1 ) THEN
         N_LAYERS = 33
         LATMOS_M1 = .TRUE.
      ELSE IF ( N_LAYERS_IN == -2 ) THEN
         N_LAYERS = 26
         LATMOS_M2 = .TRUE.
      ELSE IF ( N_LAYERS_IN == -3 ) THEN
         N_LAYERS = 3
         LATMOS_M3 = .TRUE.
      ELSE IF ( N_LAYERS_IN > 0   ) THEN
         N_LAYERS = N_LAYERS_IN
      ELSE 
         ERRMSG = 'Stoped, please check N_LAYERS in namelist.ini'
         CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
      ENDIF

      ! Check the directories
      CALL CHECK_DIRECTORY( DIR_RUN    )
      CALL CHECK_DIRECTORY( DIR_DATA   )
      CALL CHECK_DIRECTORY( DIR_OUTPUT )

      END SUBROUTINE READ_CONTROL_MENU

!------------------------------------------------------------------------------

      SUBROUTINE READ_GEOMETRY_MENU

      ! References to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : N_THETA0, N_THETA, N_PHI, N_GEOM
      USE NAMELIST_ARRAY_MOD, ONLY : THETA0, THETA, PHI
      USE ERROR_MOD,          ONLY : ERROR_STOP

      ! Local variables
      INTEGER            :: N, I
      CHARACTER(LEN=255) :: SUBSTRS(MAXDIM)
      CHARACTER(LEN=255) :: ERRMSG

      !=================================================================
      ! READ_GEOMETRY_MENU begins here!
      !=================================================================

      ! THETA0
      CALL SPLIT_ONE_LINE( SUBSTRS, N, -1, 'read_geometry_menu:4')
      READ( SUBSTRS(1:N), * ) THETA0(1:N)
      N_THETA0 = N

      ! THETA
      CALL SPLIT_ONE_LINE( SUBSTRS, N, -1, 'read_geometry_menu:5' )
      READ( SUBSTRS(1:N), * ) THETA(1:N)
      N_THETA = N

      ! PHI
      CALL SPLIT_ONE_LINE( SUBSTRS, N, -1, 'read_geometry_menu:6' )
      READ( SUBSTRS(1:N), * ) PHI(1:N)
      N_PHI = N
    
      ! N_GEOM
      N_GEOM = N_THETA0 * N_THETA * N_PHI

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_geometry_menu:6' )


      ! Screen print
      WRITE( 6, '(/,a)') 'GEOMETRY MENU:'
      WRITE( 6, '(  a)') REPEAT( '-', 48 )
      WRITE( 6, 110    ) 'Solar zenith angle      :', THETA0(1:N_THETA0)
      WRITE( 6, 110    ) 'Viewer zenith angle     :', THETA(1:N_THETA)
      WRITE( 6, 110    ) 'Relative azimuth angle  :', PHI(1:N_PHI)
 100  FORMAT(  A, I5     )
 110  FORMAT(  A, 25F6.1 )

      !================================================================
      ! Check the inputs
      !================================================================
      ! # of angles input
      IF ( N_THETA0 > 10 .OR. N_THETA > 10 .OR. N_PHI > 2 ) THEN
         ERRMSG = 'Too many angles, maximum = 10(zenith) or 2(azimuth)'
         CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
      ENDIF

      ! Angles out of range
      IF ( MAXVAL(THETA0(1:N_THETA0)) > 90 .OR.
     &     MINVAL(THETA0(1:N_THETA0)) < 0 ) THEN
         ERRMSG = 'Some SZA is out of range...Stoped!'
         CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
      ENDIF

      ! Angles out of range
      IF ( MAXVAL(THETA(1:N_THETA)) > 90 .OR.
     &     MINVAL(THETA(1:N_THETA)) < 0 ) THEN
         ERRMSG = 'Some VZA is out of range...Stoped!'
         CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
      ENDIF

      ! Angles out of range
      IF ( MAXVAL(PHI(1:N_PHI)) > 180 .OR.
     &     MINVAL(PHI(1:N_PHI)) < 0 )THEN
         ERRMSG = 'Some RAZ is out of range...Stoped!'
         CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
      ENDIF

      ! Added by xxu, 2/11/11
      ! Since RTM usually has some computation error for edge angles,
      ! we need to replace those edge angels.
      ! Chack SZA for 0 and 90
      DO I = 1, N_THETA0
         IF ( THETA0(I) <=  1d-4        ) THETA0(I) = 1d-4
         IF ( THETA0(I) >=  90d0 - 1d-4 ) THETA0(I) = 90d0 - 1d-4
      enddo

      ! Chack VZA for 0 and 90
      DO I = 1, N_THETA
         IF ( THETA(I) <=  1d-4        ) THETA(I) = 1d-4
         IF ( THETA(I) >=  90d0 - 1d-4 ) THETA(I) = 90d0 - 1d-4
      enddo

      ! Chack RAZ for 0 and 180
      DO I =1, N_PHI
         IF ( PHI(I) <= 1d-4           ) PHI(I) = 1d-4
         IF ( PHI(I) >= 180d0 - 1d-4   ) PHI(I) = 180d0 - 1d-4
      ENDDO

      END SUBROUTINE READ_GEOMETRY_MENU

!------------------------------------------------------------------------------

      SUBROUTINE READ_RTM_MENU

      ! References to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : LVLIDORT,    N_STOKS
      USE NAMELIST_ARRAY_MOD, ONLY : N_STRMS,     N_MOMENTS
      USE NAMELIST_ARRAY_MOD, ONLY : RECEP_DIR
      USE NAMELIST_ARRAY_MOD, ONLY : NRECEP_LEV,  RECEP_LEV
      USE NAMELIST_ARRAY_MOD, ONLY : I_SCENARIO,  N_LAYERS
      USE ERROR_MOD,          ONLY : ERROR_STOP

      ! Local variables
      INTEGER            :: N, I
      CHARACTER(LEN=255) :: SUBSTRS(MAXDIM)
      CHARACTER(LEN=255) :: ERRMSG

      !=================================================================
      ! READ_RTM_MENU begins here!
      !=================================================================

      ! LVLIDORT
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_rtm_menu:1')
      READ( SUBSTRS(1:N), * ) LVLIDORT

      ! N_STOKS
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_rtm_menu:2')
      READ( SUBSTRS(1:N), * ) N_STOKS

      ! N_STRMS
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_rtm_menu:3')
      READ( SUBSTRS(1:N), * ) N_STRMS

      ! N_MOMENTS
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_rtm_menu:4')
      READ( SUBSTRS(1:N), * ) N_MOMENTS

      ! This now moved to the SURFACE MENU (xxu, 4/26/13)
      ! SURF_REFL
      !CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_rtm_menu:5')
      !READ( SUBSTRS(1:N), * ) SURF_REFL

      ! RECEP_LEV & NRECEP_LEV
      CALL SPLIT_ONE_LINE( SUBSTRS, N, -1, 'read_rtm_menu:6')
      READ( SUBSTRS(1:N), * ) RECEP_LEV(1:N)
      NRECEP_LEV = N

      ! RECEP_DIR
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_rtm_menu:7')
      READ( SUBSTRS(1:N), * ) RECEP_DIR

      ! I_SCENARIO
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_rtm_menu:8')
      READ( SUBSTRS(1:N), * ) I_SCENARIO

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_rtm_menu:9' )

      ! Screen print
      WRITE( 6, '(/,a)') 'RTM MENU:'
      WRITE( 6, '(  a)') REPEAT( '-', 48 )
      WRITE( 6, 120    ) 'Turn on VLIDORT?        :', LVLIDORT
      WRITE( 6, 100    ) '# of Stokes components  :', N_STOKS
      WRITE( 6, 100    ) '# of dsicrete streams   :', N_STRMS
      WRITE( 6, 100    ) '# of P Legendre terms   :', N_MOMENTS
      !WRITE( 6,  90    ) 'Surface reflectence     :', SURF_REFL
      WRITE( 6, 110    ) 'Receptor levels         :', 
     &                   RECEP_LEV(1:NRECEP_LEV)
      WRITE( 6, 100    ) 'Receptor direction      :', RECEP_DIR
      WRITE( 6, 100    ) 'Index of SSC scenario   :', I_SCENARIO
  90  FORMAT( A, F8.4  )
 100  FORMAT( A, I5     )
 110  FORMAT( A, 10F6.1 )
 120  FORMAT( A, L5     )

      !================================================================
      ! Check the inputs
      !================================================================
      IF ( LVLIDORT ) THEN

         ! Check number of Stokes parameters
         IF ( N_STOKS /= 1 .AND. N_STOKS /= 2 .AND. 
     &        N_STOKS /= 3 .AND. N_STOKS /= 4 ) THEN
            ERRMSG = '# of Stokes components must be 1, 2, 3, or 4!'
            CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
         ENDIF
 
         ! Check receptor direction 
         IF ( RECEP_DIR /= 1 .AND. RECEP_DIR /= -1 ) THEN
            ERRMSG = 'Receptor direction must be -1, or 1!'
            CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
         ENDIF
 
         ! Check Index of SSC scenario  
         !IF ( I_SCENARIO > 6 .OR. I_SCENARIO < 0 ) THEN 
         !   ERRMSG = 'Index of SSC scenario must be 0-6!'
         !   CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
         !ENDIF
         SELECT CASE ( I_SCENARIO )
         CASE ( 0, 1, 2, 3, 4, 5, 10, 11, 12, 13, 14,15 )
            I_SCENARIO = I_SCENARIO
         CASE DEFAULT
            I_SCENARIO = 1
            WRITE(6, *) ' WARNNING: Index of SSC scenario is out' //
     &                  ' of range: 0-5,10-15.'
            WRITE(6, *) '           Here forced to use: I_SCENARIO = 1 '
         END SELECT

         ! Check receptor levels
         DO I = 1, NRECEP_LEV
            IF ( RECEP_LEV(I) < 0 ) RECEP_LEV(I) = REAL(N_LAYERS,8)
         ENDDO

      ENDIF

      END SUBROUTINE READ_RTM_MENU

!------------------------------------------------------------------------------

      SUBROUTINE READ_SURFACE_MENU

      ! References to F90 Modules
      USE NAMELIST_ARRAY_MOD, ONLY : LLAMBERTIAN
      USE NAMELIST_ARRAY_MOD, ONLY : LBRDF
      USE NAMELIST_ARRAY_MOD, ONLY : SURF_REFL
      USE NAMELIST_ARRAY_MOD, ONLY : N_BRDF,       BRDF_NPARS
      USE NAMELIST_ARRAY_MOD, ONLY : BRDF_NAMES,   BRDF_INDEX
      USE NAMELIST_ARRAY_MOD, ONLY : BRDF_FACTOR,  BRDF_PAR
      USE ERROR_MOD,          ONLY : ERROR_STOP

       ! Local variables
      INTEGER            :: N, I
      CHARACTER(LEN=255) :: SUBSTRS(MAXDIM)
      CHARACTER(LEN=255) :: ERRMSG

      !=================================================================
      ! READ_SURFACE_MENU begins here!
      !=================================================================

      ! LLAMBERTIAN
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_surface_menu:1')
      READ( SUBSTRS(1), * ) LLAMBERTIAN

      ! SURF_REFL
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_surface_menu:2')
      READ( SUBSTRS(1), * ) SURF_REFL

      ! LBRDF
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_surface_menu:3')
      READ( SUBSTRS(1), * ) LBRDF

      ! N_BRDF
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_surface_menu:3')
      READ( SUBSTRS(1), * ) N_BRDF

      ! The max number of N_BRDF is 3
      IF ( N_BRDF > 3 ) THEN 
         ERRMSG = '# of BRDF kernels cannot greater than 3'
         CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
      ENDIF

      ! Skip this line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 7, 'read_surface_menu:4' )

      DO I = 1, N_BRDF

         CALL SPLIT_ONE_LINE( SUBSTRS, N, 7, 'read_surface_menu:5' )
         READ( SUBSTRS(1), * ) BRDF_NAMES(I)
         READ( SUBSTRS(2), * ) BRDF_INDEX(I)
         READ( SUBSTRS(3), * ) BRDF_FACTOR(I)
         READ( SUBSTRS(4), * ) BRDF_NPARS(I)
         READ( SUBSTRS(5), * ) BRDF_PAR(I,1)
         READ( SUBSTRS(6), * ) BRDF_PAR(I,2)
         READ( SUBSTRS(7), * ) BRDF_PAR(I,3)

      ENDDO

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_surface_menu:4' )

      ! Screen print
      WRITE( 6, '(/,a)') 'SURFACE MENU:'
      WRITE( 6, '(  a)') REPEAT( '-', 48 )
      WRITE( 6, 100    ) 'Do Lambertian surface?  :', LLAMBERTIAN
      WRITE( 6, 110    ) '  - surface reflecetance:', SURF_REFL
      WRITE( 6, 100    ) 'Do BRDF surface?        :', LBRDF
      WRITE( 6, 120    ) '  - # of BRDF kernels   :', N_BRDF
      WRITE( 6, 130    ) '  - BRDF kernel entries :' 
      DO I = 1, N_BRDF
         WRITE( 6, 140 ) '           ==> kernel#1 :', BRDF_NAMES(I), 
     &                   BRDF_INDEX(I), BRDF_FACTOR(I), BRDF_NPARS(I),
     &                   BRDF_PAR(I,1:3)
      ENDDO 
 100  FORMAT( A, L5     )
 110  FORMAT( A, 10F6.1 )
 120  FORMAT( A, I5     )
 130  FORMAT( A, "Name   Index  Factor  #PARS  PAR(1)  PAR(2)  PAR(3)") 
 140  FORMAT( A, A10, 1X, I2, F8.4, I4, 1X, 3F8.3 )

      !================================================================
      ! Check the inputs
      !================================================================
      ! Both Lambertian and BRDF are set to be false
      IF ( .NOT. LLAMBERTIAN .AND. .NOT. LBRDF ) THEN 
         ERRMSG = 'One of Lambertian and BRDF must be true'
         CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
      ENDIF

      ! Both Lambertian and BRDF are set to be true
      IF ( LLAMBERTIAN .AND. LBRDF ) THEN 
         ERRMSG = 'Only one of Lambertian and BRDF can be true'
         CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
      ENDIF

      ! Return to the calling routine
      END SUBROUTINE READ_SURFACE_MENU

!------------------------------------------------------------------------------

      SUBROUTINE READ_JACOBIAN_MENU

      ! References to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_GAS,  LJACOB_AOD
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_SSA,  LJACOB_REFR
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_SIZE, LJACOB_EPS
      USE NAMELIST_ARRAY_MOD, ONLY : NONVAR_MASS, NONVAR_AOD
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_SURF, LJACOB_MFRC
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_PROF, LJACOB_MASS
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_BRDF_FACTOR
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_BRDF_PAR
      USE NAMELIST_ARRAY_MOD, ONLY : LVLIDORT_FD
      USE ERROR_MOD,          ONLY : ERROR_STOP

      ! Local variables
      INTEGER            :: N
      CHARACTER(LEN=255) :: SUBSTRS(MAXDIM)
      CHARACTER(LEN=255) :: ERRMSG

      !=================================================================
      ! READ_JACOBIAN_MENU begins here!
      !=================================================================

      ! LJACOB
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:1')
      READ( SUBSTRS(1:N), * ) LJACOB
 
      ! LJACOB_GAS
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:2')
      READ( SUBSTRS(1:N), * ) LJACOB_GAS

      ! LJACOB_AOD
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:2b')
      READ( SUBSTRS(1:N), * ) LJACOB_AOD

      ! LJACOB_SSA
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:3')
      READ( SUBSTRS(1:N), * ) LJACOB_SSA

      ! LJACOB_MASS
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:3a')
      READ( SUBSTRS(1:N), * ) LJACOB_MASS

      ! LJACOB_MFRC
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:6')
      READ( SUBSTRS(1:N), * ) LJACOB_MFRC

      ! LJACOB_REFR
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:4')
      READ( SUBSTRS(1:N), * ) LJACOB_REFR

      ! LJACOB_EPS
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:4')
      READ( SUBSTRS(1:N), * ) LJACOB_EPS

      ! LJACOB_SIZE
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:5')
      READ( SUBSTRS(1:N), * ) LJACOB_SIZE

      ! LJACOB_PROF
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:7')
      READ( SUBSTRS(1:N), * ) LJACOB_PROF

      ! NONVAR_MASS
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:9')
      READ( SUBSTRS(1:N), * ) NONVAR_MASS

      ! NONVAR_AOD
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:10')
      READ( SUBSTRS(1:N), * ) NONVAR_AOD

      ! LJACOB_SURF
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:11')
      READ( SUBSTRS(1:N), * ) LJACOB_SURF
      
      ! LJACOB_BRDF_FACTOR
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:11a')
      READ( SUBSTRS(1:N), * ) LJACOB_BRDF_FACTOR

      ! LACOB_BRDF_PAR
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:11b')
      READ( SUBSTRS(1:N), * ) LJACOB_BRDF_PAR

      ! LVLIDORT_FD
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:12')
      READ( SUBSTRS(1:N), * ) LVLIDORT_FD

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_jacobian_menu:13' )

      ! Screen print
      WRITE( 6, '(/,a)') 'JACOBIAN MENU'
      WRITE( 6, '(  a)') REPEAT( '-', 48 )
      WRITE( 6, 120    ) 'Turn on atmos. Jacob.?  :', LJACOB
      WRITE( 6, 120    ) '  ==> wrt AOD?          :', LJACOB_AOD
      WRITE( 6, 120    ) '  ==> wrt SSA?          :', LJACOB_SSA
      WRITE( 6, 120    ) '  ==> wrt aerosol mass? :', LJACOB_MASS
      WRITE( 6, 120    ) '  ==> wrt mode fraction?:', LJACOB_MFRC
      WRITE( 6, 120    ) '  ==> wrt refractivity? :', LJACOB_REFR
      WRITE( 6, 120    ) '  ==> wrt shape factor? :', LJACOB_EPS 
      WRITE( 6, 120    ) '  ==> wrt size dist?    :', LJACOB_SIZE
      WRITE( 6, 120    ) '  ==> wrt profile?      :', LJACOB_PROF
      WRITE( 6, 120    ) 'Non-variation of mass?  :', NONVAR_MASS
      WRITE( 6, 120    ) 'Non-variation of AOD?   :', NONVAR_AOD
      WRITE( 6, 120    ) 'Turn on surface Jacob.? :', LJACOB_SURF
      WRITE( 6, 120    ) '  ==> wrt BRDF factor?  :', LJACOB_BRDF_FACTOR
      WRITE( 6, 120    ) '  ==> wrt BRDF pars?    :', LJACOB_BRDF_PAR
      WRITE( 6, 120    ) 'Do FD verication?       :', LVLIDORT_FD

 120  FORMAT( A, L5     )

      !================================================================
      ! Check the inputs
      !================================================================
      IF ( LJACOB ) THEN

         ! NONVAR_MASS and NONVAR_AOD never can be same 
         IF ( NONVAR_MASS .AND. NONVAR_AOD ) THEN
            ERRMSG = 'Non-variation of mass and AOD cannot at the'
     &             // ' same time...Stoped!'
            CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
         ENDIF
         IF ( .NOT. NONVAR_MASS .AND. .NOT. NONVAR_AOD ) THEN
            ERRMSG = 'Non-variation of mass and AOD cannot at the'
     &             // ' same time...Stoped!'
            CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
         ENDIF

         ! Jacobian for AOD and SSA is necessary for the FD test
         IF ( LVLIDORT_FD ) THEN
         IF ( .NOT. LJACOB_AOD .OR. .NOT. LJACOB_SSA ) THEN
            ERRMSG = 'Jacobian for AOD and SSA is necessary for the'
     &            // ' FD verification...Stoped!'
            CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
         ENDIF
         ENDIF

      ! If Jacobian is turned off, all the sub-logical variable are false
      ELSE

          ! If Jacobian is turned off, all the sub-logical variable off
         LJACOB_AOD  = .FALSE.
         LJACOB_SSA  = .FALSE.
         LJACOB_REFR = .FALSE.
         LJACOB_SIZE = .FALSE.
         LJACOB_MFRC = .FALSE.
         !LJACOB_USER = .FALSE.
         LJACOB_SURF = .FALSE.
         LVLIDORT_FD = .FALSE.

      ENDIF     

      ! Surface reflectence
      IF ( LJACOB_SURF ) THEN 

         ! Needs update (xxu, 4/29/13)
         IF ( LJACOB_BRDF_PAR ) THEN 
            ERRMSG = 'BRDF parameter jacobian has not implemented!'
            CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )

         ENDIF

      ELSE

         ! no surface Jacobian calculation
         LJACOB_BRDF_FACTOR = .FALSE.
         LJACOB_BRDF_PAR    = .FALSE.

      ENDIF

      END SUBROUTINE READ_JACOBIAN_MENU

!------------------------------------------------------------------------------

      SUBROUTINE  READ_AEROSOL_MENU
 
      ! References to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : LAEROSOL,     LJACOB_MFRC
      USE NAMELIST_ARRAY_MOD, ONLY : NMODE,        MODEFRC
      USE NAMELIST_ARRAY_MOD, ONLY : LAOD,         LMASS
      USE NAMELIST_ARRAY_MOD, ONLY : COLUMN_AOD0,  COLUMN_MASS0
      USE NAMELIST_ARRAY_MOD, ONLY : AERDEN,       SSMODEL
      USE NAMELIST_ARRAY_MOD, ONLY : RMRI,         SIZERANGE
      USE NAMELIST_ARRAY_MOD, ONLY : LMONODIS,     MONOR
      USE NAMELIST_ARRAY_MOD, ONLY : IDIST,        DISTPAR
      USE NAMELIST_ARRAY_MOD, ONLY : L_REFER_AOD,  REFER_LAMDA
      USE NAMELIST_ARRAY_MOD, ONLY : REFER_MONOR,  REFER_LMONODIS
      USE NAMELIST_ARRAY_MOD, ONLY : REFER_RMRI,   REFER_SIZERANGE
      USE NAMELIST_ARRAY_MOD, ONLY : REFER_IDIST,  REFER_DISTPAR
      USE NAMELIST_ARRAY_MOD, ONLY : IDPROF,       PROF_RANGE
      USE NAMELIST_ARRAY_MOD, ONLY : PROF_PAR
      USE NAMELIST_ARRAY_MOD, ONLY : IDSHAPE,      SHAPE_PAR
      USE ERROR_MOD,          ONLY : ERROR_STOP

      ! Local variables
      INTEGER            :: N, I, IMODE
      CHARACTER(LEN=255) :: SUBSTRS(MAXDIM)
      CHARACTER(LEN=255) :: ERRMSG

      !=================================================================
      ! READ_AEROSOL_MENU begins here!
      !=================================================================

      ! LAEROSOL
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_aerosol_menu:0' )
      READ( SUBSTRS(1:N), * ) LAEROSOL

      ! NMODE
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_aerosol_menu:1' )
      READ( SUBSTRS(1:N), * ) NMODE

      ! MODEFRC
      CALL SPLIT_ONE_LINE( SUBSTRS, N, -1, 'read_aerosol_menu:2' )
      READ( SUBSTRS(1), * ) MODEFRC(1)
      IF ( NMODE == 2 ) READ( SUBSTRS(2), * ) MODEFRC(2)

      ! LAOD, and COLUMN_AOD0
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_aerosol_menu:3' )
      READ( SUBSTRS(1), * ) LAOD
      READ( SUBSTRS(2), * ) COLUMN_AOD0

      ! LMASS, and COLUMN_MASS0
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_aerosol_menu:4' )
      READ( SUBSTRS(1), * ) LMASS
      READ( SUBSTRS(2), * ) COLUMN_MASS0

      ! AERDEN
      CALL SPLIT_ONE_LINE( SUBSTRS, N, -1, 'read_aerosol_menu:5' )
      READ( SUBSTRS(1), * ) AERDEN(1)
      IF ( NMODE == 2 ) READ( SUBSTRS(2), * ) AERDEN(2)

      ! SSMODEL
      CALL SPLIT_ONE_LINE( SUBSTRS, N, -1, 'read_aerosol_menu:6' )
      READ( SUBSTRS(1), * ) SSMODEL(1)
      IF ( NMODE == 2 ) READ( SUBSTRS(2), * ) SSMODEL(2)

      ! Mode # 1
      IMODE = 1

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_aerosol_menu:s1' )

      ! RMRI
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_aerosol_menu:7' )
      READ( SUBSTRS(1), * ) RMRI(1,IMODE)
      READ( SUBSTRS(2), * ) RMRI(2,IMODE)

      ! Shape factor
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_aerosol_menu:7a' )
      READ( SUBSTRS(1), * ) IDSHAPE(IMODE)
      READ( SUBSTRS(2), * ) SHAPE_PAR(IMODE)

      ! LMONODIS & MONOR
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_aerosol_menu:8' )
      READ( SUBSTRS(1), * ) LMONODIS(IMODE)
      READ( SUBSTRS(2), * ) MONOR(IMODE)

      ! SIZERANGE
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_aerosol_menu:9' )
      READ( SUBSTRS(1), * ) SIZERANGE(1,IMODE)
      READ( SUBSTRS(2), * ) SIZERANGE(2,IMODE)

      ! Skip this line for the title of 'Size distribution'
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 4, 'read_aerosol_menu:s2' )

      ! IDIST & DISTPAR
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 4, 'read_aerosol_menu:10' )
      READ( SUBSTRS(1), * ) IDIST(IMODE)
      READ( SUBSTRS(2), * ) DISTPAR(1,IMODE)
      READ( SUBSTRS(3), * ) DISTPAR(2,IMODE)
      READ( SUBSTRS(4), * ) DISTPAR(3,IMODE)

      ! PROF_RANGE
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_aerosol_menu:11' )
      READ( SUBSTRS(1), * ) PROF_RANGE(1,IMODE)
      READ( SUBSTRS(2), * ) PROF_RANGE(2,IMODE)

      ! Skip this line for the title of 'verti profile'
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 3, 'read_aerosol_menu:s3' )

      ! IDPROF & PROF_PAR
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 3, 'read_aerosol_menu:13' )
      READ( SUBSTRS(1), * ) IDPROF(IMODE)
      READ( SUBSTRS(2), * ) PROF_PAR(1,IMODE)
      READ( SUBSTRS(3), * ) PROF_PAR(2,IMODE)

      ! Mode # 2
      IMODE = 2

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_aerosol_menu:s4' )

      ! RMRI
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_aerosol_menu:21' )
      READ( SUBSTRS(1), * ) RMRI(1,IMODE)
      READ( SUBSTRS(2), * ) RMRI(2,IMODE)

      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_aerosol_menu:7a' )
      READ( SUBSTRS(1), * ) IDSHAPE(IMODE)
      READ( SUBSTRS(2), * ) SHAPE_PAR(IMODE)

      ! LMONODIS & MONOR
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_aerosol_menu:22' )
      READ( SUBSTRS(1), * ) LMONODIS(IMODE)
      READ( SUBSTRS(2), * ) MONOR(IMODE)

      ! SIZERANGE
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_aerosol_menu:23' )
      READ( SUBSTRS(1), * ) SIZERANGE(1,IMODE)
      READ( SUBSTRS(2), * ) SIZERANGE(2,IMODE)

      ! Skip this line for the title of 'Size distribution'
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 4, 'read_aerosol_menu:s5' )

      ! IDIST & DISTPAR
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 4, 'read_aerosol_menu:24' )
      READ( SUBSTRS(1), * ) IDIST(IMODE)
      READ( SUBSTRS(2), * ) DISTPAR(1,IMODE)
      READ( SUBSTRS(3), * ) DISTPAR(2,IMODE)
      READ( SUBSTRS(4), * ) DISTPAR(3,IMODE)

      ! PROF_RANGE
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_aerosol_menu:25' )
      READ( SUBSTRS(1), * ) PROF_RANGE(1,IMODE)
      READ( SUBSTRS(2), * ) PROF_RANGE(2,IMODE)

      ! Skip this line for the title of 'verti profile'
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 3, 'read_aerosol_menu:s6' )

      ! IDPROF & PROF_PAR
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 3, 'read_aerosol_menu:27' )
      READ( SUBSTRS(1), * ) IDPROF(IMODE)
      READ( SUBSTRS(2), * ) PROF_PAR(1,IMODE)
      READ( SUBSTRS(3), * ) PROF_PAR(2,IMODE)

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_aerosol_menu:s7' )

      ! L_REFER_AOD
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_aerosol_menu:31' ) 
      READ( SUBSTRS(1:N), * ) L_REFER_AOD

      ! REFER_LAMDA
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_aerosol_menu:32' )
      READ( SUBSTRS(1:N), * ) REFER_LAMDA
 
      ! REFER_RMRI
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_aerosol_menu:33' )   
      READ( SUBSTRS(1), * ) REFER_RMRI(1)
      READ( SUBSTRS(2), * ) REFER_RMRI(2)

      ! REFER_LMONODIS & REFER_MONOR
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_aerosol_menu:33a')
      READ( SUBSTRS(1), * ) REFER_LMONODIS
      READ( SUBSTRS(2), * ) REFER_MONOR

      ! REFER_SIZERANGE
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_aerosol_menu:34' )
      READ( SUBSTRS(1), * ) REFER_SIZERANGE(1)
      READ( SUBSTRS(2), * ) REFER_SIZERANGE(2)
 
      ! Skip this line for the title of 'Size distribution'
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 4, 'read_aerosol_menu:s8' )

      ! REFER_IDIST & REFER_DISTPAR
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 4, 'read_aerosol_menu:35' )
      READ( SUBSTRS(1), * ) REFER_IDIST
      READ( SUBSTRS(2), * ) REFER_DISTPAR(1)
      READ( SUBSTRS(3), * ) REFER_DISTPAR(2)
      READ( SUBSTRS(4), * ) REFER_DISTPAR(3)

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_aerosol_menu:36' )

      ! Screen print
      WRITE( 6, '(/,a)') 'AEROSOL MENU'
      WRITE( 6, '(  a)') REPEAT( '-', 48 )
      WRITE( 6, 100    ) 'Turn on aerosol?        : ', LAEROSOL
      WRITE( 6, 120    ) 'Number of aerosol modes : ', NMODE
      WRITE( 6, 130    ) 'Modal fractions         : ', MODEFRC
      WRITE( 6, 180    ) 'Column AOD as input?    : ', LAOD,
     &                   COLUMN_AOD0
      WRITE( 6, 180    ) 'ColMass[kg/m2] as input?: ', LMASS, 
     &                   COLUMN_MASS0
      WRITE( 6, 140    ) 'Density of each [kg/m3] : ', AERDEN
      WRITE( 6, 170    ) 'Mie(1) or T-Matrix(2)?  : ', SSMODEL
      WRITE( 6, '(a,a)') 'Mode #1 Properties      : ', REPEAT('>', 30)
      WRITE( 6, 140    ) '  ==> refractive index  : ', RMRI(:,1)
      WRITE( 6, 160    ) '  ==> shape factor      : ', 
     &                   IDSHAPE(1), SHAPE_PAR(1)
      WRITE( 6, 150    ) '  ==> monodisperse?     : ', 
     &                   LMONODIS(1), MONOR(1)
      WRITE( 6, 130    ) '  ==> size range [um]   : ', SIZERANGE(:,1)
      WRITE( 6, '(  a)') '  ==> size distribution : ' //
     &                   'DIS#  PAR(1)  PAR(2)  PAR(3)'
      WRITE( 6, 160    ) '          --> Entries   : ', 
     &                   IDIST(1), DISTPAR(:,1) 
      WRITE( 6, 130    ) '  ==> vertical range[km]: ', PROF_RANGE(:,1)
      WRITE( 6, '(  a)') '  ==> vertical profile  : PRFL# PAR(1) PAR(2)'
      WRITE( 6, 160    ) '          --> Entries   : ', 
     &                   IDPROF(1), PROF_PAR(:,1)
      WRITE( 6, '(a,a)') 'Mode #2 Properties      : ', REPEAT('>', 30)
      WRITE( 6, 140    ) '  ==> refractive index  : ', RMRI(:,2)
      WRITE( 6, 160    ) '  ==> shape factor      : ',
     &                   IDSHAPE(2), SHAPE_PAR(2)
      WRITE( 6, 150    ) '  ==> monodisperse?     : ', 
     &                   LMONODIS(2), MONOR(2)
      WRITE( 6, 130    ) '  ==> size range [um]   : ', SIZERANGE(:,2)
      WRITE( 6, '(  a)') '  ==> size distribution : ' //
     &                   'DIS#  PAR(1)  PAR(2)  PAR(3)'
      WRITE( 6, 160    ) '          --> Entries   : ', 
     &                   IDIST(2), DISTPAR(:,2) 
      WRITE( 6, 130    ) '  ==> vertical range[km]: ', PROF_RANGE(:,2)
      WRITE( 6, '(  a)') '  ==> vertical profile  : PRFL# PAR(1) PAR(2)'
      WRITE( 6, 160    ) '          --> Entries   : ', 
     &                   IDPROF(2), PROF_PAR(:,2)
      WRITE( 6, '(a,a)') 'Reference aerosol mode  : ', REPEAT('>', 30)
      WRITE( 6, 100    ) '  ==> Reference AOD?    : ', L_REFER_AOD
      WRITE( 6, 110    ) '  ==> Wavelength [nm]   : ', REFER_LAMDA
      WRITE( 6, 140    ) '  ==> Refractive index  : ', REFER_RMRI
      WRITE( 6, 150    ) '  ==> monodisperse?     : ',
     &                   REFER_LMONODIS, REFER_MONOR
      WRITE( 6, 130    ) '  ==> Size range [um]   : ', REFER_SIZERANGE
      WRITE( 6, '(  a)') '  ==> Size distribution : ' //
     &                   'DIS#    PAR(1)  PAR(2)  PAR(3)'
      WRITE( 6, 160    ) '     --> Entries        : ', 
     &                   REFER_IDIST, REFER_DISTPAR
 
 100  FORMAT( A, L5        )
 110  FORMAT( A, F12.3     )
 120  FORMAT( A, I5        )
 130  FORMAT( A, 2F12.3    )  
 140  FORMAT( A, 1P2E12.3  )
 150  FORMAT( A, L5, 2F12.3 )
 160  FORMAT( A, I4, 3F12.2 )
 170  FORMAT( A, 2I4       )
 180  FORMAT( A, L5, 1P2E12.3 )

      !================================================================
      ! Check the inputs
      !================================================================
 
      ! Only one of AOD and mass can be as input
      IF ( LAOD .AND. LMASS ) THEN
         ERRMSG = 'Only one of ColAOD and ColMass can be as input'
         CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
      ENDIF

      IF ( .NOT. LAOD .AND. .NOT. LMASS ) THEN
         ERRMSG = 'Only one of ColAOD and ColMass must be as input'
         CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
      ENDIF

      IF ( LAOD ) COLUMN_MASS0 = 0d0
      IF ( LMASS ) THEN
         L_REFER_AOD = .FALSE.
         COLUMN_AOD0 = 0d0
      ENDIF

      ! Size distribution index
      IF ( REFER_IDIST < 1 .OR. REFER_IDIST > 8 ) THEN
         ERRMSG = 'Size distribution index must be 1-8!'
         CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
      ENDIF 

      ! For mono-mode
      IF ( NMODE == 1 ) THEN 
         MODEFRC(1)  = 1d0
         MODEFRC(2)  = 0d0
         LJACOB_MFRC = .FALSE.
      ENDIF

      ! Check SSMODEL
      IF ( SSMODEL(1) /= 1 .AND. SSMODEL(1) /= 2 ) SSMODEL(1) = 1
      IF ( SSMODEL(2) /= 1 .AND. SSMODEL(2) /= 2 ) SSMODEL(2) = 1

      ! Check mono-disperse 
      IF ( .NOT. LMONODIS(1) ) MONOR(1) = 0d0
      IF ( .NOT. LMONODIS(2) ) MONOR(2) = 0d0
      IF ( .NOT. REFER_LMONODIS ) REFER_MONOR = 0d0

      END SUBROUTINE  READ_AEROSOL_MENU

!------------------------------------------------------------------------------

      SUBROUTINE READ_GAS_MENU

      ! References to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : NGAS,  LTRACEGAS
      USE NAMELIST_ARRAY_MOD, ONLY : GASID, GASNAME
      USE NAMELIST_ARRAY_MOD, ONLY : LGAS,  GASMU
      USE NAMELIST_ARRAY_MOD, ONLY : GASSF
      USE NAMELIST_ARRAY_MOD, ONLY : GAS_FWHM

      ! Local variables
      INTEGER            :: N, I
      CHARACTER(LEN=255) :: SUBSTRS(MAXDIM)

      !=================================================================
      ! READ_GAS_MENU begins here!
      !=================================================================

      ! LTRACEGAS
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_gas_menu:0' )
      READ( SUBSTRS(1:N), * ) LTRACEGAS

      ! GAS_FWHM
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_gas_menu:1' )
      READ( SUBSTRS(1:N), * ) GAS_FWHM

      ! NGAS
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_gas_menu:1' )
      READ( SUBSTRS(1:N), * ) NGAS

      ! Skip this line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 5, 'read_gas_menu:2' )     

      DO I = 1, NGAS
      
         CALL SPLIT_ONE_LINE( SUBSTRS, N, 5, 'read_gas_menu:3' )
         READ( SUBSTRS(1), * ) GASID(I)
         READ( SUBSTRS(2), * ) GASNAME(I)
         READ( SUBSTRS(3), * ) GASMU(I)
         READ( SUBSTRS(4), * ) LGAS(I)
         READ( SUBSTRS(5), * ) GASSF(I)

      ENDDO
     
      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_gas_menu:4' )

      ! Screen print
      WRITE( 6, '(/,a)') 'TRACE-GAS MENU:'
      WRITE( 6,  130   ) 'Full wid.@half max(FWHM): ', GAS_FWHM
      WRITE( 6, '(  a)') REPEAT( '-', 48 )
      WRITE( 6,  100   ) 'Turn on trace gases?    : ', LTRACEGAS
      WRITE( 6,  110   ) 'Num. of trace gases     : ', NGAS
      WRITE( 6, '(  a)') ' TR#   Name   M-Weight   Include?   Scaling'
      DO I = 1, NGAS
         WRITE( 6, 120 ) GASID(I), GASNAME(I), GASMU(I), LGAS(I),
     &                   GASSF(I)
      ENDDO

 100  FORMAT(  A, L5  )
 110  FORMAT(  A, I5  )
 120  FORMAT(  I4, 3x, A6, 1x, F7.3, 4x, L5, F7.3 )
 130  FORMAT(  A, 1PE12.5 ) 

      END SUBROUTINE READ_GAS_MENU
!------------------------------------------------------------------------------

      SUBROUTINE READ_RAYLEIGH_MENU

      ! References to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : LRAYLEIGH, LANISOTRO

      ! Local variables
      INTEGER            :: N
      CHARACTER(LEN=255) :: SUBSTRS(MAXDIM)

      !=================================================================
      ! READ_RAYLEIGH_MENU begins here!
      !=================================================================

      ! LRAYLEIGH
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_rayleigh_menu:1' )
      READ( SUBSTRS(1:N), * ) LRAYLEIGH

      ! LANISOTRO
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_rayleigh_menu:1' )
      READ( SUBSTRS(1:N), * ) LANISOTRO

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_rayleigh_menu:2' )


      ! Screen print
      WRITE( 6, '(/,a)') 'RAYLEIGH MENU'
      WRITE( 6, '(  a)') REPEAT( '-', 48 )
      WRITE(6,100) 'Turn on Rayleigh?       : ', LRAYLEIGH
      WRITE(6,100) 'Turn on anisotropy?     : ', LANISOTRO
 100  FORMAT( A, L5 )


      END SUBROUTINE READ_RAYLEIGH_MENU

!------------------------------------------------------------------------------

      SUBROUTINE READ_DIAGNOSTIC_MENU

      ! References to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : DIAG_PREFIX, LDIAG
      USE NAMELIST_ARRAY_MOD, ONLY : LDIAG01, LDIAG02, LDIAG03, LDIAG04
      USE NAMELIST_ARRAY_MOD, ONLY : LDIAG05, LDIAG06, LDIAG07, LDIAG08

      ! Local variables
      INTEGER            :: N
      CHARACTER(LEN=255) :: SUBSTRS(MAXDIM)

      !=================================================================
      ! READ_DIAGNOSTIC_MENU begins here!
      !=================================================================

      ! LDIAG
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_diagnostic_menu:1' )
      READ( SUBSTRS(1:N), * ) LDIAG

      ! DIAG_FILENAME
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_diagnostic_menu:2' )
      READ( SUBSTRS(1:N), '(A)' ) DIAG_PREFIX

      ! LDIAG01
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_diagnostic_menu:3' )
      READ( SUBSTRS(1:N), * ) LDIAG01

      ! LDIAG02
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_diagnostic_menu:4' )
      READ( SUBSTRS(1:N), * ) LDIAG02

      ! LDIAG03
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_diagnostic_menu:5' )
      READ( SUBSTRS(1:N), * ) LDIAG03

      ! LDIAG04
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_diagnostic_menu:6' )
      READ( SUBSTRS(1:N), * ) LDIAG04

      ! LDIAG05
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_diagnostic_menu:7' )
      READ( SUBSTRS(1:N), * ) LDIAG05

      ! LDIAG06
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_diagnostic_menu:8' )
      READ( SUBSTRS(1:N), * ) LDIAG06

      ! LDIAG07
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_diagnostic_menu:9' )
      READ( SUBSTRS(1:N), * ) LDIAG07

      ! LDIAG08
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_diagnostic_menu:10' )
      READ( SUBSTRS(1:N), * ) LDIAG08

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_diagnostic_menu:11' )

      ! Screen print
      WRITE( 6, '(/,a)') 'DIAGNOSTIC MENU'
      WRITE( 6, '(  a)') REPEAT( '-', 48 )
      WRITE(6,110) 'Turn on DIAGNOSTIC?     : ', LDIAG
      WRITE(6,100) 'Output NC file prefix   : ', TRIM(DIAG_PREFIX)
      WRITE(6,110) 'DIAG01: Model inputs    : ', LDIAG01
      WRITE(6,110) 'DIAG02: Atmos profiles  : ', LDIAG02
      WRITE(6,110) 'DIAG03: Linearized Mie  : ', LDIAG03
      WRITE(6,110) 'DIAG04: Trace gas       : ', LDIAG04
      WRITE(6,110) 'DIAG05: Optic profiles  : ', LDIAG05
      WRITE(6,110) 'DIAG06: VLIDORT IOP     : ', LDIAG06
      WRITE(6,110) 'DIAG07: Radiances       : ', LDIAG07
      WRITE(6,110) 'DIAG08: Jacobians       : ', LDIAG08

 100   FORMAT( A, A  ) 
 110   FORMAT( A, L5 )

      END SUBROUTINE READ_DIAGNOSTIC_MENU

!------------------------------------------------------------------------------

      SUBROUTINE READ_DEBUG_MENU

      ! References to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : LDEBUG_AOD_CALC 
      USE NAMELIST_ARRAY_MOD, ONLY : LDEBUG_RAYLEIGH_CALC
      USE NAMELIST_ARRAY_MOD, ONLY : LDEBUG_TRACEGAS_CALC
      USE NAMELIST_ARRAY_MOD, ONLY : LDEBUG_MIE_CALC
      USE NAMELIST_ARRAY_MOD, ONLY : LDEBUG_IOP_CALC
      USE NAMELIST_ARRAY_MOD, ONLY : LPRT

      ! Local variables
      INTEGER            :: N
      CHARACTER(LEN=255) :: SUBSTRS(MAXDIM)

      !=================================================================
      ! READ_DEBUG_MENU begins here!
      !=================================================================

      ! LPRT
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_debug_menu:5' )
      READ( SUBSTRS(1:N), * ) LPRT

      ! DEBUG_AOD_CALC
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_debug_menu:1' )
      READ( SUBSTRS(1:N), * ) LDEBUG_AOD_CALC
    
      ! DEBUG_MIE_CALC
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_debug_menu:2' )
      READ( SUBSTRS(1:N), * ) LDEBUG_MIE_CALC

      ! DEBUG_RAYLEIGH_CALC
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_debug_menu:3' )
      READ( SUBSTRS(1:N), * ) LDEBUG_RAYLEIGH_CALC

      ! DEBUG_TRACEGAS_CALC
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_debug_menu:4' )
      READ( SUBSTRS(1:N), * ) LDEBUG_TRACEGAS_CALC

      ! DEBUG_MIE_CALC
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_debug_menu:5' )
      READ( SUBSTRS(1:N), * ) LDEBUG_IOP_CALC

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_debug_menu:6' )

      !=================================================================
      ! Print to screen
      !=================================================================
      WRITE( 6, '(/,a)') 'DEBUG MENU'
      WRITE( 6, '(  a)') REPEAT( '-', 48 )
      WRITE( 6, 100 ) 'Turn on screen print?   : ', LPRT
      WRITE( 6, 100 ) 'Turn on aerosol debug?  : ', LDEBUG_AOD_CALC
      WRITE( 6, 100 ) 'Turn on Mie debug?      : ', LDEBUG_MIE_CALC
      WRITE( 6, 100 ) 'Turn on Rayleigh debug? : ', LDEBUG_RAYLEIGH_CALC
      WRITE( 6, 100 ) 'Turn on tracegas debug? : ', LDEBUG_TRACEGAS_CALC
      WRITE( 6, 100 ) 'Turn on VLIDORT  debug? : ', LDEBUG_IOP_CALC

100   FORMAT( A, L5 )

      END SUBROUTINE READ_DEBUG_MENU

!------------------------------------------------------------------------------

      SUBROUTINE CHECK_DIRECTORY( DIR )

      ! References to F90 modules 
      USE ERROR_MOD,     ONLY : ERROR_STOP

! !INPUT PARAMETERS: 
!
      CHARACTER(LEN=*), INTENT(INOUT) :: DIR

      INTEGER                         :: C
      CHARACTER(LEN=255)              :: MSG
      LOGICAL                         :: IT_EXISTS

      !=================================================================
      ! CHECK_DIRECTORY begins here!
      !=================================================================

      ! Locate the last non-white-space character of NEWDIR
      C = LEN_TRIM( DIR )

      ! Add the trailing directory separator if it is not present
      IF ( DIR(C:C) /= '/' ) THEN
         DIR(C+1:C+1) = '/'
      ENDIF

      !=================================================================
      ! Test if the directory actually exists
      !=================================================================

      ! Test whether directory exists w/ F90 INQUIRE function
      INQUIRE( FILE=TRIM( DIR ), EXIST=IT_EXISTS )

      ! IFORT v9 compiler requires use of the DIRECTORY keyword to 
      ! INQUIRE for checking existence of directories.
      IF ( .not. IT_EXISTS ) THEN
         INQUIRE( DIRECTORY=TRIM( DIR ), EXIST=IT_EXISTS )
      ENDIF
      ! If the directory does not exist then stop w/ an error message
      IF ( .not. IT_EXISTS ) THEN
         MSG = 'Invalid directory: ' // TRIM( DIR )
         CALL ERROR_STOP( MSG, 'CHECK_DIRECTORY ("input_mod.f")' )
      ENDIF

      END SUBROUTINE CHECK_DIRECTORY

!------------------------------------------------------------------------------

      ! End of module
      END MODULE NAMELIST_MOD
