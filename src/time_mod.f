! $id: time_mod.f, v1.1 2012/07/23 09:02:01 xxu
      MODULE TIME_MOD
!
!******************************************************************************
! Module TIME\_MOD contains ART_RTM date and time variables
!  and timesteps, and routines for accessing them.
!
!  Module Variables:
!  ============================================================================
!
!  Module Routines
!  ============================================================================
!  ( 1) SYSTEM_TIMESTAMP()   :
!  ( 2) SYSTEM_DATE_TIME     :
!  ( 3) TIMESTAMP_STRING()   :
!  ( 4) YMD_EXTRACT          : 
!
!******************************************************************************
!
      IMPLICIT NONE

      !=================================================================
      ! MODULE PRIVATE DECLARATIONS -- keep certain internal variables 
      ! and routines from being seen outside "gas_mod.f"
      !=================================================================

      ! Make everything PRIVATE ...
      PRIVATE

      ! ... except these routines
      PUBLIC :: SYSTEM_TIMESTAMP

      ! ... and these variables

      ! Module variables
      INTEGER               :: YEAR, MONTH, DAY
      INTEGER               :: HOUR, MINUTE

      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !=================================================================
      CONTAINS

!------------------------------------------------------------------------------ 

      FUNCTION SYSTEM_TIMESTAMP() RESULT( STAMP )
!
! !RETURN VALUE:
!
      CHARACTER(LEN=16) :: STAMP
! 
! !LOCAL VARIABLES:
!
      INTEGER           :: SYS_NYMD, SYS_NHMS

      !=================================================================
      ! SYSTEM_TIMESTAMP begins here!
      !=================================================================

      ! Get system date and time
      CALL SYSTEM_DATE_TIME( SYS_NYMD, SYS_NHMS )

      ! Create a string w/ system date & time
      STAMP = TIMESTAMP_STRING( SYS_NYMD, SYS_NHMS )

      END FUNCTION SYSTEM_TIMESTAMP

!------------------------------------------------------------------------------

      SUBROUTINE SYSTEM_DATE_TIME( SYS_NYMD, SYS_NHMS )
!
!******************************************************************************
! Subroutine SYSTEM\_DATE\_TIME returns the actual local date 
!  and time (as opposed to the model date and time).
! This routine is modified frim time_mod.f of GEOS-Chem by R. Yantosca.
!  (xxu, 7/23/12)
!******************************************************************************
!
! !OUTPUT PARAMETERS:
!
      INTEGER, INTENT(OUT) :: SYS_NYMD   ! System date in YYYY/MM/DD
format
      INTEGER, INTENT(OUT) :: SYS_NHMS   ! System time in YYYY/MM/DD
format
!
! !REMARKS:
!  Uses the F90 intrinsic function DATE_AND_TIME.
!
! !LOCAL VARIABLES:
!
      ! Arguments

      ! Local variables
      INTEGER              :: V(8)
      CHARACTER(LEN=8)     :: D
      CHARACTER(LEN=10)    :: T

      !=================================================================
      ! SYSTEM_DATE_TIME begins here!
      !=================================================================

      ! Initialize
      D = 'ccyymmdd'
      T = 'hhmmss.sss'

      ! Call the F90 intrinsic routine DATE_AND_TIME
      ! Return values are (/YYYY, MM, DD, GMT_MIN, HH, MM, SS, MSEC/)
      CALL DATE_AND_TIME( DATE=D, TIME=T, VALUES=V )

      ! Save to YYYYMMDD and HHMMSS format
      SYS_NYMD = ( V(1) * 10000 ) + ( V(2) * 100 ) + V(3)
      SYS_NHMS = ( V(5) * 10000 ) + ( V(6) * 100 ) + V(7)

      END SUBROUTINE SYSTEM_DATE_TIME

!------------------------------------------------------------------------------

      FUNCTION TIMESTAMP_STRING( YYYYMMDD, HHMMSS ) RESULT( TIME_STR )
!
!******************************************************************************
! Function TIMESTAMP\_STRING returns a formatted string 
!  "YYYY/MM/DD hh:mm" for the a date and time specified by YYYYMMDD and
!  hhmmss. If YYYYMMDD and hhmmss are omitted, then TIMESTAMP\_STRING will
!  create a formatted string for the current date and time.
! This routine is modified frim time_mod.f of GEOS-Chem by R. Yantosca.
!  (xxu, 7/23/12)
!******************************************************************************
!
! !INPUT PARAMETERS: 
!
      INTEGER, INTENT(IN), OPTIONAL :: YYYYMMDD   ! YYYY/MM/DD date
      INTEGER, INTENT(IN), OPTIONAL :: HHMMSS     ! hh:mm:ss time
!
! !RETURN VALUE:
!
      CHARACTER(LEN=16)             :: TIME_STR
!
! !LOCAL VARIABLES:
!
      INTEGER :: THISYEAR, THISMONTH,  THISDAY
      INTEGER :: THISHOUR, THISMINUTE, THISSECOND

      ! If YYYYMMDD is passed, then use that date.  Otherwise use the 
      ! current date stored in global variables YEAR, MONTH, DAY.
      IF ( PRESENT( YYYYMMDD ) ) THEN
         CALL YMD_EXTRACT( YYYYMMDD, THISYEAR, THISMONTH, THISDAY )
      ELSE
         THISYEAR  = YEAR
         THISMONTH = MONTH
         THISDAY   = DAY
      ENDIF

      ! If HHMMSS is passed, then use that time.  Otherwise use the 
      ! current time stored in global variables HOUR and MINUTE.
      IF ( PRESENT( HHMMSS ) ) THEN
         CALL YMD_EXTRACT( HHMMSS, THISHOUR, THISMINUTE, THISSECOND )
      ELSE
         THISHOUR   = HOUR
         THISMINUTE = MINUTE
      ENDIF

      ! For other platforms, we can just use a FORTRAN internal write
      WRITE( TIME_STR, 100 ) THISYEAR, THISMONTH,
     &                       THISDAY,  THISHOUR, THISMINUTE

      ! Format statement
 100  FORMAT( i4.4, '/', i2.2, '/', i2.2, ' ', i2.2, ':', i2.2 )

      END FUNCTION TIMESTAMP_STRING

!------------------------------------------------------------------------------

      SUBROUTINE YMD_EXTRACT( NYMD, Y, M, D )
!
!******************************************************************************
! Subroutine YMD\_EXTRACT extracts the year, month, and date 
!  from an integer variable in YYYYMMDD format.  It can also extract the 
!  hours, minutes, and seconds from a variable in HHMMSS format.
! This routine is modified frim time_mod.f of GEOS-Chem by R. Yantosca.
!  (xxu, 7/23/12)
!*****************************************************************************
!
! !INPUT PARAMETERS: 
!
      INTEGER, INTENT(IN)  :: NYMD      ! YYYY/MM/DD format date
!
! !OUTPUT PARAMETERS:
!
      INTEGER, INTENT(OUT) :: Y, M, D   ! Separated YYYY, MM, DD values
! 
! !REVISION HISTORY: 
!  21 Nov 2001 - R. Yantosca - Initial Version
!  15 Jan 2010 - R. Yantosca - Added ProTeX headers
! !LOCAL VARIABLES:
!
      REAL*8 :: REM

      ! Extract YYYY from YYYYMMDD 
      Y = INT( DBLE( NYMD ) / 1d4 )

      ! Extract MM from YYYYMMDD
      REM = DBLE( NYMD ) - ( DBLE( Y ) * 1d4 )
      M   = INT( REM / 1d2 )

      ! Extract DD from YYYYMMDD
      REM = REM - ( DBLE( M ) * 1d2 )
      D   = INT( REM )

      ! Return to calling program
      END SUBROUTINE YMD_EXTRACT

!------------------------------------------------------------------------------

      ! End of module
      END MODULE TIME_MOD
