! $Id: surface_albedo_mod.f, v1.1 2011/01/11 14:06:32 xxu
      MODULE LAMB_ALBEDO_MOD
!
!******************************************************************************
!  Module LAMB_ALBEDO_MOD prepare the surface albedo.
!
!  Module variables:
!  ============================================================================
!  (1 ) LAMB_ALBEDO (REAL*8 )   : Lambertian surface albedo
!  
!  Module Routines
!  ============================================================================
!  (1 ) GET_LAMB_ALBEDO
!
!  Revision History:
!  (1 ) This module is modified from routine 'surface_albedo.f', for
!       including a set of surface reflectance database. (xxu, 9/12/12)
!******************************************************************************
!
      IMPLICIT NONE

      ! Make everything PRIVATE...
      PRIVATE

      ! ... except these routines
      ! ... except these routines
      PUBLIC               :: GET_LAMB_ALBEDO
      PUBLIC               :: CLEANUP_LAMB_ALBEDO

      ! ... and these variables
      PUBLIC               :: LAMB_ALBEDO

      !=================================================================
      ! Module variables
      !=================================================================
      REAL*8, ALLOCATABLE  :: LAMB_ALBEDO(:)

      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !=================================================================       
      CONTAINS

!------------------------------------------------------------------------------

      SUBROUTINE INIT_LAMB_ALBEDO

      ! Refereced to F90 modules
      USE SPECTRA_MOD,        ONLY : NLAMDA
      USE ERROR_MOD,          ONLY : ALLOC_ERR
   
      ! Local variables
      INTEGER            :: AS
      LOGICAL, SAVE      :: IS_INIT = .FALSE.

      ! Return if we have already initialized
      IF ( IS_INIT ) RETURN

      !=================================================================
      ! INIT_LAMB_ALBEDO begins here!
      !=================================================================

      ! LAMB_ALBEDO
      ALLOCATE( LAMB_ALBEDO(NLAMDA), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'LAMB_ALBEDO' )
      LAMB_ALBEDO = 0d0

      ! Reset IS_INIT so we do not allocate arrays again
      IS_INIT = .TRUE.

      ! Return to the calling routine
      RETURN

      END SUBROUTINE INIT_LAMB_ALBEDO 

!-------------------------------------------------------------------------------

      SUBROUTINE CLEANUP_LAMB_ALBEDO

      !=================================================================
      ! CLEANUP_RAYLEIGH begins here!
      !=================================================================

      ! Deallocates all module arrays
      IF ( ALLOCATED( LAMB_ALBEDO ) ) DEALLOCATE( LAMB_ALBEDO )

      END SUBROUTINE CLEANUP_LAMB_ALBEDO

!------------------------------------------------------------------------------

      SUBROUTINE GET_LAMB_ALBEDO
!
!******************************************************************************
!  Subroutine GET_LAMB_ALBEDO specify the surface reflectance used for 
!  radiative transfer calculation. User may specify the surface type, or 
!  any surface reflectance values in 'namelist.ini'. 
!  Avaiable surfece types
!     2: ocean          value = 0.01
!     3: vegeation      value = 0.05
!     4: bare soil      value = 0.1
!     5: urban          value = 0.12
!   0-1: user specify   value = 'namelist.ini'
!  
!  NOTES:
!  (1 ) The 'namelist.ini' currently dose not have options for 'surface type'. 
!       Will consider to update in future. (xxu, 1/11/11)
!******************************************************************************
!
      ! Referenced to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : SURF_REFL
      USE NAMELIST_ARRAY_MOD, ONLY : LLAMBERTIAN,   LBRDF
      USE SPECTRA_MOD,        ONLY : NLAMDA,        LAMDAS
      USE SPECTRA_MOD,        ONLY : LREAD_SPECTRA, SURFACE_SPECTRA 
      USE SAO_XSEC_MOD,       ONLY : SPLINE,        SPLINT
      USE ERROR_MOD,          ONLY : ERROR_STOP

      IMPLICIT NONE

      ! Local variables 
      INTEGER, PARAMETER     :: MAX_NSAS = 3000
      INTEGER                :: NSAS

      REAL*8                 :: WV(MAX_NSAS), SAS(MAX_NSAS)
      REAL*8                 :: Y2(MAX_NSAS)
      REAL*8                 :: SAS1
 
      INTEGER                :: I 
      CHARACTER(LEN=255)    :: ERRMSG

      ! If use BRDF surface, no necessary to prepare Lambertian spectra
      ! Return to the calling routine
      IF ( LBRDF .AND. .NOT. LLAMBERTIAN ) RETURN

      !================================================================
      ! GET_LAMB_ALBEDO begins here!
      !================================================================
    
      ! Initailization
      CALL INIT_LAMB_ALBEDO

      ! The surface albedo specified in namelist.ini
      IF ( SURF_REFL >= 0d0 .AND. SURF_REFL <= 1d0 ) THEN
         LAMB_ALBEDO = SURF_REFL
         RETURN
      ENDIF

      ! Use the build-in surface albedo spectrum (SAS)
      SELECT CASE ( INT( SURF_REFL ) )
 
      ! read from spectra.dat
      CASE ( -1 )
 
         IF ( LREAD_SPECTRA ) THEN 
            LAMB_ALBEDO(1:NLAMDA) = SURFACE_SPECTRA(1:NLAMDA,1)
         ELSE
            ERRMSG = "If SURF_REFL set to -1, must read spectra.dat"
            CALL ERROR_STOP( ERRMSG, 'lamb_albedo_mod.f' )
         ENDIF

      ! Tap water
      CASE ( 2 )
 
         NSAS = 1899
         CALL SAS_TAP_WATER(  WV(1:NSAS), SAS(1:NSAS) )

      ! Green grass
      CASE ( 3 )
 
         NSAS = 410
         CALL SAS_GREEN_GRASS(  WV(1:NSAS), SAS(1:NSAS) )

      END SELECT

      ! Now spline the data

      CALL SPLINE( WV(1:NSAS), SAS(1:NSAS), NSAS, 0d0, 0d0, Y2(1:NSAS) )

      DO I = 1, NLAMDA

         IF ( LAMDAS(I) >= WV(1) .AND. LAMDAS(I) <= WV(NSAS) ) THEN
            CALL SPLINT( WV(1:NSAS), SAS(1:NSAS), Y2(1:NSAS),
     &                   NSAS, LAMDAS(I), SAS1 )
            LAMB_ALBEDO(I) = SAS1
            IF ( LAMB_ALBEDO(I) < 0d0 ) LAMB_ALBEDO(I) = 0d0
            IF ( LAMB_ALBEDO(I) > 1d0 ) LAMB_ALBEDO(I) = 1d0
         ELSE
            LAMB_ALBEDO(I) = 0d0
         ENDIF

      ENDDO

      END SUBROUTINE GET_LAMB_ALBEDO

!------------------------------------------------------------------------------
    
      ! End of module
      END MODULE LAMB_ALBEDO_MOD
