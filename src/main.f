! $Id: main.f, v1.3 2010/11/26 12:21:47 xxu
      PROGRAM MAIN
!
!******************************************************************************
!  
!  Model name     : UNL-VRTM
!  Model version  : v1.3.0
!  Updated time   : 2013/05/01 10:44:10 CST (v1.2)
!  Updated time   : 2014/07/13 09:10:00 CST (v1.3)
!  ============================================================================
!  This is the main program of UNL-VRTM, which has the capacity of 
!  calculating
!    (1) the directional Stokes vertor at certain atmos level in both upward 
!        and downward directions.
!    (2) mie properties of aerosol with certain refractive index and size
!        distribution.
!    (3) Aerosol rayleigh scattering properties.
!    (4) Trace gases absorption based HITRAN databses.
!    (5) Jacobians of Stokes components with respect aerosol parameters 
!        including AOD, SSA, refractive index, size paramters, etc.
!    (6) Derivatives of aerosol Mie properties with respect to aerosol 
!        refractive, and size parameters.
!
!  Modules referenced by main.f
!  ============================================================================
!  (1 ) READ_NAMELIST        : namelist_mod.f
!  (2 ) INIT_ATMOS           : atmprof_mod.f
!  (3 ) LOAD_ATMOS           : atmprof_mod.f 
!  (4 ) CLEANUP_ATMOS        : atmprof_mod.f
!  (5 ) INIT_AIROPTIC        : airoptic_mod.f
!  (6 ) CLEANUP_AIROPTIC     : airoptic_mod.f
!  (7 ) PRINT_AIROPTIC       : airoptic_mod.f
!  (8 ) OUTPUT_DIAG          : diag_mod.f
!  (9 ) GET_GASTAU           : gas_mod.f 
!  (10) GET_RAYLEIGH         : rayleigh_mod.f
!  (11) GET_AEROSOL_MIE      : aerosol_mod.f
!  (12) GET_AEROSOL_OD       : aerosol_mod.f 
!  (13) ASSIGN_LPAR          : lpar_mod.f 
!  (14) RUN_VLIDORT          : vlidort_mod.f
!  (15) CLEANUP_VLIDORT      : vlidort_mod.f
!
!  REVISION HISTORY:
!  (1 ) Now the code works for low vertical resolution, and runs much 
!       quicker for this low resolution. Difference from the 49-layer 
!       simulation is found less than 0.5%. ( xxu, 10/20/11)
!  (2 ) Now the code works for new linearized mie code from Dr. Robert 
!       Spurr (version released in Oct. 2011), and can run for bi-mode
!       aerosol configurations. (xxu,11/30/11)
!  (3 ) Now the code enables bi-mode aerosol simulations. (xxu, 3/21/12)
!  (4 ) Now the code works for multi-spcetrum simulation (xxu, 7/17/12)
!  (5 ) Now the linearized T-Matrix code is implemented. (xxu, 7/23/12)
!  (6 ) Now the HITRAN calculation is fully verified. (xxu, 9/22/12)
!  
!  V1.2.0  - xxu, 5/1/13
!  (7 ) Now the BRDF is avaiable. (xxu, 5/1/13)
!
!******************************************************************************
!
      ! Refereced F90 modules
      USE AEROSOL_MOD,        ONLY : GET_AEROSOL_OPTICS
      USE AIROPTIC_MOD,       ONLY : TAURAY,        PHARAY
      USE AIROPTIC_MOD,       ONLY : TAUGAS,        TAUGAS_A
      USE AIROPTIC_MOD,       ONLY : INIT_AIROPTIC
      USE AIROPTIC_MOD,       ONLY : PRINT_AIROPTIC
      USE ATMPROF_MOD,        ONLY : LOAD_ATMOS,    INIT_ATMOS
      USE BRDF_MOD,           ONLY : INIT_VBRDF
      USE DIAG_MOD,           ONLY : OUTPUT_DIAG
      USE GAS_MOD,            ONLY : GET_GAS_OPTICS
      USE GAS_MOD,            ONLY : GAS_XABS
      USE IOP_MOD,            ONLY : INIT_IOP,      AGGREGATE_IOP
      USE LPAR_MOD,           ONLY : INIT_LPAR
      USE NAMELIST_MOD,       ONLY : READ_NAMELIST
      USE NAMELIST_ARRAY_MOD, ONLY : IATMOS,        N_BRDF
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS,      N_MOMENTS
      USE NAMELIST_ARRAY_MOD, ONLY : LAEROSOL,      LTRACEGAS
      USE NAMELIST_ARRAY_MOD, ONLY : LRAYLEIGH,     LVLIDORT
      USE NAMELIST_ARRAY_MOD, ONLY : LVLIDORT_FD,   LDIAG
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB,        LPRT
      USE NAMELIST_ARRAY_MOD, ONLY : LLAMBERTIAN,   LBRDF
      USE NAMELIST_ARRAY_MOD, ONLY : SURF_REFL,     BRDF_FACTOR
      USE RAYLEIGH_MOD,       ONLY : GET_RAYLEIGH_OPTICS
      USE RAYLEIGH_MOD,       ONLY : RAYLEIGH_XSCA,  RAYLEIGH_PHAS
      USE SPECTRA_MOD,        ONLY : GET_SPECTRA
      USE SPECTRA_MOD,        ONLY : NLAMDA,        LAMDAS
      USE SPECTRA_MOD,        ONLY : LREAD_SPECTRA, SURFACE_SPECTRA
      USE LAMB_ALBEDO_MOD,    ONLY : GET_LAMB_ALBEDO
      USE LAMB_ALBEDO_MOD,    ONLY : LAMB_ALBEDO 
      USE VLIDORT_ARRAY_MOD,  ONLY : INIT_VLIDORT_ARRAY
      USE VLIDORT_MOD,        ONLY : RUN_VLIDORT

      IMPLICIT NONE

      ! Local variables
      REAL*8                :: THIS_LAMDA
      INTEGER               :: I, J, L 
     

      !================================================================
      ! ART_VRTM begin here!
      !================================================================

      ! Read variables from namelist.ini
      CALL READ_NAMELIST 

      !================================================================
      ! Initializations....
      !================================================================  
      
      ! Initialize atmosphere profiles arrays
      CALL INIT_ATMOS

      ! Initialize the air optic arrays
      CALL INIT_AIROPTIC

      ! Initialize the linearization parameters 
      CALL INIT_LPAR

      ! Initialize the input-of-optical-property (IOP) arrays
      CALL INIT_IOP

      ! Initialize the VBRDF arrays
      CALL INIT_VBRDF

      ! Initialize the VLIDORT arrays
      CALL INIT_VLIDORT_ARRAY

      !================================================================
      ! Load atmosphere profiles
      !================================================================
      CALL LOAD_ATMOS ( IATMOS )
 
      !================================================================
      ! Calculate the spectral wavelength and wavenumber,
      !  If neccessary, read spectrum, refrative index, and surface
      !  reflectence / BRDF spectrum  from spectra.dat
      !================================================================ 
      CALL GET_SPECTRA
 
      !================================================================
      ! Get the Surface albedo spectrum for lambertian
      !================================================================
      CALL GET_LAMB_ALBEDO

      !================================================================
      ! Get the trace gas absorptions
      !================================================================
      IF ( LTRACEGAS ) THEN

         ! Get gas absorption optical depth
         CALL GET_GAS_OPTICS
         WRITE(6,'(A)') ' - MAIN: Complete preparation of trace gas'

      ENDIF 

      !================================================================
      ! Rayleigh scattering 
      !================================================================
      IF ( LRAYLEIGH ) THEN
         
         CALL GET_RAYLEIGH_OPTICS
         WRITE(6,'(A)') ' - MAIN: Complete preparation of Rayleigh'

      ENDIF 

      !================================================================
      ! Start the spectral loop (wavelength)
      !================================================================
      DO J = 1, NLAMDA

         ! wavelength value in current loop
         THIS_LAMDA = LAMDAS(J)

         ! Surface reflectance of current wavelength
         IF ( LLAMBERTIAN ) THEN 

            ! Copy lambertian albedo spectrum to current wavelength
            SURF_REFL = LAMB_ALBEDO(J)

         ELSE IF ( LBRDF .AND. LREAD_SPECTRA ) THEN 
        
            ! Get the BRDF kernel factors from spectra.dat
            BRDF_FACTOR(1:N_BRDF) = SURFACE_SPECTRA(J,1:N_BRDF)   

         ENDIF 

         ! Layer loop
         DO L = 1, N_LAYERS
 
            IF ( LTRACEGAS ) THEN

               ! Assign gas absorption optical depth for current wavelenth
               TAUGAS_A(L,:) = GAS_XABS(J,L,:) 
               TAUGAS(L)     = SUM( TAUGAS_A(L,:) )

            ENDIF

            IF ( LRAYLEIGH ) THEN

               ! Assign Rayleigh optics for current wavelenth
               TAURAY(L)     = RAYLEIGH_XSCA(J,L)
               PHARAY(L,0:2,1:6) = RAYLEIGH_PHAS(J,0:2,1:6)

               ! The First term of P11 must be unity
               PHARAY(L,0,1)  = 1d0

            ENDIF

         ENDDO ! L


         !=============================================================
         ! Aerosol scattering
         !=============================================================
         IF ( LAEROSOL ) THEN

            CALL GET_AEROSOL_OPTICS( J, THIS_LAMDA )  
            WRITE(6,'(A)') ' - MAIN: Complete preparation of Aerosol'

         ENDIF

         ! Screen print the air optics 
         CALL PRINT_AIROPTIC

         !=============================================================
         ! Make IOP for VLIDORT 
         !=============================================================
         CALL AGGREGATE_IOP
         IF ( LPRT ) WRITE(6,'(A)') ' - MAIN: Complete prepare IOP'

         !=============================================================
         ! Calculate reflectance & Jacobians by running VLIDORT
         !============================================================= 
         IF (LVLIDORT) CALL RUN_VLIDORT

         ! Finite difference verification
         !IF (LJACOB .AND. LVLIDORT_FD) CALL VLIDORT_FD_VERIFY

         !=============================================================
         ! Diagnostic outputs, a netCDF file is generated
         !=============================================================

         ! Save to netcdf
         IF (LDIAG) CALL OUTPUT_DIAG( J, THIS_LAMDA )

         ! Exits the loop if no rtm and aerosol calculation
         IF ( .NOT. LAEROSOL .AND. .NOT. LVLIDORT ) EXIT

      ENDDO ! J

      !================================================================
      ! Clean up allocated arrays
      !================================================================
      CALL CLEANUP

      ! Screen print to indicate the complete running
      WRITE(6,'(/,A)') REPEAT( '*', 79 )      
      WRITE(6, 300   ) 
      WRITE(6,'(A)'  ) REPEAT( '*', 79 )

 100  FORMAT(A,I6,1P5E11.3)
 300  FORMAT(11x,"E N D   O F   U N L - V R T M   S I M U L A T I O N")

      ! End of the main program
      END PROGRAM MAIN
