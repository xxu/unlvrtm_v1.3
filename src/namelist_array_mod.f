! $Id: namelist_array_mod.f, v1.1 2010/11/29 08:59:00 xxu
      MODULE NAMELIST_ARRAY_MOD
!
!******************************************************************************
!  Module NAMELIST_ARRAY_MOD contains all the variables that read from 
!  namelist.ini by namelist_mod.f. (xxu, 11/29/10, 11/29/10)
!
!  Module Variables
!  ============================================================================
! Contro menu:
!  (1 ) LAMDA      (REAL*8 ) : Wavelength                               [ nm ]
!  (2 ) IATMOS     (INTEGER) : Index of atmosphere type
!  (3 ) N_LAYERS   (INTEGER) : # of atmospheric layers
!  (4 ) DIR_ROOT   (CH*255 ) : Root directory 
!  (5 ) DIR_RUN    (CH*255 ) : Run directory
!  (6 ) DIR_DATA   (CH*255 ) : Data directory
!  (7 ) DIR_OUTPUT (CH*255 ) : Output directory
!  (8 ) LATMOS_M1  (LOGICAL) : Switch on/off for reduced std atmos profile M1
!  (9 ) LATMOS_M2  (LOGICAL) : Switch on/off for reduced std atmos profile M2
!  (9 ) LATMOS_M2  (LOGICAL) : Switch on/off for reduced std atmos profile M3
! Geometry menu:
!  (1 ) N_THETA0   (INTEGER) : # of solar zenith angles
!  (2 ) N_THETA    (INTEGER) : # of viewing zenith angles 
!  (3 ) N_PHI      (INTEGER) : # of relative azimuth angles
!  (4 ) N_GEOM     (INTEGER) : # of combination of geometry 
!  (5 ) THETA0     (REAL*8 ) : Solar zenith angles                    [degree]
!  (6 ) THETA      (REAL*8 ) : Veiwing zenith angles                  [degree]
!  (7 ) PHI        (REAL*8 ) : Relative azimuth angles                [degree]
! RTM menu:
!  (1 ) LVLIDORT   (LOGICAL) : Switch on/off for VLIDORT calculation
!  (2 ) N_STOKS    (INTEGER) : # of stokes components
!  (3 ) N_STRMS    (INTEGER) : # of discrete ordinate streams
!  (4 ) N_MOMENTS  (INTEGER) : # of Legendre expansion terms for Phase function
!  (5 ) SURF_REFL  (REAL*8 ) : Surface reflectance
!  (6 ) NRECEP_LEV (INTEGER) : # of User specified receptor levels
!  (7 ) RECEP_LEV  (REAL*8 ) : User specified receptor level, 1=up, -1=down
!  (8 ) RECEP_DIR  (INTEGER) : User specified receptor direction, -1=BOA, 0=TOA
!  (9 ) I_SCENARIO (INTEGER) : Index for VLIDORT single scattering correction
! Jacobian menu:
!  (1 ) LJACOB     (LOGICAL) : Switch on/off for VLIDORT Jacobian calculation
!  (2 ) LJACOB_AOD (LOGICAL) : Switch on/off for Jacobian wrt AOD
!  (3 ) LJACOB_SSA (LOGICAL) : Switch on/off for Jacobian wrt aerosol SSA
!  (3 ) LJACOB_MASS(LOGICAL) : Switch on/off for Jacobian wrt aerosol mass
!  (4 ) LJACOB_REFR(LOGICAL) : Switch on/off for Jacobian wrt refractive index
!  (5 ) LJACOB_SIZE(LOGICAL) : Switch on/off for Jacobian wrt aerosol size
!  (6 ) LJACOB_PROF(LOGICAL) : Switch on/off for Jacobian wrt aerosol profile
!  (7 ) LJACOB_USER(LOGICAL) : Switch on/off for Jacobian wrt user defined pars
!  (8 ) NONVAR_MASS(LOGICAL) : Switch on/off for non-variation of aerosol mass
!  (9 ) NONVAR_AOD (LOGICAL) : Switch on/off for non-variation of AOD
!  (10) LJACOB_SURF(LOGICAL) : Switch on/off for Jacobian wrt lambertian albedo
!  (11) LVLIDORT_FD(LOGICAL) : Switch on/off for FD verification of Jacobians
! Aerosol menu
!  (1 ) LAEROSOL   (LOGICAL) : Switch on/off for including aerosol
!  (2 ) COLUMN_AOD0(REAL*8 ) : Column aerosol optical depth
!  (3 ) HAOD       (REAL*8 ) : Aerosol scale height                     [ km ] 
!  (4 ) RMRI       (REAL*8 ) : Refractive index
!  (5 ) SIZERANGE  (REAL*8 ) : Aerosol size range                       [ um ]
!  (6 ) IDIST      (INTEGER) : Distribution index
!  (7 ) DISTPAR    (REAL*8 ) : Distribution parameters                  [ um ]
!  (8 ) L_REFER_AOD(LOGICAL) : Switch on/off for using the referenced AOD
!  (9 ) REFER_LAMDA(REAL*8 ) : The wavelength for the referenced AOD
!  (10) REFER_RMRI (REAL*8 ) : Refractive index for the referenced AOD
!  (11) REFER_SIZERANGE(R*8) : Aerosol size range for the referenced AOD [ um ]
!  (12) REFER_IDIST(INTEGER) : Distribution index for the referenced AOD
!  (13) REFER_DISTPAR( R*8 ) : Distribution parameters for the refer AOD [ um ]
!  (14) IDPROF     (INTEGER) : Aerosol vertical profile index
!  (15) PROF_RANGE (REAL*8 ) : Profile lower and upper limit [km]
!  (16) PROF_PAR   (REAL*8 ) : Profile parameters  
! Tracegas menu:
!  (1 ) LTRACEGAS  (LOGICAL) : Switch on/off for including trace gas
!  (2 ) NGAS       (INTEGER) : # of trace gases 
!  (3 ) GASID      (INTEGER) : Index for each trace gas
!  (4 ) GASNAME    (CH     ) : Trace gas name
!  (5 ) LGAS       (LOGICAL) : Gas inlcusion
!  (6 ) GASMU      (REAL*8 ) : Gas molecular weight
! Rayleigh menu:
!  (1 ) LRAYLEIGH  (LOGICAL) : Switch on/off for including Rayleigh
!  (2 ) LANISOTRO  (LOGICAL) : Switch on/off for Rayleigh depolarization factor
! Diagnostic menu:
!  (1 ) DIAG_FILENAME (CH  ) : Surface reflectance
!  (2 ) LDIAG      (LOGICAL) : Switch on/off for diagnostic
!  (3 ) LDIAG01    (LOGICAL) : Switch on/off for 
!  (4 ) LDIAG02    (LOGICAL) : Switch on/off for
!  (5 ) LDIAG03    (LOGICAL) : Switch on/off for
!  (6 ) LDIAG04    (LOGICAL) : Switch on/off for 
!  (7 ) LDIAG05    (LOGICAL) : Switch on/off for
!  (8 ) LDIAG06    (LOGICAL) : Switch on/off for
!  (9 ) LDIAG07    (LOGICAL) : Switch on/off for
!  (10) LDIAG08    (LOGICAL) : Switch on/off for
! Debug menu:
!  (1 ) LDEBUG_AOD_CALC      : Switch on/off for AOD calculation debug 
!  (2 ) LDEBUG_RAYLEIGH_CALC : Switch on/off for Rayleigh calculation debug
!  (3 ) LDEBUG_TRACEGAS_CALC : Switch on/off for tracegas calculation debug
!  (4 ) LDEBUG_MIE_CALC      : Switch on/off for debug mie code calculation
!  (5 ) LDEBUG_IOP_CALC      : Switch on/off for debug VLIDORT IOP
!  (6 ) LPRT                 : Switch on/off for screen printing
!
!  NOTES:
!  (1 ) I added one more dimension for some of the aerosol menu arrays 
!        to enable the aerosol bi-mode capacity. (xxu, 11/30/11)
!
!******************************************************************************
!

      ! Make everything PBLIC ...
      PUBLIC
  
      !================================================================ 
      ! Module Variables 
      !================================================================

      ! CONTROL MENU
      REAL*8              :: LAMDA
      REAL*8              :: LAMDAb
      REAL*8              :: LAMDAe
      INTEGER             :: NLAMDA_NMLST 
      REAL*8              :: LAMDAS_NMLST(99)
      REAL*8              :: SPECTRA_STEP
      INTEGER             :: IATMOS
      INTEGER             :: N_LAYERS
      CHARACTER(LEN=255)  :: DIR_RUN
      CHARACTER(LEN=255)  :: DIR_DATA
      CHARACTER(LEN=255)  :: DIR_OUTPUT
      LOGICAL             :: LATMOS_M1
      LOGICAL             :: LATMOS_M2
      LOGICAL             :: LATMOS_M3

      ! GEOMETRY MENU
      INTEGER             :: N_THETA0
      INTEGER             :: N_THETA
      INTEGER             :: N_PHI
      INTEGER             :: N_GEOM
      REAL*8              :: THETA0(10)
      REAL*8              :: THETA(25)
      REAL*8              :: PHI(25)
 
      ! RTM MENU
      LOGICAL             :: LVLIDORT
      INTEGER             :: N_STOKS
      INTEGER             :: N_STRMS
      INTEGER             :: N_MOMENTS
      !REAL*8              :: SURF_REFL
      INTEGER             :: NRECEP_LEV
      REAL*8              :: RECEP_LEV(1)
      INTEGER             :: RECEP_DIR
      INTEGER             :: I_SCENARIO

      ! SURFACE MENU
      LOGICAL             :: LLAMBERTIAN
      LOGICAL             :: LBRDF
      REAL*8              :: SURF_REFL
      INTEGER             :: N_BRDF
      CHARACTER(LEN=10)   :: BRDF_NAMES(3)
      INTEGER             :: BRDF_INDEX(3)
      REAL*8              :: BRDF_FACTOR(3)
      INTEGER             :: BRDF_NPARS(3)
      REAL*8              :: BRDF_PAR(3,3)      

      ! JACOBIAN MENU
      LOGICAL             :: LJACOB
      LOGICAL             :: LJACOB_GAS
      LOGICAL             :: LJACOB_AOD
      LOGICAL             :: LJACOB_SSA
      LOGICAL             :: LJACOB_MASS
      LOGICAL             :: LJACOB_REFR
      LOGICAL             :: LJACOB_EPS
      LOGICAL             :: LJACOB_SIZE
      LOGICAL             :: LJACOB_MFRC
      LOGICAL             :: LJACOB_PROF
      !LOGICAL             :: LJACOB_USER
      LOGICAL             :: NONVAR_MASS
      LOGICAL             :: NONVAR_AOD
      LOGICAL             :: LJACOB_SURF
      LOGICAL             :: LJACOB_BRDF_FACTOR
      LOGICAL             :: LJACOB_BRDF_PAR
      LOGICAL             :: LVLIDORT_FD

      ! TRACEGAS MENU
      LOGICAL             :: LTRACEGAS
      REAL*8              :: GAS_FWHM
      INTEGER             :: NGAS
      INTEGER             :: GASID(22)
      CHARACTER(LEN=6)    :: GASNAME(22)
      LOGICAL             :: LGAS(22)
      REAL*8              :: GASMU(22)
      REAL*8              :: GASSF(22)

      ! AEROSOL MENU
      LOGICAL             :: LAEROSOL
      LOGICAL             :: LAOD
      LOGICAL             :: LMASS
      REAL*8              :: COLUMN_AOD0
      REAL*8              :: COLUMN_MASS0
      REAL*8              :: MASS_F0
      INTEGER             :: NMODE
      INTEGER             :: SSMODEL(2)
      REAL*8              :: AERDEN(2)
      REAL*8              :: MODEFRC(2)
      REAL*8              :: RMRI(2,2)
      INTEGER             :: IDSHAPE(2)
      REAL*8              :: SHAPE_PAR(2)
      REAL*8              :: SIZERANGE(2,2)
      LOGICAL             :: LMONODIS(2)
      REAL*8              :: MONOR(2)
      INTEGER             :: IDIST(2)
      REAL*8              :: DISTPAR(3,2)
      LOGICAL             :: L_REFER_AOD
      REAL*8              :: REFER_LAMDA
      REAL*8              :: REFER_RMRI(2)
      REAL*8              :: REFER_SIZERANGE(2)
      LOGICAL             :: REFER_LMONODIS
      REAL*8              :: REFER_MONOR
      INTEGER             :: REFER_IDIST
      REAL*8              :: REFER_DISTPAR(3)
      INTEGER             :: IDPROF(2)
      REAL*8              :: PROF_RANGE(2,2)
      REAL*8              :: PROF_PAR(2,2)

      ! RAYLEIGH MENU
      LOGICAL             :: LRAYLEIGH
      LOGICAL             :: LANISOTRO

      ! DIAGNOSTIC MENU
      CHARACTER(LEN=255)  :: DIAG_PREFIX
      LOGICAL             :: LDIAG
      LOGICAL             :: LDIAG01
      LOGICAL             :: LDIAG02
      LOGICAL             :: LDIAG03
      LOGICAL             :: LDIAG04
      LOGICAL             :: LDIAG05
      LOGICAL             :: LDIAG06
      LOGICAL             :: LDIAG07
      LOGICAL             :: LDIAG08

      ! DEBUG MENU
      LOGICAL             :: LPRT
      LOGICAL             :: LDEBUG_AOD_CALC
      LOGICAL             :: LDEBUG_RAYLEIGH_CALC
      LOGICAL             :: LDEBUG_TRACEGAS_CALC
      LOGICAL             :: LDEBUG_MIE_CALC
      LOGICAL             :: LDEBUG_IOP_CALC

      ! End of module
      END MODULE NAMELIST_ARRAY_MOD
