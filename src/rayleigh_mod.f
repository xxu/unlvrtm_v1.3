!  $Id rayleigh_mod.f, v 1.1 2010/01/19 xxu $Exp
      MODULE RAYLEIGH_MOD
!
!******************************************************************************
!  Rayleigh optical depth & phase function
!  (xxu, 1/19/10, 07/19/12)
!  
!  Module Routines:
!  ============================================================================
!  (1 ) GET_RAYLEIGH_OPTICS: GET Rayleigh optical properties 
!  (2 ) RAYLEIGH           : Calculation for Rayleigh cross section
!  (1 ) INIT_RAYLEIGH      :
!  (  ) CLEANUP_RAYLEIGH   :
!
!  Module Variables:
!  ============================================================================
!  (1 ) RAYLEIGH_XSCA
!  (2 ) RAYLEIGH_PHAS
!  (3 ) PPCO2
!
!  NOTES:
!  (1 ) The multi-spectrum rayleigh scattering enabled. (xxu,7/19/12) 
!****************************************************************************** 
!
      IMPLICIT NONE

      ! Make everything PRIVATE ...
      PRIVATE

      ! ... except these routines
      PUBLIC :: GET_RAYLEIGH_OPTICS
      PUBLIC :: CLEANUP_RAYLEIGH

      ! ... and these variables 
      PUBLIC :: RAYLEIGH_XSCA
      PUBLIC :: RAYLEIGH_PHAS

      ! Modue Variables
      REAL*8, PARAMETER     :: PPCO2 = 3.60e-04
      REAL*8, ALLOCATABLE   :: RAYLEIGH_XSCA(:,:) 
      REAL*8, ALLOCATABLE   :: RAYLEIGH_PHAS(:,:,:)

      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !=================================================================       
      CONTAINS
! 
!------------------------------------------------------------------------------
!
      SUBROUTINE GET_RAYLEIGH_OPTICS
!     &           ( LAMDA, NUM_DENSITY, BOXH,
!     &             TAURAY, PHARAY )

      ! References to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS
      USE NAMELIST_ARRAY_MOD, ONLY : LANISOTRO
      USE ATMPROF_MOD,        ONLY : LAYINT,      BOXH
      USE SPECTRA_MOD,        ONLY : NLAMDA,      LAMDAS

      ! Arguments
      !REAL*8,  INTENT( IN)  :: LAMDA, NUM_DENSITY, BOXH
      !REAL*8,  INTENT(OUT)  :: TAURAY, PHARAY(0:2,6)

      ! Local variables
      REAL*8                :: RSIG, RRHO, DEPO, KINGF
      INTEGER               :: L, J

      ! Screen printing
      WRITE(6,'(A)') ' - GET_RAYLEIGH_OPTICS: Prepare Rayleigh Scat.'

      !================================================================
      ! GET_RAYLEIGH begins here!
      !================================================================
 
      ! Initialization
      RSIG = 0d0
      RRHO = 0d0
      DEPO = 0d0
      CALL INIT_RAYLEIGH

      
      DO J = 1, NLAMDA

         ! Calculate rayleigh cross section & depolarizatin ratio
         CALL RAYLEIGH ( LAMDAS(J)/1d3, RSIG, RRHO, KINGF )
 
         ! Rayleigh optical depth (convert # density to #/cm2)
         RAYLEIGH_XSCA(J,1:N_LAYERS) = LAYINT(1:N_LAYERS)
     &                               * BOXH(1:N_LAYERS) * 1d2 * RSIG

         ! If consider the air molecular anisotropy
         IF ( LANISOTRO ) THEN 
            DEPO = RRHO
            RAYLEIGH_XSCA(J,:) = RAYLEIGH_XSCA(J,:) * KINGF
         ENDIF

         ! Phase expansion coefficients
         RAYLEIGH_PHAS(J,0,1) = 1.d0
         RAYLEIGH_PHAS(J,2,1) = (1.-DEPO)/(2.+DEPO)
         RAYLEIGH_PHAS(J,2,5) = -SQRT(6.) * ( 1.-DEPO)/(2.+DEPO)
         RAYLEIGH_PHAS(J,2,2) = 6.*( 1.-DEPO)/(2.+DEPO)
         RAYLEIGH_PHAS(J,1,4) = 3.*(1-2.*DEPO)/(2.+DEPO)

      ENDDO

      ! Return to the calling routine
      END SUBROUTINE GET_RAYLEIGH_OPTICS

!------------------------------------------------------------------------------

      SUBROUTINE RAYLEIGH ( wl, Rsig, Rrho, F)
!
!******************************************************************************
!  Reference:
!  Bodhaine, B.A., N.B. Wood, E.G. Dutton, and J.R. Slusser, On
!  Rayleigh Optical Depth Calculations", J. Atmos. Ocean. Tech.
!  16, 1854-1861, 1999.
!  ============================================================================
!  Inputs:
!    wl: wavelength (micron)
!  Outputs:
!    Rsig: Rayleigh cross section w/ 360ppm CO2
!    Rrho: Deploarization ratio
!******************************************************************************
!      
      ! Arguments
      REAL (KIND (0d0)) :: wl
      REAL (KIND (0d0)) :: Rsig, Rrho, F

      ! Local variables
      !REAL, PARAMETER   :: co2 = 3.60e-04
      REAL, PARAMETER   :: PI = 3.1415927D0
      REAL (KIND (0d0)) :: sig, Ns, Fn2, Fo2, Icm
      REAL (KIND (0D0)) :: lam, lam2, lam4, lamI2, lamI4, n360
      REAL (KIND (0D0)) :: sig360, rho

      !================================================================
      ! RAYLEIGH begins here!
      !================================================================
 
      ! Wavelength set
      lam   = wl    
      lam2  = lam*lam
      lam4  = lam**4.
      lamI2 = 1./(lam*lam)
      lamI4 = 1./lam4

      ! molecules/cm3
      Ns = 2.546899d19

      ! wavelength in inverse cm
      Icm  = 1.0e4 / lam 

      ! refractive index with 360ppm CO2
      n360 = ( 8060.77 + 2482070./(132.274 - lamI2) + 
     &          17452.9/(39.32957 - lamI2) )*1d-8 + 1d0

      ! depolarization ratios:
      Fn2 = 1.034 + 3.17e-4*lamI2
      Fo2 = 1.096 + 1.385e-3*lamI2 + 1.448e-4*lamI4
      F = ( 78.084*Fn2 + 20.946*Fo2 + 0.934*1. + ppco2*1.15 ) / 
     &    ( 78.084 + 20.946 + 0.934 + ppco2 ) 

      ! rayleigh cross-section w/ 360ppm CO2:
      sig360 = ( ( 24. * PI**3. * Icm**4. * (n360**2. - 1.)**2. ) /
     &       ( Ns**2.* (n360**2. + 2.)**2. ) )

      ! ASYMMETRY PARAMETER:
      rho = 6.*(F - 1.) / (7.*F + 3.)
  
      !  RAYLEIGH PHASE FUNCTION:
      !  NORMALIZED TO 1:
      !  P(THETA) = (3/8*pie)*( (1 + rho) + (1 - rho)cos^2(THETA) ) / (2 + rho) 

      !  NORMALIZED TO 4*pie:
      !  P(THETA) = (3/2)*( (1 + rho) + (1 - rho)cos^2(THETA) ) / (2 + rho) 

      !  SET GLOBAL PARAMETERS:
      Rsig = sig360
      F    = F
      Rrho = rho
      !   Rrho = ( 1. - rho ) / ( 1. + rho )

      RETURN

      END SUBROUTINE RAYLEIGH

!------------------------------------------------------------------------------

      SUBROUTINE INIT_RAYLEIGH

      ! Refereced to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS
      USE SPECTRA_MOD,        ONLY : NLAMDA
      USE ERROR_MOD,          ONLY : ALLOC_ERR

      ! Local variables
      INTEGER            :: AS
      LOGICAL, SAVE      :: IS_INIT = .FALSE.

      ! Return if we have already initialized
      IF ( IS_INIT ) RETURN

      !=================================================================
      ! INIT_RAYLEIGH begins here!
      !=================================================================

      ! RAYLEIGH_XSCA
      ALLOCATE( RAYLEIGH_XSCA(NLAMDA,N_LAYERS), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'RAYLEIGH_XSCA' )
      RAYLEIGH_XSCA = 0d0
   
      ! RAYLEIGH_PHAS
      ALLOCATE( RAYLEIGH_PHAS(NLAMDA,0:2,6), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'RAYLEIGH_PHAS' )
      RAYLEIGH_PHAS = 0d0

      ! Reset IS_INIT so we do not allocate arrays again
      IS_INIT = .TRUE.

      ! Return to the calling routine
      RETURN

      END SUBROUTINE INIT_RAYLEIGH

!-------------------------------------------------------------------------------

      SUBROUTINE CLEANUP_RAYLEIGH

      !=================================================================
      ! CLEANUP_RAYLEIGH begins here!
      !=================================================================

      ! Deallocates all module arrays
      IF ( ALLOCATED( RAYLEIGH_XSCA ) ) DEALLOCATE( RAYLEIGH_XSCA )
      IF ( ALLOCATED( RAYLEIGH_PHAS ) ) DEALLOCATE( RAYLEIGH_PHAS )

      END SUBROUTINE CLEANUP_RAYLEIGH

!-------------------------------------------------------------------------------

      ! End of module
      END MODULE RAYLEIGH_MOD
