! $Id: vlidort_mod.f, 1.2 2012/07/24 10:55:00 CST 
      MODULE VLIDORT_MOD
!
!*****************************************************************************
!  Module VLIDORT_MOD contains routines to initialize and run the 
!  radiative transfer model (VLIDORT) to calculate the STOKES paras 
!  and their Jacobians with respect to aerosol optical thickness.
!  (xxu, 2/6/10, 1/25/13)
!
!  Module Variables:
!  ============================================================================
!  (1 ) IU_VLIDORT_DEBUG     : Unit number for vlidort debuging file
! 
!  Module Routines
!  ============================================================================
!  (1 ) RUN_VLIDORT          : VLIDORT driver routine
!  (2 ) GET_VLD_FBOOLEAN_IN  :
!  (3 ) GET_VLD_FCONTROL_IN  :
!  (4 ) GET_VLD_MBOOLEAN_IN  :
!  (5 ) GET_VLD_MCONTROL_IN  :
!  (6 ) GET_VLD_BEAM_IN      :
!  (7 ) GET_VLD_USER_IN      :
!  (8 ) GET_VLD_CHAPMAN_IN   :
!  (9 ) GET_VLD_WRITE_IN     :
!  (10) GET_LCONTR_AND_OPTIC :
!  (11) GET_VLD_BRDF_IN      :
!  (12) GET_VLD_LSBRDF_IN    :
!
!  Modules referenced by "vlidort_mod.f"
!  ============================================================================
!  (1 ) VLIDORT master routines
!
!  REVISION HISTORY:
!  ============================================================================
!  Revision version 1.2
!    (1 ) This module is modified for newer version of VLIDORT, v2.6
!         and the old code is now saved as vlidort2p5_mod.f
!         (xxu, 1/25/13)
!    (2 ) BRDF capacity is enabled. (xxu, 4/26/13)
!  
!  Revision version 1.1
!    (1 ) This module is created for VLIDORT (v2.3) run (xxu, 2/6/10)
!    (2 ) Add routines for VLIDORT v2.4 run (xxu, 2/23/10)
!    (3 ) Add an alternative initialization routine "VLIDORT_INIT_GROUPS" 
!         (xxu,3/1/10)
!    (4 ) This module is modified for newer version of VLIDORT (version
!         2.4RT). (xxu, 8/30/10)
!    (5 ) Now a universal expression for the linearization IOP is used.
!         (xxu, 2/11/11)
!    (6 ) Add lines to replace the edge angles for  error RTM solution.
!         Such as SZA of 0 and 90, VZA of 0 and 90, and RAZ of 0 and 180 
!         degree. (xxu, 2/11/11)
!    (7 ) I_SCENARIO cases are modified after communication with Dr. Robert
!          Spurr. (xxu, 5/14/11)
!    (8 ) Remove the RUN_VLIDORT_FD routine from this module, and make
!         that as a separate routine. (xxu, 5/17/11)
!    (9 ) Now the module arrays are moved to a separated module code 
!         'vlidort_array_mod.f' (xxu, 12/8/11)
!    (10)  This module is modified for newer version of VLIDORT (version
!         2.5). (xxu, 7/25/12)
!    (11) The I_SCENARIO cases now is rearranged; 0 for Rayleigh case, 1
!          for aerosol cases, and 3 for non-SSC aerosol case. (xxu, 9/26/12)
!******************************************************************************
!
      IMPLICIT NONE

      ! Make everything PRIVATE ...
      PRIVATE

      ! ... except these routines
      PUBLIC :: RUN_VLIDORT 

      ! ... and these variables

      !================================================================
      !  Module variables
      !================================================================
      INTEGER, PARAMETER   :: IU_VLD = 91

      !================================================================ 
      ! ROUTINES follow below the "CONTAINS" statement ...
      !================================================================
      CONTAINS

!------------------------------------------------------------------------------

      SUBROUTINE RUN_VLIDORT
!
!******************************************************************************
!  Subroutine RUN_VLIDORT call the VLIDORT model to calculate the
!  radiative radiance, polarization, as well as their Jacobian
!  sensitivty with respect to the specified aerosol parameters. 
!  (xxu, 7/24/12, 7/24/12)
!
! 
!******************************************************************************
!
      !================================================================
      ! Referenced F90 modules
      !================================================================

      ! Modules from VLIDORT (v2.6)
      USE VLIDORT_PARS
      USE VLIDORT_IO_DEFS
      USE VLIDORT_LIN_IO_DEFS
      USE VLIDORT_AUX
      USE VLIDORT_L_INPUTS
      USE VLIDORT_LCS_MASTERS, ONLY: VLIDORT_LCS_MASTER
      !USE VLIDORT_LPS_MASTERS, ONLY: VLIDORT_LPS_MASTER 

      ! Modules from VBRDF
      USE VBRDF_SUP_MOD,      ONLY : VBRDF_SUP_INPUTS 
      USE VBRDF_SUP_MOD,      ONLY : VBRDF_SUP_OUTPUTS
      USE VBRDF_LINSUP_MOD,   ONLY : VBRDF_LINSUP_INPUTS
      USE VBRDF_LINSUP_MOD,   ONLY : VBRDF_LINSUP_OUTPUTS

      ! Modules from others
      USE NAMELIST_ARRAY_MOD, ONLY : LBRDF
      USE VLIDORT_ARRAY_MOD,  ONLY : STOKES_COMPS,    JACKS_PROFLS
      USE VLIDORT_ARRAY_MOD,  ONLY : JACKS_COLUMN,    JACKS_SURF
      USE BRDF_MOD,           ONLY : RUN_VBRDF
      USE ERROR_MOD,          ONLY : ERROR_STOP
      USE LPAR_MOD,           ONLY : LPAR_NAMES,   N_LPAR_TOTAL 
      
      IMPLICIT NONE

      !================================================================
      ! Routine variables
      !================================================================

      ! Sturcture from VLIDORT
      TYPE(VLIDORT_INPUT_EXCEPTION_HANDLING) :: VLD_INSTAT
      TYPE(VLIDORT_FIXED_INPUTS            ) :: VLD_FIXIN
      TYPE(VLIDORT_MODIFIED_INPUTS         ) :: VLD_MODIN
      TYPE(VLIDORT_SUP_INOUT               ) :: VLD_SUP
      TYPE(VLIDORT_OUTPUTS                 ) :: VLD_OUT
      TYPE(VLIDORT_FIXED_LINiNPUTS         ) :: VLD_LFIXIN
      TYPE(VLIDORT_MODIFIED_LINiNPUTS      ) :: VLD_LMODIN
      TYPE(VLIDORT_LINSUP_INOUT            ) :: VLD_LSUP
      TYPE(VLIDORT_LINOUTPUTS              ) :: VLD_LOUT

      ! Sturcture from VBRDF
      TYPE ( VBRDF_SUP_INPUTS              ) :: BRDF_IN
      TYPE ( VBRDF_LINSUP_INPUTS           ) :: BRDF_LIN
      TYPE ( VBRDF_SUP_OUTPUTS             ) :: BRDF_OUT
      TYPE ( VBRDF_LINSUP_OUTPUTS          ) :: BRDF_LOUT

      ! Local vriables
      INTEGER                              :: NSTOKES, NLAYERS
      INTEGER                              :: STATUS_INPUTCHECK
      INTEGER                              :: STATUS_CALCULATION
      CHARACTER(LEN=80)                    :: MESSAGE, TRACE
      LOGICAL                              :: OPENFILEFLAG

      INTEGER                              :: K, N, DIR, O1, IWF
      INTEGER                              :: IB, UM, AZ, V, UT

      !=================================================================
      ! VLIDORT_RUN begins here!
      !=================================================================

      ! Print to screen
      WRITE( 6, '(/a )' ) REPEAT( '=', 79 )
      WRITE( 6, '( a )' ) ' RUN_VLIDORT: calculate TOA reflectance '

      !================================================================
      ! Prepare inputs for VLIDORT
      !================================================================
      CALL PREPARE_VLIORT_INPUTS( VLD_FIXIN, VLD_MODIN,
     &                            VLD_LFIXIN, VLD_LMODIN )


      !================================================================
      ! Prepare BRDF and/or SLEAVE from VSUP
      !================================================================

      ! Only prepare BRDF when set BRDF 
      IF ( LBRDF ) THEN 

         CALL PREPARE_VLIORT_SUP_INPUTS
     &           ( BRDF_IN, BRDF_LIN, VLD_SUP, VLD_LSUP )

         ! Check compatibility of VBRDF and Main VLIDORT inputs
         CALL VLIDORT_VBRDF_INPUT_CHECKER
     &           ( BRDF_IN,  VLD_FIXIN, VLD_MODIN, 
     &             VLD_OUT%STATUS ) 

         ! Check the running status  
         !mick fix - added "OPENFILEFLAG = .false."
         OPENFILEFLAG = .FALSE.
         CALL VLIDORT_WRITE_STATUS
     &           ( 'VLIDORT_BRDF_Check.log',
     &             36, OPENFILEFLAG,
     &             VLD_OUT%STATUS )

         ! ERROR handling
         IF ( VLD_OUT%STATUS%TS_STATUS_INPUTCHECK == VLIDORT_SERIOUS ) 
     &   THEN

            MESSAGE  = 'Check in VLIDORT_VBRDF_INPUT_CHECKER failed'
            CALL ERROR_STOP( MESSAGE, 'vlidort_mod.f' )

         ENDIF

      ENDIF ! LBRDF 

      !================================================================
      ! all inputs are prepared, start to run VLIDORT
      !================================================================

      ! call VLIDORT master routine
      CALL VLIDORT_LCS_MASTER 
     &        ( VLD_FIXIN,
     &          VLD_MODIN,
     &          VLD_SUP,
     &          VLD_OUT,
     &          VLD_LFIXIN,
     &          VLD_LMODIN,
     &          VLD_LSUP,
     &          VLD_LOUT )
   
      WRITE(6,'(a)') '  VLIDORT runs successfully'

      ! Check the running status  
      !mick fix - added "OPENFILEFLAG = .false."
      OPENFILEFLAG = .FALSE.
      CALL VLIDORT_WRITE_STATUS
     &        ( 'VLIDORT_Execution.log', 
     &          35, OPENFILEFLAG,
     &          VLD_OUT%STATUS )

      ! ERROR handling
      IF ( VLD_OUT%STATUS%TS_STATUS_INPUTCHECK == VLIDORT_SERIOUS ) THEN

         MESSAGE  = ' inputcheck in VLIDORT_LCS_MASTER failed'
         CALL ERROR_STOP( MESSAGE, 'vlidort_mod.f' )  
      
      ENDIF

      IF ( VLD_OUT%STATUS%TS_STATUS_CALCULATION== VLIDORT_SERIOUS ) THEN

         MESSAGE = ' calculation in LIDORT_LCS_MASTER failed'
         CALL ERROR_STOP( MESSAGE, 'vlidort_mod.f' )

      ENDIF

      !================================================================
      ! Store the model output into Module Variables
      !================================================================

      ! The light direction:
      IF ( VLD_FIXIN%BOOL%TS_DO_UPWELLING ) THEN
         DIR = 1
      ELSE IF ( VLD_FIXIN%BOOL%TS_DO_DNWELLING ) THEN
         DIR = 2
      ENDIF

      NSTOKES = VLD_FIXIN%CONT%TS_NSTOKES
      NLAYERS = VLD_FIXIN%CONT%TS_NLAYERS 
     

      ! Store model outputs
      DO O1 = 1, NSTOKES
      DO V  = 1, VLD_OUT%MAIN%TS_N_GEOMETRIES
      DO UT = 1, VLD_FIXIN%USERVAL%TS_N_USER_LEVELS

         ! Stokes components
         STOKES_COMPS(UT,V,O1) = VLD_OUT%MAIN%TS_STOKES(UT,V,O1,DIR)

         ! Atmosphere profile weighting function
         IF ( VLD_LMODIN%MCONT%TS_DO_PROFILE_LINEARIZATION ) THEN
            DO IWF = 1, VLD_LFIXIN%CONT%TS_N_TOTALPROFILE_WFS
               DO N   = 1, NLAYERS
                  JACKS_PROFLS(IWF,N,UT,V,O1) = 
     &               VLD_LOUT%PROF%TS_PROFILEWF(IWF,N,UT,V,O1,DIR)
               ENDDO
               JACKS_COLUMN(IWF,UT,V,O1)
     &                       = SUM(JACKS_PROFLS(IWF,1:NLAYERS,UT,V,O1))
            ENDDO
         ENDIF

         ! Atmosphere column weighting function
         IF ( VLD_LMODIN%MCONT%TS_DO_COLUMN_LINEARIZATION ) THEN
            DO IWF = 1, VLD_LFIXIN%CONT%TS_N_TOTALCOLUMN_WFS
               JACKS_COLUMN(IWF,UT,V,O1) = 
     &             VLD_LOUT%COL%TS_COLUMNWF(IWF,UT,V,O1,DIR)
            ENDDO
         ENDIF

         ! Surface Jacobian
         IF ( VLD_LMODIN%MCONT%TS_DO_SURFACE_LINEARIZATION ) THEN
            DO IWF = 1, VLD_LFIXIN%CONT%TS_N_SURFACE_WFS
                JACKS_SURF(IWF,UT,V,O1) = 
     &              VLD_LOUT%SURF%TS_SURFACEWF(IWF,UT,V,O1,DIR)
            ENDDO
         ENDIF

      ENDDO
      ENDDO
      ENDDO

      ! Derive extra Jacobians, i.e. wrt modal fractions
      CALL MODEFRC_JACOBIAN

      !================================================================
      ! Print to screen
      !================================================================
    
      ! Print Stokes components
      WRITE(6,'(A)') ' -LOUT, SZA, VZA, RAZ, Stokes components:'
      DO UT = 1, VLD_FIXIN%USERVAL%TS_N_USER_LEVELS
      DO IB = 1, VLD_MODIN%MSUNRAYS%TS_N_SZANGLES
      DO UM = 1, VLD_FIXIN%USERVAL%TS_N_USER_VZANGLES
      DO AZ = 1, VLD_MODIN%MUSERVAL%TS_N_USER_RELAZMS
         V  = VLD_OUT%MAIN%TS_VZA_OFFSETS(IB, UM) + AZ
         WRITE(6,100) UT,
     &                VLD_MODIN%MSUNRAYS%TS_SZANGLES(IB),
     &                VLD_MODIN%MUSERVAL%TS_USER_VZANGLES_INPUT(UM),
     &                VLD_MODIN%MUSERVAL%TS_USER_RELAZMS(AZ),
     &                STOKES_COMPS(UT,V,1:NSTOKES)
      ENDDO ! AZ
      ENDDO ! UM
      ENDDO ! IB
      ENDDO ! UT

      ! Print column jacobian
      IF ( VLD_LMODIN%MCONT%TS_DO_PROFILE_LINEARIZATION .OR. 
     &     VLD_LMODIN%MCONT%TS_DO_COLUMN_LINEARIZATION ) THEN
      WRITE(6,'(A)') ' -IWF, LPAR_NAME, LOUT, SZA, VZA, RAZ, ' //
     &               'Column Jacobian:'
      DO IWF= 1, N_LPAR_TOTAL
      DO UT = 1, VLD_FIXIN%USERVAL%TS_N_USER_LEVELS
      DO IB = 1, VLD_MODIN%MSUNRAYS%TS_N_SZANGLES
      DO UM = 1, VLD_FIXIN%USERVAL%TS_N_USER_VZANGLES
      DO AZ = 1, VLD_MODIN%MUSERVAL%TS_N_USER_RELAZMS
         V  = VLD_OUT%MAIN%TS_VZA_OFFSETS(IB, UM) + AZ
         WRITE(6,110) IWF, LPAR_NAMES(IWF), UT, 
     &                VLD_MODIN%MSUNRAYS%TS_SZANGLES(IB),
     &                VLD_MODIN%MUSERVAL%TS_USER_VZANGLES_INPUT(UM),
     &                VLD_MODIN%MUSERVAL%TS_USER_RELAZMS(AZ),
     &                JACKS_COLUMN(IWF,UT,V,1:NSTOKES) 
      ENDDO ! AZ
      ENDDO ! UM
      ENDDO ! IB
      ENDDO ! UT
      ENDDO ! IWF
      ENDIF

      IF ( VLD_LMODIN%MCONT%TS_DO_SURFACE_LINEARIZATION )THEN
      WRITE(6,'(A)') ' -ISFS, SZA, VZA, RAZ, SULFACE Jacobian:'
      DO IWF = 1, VLD_LFIXIN%CONT%TS_N_SURFACE_WFS
      DO UT = 1, VLD_FIXIN%USERVAL%TS_N_USER_LEVELS
      DO IB = 1, VLD_MODIN%MSUNRAYS%TS_N_SZANGLES
      DO UM = 1, VLD_FIXIN%USERVAL%TS_N_USER_VZANGLES
      DO AZ = 1, VLD_MODIN%MUSERVAL%TS_N_USER_RELAZMS
         V  = VLD_OUT%MAIN%TS_VZA_OFFSETS(IB, UM) + AZ
         WRITE(6,120) IWF,
     &                VLD_MODIN%MSUNRAYS%TS_SZANGLES(IB),
     &                VLD_MODIN%MUSERVAL%TS_USER_VZANGLES_INPUT(UM),
     &                VLD_MODIN%MUSERVAL%TS_USER_RELAZMS(AZ),
     &                JACKS_SURF(IWF,UT,V,1:NSTOKES) 
      ENDDO ! AZ
      ENDDO ! UM
      ENDDO ! IB
      ENDDO ! UT
      ENDDO ! IWF
      ENDIF

 100  FORMAT(I4,  3F7.2, 1P4E15.7)
 110  FORMAT(I4, 1X, A12, I2, 3F7.2, 1P4E12.3)
 120  FORMAT(I4, 3F7.2, 1P4E12.3)

      ! Return to calling program
      RETURN

      END SUBROUTINE RUN_VLIDORT

!------------------------------------------------------------------------------

      SUBROUTINE PREPARE_VLIORT_INPUTS( VLD_FIXIN, VLD_MODIN,
     &                                  VLD_LFIXIN, VLD_LMODIN )

      ! Reference to F90 modules
      USE VLIDORT_IO_DEFS
      USE VLIDORT_LIN_IO_DEFS 
      USE NAMELIST_ARRAY_MOD,  ONLY : LDEBUG_IOP_CALC

      ! Arguments
      TYPE(VLIDORT_FIXED_INPUTS            ) :: VLD_FIXIN
      TYPE(VLIDORT_MODIFIED_INPUTS         ) :: VLD_MODIN
      TYPE(VLIDORT_FIXED_LINiNPUTS         ) :: VLD_LFIXIN
      TYPE(VLIDORT_MODIFIED_LINiNPUTS      ) :: VLD_LMODIN

      ! Local stuctures and variables
      TYPE(VLIDORT_FIXED_BOOlEAN      ) :: VLD_FBOOLEAN_IN
      TYPE(VLIDORT_MODIFIED_BOOLEAN   ) :: VLD_MBOOLEAN_IN
      TYPE(VLIDORT_FIXED_CONTROL      ) :: VLD_FCONTROL_IN
      TYPE(VLIDORT_MODIFIED_CONTROL   ) :: VLD_MCONTROL_IN
      TYPE(VLIDORT_FIXED_SUNRAYS      ) :: VLD_FSUNRAYS_IN
      TYPE(VLIDORT_FIXED_USERVALUES   ) :: VLD_FUSERVAL_IN
      TYPE(VLIDORT_MODIFIED_SUNRAYS   ) :: VLD_MSUNRAYS_IN
      TYPE(VLIDORT_MODIFIED_USERVALUES) :: VLD_MUSERVAL_IN
      TYPE(VLIDORT_FIXED_CHAPMAN      ) :: VLD_FCHAPMAN_IN
      TYPE(VLIDORT_MODIFIED_CHAPMAN   ) :: VLD_MCHAPMAN_IN
      TYPE(VLIDORT_FIXED_WRITE        ) :: VLD_WRITE_IN
      TYPE(VLIDORT_FIXED_LINCONTROL   ) :: VLD_FLINCONTROL_IN
      TYPE(VLIDORT_MODIFIED_LINCONTROL) :: VLD_MLINCONTROL_IN
      TYPE(VLIDORT_FIXED_OPTICAL      ) :: VLD_FOPTICAL_IN
      TYPE(VLIDORT_MODIFIED_OPTICAL   ) :: VLD_MOPTICAL_IN
      TYPE(VLIDORT_FIXED_LINOPTICAL   ) :: VLD_LINOPTICAL_IN

      INTEGER                :: IU
      INTEGER                :: I, L, N, K

      !================================================================
      ! Prepare inputs for VLIDORT
      !================================================================

      ! Get fixed boolean variables
      CALL GET_VLD_FBOOLEAN_IN( VLD_FBOOLEAN_IN )

      ! Get modified boolean variables
      CALL GET_VLD_MBOOLEAN_IN( VLD_MBOOLEAN_IN )

      ! Get fixed and modified control variables
      CALL GET_VLD_CONTROL_IN( VLD_FCONTROL_IN, VLD_MCONTROL_IN )

      ! Get geometry inputs
      CALL GET_VLD_GEOMETRY_IN( VLD_FSUNRAYS_IN, VLD_FUSERVAL_IN,
     &                          VLD_MSUNRAYS_IN, VLD_MUSERVAL_IN )

      ! Get champan function related inputs
      CALL GET_VLD_CHAPMAN_IN( VLD_FCHAPMAN_IN, VLD_MCHAPMAN_IN )

      ! Get vlidort write stucture
      CALL GET_VLD_WRITE_IN( VLD_WRITE_IN )

      ! Get optical properties and related logical variables 
      CALL GET_LCONTR_AND_OPTIC
     &          ( VLD_FLINCONTROL_IN, VLD_MLINCONTROL_IN,
     &            VLD_FOPTICAL_IN, VLD_MOPTICAL_IN, 
     &            VLD_LINOPTICAL_IN )

      ! Mow aggregate those obtained stuctures to the stuctures of
      ! sturcture
      
      ! Stucture: VLIDORT_Fixed_Inputs
      VLD_FIXIN%BOOL    = VLD_FBOOLEAN_IN
      VLD_FIXIN%CONT    = VLD_FCONTROL_IN
      VLD_FIXIN%SUNRAYS = VLD_FSUNRAYS_IN
      VLD_FIXIN%USERVAL = VLD_FUSERVAL_IN
      VLD_FIXIN%CHAPMAN = VLD_FCHAPMAN_IN
      VLD_FIXIN%OPTICAL = VLD_FOPTICAL_IN
      VLD_FIXIN%WRITE   = VLD_WRITE_IN

      ! Stucture: VLIDORT_Modified_Inputs
      VLD_MODIN%MBOOL    = VLD_MBOOLEAN_IN
      VLD_MODIN%MCONT    = VLD_MCONTROL_IN
      VLD_MODIN%MSUNRAYS = VLD_MSUNRAYS_IN
      VLD_MODIN%MUSERVAL = VLD_MUSERVAL_IN
      VLD_MODIN%MCHAPMAN = VLD_MCHAPMAN_IN
      VLD_MODIN%MOPTICAL = VLD_MOPTICAL_IN

      ! Stucture: VLIDORT_Fixed_LinInputs
      VLD_LFIXIN%CONT    = VLD_FLINCONTROL_IN
      VLD_LFIXIN%OPTICAL = VLD_LINOPTICAL_IN

      ! Stucture: VLIDORT_Fixed_LinInputs
      VLD_LMODIN%MCONT   = VLD_MLINCONTROL_IN

      ! Return if no debugging write
      IF ( .NOT. LDEBUG_IOP_CALC ) RETURN

      !================================================================
      ! Save the VLIDORT INPUTS for DEBUG
      !================================================================
      ! open file
      IU = IU_VLD
      OPEN(IU, FILE='vlidort_iop.debug', STATUS='UNKNOWN')
      WRITE(IU,'(/A)') REPEAT( '=', 79 )
      WRITE(IU,'(A)') '==> VLIDORT_FIXED_BOOlEAN :'
      WRITE(IU,101) VLD_FIXIN%BOOL%TS_DO_FULLRAD_MODE
      WRITE(IU,102) VLD_FIXIN%BOOL%TS_DO_SSCORR_TRUNCATION
      WRITE(IU,103) VLD_FIXIN%BOOL%TS_DO_THERMAL_EMISSION
      WRITE(IU,104) VLD_FIXIN%BOOL%TS_DO_SURFACE_EMISSION
      WRITE(IU,105) VLD_FIXIN%BOOL%TS_DO_PLANE_PARALLEL
      WRITE(IU,106) VLD_FIXIN%BOOL%TS_DO_SSFULL
      WRITE(IU,107) VLD_FIXIN%BOOL%TS_DO_UPWELLING
      WRITE(IU,108) VLD_FIXIN%BOOL%TS_DO_DNWELLING
      WRITE(IU,109) VLD_FIXIN%BOOL%TS_DO_QUAD_OUTPUT
      WRITE(IU,110) VLD_FIXIN%BOOL%TS_DO_TOA_CONTRIBS
      WRITE(IU,111) VLD_FIXIN%BOOL%TS_DO_LAMBERTIAN_SURFACE
      WRITE(IU,112) VLD_FIXIN%BOOL%TS_DO_SPECIALIST_OPTION_1
      WRITE(IU,113) VLD_FIXIN%BOOL%TS_DO_SPECIALIST_OPTION_2
      WRITE(IU,114) VLD_FIXIN%BOOL%TS_DO_SPECIALIST_OPTION_3
      WRITE(IU,115) VLD_FIXIN%BOOL%TS_DO_SS_EXTERNAL
      WRITE(IU,116) VLD_FIXIN%BOOL%TS_DO_SURFACE_LEAVING
      WRITE(IU,117) VLD_FIXIN%BOOL%TS_DO_SL_ISOTROPIC

      ! Formats
 101  FORMAT( 'DO_FULLRAD_MODE          :', L5 )
 102  FORMAT( 'DO_SSCORR_TRUNCATION     :', L5 )
 103  FORMAT( 'DO_THERMAL_EMISSION      :', L5 )
 104  FORMAT( 'DO_SURFACE_EMISSION      :', L5 )
 105  FORMAT( 'DO_PLANE_PARALLEL        :', L5 )
 106  FORMAT( 'DO_SSFULL                :', L5 )
 107  FORMAT( 'DO_UPWELLING             :', L5 )
 108  FORMAT( 'DO_DNWELLING             :', L5 )
 109  FORMAT( 'DO_QUAD_OUTPUT           :', L5 )
 110  FORMAT( 'DO_TOA_CONTRIBS          :', L5 )
 111  FORMAT( 'DO_LAMBERTIAN_SURFACE    :', L5 )
 112  FORMAT( 'DO_SPECIALIST_OPTION_1   :', L5 )
 113  FORMAT( 'DO_SPECIALIST_OPTION_2   :', L5 )
 114  FORMAT( 'DO_SPECIALIST_OPTION_3   :', L5 )
 115  FORMAT( 'DO_SS_EXTERNAL           :', L5 )
 116  FORMAT( 'DO_SURFACE_LEAVING       :', L5 )
 117  FORMAT( 'DO_SL_ISOTROPIC          :', L5 )

      WRITE(IU,'(/A)') REPEAT( '=', 79 )
      WRITE(IU,'(A)') '==> VLIDORT_MODIFIED_BOOLEAN :'
      WRITE(IU,121) VLD_MODIN%MBOOL%TS_DO_SSCORR_NADIR
      WRITE(IU,122) VLD_MODIN%MBOOL%TS_DO_SSCORR_OUTGOING
      WRITE(IU,123) VLD_MODIN%MBOOL%TS_DO_DOUBLE_CONVTEST
      WRITE(IU,124) VLD_MODIN%MBOOL%TS_DO_SOLAR_SOURCES
      WRITE(IU,125) VLD_MODIN%MBOOL%TS_DO_REFRACTIVE_GEOMETRY
      WRITE(IU,126) VLD_MODIN%MBOOL%TS_DO_CHAPMAN_FUNCTION
      WRITE(IU,127) VLD_MODIN%MBOOL%TS_DO_RAYLEIGH_ONLY
      WRITE(IU,128) VLD_MODIN%MBOOL%TS_DO_DELTAM_SCALING
      WRITE(IU,129) VLD_MODIN%MBOOL%TS_DO_SOLUTION_SAVING
      WRITE(IU,130) VLD_MODIN%MBOOL%TS_DO_BVP_TELESCOPING
      WRITE(IU,131) VLD_MODIN%MBOOL%TS_DO_USER_VZANGLES
      WRITE(IU,132) VLD_MODIN%MBOOL%TS_DO_ADDITIONAL_MVOUT
      WRITE(IU,133) VLD_MODIN%MBOOL%TS_DO_MVOUT_ONLY
      WRITE(IU,134) VLD_MODIN%MBOOL%TS_DO_THERMAL_TRANSONLY

 121  FORMAT('DO_SSCORR_NADIR          :', L5 ) 
 122  FORMAT('DO_SSCORR_OUTGOING       :', L5 )
 123  FORMAT('DO_DOUBLE_CONVTEST       :', L5 )
 124  FORMAT('DO_SOLAR_SOURCES         :', L5 )
 125  FORMAT('DO_REFRACTIVE_GEOMETRY   :', L5 )
 126  FORMAT('DO_CHAPMAN_FUNCTION      :', L5 )
 127  FORMAT('DO_RAYLEIGH_ONLY         :', L5 )
 128  FORMAT('DO_DELTAM_SCALING        :', L5 )
 129  FORMAT('DO_SOLUTION_SAVING       :', L5 )
 130  FORMAT('DO_BVP_TELESCOPING       :', L5 )
 131  FORMAT('DO_USER_VZANGLES         :', L5 )
 132  FORMAT('DO_ADDITIONAL_MVOUT      :', L5 )
 133  FORMAT('DO_MVOUT_ONLY            :', L5 )
 134  FORMAT('DO_THERMAL_TRANSONLY     :', L5 )

      WRITE(IU,'(/A)') REPEAT( '=', 79 )
      WRITE(IU,'(A)') '==> VLIDORT_FIXED_CONTROL '
     &                // ' & VLIDORT_MODIFIED_CONTROL:'
      WRITE(IU,141) VLD_FIXIN%CONT%TS_NSTOKES
      WRITE(IU,142) VLD_FIXIN%CONT%TS_NSTREAMS
      WRITE(IU,143) VLD_FIXIN%CONT%TS_NLAYERS
      WRITE(IU,144) VLD_FIXIN%CONT%TS_NFINELAYERS
      WRITE(IU,145) VLD_FIXIN%CONT%TS_N_THERMAL_COEFFS
      WRITE(IU,146) VLD_FIXIN%CONT%TS_VLIDORT_ACCURACY
      WRITE(IU,147) VLD_FIXIN%CONT%TS_NLAYERS_NOMS
      WRITE(IU,148) VLD_FIXIN%CONT%TS_NLAYERS_CUTOFF
      WRITE(IU,149) VLD_MODIN%MCONT%TS_NGREEK_MOMENTS_INPUT

 141  FORMAT('NSTOKES                   :', I5 )
 142  FORMAT('NSTREAMS                  :', I5 )
 143  FORMAT('NLAYERS                   :', I5 )
 144  FORMAT('NFINELAYERS               :', I5 )
 145  FORMAT('N_THERMAL_COEFFS          :', I5 )
 146  FORMAT('VLIDORT_ACCURACY          :', 1PD12.3 )
 147  FORMAT('NLAYERS_NOMS              :', I5 )
 148  FORMAT('NLAYERS_CUTOFF            :', I5 )
 149  FORMAT('NGREEK_MOMENTS_INPUT      :', I5 )

      WRITE(IU,'(/A)') REPEAT( '=', 79 )
      WRITE(IU,'(A)') '==> VLIDORT_FIXED_SUNRAYS'
     &              // 'VLIDORT_MODIFIED_SUNRAYS:'
      WRITE(IU,151) VLD_FIXIN%SUNRAYS%TS_FLUX_FACTOR
      WRITE(IU,152) VLD_MODIN%MSUNRAYS%TS_N_SZANGLES
      DO I = 1, VLD_MODIN%MSUNRAYS%TS_N_SZANGLES
         WRITE(IU,153) VLD_MODIN%MSUNRAYS%TS_SZANGLES(I), I
      ENDDO

 151  FORMAT('TS_FLUX_FACTOR            :', 1PD12.3 )
 152  FORMAT('N_SZANGLES                :', I5 )
 153  FORMAT('SZANGLES(I) =' F10.3, 4X, 'as I =', I5 )

      WRITE(IU,'(/A)') REPEAT( '=', 79 )
      WRITE(IU,'(A)') '==> VLIDORT_FIXED_USERVAL ' 
     &               // ' & VLIDORT_MODIFIED_USERVAL:' 
      WRITE(IU,161) VLD_FIXIN%USERVAL%TS_N_USER_VZANGLES
      DO I = 1, VLD_FIXIN%USERVAL%TS_N_USER_VZANGLES
         WRITE(IU,162) VLD_MODIN%MUSERVAL%TS_USER_VZANGLES_INPUT(I), I
      ENDDO
      WRITE(IU,163) VLD_MODIN%MUSERVAL%TS_N_USER_RELAZMS
      DO I = 1, VLD_MODIN%MUSERVAL%TS_N_USER_RELAZMS
         WRITE(IU,164) VLD_MODIN%MUSERVAL%TS_USER_RELAZMS(I), I
      ENDDO
      WRITE(IU,165) VLD_FIXIN%USERVAL%TS_N_USER_LEVELS
      DO I = 1, VLD_FIXIN%USERVAL%TS_N_USER_LEVELS
         WRITE(IU,166) VLD_MODIN%MUSERVAL%TS_USER_LEVELS(I), I
      ENDDO 
      WRITE(IU,167) VLD_MODIN%MUSERVAL%TS_GEOMETRY_SPECHEIGHT
 161  FORMAT('N_USER_VZANGLES           :', I5 )
 162  FORMAT('USER_VZANGLES(I) =', F10.3, 4X, 'as I =', I5 )
 163  FORMAT('N_USER_RELAZMS            :', I5 )
 164  FORMAT('USER_RELAZMS(I)  =', F10.3, 4X, 'as I =', I5 )
 165  FORMAT('N_USER_LEVELS             :', I5 )    
 166  FORMAT('USER_LEVELS(I)   =', F10.3, 4X, 'as I =', I5 )
 167  FORMAT('GEOMETRY_SPECHEIGHT       :', 1PD12.3 )

      WRITE(IU,'(/A)') REPEAT( '=', 79 )
      WRITE(IU,'(A)') '==> VLIDORT_FIXED_WRITE:'
      WRITE(IU,171) VLD_FIXIN%WRITE%TS_DO_DEBUG_WRITE
      WRITE(IU,172) VLD_FIXIN%WRITE%TS_DO_WRITE_INPUT
      WRITE(IU,173) TRIM(VLD_FIXIN%WRITE%TS_INPUT_WRITE_FILENAME)
      WRITE(IU,174) VLD_FIXIN%WRITE%TS_DO_WRITE_SCENARIO
      WRITE(IU,175) TRIM(VLD_FIXIN%WRITE%TS_SCENARIO_WRITE_FILENAME)
      WRITE(IU,176) VLD_FIXIN%WRITE%TS_DO_WRITE_FOURIER
      WRITE(IU,177) TRIM(VLD_FIXIN%WRITE%TS_FOURIER_WRITE_FILENAME)
      WRITE(IU,178) VLD_FIXIN%WRITE%TS_DO_WRITE_RESULTS
      WRITE(IU,179) TRIM(VLD_FIXIN%WRITE%TS_RESULTS_WRITE_FILENAME)
 171  FORMAT('DO_DEBUG_WRITE            :', L5 )
 172  FORMAT('DO_WRITE_INPUT            :', L5 )
 173  FORMAT('INPUT_WRITE_FILENAME      :', A  )
 174  FORMAT('DO_WRITE_SCENARIO         :', L5 )
 175  FORMAT('SCENARIO_WRITE_FILENAME   :', A  )
 176  FORMAT('DO_WRITE_FOURIER          :', L5 )
 177  FORMAT('FOURIER_WRITE_FILENAME    :', A  )
 178  FORMAT('DO_WRITE_RESULTS          :', L5 )
 179  FORMAT('RESULTS_WRITE_FILENAME    :', A  )

      WRITE(IU,'(/A)') REPEAT( '=', 79 )
      WRITE(IU,'(A)') '==> VLIDORT_FIXED_CHAPMAN'
     &              //' & VLIDORT_MODIFIED_CHAPMAN:'
      WRITE(IU,181) VLD_MODIN%MCHAPMAN%TS_EARTH_RADIUS
      WRITE(IU,182) VLD_FIXIN%CHAPMAN%TS_RFINDEX_PARAMETER
      WRITE(IU,183) 
      WRITE(IU,184) 0, VLD_FIXIN%CHAPMAN%TS_HEIGHT_GRID(0),
     &                    VLD_FIXIN%CHAPMAN%TS_PRESSURE_GRID(0),
     &                    VLD_FIXIN%CHAPMAN%TS_TEMPERATURE_GRID(0)
      DO L = 1, VLD_FIXIN%CONT%TS_NLAYERS
         WRITE(IU,184) L, VLD_FIXIN%CHAPMAN%TS_HEIGHT_GRID(L), 
     &                    VLD_FIXIN%CHAPMAN%TS_PRESSURE_GRID(L),
     &                    VLD_FIXIN%CHAPMAN%TS_TEMPERATURE_GRID(L), 
     &                    VLD_FIXIN%CHAPMAN%TS_FINEGRID(L)
      ENDDO

 181  FORMAT('EARTH_RADIUS              :', 1PD12.3 )
 182  FORMAT('RFINDEX_PARAMETER         :', 1PD12.3 ) 
 183  FORMAT(' Lev |     H     |     P     |     T     | #Finegrid') 
 184  FORMAT(1X, I4, 1P3E12.3, I6)

      WRITE(IU,'(/A)') REPEAT( '=', 79 )
      WRITE(IU,'(A)') '==> VLIDORT_FIXED_LINCONTROL'
     &                //' & VLIDORT_MODIFIED_LINCONTROL:'
      WRITE(IU,191) VLD_LFIXIN%CONT%TS_DO_SIMULATION_ONLY
      WRITE(IU,192) VLD_LFIXIN%CONT%TS_DO_SURFBB_LINEARIZATION
      WRITE(IU,193) VLD_LFIXIN%CONT%TS_N_TOTALCOLUMN_WFS
      WRITE(IU,194) VLD_LFIXIN%CONT%TS_N_TOTALPROFILE_WFS
      WRITE(IU,195) VLD_LFIXIN%CONT%TS_N_SURFACE_WFS
      WRITE(IU,196) VLD_LFIXIN%CONT%TS_N_SLEAVE_WFS
      WRITE(IU,197) VLD_LFIXIN%CONT%TS_DO_LTE_LINEARIZATION
      WRITE(IU,198) VLD_LMODIN%MCONT%TS_DO_COLUMN_LINEARIZATION
      WRITE(IU,199) VLD_LMODIN%MCONT%TS_DO_PROFILE_LINEARIZATION
      WRITE(IU,200) VLD_LMODIN%MCONT%TS_DO_ATMOS_LINEARIZATION
      WRITE(IU,201) VLD_LMODIN%MCONT%TS_DO_SURFACE_LINEARIZATION
      WRITE(IU,202) VLD_LMODIN%MCONT%TS_DO_LINEARIZATION
      WRITE(IU,203) VLD_LMODIN%MCONT%TS_DO_SLEAVE_WFS
 191  FORMAT('DO_SIMULATION_ONLY        :', L5 )
 192  FORMAT('DO_SURFBB_LINEARIZATION   :', L5 )
 193  FORMAT('N_TOTALCOLUMN_WFS         :', I5 )
 194  FORMAT('N_TOTALPROFILE_WFS        :', I5 )
 195  FORMAT('N_SURFACE_WFS             :', I5 )
 196  FORMAT('N_SLEAVE_WFS              :', I5 )
 197  FORMAT('DO_LTE_LINEARIZATION      :', L5 )
 198  FORMAT('DO_COLUMN_LINEARIZATION   :', L5 )
 199  FORMAT('DO_PROFILE_LINEARIZATION  :', L5 )
 200  FORMAT('DO_ATMOS_LINEARIZATION    :', L5 )
 201  FORMAT('DO_SURFACE_LINEARIZATION  :', L5 )
 202  FORMAT('DO_LINEARIZATION          :', L5 )
 203  FORMAT('DO_SLEAVE_WFS             :', L5 )

      WRITE(IU,'(/A)') REPEAT( '=', 79 )
      WRITE(IU,'(A)') '==> VLIDORT_FIXED_OPTICAL'
     &             // ' & VLIDORT_MODIFIED_OPTICAL'
      WRITE(IU,211) VLD_FIXIN%OPTICAL%TS_LAMBERTIAN_ALBEDO
      WRITE(IU,212) VLD_FIXIN%OPTICAL%TS_SURFACE_BB_INPUT
      WRITE(IU,213) 
      DO L = 1, VLD_FIXIN%CONT%TS_NLAYERS
         WRITE(IU,214) L, VLD_FIXIN%OPTICAL%TS_DELTAU_VERT_INPUT(L), 
     &                    VLD_MODIN%MOPTICAL%TS_OMEGA_TOTAL_INPUT(L)
      ENDDO

      WRITE(IU,215)
      DO L = 1, VLD_FIXIN%CONT%TS_NLAYERS
      DO N = 0, VLD_MODIN%MCONT%TS_NGREEK_MOMENTS_INPUT
         WRITE(IU,216) L, N, 
     &     VLD_FIXIN%OPTICAL%TS_GREEKMAT_TOTAL_INPUT(N,L,1), 
     &     VLD_FIXIN%OPTICAL%TS_GREEKMAT_TOTAL_INPUT(N,L,2),
     &     VLD_FIXIN%OPTICAL%TS_GREEKMAT_TOTAL_INPUT(N,L,5),
     &     VLD_FIXIN%OPTICAL%TS_GREEKMAT_TOTAL_INPUT(N,L,6),
     &     VLD_FIXIN%OPTICAL%TS_GREEKMAT_TOTAL_INPUT(N,L,11),
     &     VLD_FIXIN%OPTICAL%TS_GREEKMAT_TOTAL_INPUT(N,L,12),
     &     VLD_FIXIN%OPTICAL%TS_GREEKMAT_TOTAL_INPUT(N,L,15),
     &     VLD_FIXIN%OPTICAL%TS_GREEKMAT_TOTAL_INPUT(N,L,16)
      ENDDO
      ENDDO
 211  FORMAT('LAMBERTIAN_ALBEDO         :', 1P1E20.12 )
 212  FORMAT('SURFACE_BB_INPUT          :', 1P1E12.3 )
 213  FORMAT('  Layer|  Delta   |  Omega' )
 214  FORMAT(2X, I4, 1P2E20.12 )
 215  FORMAT(' Lay| N |   GK1   |   GK2   |   GK5   |   GK6   |', 
     &                '   GK11  |   GK12  |   GK15  |   GK16' )
 216  FORMAT(2I4, 1P8E20.12)

      WRITE(IU,'(/A)') REPEAT( '=', 79 )
      WRITE(IU,'(A)') '==>VLIDORT_FIXED_LINOPTICAL:'
 
      WRITE(IU,221)
      DO K = 1, VLD_LFIXIN%CONT%TS_N_TOTALCOLUMN_WFS
      DO L = 1, VLD_FIXIN%CONT%TS_NLAYERS
         WRITE(IU,222) K,VLD_LFIXIN%CONT%TS_COLUMNWF_NAMES(K),  
     &          L, VLD_LFIXIN%OPTICAL%TS_L_DELTAU_VERT_INPUT(K,L),
     &             VLD_LFIXIN%OPTICAL%TS_L_OMEGA_TOTAL_INPUT(K,L)
      ENDDO
      ENDDO

      WRITE(IU,223)
      DO K = 1, VLD_LFIXIN%CONT%TS_N_TOTALCOLUMN_WFS

         WRITE(IU,224) K,VLD_LFIXIN%CONT%TS_COLUMNWF_NAMES(K)
         DO L = 1, VLD_FIXIN%CONT%TS_NLAYERS
         DO N = 0, VLD_MODIN%MCONT%TS_NGREEK_MOMENTS_INPUT
            WRITE(IU,225) L, N,
     &        VLD_LFIXIN%OPTICAL%TS_L_GREEKMAT_TOTAL_INPUT(K,N,L,1),
     &        VLD_LFIXIN%OPTICAL%TS_L_GREEKMAT_TOTAL_INPUT(K,N,L,2),
     &        VLD_LFIXIN%OPTICAL%TS_L_GREEKMAT_TOTAL_INPUT(K,N,L,5),
     &        VLD_LFIXIN%OPTICAL%TS_L_GREEKMAT_TOTAL_INPUT(K,N,L,6),
     &        VLD_LFIXIN%OPTICAL%TS_L_GREEKMAT_TOTAL_INPUT(K,N,L,11),
     &        VLD_LFIXIN%OPTICAL%TS_L_GREEKMAT_TOTAL_INPUT(K,N,L,12),
     &        VLD_LFIXIN%OPTICAL%TS_L_GREEKMAT_TOTAL_INPUT(K,N,L,15),
     &        VLD_LFIXIN%OPTICAL%TS_L_GREEKMAT_TOTAL_INPUT(K,N,L,16)
         ENDDO
         ENDDO
      ENDDO
 221  FORMAT(' IPAR|  PARNAME  |Layer|  L_Delta  |  L_Omega' )
 222  FORMAT(1X, I4,2X,  A10, I4, 1P2E20.12)
 223  FORMAT(' Lay| N |   GK1   |   GK2   |   GK5   |   GK6   |',
     &                '   GK11  |   GK12  |   GK15  |   GK16' )
 224  FORMAT(' IPAR:', I4, 3X, 'PARNAME:', A12 )
 225  FORMAT(2I4, 1P8E20.12) 

      CLOSE(IU)

      END SUBROUTINE PREPARE_VLIORT_INPUTS

!------------------------------------------------------------------------------

      SUBROUTINE PREPARE_VLIORT_SUP_INPUTS
     &              ( BRDF_IN, BRDF_LIN, 
     &                VLD_SUP, VLD_LSUP )
!
!******************************************************************************
! Function PREPARE_VLIORT_SUP_INPUTS return the VLIDORT inputs from
! BRDF, or SLEAVE supplimentary packages.
! (xxu, 4/26/13)
! 
!******************************************************************************
!
      ! Referenced F90 modules
      USE VLIDORT_IO_DEFS,    ONLY : VLIDORT_SUP_INOUT
      USE VLIDORT_LIN_IO_DEFS,ONLY : VLIDORT_LINSUP_INOUT
      USE VBRDF_SUP_MOD,      ONLY : VBRDF_SUP_OUTPUTS
      USE VBRDF_SUP_MOD,      ONLY : VBRDF_SUP_INPUTS
      USE VBRDF_LINSUP_MOD,   ONLY : VBRDF_LINSUP_OUTPUTS
      USE VBRDF_LINSUP_MOD,   ONLY : VBRDF_LINSUP_INPUTS
      USE BRDF_MOD,           ONLY : RUN_VBRDF
      USE NAMELIST_ARRAY_MOD, ONLY : LBRDF

      ! Sturctures
      TYPE ( VLIDORT_SUP_INOUT             ) :: VLD_SUP
      TYPE ( VLIDORT_LINSUP_INOUT          ) :: VLD_LSUP
      TYPE ( VBRDF_SUP_INPUTS              ) :: BRDF_IN
      TYPE ( VBRDF_LINSUP_INPUTS           ) :: BRDF_LIN
      TYPE ( VBRDF_SUP_OUTPUTS             ) :: BRDF_OUT
      TYPE ( VBRDF_LINSUP_OUTPUTS          ) :: BRDF_LOUT

      ! Local variables

      !================================================================
      ! PREPARE_VLIORT_SUP_INPUTS begins here
      !================================================================
      WRITE(6,100)
 100  FORMAT(2X,"-PREPARE_VLIORT_SUP_INPUTS:BRDF,and/or SLEAVE ")
      
      !================================================================
      ! Prepare BRDF SUP input for VLIDORT
      !================================================================
      IF ( LBRDF ) THEN

         ! Run the BRDF model
         CALL RUN_VBRDF( BRDF_IN, BRDF_LIN, BRDF_OUT, BRDF_LOUT )

         ! Copy VBRDF outputs to VLIDORT's BRDF inputs
         VLD_SUP%BRDF%TS_BRDF_F_0        = BRDF_OUT%BS_BRDF_F_0
         VLD_SUP%BRDF%TS_BRDF_F          = BRDF_OUT%BS_BRDF_F
         VLD_SUP%BRDF%TS_USER_BRDF_F_0   = BRDF_OUT%BS_USER_BRDF_F_0
         VLD_SUP%BRDF%TS_USER_BRDF_F     = BRDF_OUT%BS_USER_BRDF_F
         VLD_SUP%BRDF%TS_EXACTDB_BRDFUNC = BRDF_OUT%BS_EXACTDB_BRDFUNC
         VLD_SUP%BRDF%TS_EMISSIVITY      = BRDF_OUT%BS_EMISSIVITY
         VLD_SUP%BRDF%TS_USER_EMISSIVITY = BRDF_OUT%BS_USER_EMISSIVITY

         VLD_LSUP%BRDF%TS_LS_BRDF_F_0    = BRDF_LOUT%BS_LS_BRDF_F_0
         VLD_LSUP%BRDF%TS_LS_BRDF_F      = BRDF_LOUT%BS_LS_BRDF_F
         VLD_LSUP%BRDF%TS_LS_USER_BRDF_F_0
     &                                   = BRDF_LOUT%BS_LS_USER_BRDF_F_0
         VLD_LSUP%BRDF%TS_LS_USER_BRDF_F = BRDF_LOUT%BS_LS_USER_BRDF_F
         VLD_LSUP%BRDF%TS_LS_EXACTDB_BRDFUNC 
     &                                 = BRDF_LOUT%BS_LS_EXACTDB_BRDFUNC
         VLD_LSUP%BRDF%TS_LS_USER_EMISSIVITY 
     &                                 = BRDF_LOUT%BS_LS_USER_EMISSIVITY
         VLD_LSUP%BRDF%TS_LS_EMISSIVITY  = BRDF_LOUT%BS_LS_EMISSIVITY

      ENDIF ! LBRDF


      END SUBROUTINE PREPARE_VLIORT_SUP_INPUTS

!------------------------------------------------------------------------------

      SUBROUTINE GET_VLD_FBOOLEAN_IN( VLD_FBOOLEAN_IN )
!
!******************************************************************************
! Function GET_VLD_FBOOLEAN_IN return the initialized TYPE variable for
! calling VLIDORT_L_MASTER.
! 
! Variables:
! =============================================================================
! TS_DO_FULLRAD_MODE        : Full Stokes vector calculation 
! TS_DO_SSCORR_TRUNCATION   : Flag for SSCORR truncation ( delta-M
! scaling)
! TS_DO_SSFULL              : Full-up single scatter calculation
! TS_DO_THERMAL_EMISSION    : Thermal emission flags
! TS_DO_SURFACE_EMISSION    : Surface emission flags
! TS_DO_PLANE_PARALLEL      : Plane-parallel treatment of direct beam
! TS_DO_UPWELLING           : upwelling output
! TS_DO_DNWELLING           : downwelling output
! TS_DO_QUAD_OUTPUT         : stream angle (quadrature outpu)
! TS_DO_TOA_CONTRIBS        : top of the atmosphere contributions
! TS_DO_LAMBERTIAN_SURFACE  : Lambertian surface
! TS_DO_SPECIALIST_OPTION_1 : Special Options
! TS_DO_SPECIALIST_OPTION_2 : Special Options
! TS_DO_SPECIALIST_OPTION_3 : Special Options
!
!******************************************************************************
!
      ! Referenced F90 modules
      USE VLIDORT_INPUTS_DEF, ONLY : VLIDORT_FIXED_BOOlEAN   
      USE NAMELIST_ARRAY_MOD, ONLY : RECEP_DIR
      USE NAMELIST_ARRAY_MOD, ONLY : I_SCENARIO
      USE NAMELIST_ARRAY_MOD, ONLY : LDEBUG_IOP_CALC
      USE NAMELIST_ARRAY_MOD, ONLY : LLAMBERTIAN

      ! Arguments
      TYPE(VLIDORT_FIXED_BOOlEAN) :: VLD_FBOOLEAN_IN

      VLD_FBOOLEAN_IN%TS_DO_FULLRAD_MODE      = .TRUE.
      VLD_FBOOLEAN_IN%TS_DO_SSCORR_TRUNCATION = .FALSE.
      VLD_FBOOLEAN_IN%TS_DO_THERMAL_EMISSION  = .FALSE. ! TRUE for night 
      VLD_FBOOLEAN_IN%TS_DO_SURFACE_EMISSION  = .FALSE. ! TRUE for night time
      VLD_FBOOLEAN_IN%TS_DO_PLANE_PARALLEL    = .TRUE.
      VLD_FBOOLEAN_IN%TS_DO_SS_EXTERNAL       = .FALSE.

      ! Single scattering scenarios
      SELECT CASE ( I_SCENARIO )
      CASE ( 0, 1, 2, 3, 4, 5, 6 )
         VLD_FBOOLEAN_IN%TS_DO_SSFULL = .FALSE.
      CASE ( 10, 11, 12, 13, 14, 15, 16 )
         VLD_FBOOLEAN_IN%TS_DO_SSFULL = .TRUE.
      END SELECT

      ! Set user receptor direction
      IF ( RECEP_DIR == -1 ) THEN
         VLD_FBOOLEAN_IN%TS_DO_UPWELLING = .TRUE.
         VLD_FBOOLEAN_IN%TS_DO_DNWELLING = .FALSE.
      ELSE IF ( RECEP_DIR == 1 ) THEN
         VLD_FBOOLEAN_IN%TS_DO_UPWELLING = .FALSE.
         VLD_FBOOLEAN_IN%TS_DO_DNWELLING = .TRUE.
      ENDIF

      VLD_FBOOLEAN_IN%TS_DO_QUAD_OUTPUT         = .FALSE.
      VLD_FBOOLEAN_IN%TS_DO_TOA_CONTRIBS        = .FALSE.
      VLD_FBOOLEAN_IN%TS_DO_LAMBERTIAN_SURFACE  = LLAMBERTIAN
      VLD_FBOOLEAN_IN%TS_DO_SPECIALIST_OPTION_1 = .FALSE.
      VLD_FBOOLEAN_IN%TS_DO_SPECIALIST_OPTION_2 = .FALSE.
      VLD_FBOOLEAN_IN%TS_DO_SPECIALIST_OPTION_3 = .FALSE.
      VLD_FBOOLEAN_IN%TS_DO_SURFACE_LEAVING     = .FALSE.
      VLD_FBOOLEAN_IN%TS_DO_SL_ISOTROPIC        = .FALSE.

      ! Return to the calling routine  
      RETURN

      END SUBROUTINE GET_VLD_FBOOLEAN_IN

!------------------------------------------------------------------------------

      SUBROUTINE GET_VLD_CONTROL_IN( VLD_FCONTROL_IN, VLD_MCONTROL_IN )
!
!******************************************************************************
! Function GET_VLD_CONTROL_IN return the initialized TYPE variable for
! calling VLIDORT_L_MASTER.
! 
! Variables:
! =============================================================================
! TS_NSTOKES                 : Number of Stokes parameters
! TS_NSTREAMS                : Number of discrete ordinate streams
! TS_NLAYERS                 : Number of computational layers
! TS_NFINELAYERS             : Number of fine layers (spherical corr. only)
! TS_N_THERMAL_COEFFS        : Number of thermal coefficients (default=2)
! TS_VLIDORT_ACCURACY        : Accuracy for convergence of Fourier series
! TS_NLAYERS_NOMS            : Special Options
! TS_NLAYERS_CUTOFF          : Special Options
!
! NOTES:
!  - Morged the modified control variables to this (xxu, 1/28/13)
!******************************************************************************
!
      ! Referenced F90 modules
      USE VLIDORT_INPUTS_DEF, ONLY : VLIDORT_FIXED_CONTROL    
      USE VLIDORT_INPUTS_DEF, ONLY : VLIDORT_MODIFIED_CONTROL
      USE NAMELIST_ARRAY_MOD, ONLY : N_MOMENTS
      USE NAMELIST_ARRAY_MOD, ONLY : N_STOKS, N_STRMS, N_LAYERS 
 

      ! Arguments
      TYPE(VLIDORT_FIXED_CONTROL)    :: VLD_FCONTROL_IN   
      TYPE(VLIDORT_MODIFIED_CONTROL) :: VLD_MCONTROL_IN

      ! Fixed control
      VLD_FCONTROL_IN%TS_NSTOKES           = N_STOKS
      VLD_FCONTROL_IN%TS_NSTREAMS          = N_STRMS
      VLD_FCONTROL_IN%TS_NLAYERS           = N_LAYERS
      VLD_FCONTROL_IN%TS_NFINELAYERS       = 1 
      VLD_FCONTROL_IN%TS_N_THERMAL_COEFFS  = 2
      VLD_FCONTROL_IN%TS_VLIDORT_ACCURACY  = 1d-4
      !VLD_FCONTROL_IN%TS_NLAYERS_NOMS      = 0d0
      !VLD_FCONTROL_IN%TS_NLAYERS_CUTOFF    = 0d0

      ! modified control 
      VLD_MCONTROL_IN%TS_NGREEK_MOMENTS_INPUT = N_MOMENTS-1

      ! Return to the calling routine  
      RETURN

      END SUBROUTINE GET_VLD_CONTROL_IN

!------------------------------------------------------------------------------

      SUBROUTINE GET_VLD_MBOOLEAN_IN( VLD_MBOOLEAN_IN )
!
!******************************************************************************
! Function GET_VLD_MBOOLEAN_IN return the initialized TYPE variable for
! calling VLIDORT_L_MASTER.
! 
! Variables:
! =============================================================================
! TS_DO_SSCORR_NADIR        : nadir single scatter correction
! TS_DO_SSCORR_OUTGOING     : outgoing single scatter correction
! TS_DO_DOUBLE_CONVTEST     : double convergence test
! TS_DO_SOLAR_SOURCES       : Use solar sources
! TS_DO_REFRACTIVE_GEOMETRY : Do refractive geometry
! TS_DO_CHAPMAN_FUNCTION    : internal Chapman function calculation
! TS_DO_RAYLEIGH_ONLY       : Rayleigh atmosphere only
! TS_DO_DELTAM_SCALING      : delta-M scaling
! TS_DO_SOLUTION_SAVING     : solution saving
! TS_DO_BVP_TELESCOPING     : boundary-value telescoping
! TS_DO_USER_VZANGLES       : Use user-defined viewing zenith angles
! TS_DO_ADDITIONAL_MVOUT    : mean-value output additionally
! TS_DO_MVOUT_ONLY          : only mean-value output
! TS_DO_THERMAL_TRANSONLY   : thermal emission, transmittance only
!******************************************************************************
!
      ! Referenced F90 modules
      USE VLIDORT_INPUTS_DEF,    ONLY : VLIDORT_MODIFIED_BOOLEAN   
      USE NAMELIST_ARRAY_MOD,    ONLY : I_SCENARIO

      ! Return value
      TYPE(VLIDORT_MODIFIED_BOOLEAN) :: VLD_MBOOLEAN_IN

      VLD_MBOOLEAN_IN%TS_DO_SSCORR_NADIR        = .FALSE. ! 
      VLD_MBOOLEAN_IN%TS_DO_SSCORR_OUTGOING     = .FALSE.  ! Better on for aerosol
      VLD_MBOOLEAN_IN%TS_DO_DOUBLE_CONVTEST     = .TRUE.  ! True for aerosol, fals for night time
      VLD_MBOOLEAN_IN%TS_DO_SOLAR_SOURCES       = .TRUE.  ! False for night time
      VLD_MBOOLEAN_IN%TS_DO_REFRACTIVE_GEOMETRY = .FALSE. ! 
      VLD_MBOOLEAN_IN%TS_DO_CHAPMAN_FUNCTION    = .FALSE.
      VLD_MBOOLEAN_IN%TS_DO_RAYLEIGH_ONLY       = .FALSE.
      VLD_MBOOLEAN_IN%TS_DO_DELTAM_SCALING      = .TRUE.
      VLD_MBOOLEAN_IN%TS_DO_SOLUTION_SAVING     = .FALSE. ! Check on this for speeding
      VLD_MBOOLEAN_IN%TS_DO_BVP_TELESCOPING     = .FALSE. ! Check on this for speeding
      VLD_MBOOLEAN_IN%TS_DO_USER_VZANGLES       = .TRUE.
      VLD_MBOOLEAN_IN%TS_DO_ADDITIONAL_MVOUT    = .FALSE. ! Flux cal
      VLD_MBOOLEAN_IN%TS_DO_MVOUT_ONLY          = .FALSE.
      VLD_MBOOLEAN_IN%TS_DO_THERMAL_TRANSONLY   = .FALSE.


      ! Single scattering scenarios
      SELECT CASE ( I_SCENARIO )

      CASE ( 0, 10 )
         VLD_MBOOLEAN_IN%TS_DO_SSCORR_NADIR        = .FALSE.
         VLD_MBOOLEAN_IN%TS_DO_SSCORR_OUTGOING     = .FALSE.
         VLD_MBOOLEAN_IN%TS_DO_DOUBLE_CONVTEST     = .TRUE.
         VLD_MBOOLEAN_IN%TS_DO_DELTAM_SCALING      = .FALSE.
      CASE ( 1, 11 )
         VLD_MBOOLEAN_IN%TS_DO_SSCORR_NADIR        = .FALSE.
         VLD_MBOOLEAN_IN%TS_DO_SSCORR_OUTGOING     = .TRUE.
         VLD_MBOOLEAN_IN%TS_DO_DOUBLE_CONVTEST     = .TRUE.
         VLD_MBOOLEAN_IN%TS_DO_DELTAM_SCALING      = .TRUE.
      CASE ( 2, 12 )
         VLD_MBOOLEAN_IN%TS_DO_SSCORR_NADIR        = .TRUE.
         VLD_MBOOLEAN_IN%TS_DO_SSCORR_OUTGOING     = .FALSE.
         VLD_MBOOLEAN_IN%TS_DO_DOUBLE_CONVTEST     = .TRUE.
         VLD_MBOOLEAN_IN%TS_DO_DELTAM_SCALING      = .TRUE.
      CASE ( 3, 13 )
         VLD_MBOOLEAN_IN%TS_DO_SSCORR_NADIR        = .FALSE.
         VLD_MBOOLEAN_IN%TS_DO_SSCORR_OUTGOING     = .FALSE.
         VLD_MBOOLEAN_IN%TS_DO_DOUBLE_CONVTEST     = .TRUE.
         VLD_MBOOLEAN_IN%TS_DO_DELTAM_SCALING      = .TRUE.
      CASE ( 4, 14 )
         VLD_MBOOLEAN_IN%TS_DO_SSCORR_NADIR        = .FALSE.
         VLD_MBOOLEAN_IN%TS_DO_SSCORR_OUTGOING     = .FALSE.
         VLD_MBOOLEAN_IN%TS_DO_DOUBLE_CONVTEST     = .TRUE.
         VLD_MBOOLEAN_IN%TS_DO_DELTAM_SCALING      = .TRUE.
         VLD_MBOOLEAN_IN%TS_DO_SOLUTION_SAVING     = .TRUE. 
         VLD_MBOOLEAN_IN%TS_DO_BVP_TELESCOPING     = .TRUE.
      CASE ( 5, 15 )
         VLD_MBOOLEAN_IN%TS_DO_SSCORR_NADIR        = .FALSE.
         VLD_MBOOLEAN_IN%TS_DO_SSCORR_OUTGOING     = .TRUE.
         VLD_MBOOLEAN_IN%TS_DO_DOUBLE_CONVTEST     = .TRUE.
         VLD_MBOOLEAN_IN%TS_DO_DELTAM_SCALING      = .TRUE.
         VLD_MBOOLEAN_IN%TS_DO_SOLUTION_SAVING     = .TRUE. 
         VLD_MBOOLEAN_IN%TS_DO_BVP_TELESCOPING     = .TRUE.

      END SELECT

      END SUBROUTINE GET_VLD_MBOOLEAN_IN

!------------------------------------------------------------------------------

      SUBROUTINE GET_VLD_GEOMETRY_IN( VLD_FSUNRAYS_IN, VLD_FUSERVAL_IN, 
     &                                VLD_MSUNRAYS_IN, VLD_MUSERVAL_IN)
!
!******************************************************************************
! Function GET_VLD_BEAM_IN return the initialized TYPE variable for
! calling VLIDORT_L_MASTER.
! 
! Variables:
! =============================================================================
! TS_FLUX_FACTOR            : Flux factor ( should be 1 or pi )
! TS_N_SZANGLES             : number of solar beams
! TS_SZANGLES               : Bottom-of-atmosphere solar zenith angles (degree)
!******************************************************************************
!
      ! Referenced F90 modules
      USE VLIDORT_INPUTS_DEF, ONLY : VLIDORT_FIXED_SUNRAYS
      USE VLIDORT_INPUTS_DEF, ONLY : VLIDORT_FIXED_USERVALUES
      USE VLIDORT_INPUTS_DEF, ONLY : VLIDORT_MODIFIED_SUNRAYS
      USE VLIDORT_INPUTS_DEF, ONLY : VLIDORT_MODIFIED_USERVALUES
      USE NAMELIST_ARRAY_MOD, ONLY : N_THETA0,   THETA0
      USE NAMELIST_ARRAY_MOD, ONLY : N_THETA,    THETA
      USE NAMELIST_ARRAY_MOD, ONLY : N_PHI,      PHI
      USE NAMELIST_ARRAY_MOD, ONLY : NRECEP_LEV, RECEP_LEV
      USE ATMPROF_MOD,        ONLY : ATMZ


      ! Arguments
      TYPE(VLIDORT_FIXED_SUNRAYS)       :: VLD_FSUNRAYS_IN
      TYPE(VLIDORT_FIXED_USERVALUES)    :: VLD_FUSERVAL_IN
      TYPE(VLIDORT_MODIFIED_SUNRAYS)    :: VLD_MSUNRAYS_IN
      TYPE(VLIDORT_MODIFIED_USERVALUES) :: VLD_MUSERVAL_IN
 
      ! Local variables
      INTEGER                        :: I
      REAL*8, PARAMETER              :: TOL = 1D-4

      ! Added by xxu, 2/11/11
      ! Since RTM usually has some computation error for edge angles,
      ! we need to replace those edge angels.
      ! Chack SZA for 0 and 90
      DO I = 1, N_THETA0
         IF ( THETA0(I) <=  TOL         ) THETA0(I) = TOL 
         IF ( THETA0(I) >=  90d0 - TOL  ) THETA0(I) = 90d0 - TOL 
      enddo

      ! Chack VZA for 0 and 90
      DO I = 1, N_THETA
         IF ( THETA(I) <=  TOL         ) THETA(I) = TOL  
         IF ( THETA(I) >=  90d0 - TOL  ) THETA(I) = 90d0 - TOL 
      enddo

      ! Chack RAZ for 0 and 180
      DO I =1, N_PHI
         IF ( PHI(I) <= TOL            ) PHI(I) = TOL 
         IF ( PHI(I) >= 180d0 - TOL    ) PHI(I) = 180d0 - TOL 
      ENDDO
 
      !=================================================================
      !  VLD_FSUNRAYS_IN
      !=================================================================
      VLD_FSUNRAYS_IN%TS_FLUX_FACTOR  = 1d0

      !=================================================================
      !  VLD_FUSERVAL_IN
      !=================================================================
      VLD_FUSERVAL_IN%TS_N_USER_VZANGLES = N_THETA
      VLD_FUSERVAL_IN%TS_N_USER_LEVELS   = NRECEP_LEV

      !=================================================================
      !  VLD_MSUNRAYS_IN
      !=================================================================
      VLD_MSUNRAYS_IN%TS_N_SZANGLES  = N_THETA0
      VLD_MSUNRAYS_IN%TS_SZANGLES(1:N_THETA0) = THETA0(1:N_THETA0)

      !=================================================================
      !  VLD_MUSERVAL_IN
      !=================================================================
      VLD_MUSERVAL_IN%TS_N_USER_RELAZMS        = N_PHI
      VLD_MUSERVAL_IN%TS_USER_RELAZMS(1:N_PHI) = PHI(1:N_PHI)
      VLD_MUSERVAL_IN%TS_USER_VZANGLES_INPUT(1:N_THETA)
     &                                         = THETA(1:N_THETA)
      VLD_MUSERVAL_IN%TS_USER_LEVELS(1:NRECEP_LEV) 
     &                                         = RECEP_LEV(1:NRECEP_LEV)
      VLD_MUSERVAL_IN%TS_GEOMETRY_SPECHEIGHT   = ATMZ(1) ! surface

      RETURN

      END SUBROUTINE GET_VLD_GEOMETRY_IN

!------------------------------------------------------------------------------

      SUBROUTINE GET_VLD_CHAPMAN_IN( VLD_FCHAPMAN_IN, VLD_MCHAPMAN_IN )
!
!******************************************************************************
! Function GET_VLD_CHAPMAN_IN return the initialized TYPE variable for
! calling VLIDORT_L_MASTER.
! 
! Variables:
! =============================================================================
! TS_HEIGHT_GRID      : multilayer Height [km]
! TS_PRESSURE_GRID    : Pressures in [mb], Required for the Chapman function 
! TS_TEMPERATURE_GRID : temperatures in [K], Required for the Chapman
! TS_FINEGRID         : Number of fine layer gradations
!                       Required for the Chapman function, refractive
!                       geometry
! TS_EARTH_RADIUS     : Earth radius in [km] for Chapman function
! TS_RFINDEX_PARAMETER: Refractive index parameter (only for Chapman 
!                       function calculations with refractive index)
!******************************************************************************
!
      ! Referenced F90 modules
      USE VLIDORT_INPUTS_DEF,   ONLY : VLIDORT_FIXED_CHAPMAN
      USE VLIDORT_INPUTS_DEF,   ONLY : VLIDORT_MODIFIED_CHAPMAN
      USE ATMPROF_MOD,          ONLY : ATMZ, ATMP, ATMT
      USE NAMELIST_ARRAY_MOD,   ONLY : N_LAYERS

      ! Arguments
      TYPE(VLIDORT_FIXED_CHAPMAN   ) :: VLD_FCHAPMAN_IN
      TYPE(VLIDORT_MODIFIED_CHAPMAN) :: VLD_MCHAPMAN_IN


      ! Local variables
      INTEGER                        :: L, REVSE_L

      DO L = 0, N_LAYERS

         REVSE_L = N_LAYERS - L + 1
         VLD_FCHAPMAN_IN%TS_HEIGHT_GRID(L)      = ATMZ(REVSE_L)
         VLD_FCHAPMAN_IN%TS_PRESSURE_GRID(L)    = ATMP(REVSE_L)
         VLD_FCHAPMAN_IN%TS_TEMPERATURE_GRID(L) = ATMT(REVSE_L)
      ENDDO

      VLD_FCHAPMAN_IN%TS_FINEGRID           = 1
      VLD_FCHAPMAN_IN%TS_RFINDEX_PARAMETER  = 0.000288d0
      VLD_MCHAPMAN_IN%TS_EARTH_RADIUS       = 6371.0d0

      ! Return to the calling routine
      END SUBROUTINE GET_VLD_CHAPMAN_IN

!------------------------------------------------------------------------------

      SUBROUTINE GET_VLD_WRITE_IN( VLD_WRITE_IN )

      ! Referenced F90 modules
      USE VLIDORT_INPUTS_DEF,   ONLY : VLIDORT_FIXED_WRITE
      USE NAMELIST_ARRAY_MOD,   ONLY : LDEBUG_IOP_CALC

      ! Arguments
      TYPE(VLIDORT_FIXED_WRITE) :: VLD_WRITE_IN

      ! Local variables
      LOGICAL                   :: DEBUG_VLIDORT_DEEP = .FALSE.

      VLD_WRITE_IN%TS_DO_DEBUG_WRITE         = DEBUG_VLIDORT_DEEP
      VLD_WRITE_IN%TS_DO_WRITE_INPUT         = DEBUG_VLIDORT_DEEP
      VLD_WRITE_IN%TS_INPUT_WRITE_FILENAME   = 'vld_std_input'
      VLD_WRITE_IN%TS_DO_WRITE_SCENARIO      = DEBUG_VLIDORT_DEEP
      VLD_WRITE_IN%TS_SCENARIO_WRITE_FILENAME= 'vld_std_scenario'
      VLD_WRITE_IN%TS_DO_WRITE_FOURIER       = DEBUG_VLIDORT_DEEP
      VLD_WRITE_IN%TS_FOURIER_WRITE_FILENAME = 'vld_std_fourier'
      VLD_WRITE_IN%TS_DO_WRITE_RESULTS       = DEBUG_VLIDORT_DEEP
      VLD_WRITE_IN%TS_RESULTS_WRITE_FILENAME = 'vld_std_result'

      ! Return to the calling routine
      END SUBROUTINE GET_VLD_WRITE_IN

!------------------------------------------------------------------------------

      SUBROUTINE GET_LCONTR_AND_OPTIC
     &             ( FLCONTR, MLCONTR, FOPTICAL, MOPTICAL, L_OPTIC )

!
!******************************************************************************
! 
! Vairables
! =============================================================================
! TS_DO_SIMULATION_ONLY      : Simulation only (No Jacobian calculation)
! TS_DO_LTE_LINEARIZATION    : LTE temperature weighting functions
! TS_DO_SURFBB_LINEARIZATION : Surface blackbody weighting functions
! TS_LAYER_VARY_FLAG         : 
! TS_LAYER_VARY_NUMBER       : 
! TS_N_TOTALCOLUMN_WFS       :  Number of atmospheric column weighting f
! TS_N_TOTALPROFILE_WFS      : Number of atmospheric profile weighting f
! TS_N_SURFACE_WFS           : Number of surface property weighting f
! TS_COLUMNWF_NAMES          : Atmospheric column Jacobian names
! TS_PROFILEWF_NAMES         : Atmospheric profile Jacobian name
!******************************************************************************
!
      ! Referenced F90 modules
      USE VLIDORT_INPUTS_DEF,    ONLY : VLIDORT_FIXED_OPTICAL
      USE VLIDORT_INPUTS_DEF,    ONLY : VLIDORT_MODIFIED_OPTICAL
      USE VLIDORT_LININPUTS_DEF, ONLY : VLIDORT_FIXED_LINOPTICAL 
      USE VLIDORT_LININPUTS_DEF, ONLY : VLIDORT_FIXED_LINCONTROL
      USE VLIDORT_LININPUTS_DEF, ONLY : VLIDORT_MODIFIED_LINCONTROL   
      USE NAMELIST_ARRAY_MOD,    ONLY : N_LAYERS,     N_MOMENTS
      USE NAMELIST_ARRAY_MOD,    ONLY : N_STOKS
      USE NAMELIST_ARRAY_MOD,    ONLY : LJACOB
      USE NAMELIST_ARRAY_MOD,    ONLY : LJACOB_SURF,  SURF_REFL
      USE LPAR_MOD,              ONLY : N_LPAR_4VLD,  N_LPAR_EXTRA
      USE LPAR_MOD,              ONLY : N_LPAR_TOTAL, LPAR_NAMES
      USE LPAR_MOD,              ONLY : N_LPAR_SURF
      USE IOP_MOD,               ONLY : IOP_TAU,  IOP_SSA,  IOP_PHA
      USE IOP_MOD,               ONLY : LIOP_TAU, LIOP_SSA, LIOP_PHA
      USE IOP_MOD,               ONLY : LIOP_JCB

      ! Arguments (types)
      TYPE(VLIDORT_FIXED_LINCONTROL   ) :: FLCONTR
      TYPE(VLIDORT_MODIFIED_LINCONTROL) :: MLCONTR
      TYPE(VLIDORT_FIXED_OPTICAL      ) :: FOPTICAL
      TYPE(VLIDORT_MODIFIED_OPTICAL   ) :: MOPTICAL
      TYPE(VLIDORT_FIXED_LINOPTICAL   ) :: L_OPTIC

      ! Local variables
      INTEGER                              :: GK, GMMASK(8), NMASK
      INTEGER                              :: I, L, N, K, Q
      !DATA GMMASK / 1, 2, 5, 6, 11, 12, 15, 16 /
      DATA GMMASK / 1, 6, 11, 16, 2, 5, 12, 15 /

      ! Surpress the Emission/Temperature Jacobian
      FLCONTR%TS_DO_LTE_LINEARIZATION    = .FALSE.
      FLCONTR%TS_DO_SURFBB_LINEARIZATION = .FALSE.
      FOPTICAL%TS_THERMAL_BB_INPUT       = 0d0
      FOPTICAL%TS_SURFACE_BB_INPUT       = 0d0
      FOPTICAL%TS_LTE_DELTAU_VERT_INPUT  = 0d0
      FOPTICAL%TS_LTE_THERMAL_BB_INPUT   = 0d0

      !================================================================
      ! Atmospheric Jacobian calculation scenario
      !================================================================
      IF ( LJACOB ) THEN

         FLCONTR%TS_DO_SIMULATION_ONLY       = .FALSE.
         MLCONTR%TS_DO_LINEARIZATION         = .TRUE.
         MLCONTR%TS_DO_ATMOS_LINEARIZATION   = .TRUE.
         MLCONTR%TS_DO_PROFILE_LINEARIZATION = .FALSE.
         MLCONTR%TS_DO_COLUMN_LINEARIZATION  = .TRUE.

         FLCONTR%TS_N_TOTALCOLUMN_WFS        = N_LPAR_4VLD
         FLCONTR%TS_N_TOTALPROFILE_WFS       = 0
         FLCONTR%TS_COLUMNWF_NAMES(1:N_LPAR_4VLD) 
     &           = LPAR_NAMES(1:N_LPAR_4VLD)
         FLCONTR%TS_PROFILEWF_NAMES(1:N_LPAR_4VLD) 
     &           = LPAR_NAMES(1:N_LPAR_4VLD)       

      ELSE

         FLCONTR%TS_DO_SIMULATION_ONLY       = .TRUE.
         MLCONTR%TS_DO_LINEARIZATION         = .FALSE.
         MLCONTR%TS_DO_ATMOS_LINEARIZATION   = .FALSE.
         MLCONTR%TS_DO_PROFILE_LINEARIZATION = .FALSE.
         MLCONTR%TS_DO_COLUMN_LINEARIZATION  = .FALSE.
         FLCONTR%TS_N_TOTALCOLUMN_WFS        = 0
         FLCONTR%TS_N_TOTALPROFILE_WFS       = 0

      ENDIF

      !================================================================ 
      ! VLIDORT inputs of optical properties
      !================================================================

      ! Controls number of non-zero entries in the Greek matrix
      NMASK = 1
      IF ( N_STOKS > 1 ) NMASK = 8 

      ! Layer loop
      DO L = 1, N_LAYERS

         IF ( IOP_SSA(L) >= 0.99999d0 ) IOP_SSA(L) = 0.99999d0

         ! Bulk optical properties
         FOPTICAL%TS_DELTAU_VERT_INPUT(L) = IOP_TAU(L)
         MOPTICAL%TS_OMEGA_TOTAL_INPUT(L) = IOP_SSA(L) 
!         WRITE(6,'(A,I4,1P4E12.3)') 'XXX', L, 
!     &                   IOP_TAU(L),IOP_SSA(L), 
!     &                   FOPTICAL%TS_DELTAU_VERT_INPUT(L), 
!     &                   MOPTICAL%TS_OMEGA_TOTAL_INPUT(L) 


         DO K = 1, NMASK
            GK = GMMASK(K)
            DO N = 0, N_MOMENTS-1
               FOPTICAL%TS_GREEKMAT_TOTAL_INPUT(N,L,GK) = IOP_PHA(N,L,K)
            ENDDO
         ENDDO

         ! Linearized optical properties for Jacobian calculation
         IF ( LJACOB .AND. N_LPAR_TOTAL >=1 ) THEN

            ! Prepare the linearized IOP
            DO Q = 1, N_LPAR_4VLD
            IF ( LIOP_JCB(Q,L) == 1 ) THEN
 
               FLCONTR%TS_LAYER_VARY_FLAG(L)   = .TRUE.
               FLCONTR%TS_LAYER_VARY_NUMBER(L) = N_LPAR_4VLD

               L_OPTIC%TS_L_DELTAU_VERT_INPUT(Q,L) = LIOP_TAU(Q,L)
               L_OPTIC%TS_L_OMEGA_TOTAL_INPUT(Q,L) = LIOP_SSA(Q,L)

               DO K = 1, NMASK

                  GK = GMMASK(K)
                  DO N = 0, N_MOMENTS-1
                   L_OPTIC%TS_L_GREEKMAT_TOTAL_INPUT(Q,N,L,GK)
     &                                          = LIOP_PHA(Q,N,L,K)
                  ENDDO ! N
 
               ENDDO ! K

            ELSE 
             
               FLCONTR%TS_LAYER_VARY_FLAG(L)   = .FALSE.
               FLCONTR%TS_LAYER_VARY_NUMBER(L) = 0

            ENDIF 
            ENDDO ! Q

         ENDIF

      ! End layer loop 
      ENDDO

      !================================================================ 
      ! Surface Jacobian Configuration
      !================================================================ 
      IF ( LJACOB_SURF ) THEN

         MLCONTR%TS_DO_SURFACE_LINEARIZATION = .TRUE.
         FLCONTR%TS_N_SURFACE_WFS            = N_LPAR_SURF

      ELSE

         MLCONTR%TS_DO_SURFACE_LINEARIZATION = .FALSE.
         FLCONTR%TS_N_SURFACE_WFS            = 0

      ENDIF

      !================================================================ 
      ! Surface Reflectance
      !================================================================ 
      FOPTICAL%TS_LAMBERTIAN_ALBEDO = SURF_REFL 

      ! Return to the calling routine
      END SUBROUTINE GET_LCONTR_AND_OPTIC

!------------------------------------------------------------------------------

      SUBROUTINE VLIDORT_VBRDF_INPUT_CHECKER (
     &              BRDF_IN,               ! Inputs
     &              VLD_FIXIN,             ! Inputs
     &              VLD_MODIN,             ! Inputs
     &              VLD_BRDF_STAT        ) ! Outputs

      USE VLIDORT_PARS

      USE VBRDF_SUP_INPUTS_DEF
      USE VLIDORT_INPUTS_DEF
      USE VLIDORT_OUTPUTS_DEF

      IMPLICIT NONE

      ! Arguments
      TYPE(VBRDF_SUP_INPUTS), INTENT(IN)             :: BRDF_IN
      TYPE(VLIDORT_FIXED_INPUTS), INTENT (IN)        :: VLD_FIXIN
      TYPE(VLIDORT_MODIFIED_INPUTS), INTENT (IN)     :: VLD_MODIN
      TYPE(VLIDORT_EXCEPTION_HANDLING), INTENT(OUT)  :: VLD_BRDF_STAT

      !================================================================
      !  Local variables
      !================================================================

      !  BRDF supplement inputs
      !  ----------------------

      !  User stream, BRDF surface and surface emission flags
      LOGICAL ::             BS_DO_BRDF_SURFACE
      LOGICAL ::             BS_DO_USER_STREAMS
      LOGICAL ::             BS_DO_SURFACE_EMISSION

      !  Number of Stokes vector components
      INTEGER ::             BS_NSTOKES

      !  Number of discrete ordinate streams
      INTEGER ::             BS_NSTREAMS

      !  BOA solar zenith angles
      INTEGER ::             BS_NBEAMS
      DOUBLE PRECISION ::    BS_BEAM_SZAS ( MAXBEAMS )

      !  User-defined relative azimuths (mandatory for Fourier > 0)
      INTEGER ::             BS_N_USER_RELAZMS
      DOUBLE PRECISION ::    BS_USER_RELAZMS (MAX_USER_RELAZMS)

      !  User-defined zenith angle input
      INTEGER ::             BS_N_USER_STREAMS
      DOUBLE PRECISION ::    BS_USER_ANGLES_INPUT (MAX_USER_STREAMS)

      !  VLIDORT Main inputs
      !  -------------------
      LOGICAL ::             DO_USER_VZANGLES
      LOGICAL ::             DO_MVOUT_ONLY
      LOGICAL ::             DO_LAMBERTIAN_SURFACE
      LOGICAL ::             DO_SURFACE_EMISSION

      INTEGER ::             NSTOKES
      INTEGER ::             NSTREAMS

      INTEGER ::             N_SZANGLES
      DOUBLE PRECISION ::    SZANGLES ( MAX_SZANGLES )
      INTEGER ::             N_USER_RELAZMS
      DOUBLE PRECISION ::    USER_RELAZMS ( MAX_USER_RELAZMS )
      INTEGER ::             N_USER_VZANGLES
      DOUBLE PRECISION ::    USER_VZANGLES ( MAX_USER_VZANGLES )

      !  Exception handling
      INTEGER ::             STATUS_INPUTCHECK
      INTEGER ::             NMESSAGES
      CHARACTER (LEN=120) :: MESSAGES ( 0:MAX_MESSAGES )
      CHARACTER (LEN=120) :: ACTIONS ( 0:MAX_MESSAGES )

      !  Others
      INTEGER          :: NM, I
      CHARACTER(Len=2) :: C2

      !================================================================
      !  Begin copy inputs to local variables
      !================================================================

      !  BRDF Control inputs
      BS_DO_USER_STREAMS     = BRDF_In%BS_DO_USER_STREAMS
      BS_DO_BRDF_SURFACE     = BRDF_In%BS_DO_BRDF_SURFACE
      BS_DO_SURFACE_EMISSION = BRDF_In%BS_DO_SURFACE_EMISSION

      !  BRDF Geometry inputs
      BS_NSTOKES             = BRDF_IN%BS_NSTOKES
      BS_NSTREAMS            = BRDF_IN%BS_NSTREAMS
      BS_NBEAMS              = BRDF_IN%BS_NBEAMS
      BS_BEAM_SZAS           = BRDF_IN%BS_BEAM_SZAS
      BS_N_USER_RELAZMS      = BRDF_IN%BS_N_USER_RELAZMS
      BS_USER_RELAZMS        = BRDF_IN%BS_USER_RELAZMS
      BS_N_USER_STREAMS      = BRDF_IN%BS_N_USER_STREAMS
      BS_USER_ANGLES_INPUT   = BRDF_IN%BS_USER_ANGLES_INPUT

      !  VLIDORT Fixed Boolean inputs
      DO_SURFACE_EMISSION   = VLD_FIXIN%BOOL%TS_DO_SURFACE_EMISSION
      DO_LAMBERTIAN_SURFACE = VLD_FIXIN%BOOL%TS_DO_LAMBERTIAN_SURFACE

      !  VLIDORT Modified Boolean inputs
      !DO_USER_STREAMS  = VLD_MODIN%MBOOL%TS_DO_USER_STREAMS
      DO_USER_VZANGLES = VLD_MODIN%MBOOL%TS_DO_USER_VZANGLES
      DO_MVOUT_ONLY    = VLD_MODIN%MBOOL%TS_DO_MVOUT_ONLY

      !  VLIDORT Fixed Control inputs
      NSTOKES  = VLD_FIXIN%CONT%TS_NSTOKES
      NSTREAMS = VLD_FIXIN%CONT%TS_NSTREAMS

      !  VLIDORT Fixed Beam inputs
      !BEAM_SZAS  = VLD_MODIN%MSUNRAYS%TS_BEAM_SZAS
      SZANGLES   = VLD_MODIN%MSUNRAYs%TS_SZANGLES

      !  VLIDORT Modified Beam inputs
      !NBEAMS     = VLD_MODIN%MSUNRAYS%TS_NBEAMS
      N_SZANGLES = VLD_MODIN%MSUNRAYS%TS_N_SZANGLES

      !  VLIDORT Fixed User Value inputs
      !N_USER_STREAMS  = VLD_FIXIN%UsERVAL%TS_N_USER_STREAMS
      N_USER_VZANGLES = VLD_FIXIN%USERVAL%TS_N_USER_VZANGLES

      !  VLIDORT Modified User Value inputs
      N_USER_RELAZMS  = VLD_MODIN%MUSERVAL%TS_N_USER_RELAZMS
      USER_RELAZMS    = VLD_MODIN%MUSERVAL%TS_USER_RELAZMS

      !USER_ANGLES     = VLIDORT_ModIn%MUserVal%TS_USER_ANGLES_INPUT
      USER_VZANGLES   = VLD_MODIN%MUSERVAL%TS_USER_VZANGLES_INPUT

      !  End copy inputs to local variables

      !  Initialize output status
      STATUS_INPUTCHECK = VLIDORT_SUCCESS
      MESSAGES(1:MAX_MESSAGES) = ' '
      ACTIONS (1:MAX_MESSAGES) = ' '

      NMESSAGES   = 0
      MESSAGES(0) = 'Successful Check of BRDF/MAIN compatibility'
      ACTIONS(0)  = 'No Action required for this Task'

      NM = NMESSAGES

      !  Checks

      IF ( BS_DO_BRDF_SURFACE.neqv.(.not.DO_LAMBERTIAN_SURFACE) ) THEN
        NM = NM + 1
        MESSAGES(NM) = 'BRDF surface not set for VLIDORT Main'
        ACTIONS(NM)  = 'DO_LAMBERTIAN_SURFACE should be False!'
        STATUS_INPUTCHECK = VLIDORT_SERIOUS
      ENDIF

      IF ( BS_DO_SURFACE_EMISSION.neqv.DO_SURFACE_EMISSION ) THEN
        NM = NM + 1
        MESSAGES(NM) = 'Surface emission flags do not agree'
        ACTIONS(NM)  = 'Check flag compatibility!'
        STATUS_INPUTCHECK = VLIDORT_SERIOUS
      ENDIF

      IF ( BS_DO_USER_STREAMS .neqv. DO_USER_VZANGLES) THEN
        NM = NM + 1
        MESSAGES(NM) = 'User Streams/VZangles flags do not agree'
        ACTIONS(NM)  = 'Check flag compatibility!'
        STATUS_INPUTCHECK = VLIDORT_SERIOUS
      ENDIF

      IF ( BS_DO_USER_STREAMS .neqv. (.not.DO_MVOUT_ONLY) ) THEN
        NM = NM + 1
        MESSAGES(NM) = 'User Streams and DO_MVOUT_ONLY not agree'
        ACTIONS(NM)  = 'Check flag compatibility!'
        STATUS_INPUTCHECK = VLIDORT_SERIOUS
      ENDIF

      IF ( BS_NSTREAMS .ne. NSTREAMS) THEN
        NM = NM + 1
        MESSAGES(NM) = 'Number of discrete ordinates does not agree'
        ACTIONS(NM)  = 'Check NSTREAMS input'
        STATUS_INPUTCHECK = VLIDORT_SERIOUS
      ENDIF

      IF ( BS_NSTOKES .ne. NSTOKES) THEN
        NM = NM + 1
        MESSAGES(NM) = 'Number of Stokes components does not agree'
        ACTIONS(NM)  = 'Check NSTOKES input'
        STATUS_INPUTCHECK = VLIDORT_SERIOUS
      ENDIF

      !  Angles

      IF ( BS_NBEAMS .ne. N_SZANGLES) THEN
        NM = NM + 1
        MESSAGES(NM) = 'Number of Solar beams does not agree'
        ACTIONS(NM)  = 'Check BS_NBEAMS and N_SZANGLES input'
        STATUS_INPUTCHECK = VLIDORT_SERIOUS
      ELSE
        DO I = 1, N_SZANGLES
          if ( BS_BEAM_SZAS(I) .ne. SZANGLES(I) ) THEN
            write(C2,'(I2)')I
            NM = NM + 1
            MESSAGES(NM) = 'Solar beam angle does not agree, # '//C2
            ACTIONS(NM)  = 'Check BS_BEAM_SZAS and SZANGLES input'
            STATUS_INPUTCHECK = VLIDORT_SERIOUS
          endif
        ENDDO
      ENDIF

      IF ( BS_N_USER_STREAMS .ne. N_USER_VZANGLES) THEN
        NM = NM + 1
        MESSAGES(NM) = 'Number of viewing zenith angles does not agree'
        ACTIONS(NM)  = 'Check N_USER_STREAMS and N_USER_VZANGLES input'
        STATUS_INPUTCHECK = VLIDORT_SERIOUS
      ELSE
        DO I = 1, N_USER_VZANGLES
          if ( BS_USER_ANGLES_INPUT(I) .ne. USER_VZANGLES(I) ) THEN
            write(C2,'(I2)')I
            NM = NM + 1
            MESSAGES(NM) = 'View zenith angle does not agree, # '//C2
            ACTIONS(NM)  = 'Check BS_USER_ANGLES & USER_VZANGLES input'
            STATUS_INPUTCHECK = VLIDORT_SERIOUS
          endif
        ENDDO
      ENDIF

      IF ( BS_N_USER_RELAZMS .ne. N_USER_RELAZMS) THEN
        NM = NM + 1
        MESSAGES(NM) = 'Number of viewing zenith angles does not agree'
        ACTIONS(NM)  = 'Check BS_N_USER_RELAZMS & N_USER_RELAZMS input'
        STATUS_INPUTCHECK = VLIDORT_SERIOUS
      ELSE
        DO I = 1, N_USER_RELAZMS
          if ( BS_USER_RELAZMS(I) .ne.USER_RELAZMS(I) ) THEN
            write(C2,'(I2)')I
            NM = NM + 1
            MESSAGES(NM) = 'Azimuth angle does not agree, # '//C2
            ACTIONS(NM)  = 'Check BS_USER_RELAZMS & USER_RELAZMS input'
            STATUS_INPUTCHECK = VLIDORT_SERIOUS
          endif
        ENDDO
      ENDIF

      !  Tally up messages

      NMESSAGES = NM

      !  Copy Exception handling output

      VLD_BRDF_STAT%TS_STATUS_INPUTCHECK = STATUS_INPUTCHECK
      VLD_BRDF_STAT%TS_NCHECKMESSAGES    = NMESSAGES
      VLD_BRDF_STAT%TS_CHECKMESSAGES     = MESSAGES
      VLD_BRDF_STAT%TS_ACTIONS           = ACTIONS

      !  Finish

      RETURN
      END SUBROUTINE VLIDORT_VBRDF_INPUT_CHECKER

!------------------------------------------------------------------------------

      ! End of module
      END MODULE VLIDORT_MOD
