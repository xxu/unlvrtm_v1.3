! $Id: spectra_mod.f, v1.0 2012/07/17 12:42:41 CST xxu
      MODULE SPECTRA_MOD
!
!******************************************************************************
!  Module SPECTRA_MOD contains routines to address the spectra
!  parameters, i.e. wavelength, wavenumber, etc.
!  (xxu, 7/17/2012, 7/17/2012)
!
!  Module Variables:
!  ============================================================================
!  (1 ) NLAMDA      (INTEGER): # of spectra 
!  (2 ) LAMDAS      (REAL*8 ): Wavelengths [nm]
!  (3 ) WAVENUMS    (REAL*8 ): Wave numbers [cm^-1]
!
!  Module Routines:
!  ============================================================================ 
!  (1 ) GET_SPECTRA          : 
!  (2 ) INIT_SPECTRA         :
!  (3 ) CLEANUP_SPECTRA      : 
!  
!******************************************************************************
!
      IMPLICIT NONE

      !=================================================================
      ! MODULE PRIVATE DECLARATIONS -- keep certain internal variables 
      ! and routines from being seen outside "spectra_mod.f"
      !=================================================================

      ! Make everything PRIVATE ...
      PRIVATE

      ! ... except these routines
      PUBLIC                :: GET_SPECTRA
      PUBLIC                :: CLEANUP_SPECTRA

      ! ... and these variables
      PUBLIC                :: LREAD_SPECTRA, LINDIVIDUAL_SPECTRA
      PUBLIC                :: NLAMDA
      PUBLIC                :: LAMDAS
      PUBLIC                :: WAVENUMS
      PUBLIC                :: NREAL0
      PUBLIC                :: NIMAG0
      PUBLIC                :: SURFACE_SPECTRA

      ! Module variables
      LOGICAL               :: LREAD_SPECTRA, LINDIVIDUAL_SPECTRA
      INTEGER               :: NLAMDA
      REAL*8, ALLOCATABLE   :: LAMDAS(:)
      REAL*8, ALLOCATABLE   :: WAVENUMS(:) 
      REAL*8, ALLOCATABLE   :: NREAL0(:,:)
      REAL*8, ALLOCATABLE   :: NIMAG0(:,:)
      REAL*8, ALLOCATABLE   :: SURFACE_SPECTRA(:,:)

      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !=================================================================
      CONTAINS

!-------------------------------------------------------------------------------

      SUBROUTINE GET_SPECTRA
!
!******************************************************************************
!  Subroutine GET_SPECTRA calculates the spectral wavelength and
!  wavenumbers based on the inputed starting and end wavelength as well
!  as the spectral resoultion.
!  (xxu, 7/17/12)
!
!  NOTES:
!  (1 ) Now the spectral step can be wavenumber if SPECTRA_STEP is
!  negative. (xxu, 7/17/12)
!******************************************************************************
!
      ! Refereces to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : LAMDAb,       LAMDAe
      USE NAMELIST_ARRAY_MOD, ONLY : NLAMDA_NMLST, LAMDAS_NMLST
      USE NAMELIST_ARRAY_MOD, ONLY : SPECTRA_STEP
      USE NAMELIST_ARRAY_MOD, ONLY : LPRT
      USE ERROR_MOD,          ONLY : ERROR_STOP

      ! Local parameters (~0.01 nm or 0.1 cm^-1)
      REAL*8, PARAMETER     :: MIN_LAMDA_STEP = 1d-2
      REAL*8, PARAMETER     :: MIN_WAVEN_STEP = 1d-1

      ! Local variables
      INTEGER               :: I
      INTEGER               :: IDCASE
      CHARACTER(LEN=255)    :: ERRMSG

      !================================================================
      ! GET_SPECTRA begins here!
      !================================================================

      WRITE(6, 100)
 100  FORMAT( ' - GET_SPECTRA: calculate spectral parameters' )

      ! Determine if needs to read spectrum data
      IF ( NLAMDA_NMLST == 1 .AND. LAMDAb == -1.0d0 ) THEN
         LREAD_SPECTRA = .TRUE.
      ELSE
         LREAD_SPECTRA = .FALSE.
      ENDIF

      ! Get the index for each case
      IF ( LREAD_SPECTRA ) THEN 
         IDCASE = -1 
      ELSE
         IF ( SPECTRA_STEP == 0 .OR. NLAMDA_NMLST == 1 ) IDCASE = 1
         IF ( SPECTRA_STEP > 0 .AND. NLAMDA_NMLST == 2 ) IDCASE = 2
         IF ( SPECTRA_STEP < 0 .AND. NLAMDA_NMLST == 2 ) IDCASE = 3 
         IF ( NLAMDA_NMLST > 2                         ) IDCASE = 4
      ENDIF

      ! Case statement for spectrum scenarios
      SELECT CASE ( IDCASE )  

      !================================================================
      ! Reads spectrum
      !================================================================
      CASE( -1 )
         
         CALL READ_SPECTRA  

      !================================================================
      ! Monospectral
      !================================================================
      ! IF ( SPECTRA_STEP == 0 .OR. NLAMDA_NMLST == 1 ) THEN
      CASE ( 1 ) 

         ! Single wavelength
         NLAMDA = 1

         ! Allocate arrays
         CALL INIT_SPECTRA

         ! Use the satrting wavelength
         LAMDAS(1)   = LAMDAb
         WAVENUMS(1) = 1d7 / LAMDAS(1)  


      !================================================================
      ! Equal wavelength interval
      !================================================================
      !IF ( SPECTRA_STEP > 0 .AND. NLAMDA_NMLST == 2 ) THEN
      CASE ( 2 )

         ! Check the minimum wavelength interval
         IF ( SPECTRA_STEP < MIN_LAMDA_STEP ) THEN
            ERRMSG = "SPECTRA_STEP exceeds minimum 0.01 nm"
            CALL ERROR_STOP( ERRMSG, 'spectra_mod.f' )
         ENDIF


         ! # of wavelength 
         NLAMDA = CEILING( ( LAMDAe - LAMDAb ) / SPECTRA_STEP )
         NLAMDA = NLAMDA + 1         

         ! Allocate arrays 
         CALL INIT_SPECTRA

         DO I = 1, NLAMDA

            LAMDAS(I)   = LAMDAb + SPECTRA_STEP * REAL(I-1,8)
            WAVENUMS(I) = 1d7 / LAMDAS(I)

         ENDDO

      !================================================================
      ! Equal wavenumber interval
      !================================================================
      !IF ( SPECTRA_STEP < 0 .AND. NLAMDA_NMLST == 2 ) THEN 
      CASE( 3 ) 

         ! Check the minimum wavelength interval
         IF ( ABS(SPECTRA_STEP) < MIN_WAVEN_STEP ) THEN
            ERRMSG = "SPECTRA_STEP exceeds minimum 0.1 cm^-1"
            CALL ERROR_STOP( ERRMSG, 'spectra_mod.f' )
         ENDIF

         ! # of wavelength
         NLAMDA = CEILING( (1d7/LAMDAb-1d7/LAMDAe) / ABS(SPECTRA_STEP) )
         NLAMDA = NLAMDA + 1

         ! Allocate arrays
         CALL INIT_SPECTRA

         DO I = 1, NLAMDA 
            WAVENUMS(I) = 1d7 / LAMDAb + SPECTRA_STEP * REAL(I-1,8)
            LAMDAS(I)   = 1d7 / WAVENUMS(I)
         ENDDO

      !================================================================
      ! Direct inputs of multi-spectra
      !================================================================
      !IF ( NLAMDA_NMLST > 2 ) THEN
      CASE( 4 )

         NLAMDA = NLAMDA_NMLST

         ! Allocate arrays
         CALL INIT_SPECTRA

         DO I = 1, NLAMDA

            LAMDAS(I)   = LAMDAS_NMLST(I)
            WAVENUMS(I) = 1d7 / LAMDAS(I)

         ENDDO

      ! End of Case statement
      END SELECT

      ! Screen printing
      IF ( LPRT ) THEN
        
         WRITE(6, 110 ) 
         DO I = 1, NLAMDA 
            WRITE(6, 120 ) I, LAMDAS(I), WAVENUMS(I)
         ENDDO 

      ENDIF

      LAMDAb = LAMDAS(1)
      LAMDAe = LAMDAS(NLAMDA) 

      ! Determine of needs to seperate the whole spectrum into
      ! individules:
      IF ( LAMDAe - LAMDAb > 400 ) THEN
         LINDIVIDUAL_SPECTRA = .TRUE.
      ELSE
         LINDIVIDUAL_SPECTRA = .FALSE.
      ENDIF

      ! Give a warning if the wavelength is out of range
      IF ( LAMDAb < 200 .OR. LAMDAe > 4000 ) THEN
         PRINT*, LAMDAb, LAMDAe
         ERRMSG = "Wavelenth out of range [200-4000nm]"
         CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
      ENDIF

      ! Ending lamda must be larger or equal to the start one
      IF ( LAMDAb > LAMDAe ) THEN
         PRINT*, LAMDAb, LAMDAe
         ERRMSG = "Starting lamda is larger than ending one"
         CALL ERROR_STOP( ERRMSG, 'namelist_mod.f' )
      ENDIF       

      ! Formats
 110  FORMAT( " index#, lamda[nm],  wavenumber[cm^-1]" )
 120  FORMAT( I6, 2F12.4 )

      ! Return the calling routine
      END SUBROUTINE GET_SPECTRA

!-------------------------------------------------------------------------------

      SUBROUTINE READ_SPECTRA

      ! References to F90 modules
      USE ERROR_MOD,          ONLY : ERROR_STOP
      USE NAMELIST_ARRAY_MOD, ONLY : NMODE,  LPRT

      ! Local variables
      INTEGER, PARAMETER :: IU_SPECTRA = 36
      INTEGER            :: IU, IOS 
      CHARACTER*255      :: FILENAME = 'spectra.dat'
      CHARACTER*255      :: MESSAGE
      INTEGER            :: I, J
      REAL*4             :: ONELINE(8) 

      !=================================================================
      ! READ_SPECTRA begins here!
      !=================================================================
      WRITE(6,100) 
 100  FORMAT(2X,'-READ_SPECTRA: read data from spectra.dat')

      ! Open file
      IU = IU_SPECTRA 
      OPEN(UNIT=IU, FILE=TRIM(FILENAME), STATUS='OLD', IOSTAT=IOS )
      IF ( IOS /= 0 ) THEN
         MESSAGE = 'Fail to open file: '//TRIM( FILENAME )
         CALL ERROR_STOP( MESSAGE, 'spectra_mod.f' )
      ENDIF

      ! Read comments lines, 10 lines in total
      DO J = 1, 10
         READ(UNIT=IU,FMT=*)
      ENDDO

      ! Read number of spectrum 
      READ(UNIT=IU,FMT=*) NLAMDA

      ! allocate module arrays
      CALL INIT_SPECTRA
      IF ( LPRT ) WRITE(6,110) NLAMDA

      ! Now read the spectra and refractive indices
      DO I = 1, NLAMDA

         READ(UNIT=IU,FMT=*) ONELINE(1:8)

         ! Copy read values to variables
         LAMDAS(I)   = ONELINE(1)
         NREAL0(I,1) = ONELINE(2)
         NIMAG0(I,1) = ONELINE(3)
         NREAL0(I,2) = ONELINE(4)
         NIMAG0(I,2) = ONELINE(5)
         SURFACE_SPECTRA(I,1:3) = ONELINE(6:8)

         ! Wavenumber
         WAVENUMS(I) = 1d7 / LAMDAS(I)

        IF ( LPRT ) WRITE(6,120) LAMDAS(I),
     &                       NREAL0(I,1), NIMAG0(I,1),
     &                       NREAL0(I,2), NIMAG0(I,2),
     &                       SURFACE_SPECTRA(I,1:3)

      ENDDO

      ! Close file
      CLOSE( UNIT=IU )

      ! Formats
 110  FORMAT( 2X, 'NLAMDA = ', I4 )
 120  FORMAT( F11.4, 2(F8.3, E11.3), 3F8.3 )

      END SUBROUTINE READ_SPECTRA

!-------------------------------------------------------------------------------

      SUBROUTINE INIT_SPECTRA
!
!******************************************************************************
!  Subroutine INIT_SPECTRA allocate module variables
!  (xxu, 7/17/11)
!******************************************************************************
!
      ! Refereced to F90 modules
      USE ERROR_MOD,          ONLY : ALLOC_ERR

      ! Local variables
      INTEGER            :: AS
      LOGICAL, SAVE      :: IS_INIT = .FALSE.

      ! Return if we have already initialized
      IF ( IS_INIT ) RETURN 

      !=================================================================
      ! INIT_SPECTRA begins here!
      !=================================================================

      ! LAMDAS
      ALLOCATE( LAMDAS(NLAMDA), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'LAMDAS' )
      LAMDAS = 0d0

      ! WAVENUMS
      ALLOCATE( WAVENUMS(NLAMDA), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'WAVENUMS' )
      WAVENUMS = 0d0

      ! NREAL0
      ALLOCATE( NREAL0(NLAMDA,2), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'NREAL0' )
      NREAL0 = 0d0

      ! NIMAG0
      ALLOCATE( NIMAG0(NLAMDA,2), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'NIMAG0' )
      NIMAG0 = 0d0

      ! SURFACE_SPECTRA
      ALLOCATE( SURFACE_SPECTRA(NLAMDA,3), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'SURFACE_SPECTRA' )
      SURFACE_SPECTRA = 0d0

      ! Reset IS_INIT so we do not allocate arrays again
      IS_INIT = .TRUE.

      WRITE(6,100)
 100  FORMAT(" - INIT_SPECTRA: initialize spectra arrays" )

      ! Return to the calling routine
      RETURN

      END SUBROUTINE INIT_SPECTRA

!-------------------------------------------------------------------------------

      SUBROUTINE CLEANUP_SPECTRA

      !=================================================================
      ! CLEANUP_SPCETRA begins here!
      !=================================================================

      ! Deallocates all module arrays
      IF ( ALLOCATED( LAMDAS          ) ) DEALLOCATE( LAMDAS          )
      IF ( ALLOCATED( WAVENUMS        ) ) DEALLOCATE( WAVENUMS        )
      IF ( ALLOCATED( NREAL0          ) ) DEALLOCATE( NREAL0          ) 
      IF ( ALLOCATED( NIMAG0          ) ) DEALLOCATE( NIMAG0          )
      IF ( ALLOCATED( SURFACE_SPECTRA ) ) DEALLOCATE( SURFACE_SPECTRA )

      END SUBROUTINE CLEANUP_SPECTRA

!-------------------------------------------------------------------------------

      ! End of module 
      END MODULE SPECTRA_MOD
