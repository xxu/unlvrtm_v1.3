! $id: brdf_mod.f, v1.0 2013/04/22 xxu Exp      
      MODULE BRDF_MOD
!******************************************************************************
!  Module BRDF_MOD contains routine to deal with BRDF calculations.
!  (xxu, 4/22/13)
! 
!  Module Variables:
!  ============================================================================
!  (1 ) BRDFVAL
!  (2 ) BRDFJCB
! 
!  Module Routines
!  ============================================================================
!  (1 ) RUN_VBRDF
!  (2 ) PREPARE_VBRDF_INPUTS
!  (3 ) INIT_VBRDF
!  (4 ) CLEANUP_VBRDF
!
!  REVISION HISTROY:
!   
!******************************************************************************
!
      IMPLICIT NONE

      !=================================================================
      ! MODULE PRIVATE DECLARATIONS -- keep certain internal variables 
      ! and routines from being seen outside "brdf_mod.f"
      !=================================================================

      ! Make everything PRIVATE ...
      PRIVATE

      ! ... except these routines
      PUBLIC :: RUN_VBRDF
      PUBLIC :: INIT_VBRDF
      PUBLIC :: CLEANUP_VBRDF

      ! ... and these variables
      PUBLIC :: BRDFVAL
      PUBLIC :: BRDFJCB

      ! Module variables
      REAL*8, ALLOCATABLE    :: BRDFVAL(:,:,:,:)
      REAL*8, ALLOCATABLE    :: BRDFJCB(:,:,:,:,:)

      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !=================================================================
      CONTAINS

!------------------------------------------------------------------------------

      SUBROUTINE RUN_VBRDF ( VBRDF_SUP_IN, VBRDF_LINSUP_IN, 
     &                       VBRDF_SUP_OUT, VBRDF_LINSUP_OUT )
!
!
!******************************************************************************
! Routine RUN_VBRDF runs BRDF models and output variables required by
! the VLIDORT model.
! (xxu, 4/22/13)
!
!******************************************************************************
!
      ! Reference to F90 modules
      USE VBRDF_SUP_MOD
      USE VBRDF_LINSUP_MOD
      USE VLIDORT_PARS,       ONLY : MAX_SURFACEWFS
      USE NAMELIST_ARRAY_MOD, ONLY : N_THETA0,   THETA0
      USE NAMELIST_ARRAY_MOD, ONLY : N_THETA,    THETA
      USE NAMELIST_ARRAY_MOD, ONLY : N_PHI,      PHI
      USE NAMELIST_ARRAY_MOD, ONLY : N_STOKS,    LPRT

      ! Arguments
      TYPE ( VBRDF_SUP_OUTPUTS    ) :: VBRDF_SUP_OUT
      TYPE ( VBRDF_LINSUP_OUTPUTS ) :: VBRDF_LINSUP_OUT     

      ! Local structures
      TYPE ( VBRDF_SUP_INPUTS     ) :: VBRDF_SUP_IN
      TYPE ( VBRDF_LINSUP_INPUTS  ) :: VBRDF_LINSUP_IN
      !TYPE ( VBRDF_SUP_OUTPUTS    ) :: VBRDF_SUP_OUT
      !TYPE ( VBRDF_LINSUP_OUTPUTS ) :: VBRDF_LINSUP_OUT

      ! Local variables
      LOGICAL ::          DO_DEBUG_RESTORATION
      INTEGER ::          BS_NMOMENTS_INPUT
      INTEGER ::          I, J, K

      !================================================================
      ! RUN_VBRDF begins here
      !================================================================
      WRITE(6,100)
 100  FORMAT(2X "-RUN_VBRDF: driver for BRDF models")

      ! Get prepared inputs for BRDF model
      CALL PREPARE_VBRDF_INPUTS( VBRDF_SUP_IN, VBRDF_LINSUP_IN )

      ! Do not want debug restoration
      DO_DEBUG_RESTORATION = .FALSE.

      ! A normal calculation will require
      BS_NMOMENTS_INPUT = 2 * VBRDF_SUP_IN%BS_NSTREAMS - 1

      ! Linearized VBRDF call
      ! The output will now be used for the Main VLIDORT calculation
      CALL VBRDF_LIN_MAINMASTER ( 
     &      DO_DEBUG_RESTORATION,  ! Inputs
     &      BS_NMOMENTS_INPUT,     ! Inputs
     &      VBRDF_SUP_IN,          ! Inputs
     &      VBRDF_LINSUP_IN,       ! Inputs
     &      VBRDF_SUP_OUT,         ! Outputs
     &      VBRDF_LINSUP_OUT )     ! Outputs

      ! Copy VBRDF outputs to module variables
      BRDFVAL(1,1:N_THETA,1:N_PHI,1:N_THETA0) = 
     &   VBRDF_SUP_OUT%BS_EXACTDB_BRDFUNC
     &   (1,1:N_THETA,1:N_PHI,1:N_THETA0)
      BRDFJCB(:,1,1:N_THETA,1:N_PHI,1:N_THETA0) = 
     &   VBRDF_LINSUP_OUT%BS_LS_EXACTDB_BRDFUNC
     &   (:,1,1:N_THETA,1:N_PHI,1:N_THETA0)

      ! screen print
      IF ( LPRT ) THEN 
         WRITE(6,110) 
         DO K = 1, N_THETA0
         DO I = 1, N_THETA
         DO J = 1, N_PHI
            WRITE(6,120) THETA0(K), THETA(I), PHI(J), BRDFVAL(1,I,J,K)
         ENDDO
         ENDDO
         ENDDO
      ENDIF

 110  FORMAT(2X,"SZA, VZA, AZM, BRDF")
 120  FORMAT(2X, 3F8.2, 1P4E11.3)

      ! Return to calling routine
      END SUBROUTINE RUN_VBRDF

!------------------------------------------------------------------------------

      SUBROUTINE PREPARE_VBRDF_INPUTS( VBRDF_SUP_IN, 
     &                                 VBRDF_LINSUP_IN )
!
!******************************************************************************
! Routine PREPARE_VBRDF_INPUTS prepare BDRDF inputing variables
! (xxu, 4/22/13)
!
!******************************************************************************
!
      ! Reference to F90 modules
      USE VBRDF_SUP_MOD
      USE VBRDF_LINSUP_MOD 
      !USE VLIDORT_PARS
      USE NAMELIST_ARRAY_MOD, ONLY : N_THETA0,   THETA0
      USE NAMELIST_ARRAY_MOD, ONLY : N_THETA,    THETA
      USE NAMELIST_ARRAY_MOD, ONLY : N_PHI,      PHI
      USE NAMELIST_ARRAY_MOD, ONLY : N_STOKS, N_STRMS
      USE NAMELIST_ARRAY_MOD, ONLY : LLAMBERTIAN
      USE NAMELIST_ARRAY_MOD, ONLY : LBRDF
      USE NAMELIST_ARRAY_MOD, ONLY : SURF_REFL
      USE NAMELIST_ARRAY_MOD, ONLY : N_BRDF,       BRDF_NPARS
      USE NAMELIST_ARRAY_MOD, ONLY : BRDF_NAMES,   BRDF_INDEX
      USE NAMELIST_ARRAY_MOD, ONLY : BRDF_FACTOR,  BRDF_PAR
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_BRDF_FACTOR
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_BRDF_PAR

      ! Arguments
      TYPE ( VBRDF_SUP_INPUTS     ) :: VBRDF_SUP_IN
      TYPE ( VBRDF_LINSUP_INPUTS  ) :: VBRDF_LINSUP_IN

      ! Local variables
      INTEGER                :: I

      !================================================================
      ! PREPARE_VBRDF_INPUTS begins here!
      !================================================================
      WRITE(6,100) 
 100  FORMAT(2X "-PREPARE_VBRDF_INPUTS: prepare BRDF Model inputs")

      !================================================================
      ! Structure VBRDF_SUP_INPUTS from vbrdf_sup_inputs_def.f90
      !================================================================
      VBRDF_SUP_IN%BS_DO_USER_STREAMS       = .TRUE.
      VBRDF_SUP_IN%BS_DO_BRDF_SURFACE       = LBRDF
      VBRDF_SUP_IN%BS_DO_SURFACE_EMISSION   = .FALSE.
      VBRDF_SUP_IN%BS_NSTOKES               = N_STOKS
      VBRDF_SUP_IN%BS_NSTREAMS              = N_STRMS
      VBRDF_SUP_IN%BS_NBEAMS                = N_THETA0
      VBRDF_SUP_IN%BS_BEAM_SZAS(1:N_THETA0) = THETA0(1:N_THETA0)
      VBRDF_SUP_IN%BS_N_USER_RELAZMS        = N_PHI
      VBRDF_SUP_IN%BS_USER_RELAZMS(1:N_PHI) = PHI(1:N_PHI)
      VBRDF_SUP_IN%BS_N_USER_STREAMS        = N_THETA
      VBRDF_SUP_IN%BS_USER_ANGLES_INPUT(1:N_THETA) = THETA(1:N_THETA)
 
      ! Need updates corresponding to namelist
      VBRDF_SUP_IN%BS_N_BRDF_KERNELS        = N_BRDF
      VBRDF_SUP_IN%BS_BRDF_NAMES(1:N_BRDF)  = BRDF_NAMES(1:N_BRDF)
      VBRDF_SUP_IN%BS_WHICH_BRDF(1:N_BRDF)  = BRDF_INDEX(1:N_BRDF)
      VBRDF_SUP_IN%BS_N_BRDF_PARAMETERS(1:N_BRDF)  =
     &                                        BRDF_NPARS(1:N_BRDF)

      ! For Li-Sparse kernal, parameters are: h/b, and b/r, which are
      ! correspondingly assumed 2 and 1 by Lucht (2000)
      VBRDF_SUP_IN%BS_BRDF_PARAMETERS(1,1:N_BRDF) = BRDF_PAR(1,1:N_BRDF)
      VBRDF_SUP_IN%BS_BRDF_PARAMETERS(2,1:N_BRDF) = BRDF_PAR(2,1:N_BRDF)
      VBRDF_SUP_IN%BS_BRDF_PARAMETERS(3,1:N_BRDF) = BRDF_PAR(3,1:N_BRDF)
      VBRDF_SUP_IN%BS_LAMBERTIAN_KERNEL_FLAG = LLAMBERTIAN
      VBRDF_SUP_IN%BS_BRDF_FACTORS(1:N_BRDF) = BRDF_FACTOR(1:N_BRDF)
      VBRDF_SUP_IN%BS_NSTREAMS_BRDF          = 100
      VBRDF_SUP_IN%BS_DO_SHADOW_EFFECT       = .TRUE.
      VBRDF_SUP_IN%BS_DO_EXACTONLY           = .FALSE.
      VBRDF_SUP_IN%BS_DO_GLITTER_MSRCORR     = .FALSE.
      VBRDF_SUP_IN%BS_DO_GLITTER_MSRCORR_EXACTONLY = .FALSE.
      VBRDF_SUP_IN%BS_GLITTER_MSRCORR_ORDER  = 0
      VBRDF_SUP_IN%BS_GLITTER_MSRCORR_NMUQUAD = 40
      VBRDF_SUP_IN%BS_GLITTER_MSRCORR_NPHIQUAD = 100

      !================================================================
      ! Structure from vbrdf_lin_sup_inputs_def.f90
      !================================================================
      IF ( LJACOB_BRDF_FACTOR ) THEN 
         VBRDF_LINSUP_IN%BS_DO_KERNEL_FACTOR_WFS(1:N_BRDF) = .TRUE.
         VBRDF_LINSUP_IN%BS_N_KERNEL_FACTOR_WFS            = N_BRDF
      ENDIF
      VBRDF_LINSUP_IN%BS_DO_KERNEL_PARAMS_WFS(:,:) = .FALSE. 
      VBRDF_LINSUP_IN%BS_DO_KPARAMS_DERIVS(:)      = .FALSE.
      VBRDF_LINSUP_IN%BS_N_KERNEL_PARAMS_WFS       = 0
 
      VBRDF_LINSUP_IN%BS_N_SURFACE_WFS
     &       =  VBRDF_LINSUP_IN%BS_N_KERNEL_FACTOR_WFS
     &       + VBRDF_LINSUP_IN%BS_N_KERNEL_PARAMS_WFS

      ! Screen print
      DO I = 1, N_BRDF
         WRITE(6,110) VBRDF_SUP_IN%BS_BRDF_NAMES(I), 
     &                VBRDF_SUP_IN%BS_WHICH_BRDF(I),
     &                VBRDF_SUP_IN%BS_BRDF_FACTORS(I),
     &                VBRDF_SUP_IN%BS_N_BRDF_PARAMETERS(I),
     &                VBRDF_SUP_IN%BS_BRDF_PARAMETERS(I,1:3)
      ENDDO
110   FORMAT( A10, 1X, I2, F8.4, I4, 1X, 3F8.3 )

      ! End of routine
      END SUBROUTINE PREPARE_VBRDF_INPUTS

!------------------------------------------------------------------------------

      SUBROUTINE INIT_VBRDF ( RESET_IS_INIT )

      ! References to F90 modules 
      USE ERROR_MOD,          ONLY : ALLOC_ERR
      USE VLIDORT_PARS,       ONLY : MAX_SURFACEWFS  
      USE NAMELIST_ARRAY_MOD, ONLY : N_THETA, N_PHI, N_THETA0
      USE NAMELIST_ARRAY_MOD, ONLY : N_STOKS

      ! Arguments
      LOGICAL, OPTIONAL      :: RESET_IS_INIT

      ! Local Variables
      INTEGER                :: AS
      LOGICAL, SAVE          :: IS_INIT = .FALSE.

      ! Reset IS_INIT as false when clean up, and return
      IF ( PRESENT( RESET_IS_INIT ) ) THEN
         IS_INIT = .FALSE.
         PRINT*, ' INIT_VBRDF: IS_INIT is reset to FALSE'
         RETURN
      ENDIF

      ! Return if we have already initialized
      IF ( IS_INIT ) RETURN

      !=================================================================
      ! INIT_INV_OBS begins here!
      !=================================================================
      WRITE(6, 100)
 100  FORMAT( " - INIT_VBRDF: Initialize the VBRDF arrays" )


      ! BRDFVAL
      ALLOCATE( BRDFVAL(N_STOKS,N_THETA,N_PHI,N_THETA0), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'BRDFVAL' )
      BRDFVAL = 0d0

      ! BRDFJCB
      ALLOCATE( BRDFJCB(MAX_SURFACEWFS,N_STOKS,N_THETA,N_PHI,N_THETA0),
     &          STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'BRDFJCB' )
      BRDFJCB = 0d0

      ! Reset IS_INIT so we do not allocate arrays again
      IS_INIT = .TRUE.

      END SUBROUTINE INIT_VBRDF

!------------------------------------------------------------------------------

      SUBROUTINE CLEANUP_VBRDF

      ! Local variables
      LOGICAL                :: RESET_IS_INIT = .TRUE.

      !  clean up the output arrays of this module
      IF ( ALLOCATED( BRDFVAL      ) ) DEALLOCATE ( BRDFVAL      )
      IF ( ALLOCATED( BRDFJCB      ) ) DEALLOCATE ( BRDFJCB      )

      ! Reset IS_INIT in routine INIT_VBRDF
      CALL INIT_VBRDF( RESET_IS_INIT )

      END SUBROUTINE CLEANUP_VBRDF

!------------------------------------------------------------------------------

      ! End of module
      END MODULE BRDF_MOD
