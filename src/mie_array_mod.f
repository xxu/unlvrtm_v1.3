! $Id: mie_array_mod.f, v 1.1 2009/12/01 12:00:00 xxu Exp $
      MODULE MIE_ARRAY_MOD
!
!******************************************************************************
!  This Module includes the parameters and variables for Linearized Mie code.
!  Code is modified form the header file 'LMie.PARS'
!  (xxu, 12/1/09)
!  
!  Module Variables:
!  ============================================================================
! Dimenisons:
!  (1 ) max_Mie_angles        (I) : Maximum # of angles
!  (2 ) max_Mie_sizes         (I) : Maximum # of size blocks
!  (3 ) max_Mie_points        (I) : Maximum # of Mie integral points
!  (4 ) max_Mie_distpoints    (I) : Maximum # of Ditribution points
! Schemes:
!  (5 ) do_external_angles    (L) : Switch on/off for using external angles
!  (6 ) do_coeffct_angles     (L) : Switch on/off for calculating # of angles
!  (7 ) do_m_derivatives      (L) : Switch on/off for refractive index linearization
! Parameter inputs:
!  (8 ) idis                  (I) : Index for dize distribution from 1 to 8
!  (9 ) nr_parameters         (R) : Size parameters corresponding to 'idis'
!  (10) do_nr_derivatives     (L) : Switch on/off for size parameter linearization
!  (11) nblocks               (I) : # of integration blocks
!  (12) nweights              (I) : # of quadrature weights for integration block
!  (13) cutoff                (R) : Distribution cutoff values, if set negatvie...
!  (14) n_external_angles     (I) : # of user spicified angles
!  (15) external_angle_cosines(R) : Cosine values of user spicified angles
!  (16) startup               (L) : Switch on/off for doing Mie calculation
!  (17) n_coeffct_angles      (I) : # of angles for coefficient calculation
!  (18) coeff_cosines         (R) : Cosine values for all angles
!  (19) coeff_weights         (R) : Quadrature weights for all angles
!  (20) m_complex             (X) : Complex refractive index
!  (21) wavelength            (R) : Wavelength in micron
!  (22) xparticle_limit       (R) : Upper limit on partile size
! Outputs By Mie_MAIN:
!  (23) MIE_BULK              (R) : Claculated Mie parameters
!  (24) MIE_BULK_D            (R) : Linearized Mie parameters
!  (25) DIST                  (R) : Calculated distribution parameters
!  (26) DIST_D                (R) : Linearized distribution parameters
!  (27) FMAT                  (R) : F-matrix
!  (28) FMAT_D                (R) : Linearized F-matrix
!  (29) failmie               (L) : Subroutine falure flag
!  (30) message               (A) : Debug output/failure message
!  (31) trace                 (A) : Debug output/failure message
!  (32) action                (A) :
!  (33) rmin                  (R) : Upper size range in micron
!  (34) rmax                  (R) : Lower size range in micron
! Outputs by DEVELOP_D: 
!  (35) expcoeffs             (R) : Expanded greek matrix coefficients
!  (36) expcoeffs_d           (R) : Linearization of greek matrix coefficients
!
!******************************************************************************
!
      ! Referenced to Mie lib modules
      USE MIE_PRECISION
      USE MIE_constants,    ONLY : d_zero, d_half, d_one, d_two,
     &                             d_three, c_i      

      !================================================================
      ! Dimensioning inputs
      !================================================================
      INTEGER, PARAMETER :: max_Mie_angles = 1001
      INTEGER, PARAMETER :: max_Mie_sizes  = 20
      INTEGER, PARAMETER :: max_Mie_points = 600 
      INTEGER, PARAMETER :: max_Mie_distpoints = 20

      !================================================================
      ! Code setting
      !================================================================
      LOGICAL, PARAMETER :: do_external_angles = .FALSE.
      LOGICAL, PARAMETER :: do_coeffct_angles  = .TRUE.
      LOGICAL, PARAMETER :: do_m_derivatives   = .TRUE.

      !================================================================
      ! Mie inputs
      !================================================================
      INTEGER            :: idis = 4  ! Log-normal in default
      REAL   (KIND=dp)   :: nr_parameters(3)
      LOGICAL            :: do_nr_derivatives(3) =
     &                       (/.TRUE., .TRUE., .FALSE./)
      INTEGER, PARAMETER :: nblocks  = 100
      INTEGER, PARAMETER :: nweights = 10
      REAL   (KIND=dp), PARAMETER ::  cutoff = -999D0

      INTEGER, PARAMETER :: n_external_angles = 19
      REAL   (KIND=dp)   :: external_angle_cosines(max_Mie_angles)

      LOGICAL            :: startup
      INTEGER            :: n_coeffct_angles
      REAL   (KIND=dp)   :: coeff_cosines(max_Mie_angles)
      REAL   (KIND=dp)   :: coeff_weights(max_Mie_angles)

      COMPLEX(KIND=dp)   :: m_complex
      REAL   (KIND=dp)   :: wavelength
      REAL   (KIND=dp), PARAMETER :: xparticle_limit = 1000

      !================================================================
      !  Output variables
      !================================================================

      REAL   (KIND=dp)   :: MIE_BULK(4), MIE_BULK_D(4,5)
      REAL   (KIND=dp)   :: DIST(5), DIST_D(5,3)
      REAL   (KIND=dp)   :: FMAT(4,max_Mie_angles)
      REAL   (KIND=dp)   :: FMAT_D(4,5,max_Mie_angles)

      LOGICAL            :: failmie
      CHARACTER          :: message, trace, action
      REAL   (KIND=dp)   :: rmin, rmax

      !================================================================
      ! Used for routine "DEVELOP"
      !================================================================
      REAL   (KIND=dp)   :: expcoeffs(6,0:max_Mie_angles)
      REAL   (KIND=dp)   :: expcoeffs_d(6,5,0:max_Mie_angles)

      CONTAINS

!------------------------------------------------------------------------------

      SUBROUTINE DEBUG_MIE_CALCULATION

      ! Referenced to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : N_MOMENTS

      ! Local variables
      INTEGER :: II
      INTEGER :: IU

      IU = 6

      ! Debug output of mie calculation
      WRITE(IU,*) ' '
      WRITE(IU,*) '**************************************************'
      WRITE(IU,*) '********** L I N E A R I Z E D     M I E *********'
      WRITE(IU,*) '**************************************************'
      WRITE(IU,110) '==> Wavelength[um]     :', Wavelength
      WRITE(IU,120) '==> Refractive Index   :', m_complex
      WRITE(IU,110) '==> Extinction Coeff   :', mie_bulk(1)
      WRITE(IU,110) '==> Scattering Coeff   :', mie_bulk(2)
      WRITE(IU,110) '==> Extinction XSec    :', mie_bulk(1)*dist(2)
      WRITE(IU,110) '==> Scattering XSec    :', mie_bulk(2)*dist(2)
      WRITE(IU,110) '==> Asymmetry Parameter:', mie_bulk(3)
      WRITE(IU,110) '==> Single Sca Albedo  :', mie_bulk(4)
      WRITE(IU,110) '==> Total Density      :', dist(1)
      WRITE(IU,110) '==> Geometric XSec     :', dist(2)
      WRITE(IU,110) '==> Distribution Volume:', dist(3)
      WRITE(IU,110) '==> Effective Radius   :', dist(4)
      WRITE(IU,110) '==> Effective Variance :', dist(5)
      WRITE(IU,110) '==> Geometric Radius   :', nr_parameters(1)
      WRITE(IU,120) '==> Min & Max Radius   :', rmin, rmax
      WRITE(IU,100) '==> n_coeffct_angles   :', n_coeffct_angles

      DO II = 1, 4
         WRITE(IU,150) '==> Mie_bulk_d         :',Mie_bulk_d(II,1:5)
         WRITE(IU,130) '==> DIST_D             :', DIST_D(II, 1:3)
      ENDDO

      WRITE(IU,*) '  '
      WRITE(IU,*) '********** F-MATRIX               ***********'
      DO II = 1, n_coeffct_angles
         WRITE(IU,180) II,  FMAT(:,II)
      ENDDO

      WRITE(IU,*) '  '
      WRITE(IU,*) '********** F-MATRIX DERIVATIVES   ***********'
      DO II = 1, n_coeffct_angles
         WRITE(IU,180) II,  FMAT_D(1,:,II)
      ENDDO

      WRITE(IU,*) '  '
      WRITE(IU,*) '********** EXPANSION COEFFICIENTS ***********'
      DO II = 0, N_MOMENTS-1
         WRITE(IU,160) II,  expcoeffs(:,II)
      ENDDO 
      WRITE(IU,*) '  '
      WRITE(IU,*) '*** Derivative of P11 w.r.t 5 parameters  ***'
      DO II = 0, N_MOMENTS-1
         WRITE(IU,170) II, expcoeffs_d(1,:,II)
      ENDDO

 100  FORMAT(A, I12  )
 110  FORMAT(A, F12.7)
 120  FORMAT(A, 2F12.7)
 130  FORMAT(A, 1P3E11.3)
 150  FORMAT(A, 1P5E11.3)
 160  FORMAT(I4, 6F11.7)
 170  FORMAT(I4, 5F11.7)
 180  FORMAT(I4, 6F12.5)
      END SUBROUTINE DEBUG_MIE_CALCULATION

!------------------------------------------------------------------------------

      ! End of module
      END MODULE MIE_ARRAY_MOD

