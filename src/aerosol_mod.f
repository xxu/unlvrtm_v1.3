!  $Id: aerosol_mod.f, v1.1 2010/12/16 12:00:00 xxu
      MODULE AEROSOL_MOD
!
!******************************************************************************
!  Module AEROSOL_MOD prepare the aerosol optical thickness for each layer
!  with certain aerosol scale height, and optical properties generated from
!  a linearized Mie code (Mie code is from Robert Spurr).
!  
!  Module Variables:
!  ============================================================================
!  (1 ) QRATIO   (REAL*8   ) : The ratio of extinction for two Mie cases [none] 
!
!  Module Routines
!  ============================================================================
!  (1 ) GET_AEROSOL_OPTICS   : Calculate aerosol loading and optical properties
!  (1 ) GET_AEROSOL_PROF     : Calculate aerosol profiles and derivatives
!  (2 ) INIT_AEROSOL         : Allocate module arrays
!  (3 ) CLEANUP_AEROSOL      : Deallocate module arrays 
!
!  NOTES:
!  (1 ) Now use the compiled library of Mie code (1/9/11,xxu)
!  (2 ) Now use a seperated module for aerosol Mie calculation, so 
!       GET_AEROSOL_MIE is removed. ( 11/29/11, xxu )
!  (3 ) Aerosol profiles implemented. ( 4/3/12, xxu )
!  (4 ) Now, we define the mode fraction as MASS fraction. The ratio of
!        aerosol volume, number, and optical depth will be calculated
!        and saved. (xxu, 7/3/12)
!  (5 ) Commented out the FMATRIX Jacobians in the module variable to 
!       save memeory, as they are not used in our model. (xxu,7/23/12)
!
!******************************************************************************
!
      IMPLICIT NONE

      ! Make everything PUBLIC ...
      PUBLIC  

      ! ... except these routines
      PRIVATE              :: GET_AEROSOL_PROF

      ! ... and these variables
      PRIVATE              :: QRATIO

      !=================================================================
      ! Module variables
      !=================================================================
      REAL*8               :: QRATIO 

      ! Aerosol loading & priofile parameters
      REAL*8,  ALLOCATABLE :: COLUMN_MASS(:)
      REAL*8,  ALLOCATABLE :: COLUMN_AOD(:)
      REAL*8,  ALLOCATABLE :: COLUMN_NUM(:)
      REAL*8,  ALLOCATABLE :: PROF_MASS(:,:)
      REAL*8,  ALLOCATABLE :: PROF_AOD(:,:)
      REAL*8,  ALLOCATABLE :: PROF_NUM(:,:)
      REAL*8,  ALLOCATABLE :: LPROF_MASS_COL(:,:)
      REAL*8,  ALLOCATABLE :: LPROF_MASS_PAR(:,:,:)
      REAL*8,  ALLOCATABLE :: LPROF_AOD_COL(:,:)
      REAL*8,  ALLOCATABLE :: LPROF_AOD_PAR(:,:,:)

      REAL*8,  ALLOCATABLE :: MASS_EFF(:)
      REAL*8,  ALLOCATABLE :: KEXT_MASS(:)
      REAL*8,  ALLOCATABLE :: KSCA_MASS(:)

      ! Aerosol single scattering properties
      INTEGER, PARAMETER   :: NPANGLE = 181
      REAL*8,  ALLOCATABLE :: ASYMM(:)
      INTEGER, ALLOCATABLE :: NCOEFFS(:)
      REAL*8,  ALLOCATABLE :: BULK_AOP(:,:)
      REAL*8,  ALLOCATABLE :: EXPCOEFFS(:,:,:)
      REAL*8,  ALLOCATABLE :: FMATRIX(:,:,:)
      REAL*8,  ALLOCATABLE :: DIST(:,:)

      REAL*8,  ALLOCATABLE :: LPSD_BULK   (:,:,:  )
      REAL*8,  ALLOCATABLE :: LPSD_ASYMM  (:,:    )
      REAL*8,  ALLOCATABLE :: LPSD_EXPCOEF(:,:,:,:)
      REAL*8,  ALLOCATABLE :: LPSD_FMATRIX(:,:,:,:)
      REAL*8,  ALLOCATABLE :: LPSD_DIST   (:,:,:  )
      REAL*8,  ALLOCATABLE :: LRFE_BULK   (:,:,:  )
      REAL*8,  ALLOCATABLE :: LRFE_ASYMM  (:,:    )
      REAL*8,  ALLOCATABLE :: LRFE_EXPCOEF(:,:,:,:)
      REAL*8,  ALLOCATABLE :: LRFE_FMATRIX(:,:,:,:)

      REAL*8,  ALLOCATABLE :: LFRC_BULK   (:,:    )
      REAL*8,  ALLOCATABLE :: LFRC_ASYMM  (:      )
      REAL*8,  ALLOCATABLE :: LFRC_EXPCOEF(:,:,:  )
      REAL*8,  ALLOCATABLE :: LFRC_FMATRIX(:,:,:  )      
      
      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !=================================================================       
      CONTAINS
  
!------------------------------------------------------------------------------

      SUBROUTINE GET_AEROSOL_OPTICS( IDLAMDA, LAMDA ) 
!
!******************************************************************************
!  Subroutine GET_AEROSOL_OPTICS calculates the aerosol single scattering 
!   properties for either single or multi-mode. 
!  (7/3/12, xxu)
!******************************************************************************
!
      ! Referenced to F90 modules
      USE NAMELIST_ARRAY_MOD,  ONLY : NMODE,       MODEFRC
      USE NAMELIST_ARRAY_MOD,  ONLY : SSMODEL,     AERDEN
      USE NAMELIST_ARRAY_MOD,  ONLY : LAOD,        LMASS
      USE NAMELIST_ARRAY_MOD,  ONLY : COLUMN_AOD0, COLUMN_MASS0 
      USE NAMELIST_ARRAY_MOD,  ONLY : N_LAYERS,    N_MOMENTS
      USE NAMELIST_ARRAY_MOD,  ONLY : LPRT,        L_REFER_AOD
      USE NAMELIST_ARRAY_MOD,  ONLY : RMRI
      USE SPECTRA_MOD,         ONLY : LREAD_SPECTRA
      USE SPECTRA_MOD,         ONLY : NREAL0,      NIMAG0
      USE AIROPTIC_MOD,        ONLY : TAUAER,      SSAAER
      USE AIROPTIC_MOD,        ONLY : PHAAER

      ! Arguments
      REAL*8, INTENT(IN)    :: LAMDA
      INTEGER, INTENT(IN)   :: IDLAMDA

      ! Local Variables
      INTEGER               :: IM
      INTEGER               :: I, J, N
      INTEGER               :: NEW_N_MOMENTS
      REAL*8                :: FRC(NMODE)
      REAL*8                :: QFACTOR
      REAL*8                :: REFER_DIST(5)
      REAL*8                :: REFER_BULK_AOP(5)
      REAL*8                :: REFER_KEXT_MASS

      ! Screen print
      WRITE(6,*) ' - GET_AEROSOL_OPTICS: aerosol optical properties'

      !================================================================
      ! GET_AEROSOL_OPTICS begins here!
      !================================================================ 

      ! IF use read spectra of refractive index
      IF ( LREAD_SPECTRA ) THEN
         RMRI(1,1:NMODE) = NREAL0(IDLAMDA,1:NMODE)
         RMRI(2,1:NMODE) = NIMAG0(IDLAMDA,1:NMODE)
      ENDIF

      ! Intialization for aerosol arrays
      CALL INIT_AEROSOL

      ! Conduct Mie or/and T-Matrix calculation for each mode
      DO IM = 1, NMODE 

         ! Linearized Mie code 
         IF ( SSMODEL(IM) == 1 ) CALL RUN_LMIE( LAMDA, IM ) 

         ! Linearized T-Matrix Code (Future developments) 
         IF ( SSMODEL(IM) == 2 ) CALL RUN_LTMATRIX( LAMDA, IM )

         !=============================================================
         ! Mass of a single particle [kg]
         ! mass = Volume X aerosol density
         ! Here the unit of each:
         !    DIST(3,IM)  : aerosol volume normalized per particle [um^3  ]
         !    AERDEN(IM)  : input aerosol density [kg/m^3]
         !    MASS_EFF(IM): effective mass [kg    ]
         !=============================================================
         MASS_EFF(IM) = DIST(3,IM) * AERDEN(IM) * 1d-18

         !=============================================================
         ! Mass extinction coefficient:
         !  k_ext_mass = 0.75 * Q_ext / (Aer_Den * r_eff)
         !=============================================================
         KEXT_MASS(IM) = 0.75 * BULK_AOP(4,IM) 
     &                  / AERDEN(IM) / DIST(4,IM) * 1d6 
 
         KSCA_MASS(IM) = 0.75 * BULK_AOP(5,IM) 
     &                  / AERDEN(IM) / DIST(4,IM) * 1d6

      ENDDO

      !================================================================ 
      ! Obtained the column modal mass based on:
      !    - specified AOD and modal AOD fraction, or
      !    - specified reference AOD and modal mass fraction, or
      !    - specified column mass and modal mass fraction.
      ! using:  
      !   AOD = M * 0.75 * Q_ext / (Aer_Den * r_eff)
      !       = M * k_ext_mass 
      !================================================================

      ! If the column AOD as input, but not using the reference AOD
      IF ( LAOD .AND. .NOT. L_REFER_AOD ) THEN

         ! Calculate column mass from column AOD
         ! Here MODEFRC is AOD fraction
         DO IM = 1, NMODE
            COLUMN_MASS(IM) = COLUMN_AOD0 * MODEFRC(IM) 
     &                      / KEXT_MASS(IM)
         ENDDO

      ! If the reference AOD is used
      ELSE IF ( LAOD .AND. L_REFER_AOD ) THEN

         ! Run the Mie code for reference AOD
         CALL RUN_LMIE_REFERENCE( REFER_DIST, REFER_BULK_AOP )

         ! mass extinction Xsec [m^2/kg]
         REFER_KEXT_MASS = 0.75 * REFER_BULK_AOP(4)
     &                    / AERDEN(1) / REFER_DIST(4) * 1d6 

         ! Column modal mass. Here MODEFRC is mass fraction
         DO IM = 1, NMODE 
            COLUMN_MASS(IM) = COLUMN_AOD0 / REFER_KEXT_MASS 
     &                      * MODEFRC(IM)
         ENDDO

      ! If the column mass as input
      ELSE IF ( LMASS ) THEN

         ! Here MODEFRC is mass fraction
         DO IM = 1, NMODE
            COLUMN_MASS(IM) = COLUMN_MASS0 * MODEFRC(IM)
         ENDDO

      ENDIF

      ! Fix mass for Finite difference test only
      !WRITE(6,"(A,1PD15.7)") '  COLUMN_MASS0 = ',
      ! &                       SUM(COLUMN_MASS(1:NMODE)

      !================================================================
      ! Distribute the total mass to each mode and calculate profile
      !================================================================
      
      DO IM = 1, NMODE

         ! Calculate mass profile and its sensitivity        
         CALL GET_AEROSOL_PROF(IM) 

         ! AOD profile based on mass profile
         ! assuming homogeneous aerosol properties for each mode
         PROF_AOD(:,IM)        = KEXT_MASS(IM) * PROF_MASS(:,IM)
         LPROF_AOD_COL(:,IM)   = KEXT_MASS(IM) * LPROF_MASS_COL(:,IM)
         LPROF_AOD_PAR(:,:,IM) = KEXT_MASS(IM) * LPROF_MASS_PAR(:,:,IM)

         ! Number profile 
         PROF_NUM(:,IM)        = PROF_MASS(:,IM) / MASS_EFF(IM)

         ! Column quantites
         COLUMN_AOD(IM)  = SUM( PROF_AOD(:,IM)  )
         COLUMN_NUM(IM)  = SUM( PROF_NUM(:,IM)  )

      ENDDO

      ! Screen print
      WRITE( 6, '(/a)') REPEAT( '-', 79 ) 
      IF ( NMODE == 1 ) WRITE(6,200) 
      IF ( NMODE == 2 ) WRITE(6,201) 
      WRITE(6,205)
      WRITE( 6, '(a)' ) REPEAT( '-', 79 )
      IF (LPRT) THEN
        DO I = 1, N_LAYERS
           WRITE(6,210) I, PROF_AOD(I,:), PROF_MASS(I,:), PROF_NUM(I,:)
        ENDDO
        WRITE( 6, '(a)') REPEAT( '-', 79 )
      ENDIF
      WRITE(6,211), COLUMN_AOD(:), COLUMN_MASS(:), COLUMN_NUM(:)
      WRITE( 6, '(a)') REPEAT( '-', 79 )

      !================================================================
      ! Assign to aerosol variables in airoptic_mod.f
      ! Currently, aerosol optical properties are assumed to be 
      ! uniform, so we only run the Mie code for the first layer.
      ! Later will be updated. (xxu, 1/13/11)
      !================================================================ 

      ! Check if the N_MOMENTS needs to be updated here
      IF ( NMODE == 1 ) NEW_N_MOMENTS = NCOEFFS(1)
      IF ( NMODE == 2 ) NEW_N_MOMENTS = MIN( NCOEFFS(1), NCOEFFS(2) )
      IF ( NEW_N_MOMENTS < N_MOMENTS ) THEN
         WRITE(6,100), N_MOMENTS, NEW_N_MOMENTS
         N_MOMENTS = NEW_N_MOMENTS
      ENDIF

      DO I = 1, N_LAYERS

         ! Assign total aerosol optical depth
         TAUAER(I,1:NMODE) = PROF_AOD(I,1:NMODE)

         ! aerosol single scattering albedo
         SSAAER(I,1:NMODE) = BULK_AOP(3,1:NMODE)

         ! Ensemble aerosol phase function expansions
         DO N = 0, N_MOMENTS - 1 
         DO J = 1, 6
            PHAAER(I,N,J,1:NMODE) =  EXPCOEFFS(J, N, 1:NMODE)
         ENDDO
         ENDDO

         IF ( LPRT ) WRITE(6,110) I, TAUAER(I,:), SSAAER(I,:)

      ENDDO

 100  FORMAT( " - N_MOMENTS is updated from ", I4, " to", I4, 
     &        "Location: GET_AEROSOL_OPTICS" )
 110  FORMAT( " Aerosol: ", I4, 1P4E12.3 )
 200  FORMAT( "#Layer      AOD            Mass          Number  " )
 201  FORMAT( "#Layer AOD(1) AOD(2) Mass(1) Mass(2) Num(1) Num(2)" )
 205  FORMAT( "  -->unit: [none]        [kg/m^2]       [#/m^2]  " ) 
 210  FORMAT( I6, 1P6E11.4 )
 211  FORMAT( "Column", 1P6E11.4 )

      ! Return to the calling routine
      RETURN

      END SUBROUTINE GET_AEROSOL_OPTICS

!------------------------------------------------------------------------------
 
      SUBROUTINE GET_AEROSOL_PROF( IMODE ) 
!
!******************************************************************************
!  Subroutine GET_AEROSOL_OD calculates the profile of aerosol. In troposhere,
!  aerosol optical thickness exponatially dcreases by altitude divided by 
!  the scale height.
!  (1/9/11, xxu)
!******************************************************************************
!
      ! Referenced to F90 modules
      USE NAMELIST_ARRAY_MOD,  ONLY : N_LAYERS
      USE NAMELIST_ARRAY_MOD,  ONLY : NMODE,  MODEFRC
      USE NAMELIST_ARRAY_MOD,  ONLY : COLUMN_AOD0
      USE NAMELIST_ARRAY_MOD,  ONLY : IDPROF, PROF_RANGE, PROF_PAR
      USE NAMELIST_ARRAY_MOD,  ONLY : LJACOB_PROF
      USE ATMPROF_MOD,         ONLY : ATMZ
      USE AEROSOL_PROFILE_MOD

      ! Arguments 
      INTEGER, INTENT(IN) :: IMODE

      ! Local variable
      REAL*8              :: FCORR
      LOGICAL             :: DO_DERIVATIVES
      REAL*8              :: HGRID(0:N_LAYERS)
      REAL*8              :: PROFILE(N_LAYERS)
      REAL*8              :: PROFILE_GDT0(N_LAYERS)
      REAL*8              :: PROFILE_GDT1(N_LAYERS)
      REAL*8              :: PROFILE_GDT2(N_LAYERS)
      LOGICAL             :: FAIL
      CHARACTER*100       :: MESSAGE, ACTION
 
      INTEGER             :: I

      !================================================================
      ! GET_AEROSOL_OD begins here!
      !================================================================ 

      ! Intialization
      CALL INIT_AEROSOL

      ! Turn on/off derivatives 
      DO_DERIVATIVES = .FALSE.
      IF ( LJACOB_PROF ) DO_DERIVATIVES = .TRUE.

      ! Height grid
      DO I = 0, N_LAYERS
         HGRID(I) = ATMZ(N_LAYERS-I+1)
      ENDDO

      ! CASE statement for aerosol profile
      SELECT CASE ( IDPROF(IMODE) )

      ! Uniform
      CASE ( 1 )
         CALL PROFILES_UNIFORM ( N_LAYERS, N_LAYERS, 
     &         HGRID, DO_DERIVATIVES, 
     &         PROF_RANGE(2,IMODE), PROF_RANGE(1,IMODE),
     &         COLUMN_MASS(IMODE), 
     &         PROFILE, PROFILE_GDT0, 
     &         FAIL, MESSAGE, ACTION ) 

      ! Exponential
      CASE ( 2 )
         CALL PROFILES_EXPONE ( N_LAYERS, N_LAYERS,
     &         HGRID, DO_DERIVATIVES,
     &         PROF_RANGE(2,IMODE), PROF_RANGE(1,IMODE), 
     &         PROF_PAR(1,IMODE), COLUMN_MASS(IMODE),
     &         PROFILE, PROFILE_GDT0, PROFILE_GDT1, 
     &         FAIL, MESSAGE, ACTION ) 

      ! GDF
      CASE ( 3 )  
         CALL PROFILES_GDFONE ( N_LAYERS, N_LAYERS,
     &         HGRID, DO_DERIVATIVES,
     &         PROF_RANGE(2,IMODE), PROF_PAR(1,IMODE), 
     &         PROF_RANGE(1,IMODE), PROF_PAR(2,IMODE),
     &         COLUMN_MASS(IMODE),
     &         PROFILE, PROFILE_GDT0, PROFILE_GDT1, PROFILE_GDT2,
     &         FAIL, MESSAGE, ACTION )
        
      END SELECT
 
      ! Assign to the module varaibles
      DO I = 1, N_LAYERS

         PROF_MASS(I,IMODE)        = PROFILE(N_LAYERS-I+1)
         LPROF_MASS_COL(I,IMODE)   = PROFILE_GDT0(N_LAYERS-I+1) 
         LPROF_MASS_PAR(I,1,IMODE) = PROFILE_GDT1(N_LAYERS-I+1)
         LPROF_MASS_PAR(I,2,IMODE) = PROFILE_GDT2(N_LAYERS-I+1)

      ENDDO


      ! QRATIO = 1.0      
 
      ! Aerosol optical thickness for stratosphere is zero
      !IF ( LEVZ(1) >= 25 ) THEN
      !
      !   TAU_AERO = 0d0
      !   RETURN
      !
      !ENDIF 

      ! Correction factor for AOD
      !FCORR = 1d0 / (1d0 - EXP( - 20d0 / HAOD ))

      ! Aerosol optical thickness for troposphere
      !TAU_AERO = COLUMN_AOD * FCORR * QRATIO
      ! &         * ( EXP( - LEVZ(1) / HAOD ) - EXP( - LEVZ(2) / HAOD ) )

      ! Precision cut
      !IF ( TAU_AERO <= 1d-6 ) TAU_AERO = 0d0

      END SUBROUTINE GET_AEROSOL_PROF
!
!----------------------------------------------------------------------
!
      SUBROUTINE INIT_AEROSOL

      ! References to F90 modules
      USE ERROR_MOD,           ONLY : ALLOC_ERR
      USE NAMELIST_ARRAY_MOD,  ONLY : N_LAYERS,  NMODE
      USE NAMELIST_ARRAY_MOD,  ONLY : N_MOMENTS

      ! Local Variables
      INTEGER            :: AS
      LOGICAL, SAVE      :: IS_INIT = .FALSE.

      !=================================================================
      ! INIT_AEROSOL begins here!
      !=================================================================
     
      ! Return if we have already initialized
      IF ( IS_INIT ) RETURN

      !=================================================================
      ! For aerosol loading and profiles
      !=================================================================

      ! COLUMN_MASS
      ALLOCATE ( COLUMN_MASS(NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'COLUMN_MASS' )
      COLUMN_MASS = 0d0

      ! COLUMN_AOD
      ALLOCATE ( COLUMN_AOD(NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'COLUMN_AOD' )
      COLUMN_AOD = 0d0

      ! COLUMN_NUM
      ALLOCATE ( COLUMN_NUM(NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'COLUMN_NUM' )
      COLUMN_NUM = 0d0
       
      ! PROF_MASS
      ALLOCATE ( PROF_MASS(N_LAYERS,NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'PROF_MASS' )
      PROF_MASS = 0d0 

      ! PROF_AOD
      ALLOCATE ( PROF_AOD(N_LAYERS,NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'PROF_AOD' )
      PROF_AOD = 0d0

      ! PROF_NUM
      ALLOCATE ( PROF_NUM(N_LAYERS,NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'PROF_NUM' )
      PROF_NUM = 0d0

      ! LPROF_MASS_COL
      ALLOCATE ( LPROF_MASS_COL(N_LAYERS,NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'LPROF_MASS_COL' )
      LPROF_MASS_COL = 0d0

      ! LPROF_MASS_PAR
      ALLOCATE ( LPROF_MASS_PAR(N_LAYERS,2,NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'LPROF_MASS_PAR' )
      LPROF_MASS_PAR = 0d0

      ! LPROF_AOD_COL
      ALLOCATE ( LPROF_AOD_COL(N_LAYERS,NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'LPROF_AOD_COL' )
      LPROF_AOD_COL = 0d0

      ! LPROF_AOD_PAR
      ALLOCATE ( LPROF_AOD_PAR(N_LAYERS,2,NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'LPROF_AOD_PAR' )
      LPROF_AOD_PAR = 0d0

      ! MASS_EFF
      ALLOCATE ( MASS_EFF(NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'MASS_EFF' )
      MASS_EFF = 0d0

      ! KEXT_MASS
      ALLOCATE ( KEXT_MASS(NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'KEXT_MASS' )
      KEXT_MASS = 0d0

      ! KSCA_MASS
      ALLOCATE ( KSCA_MASS(NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'KSCA_MASS' )
      KSCA_MASS = 0d0

      !=================================================================
      ! For MIE and TMATRIX calculations
      !=================================================================

      ! ASYMM
      ALLOCATE ( ASYMM(NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'ASYMM' )
      ASYMM = 0d0

      ! NCOEFFS
      ALLOCATE ( NCOEFFS(NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'NCOEFFS' )
      NCOEFFS = 0

      ! BULK_AOP
      ALLOCATE ( BULK_AOP(5,NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'BULK_AOP' )
      BULK_AOP = 0d0

      ! EXPCOEFFS
      ALLOCATE ( EXPCOEFFS(6,0:N_MOMENTS,NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'EXPCOEFFS' )
      EXPCOEFFS = 0d0

      ! FMATRIX
      ALLOCATE ( FMATRIX(6,NPANGLE,NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'FMATRIX' )
      FMATRIX = 0d0

      ! DIST
      ALLOCATE ( DIST(5,NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'DIST' )
      DIST = 0d0
     
      ! LPSD_BULK
      ALLOCATE ( LPSD_BULK(5,3,NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'LPSD_BULK' )
      LPSD_BULK = 0d0

      ! LPSD_ASYMM
      ALLOCATE ( LPSD_ASYMM(3,NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'LPSD_ASYMM' )
      LPSD_ASYMM = 0d0

      ! LPSD_EXPCOEF
      ALLOCATE ( LPSD_EXPCOEF(6,0:N_MOMENTS,3,NMODE), STAT=AS)
      IF (AS/=0) CALL ALLOC_ERR( 'LPSD_EXPCOEF' )
      LPSD_EXPCOEF = 0d0

      ! LPSD_FMATRIX
      ALLOCATE ( LPSD_FMATRIX(6,NPANGLE,3,NMODE), STAT=AS)
      IF (AS/=0) CALL ALLOC_ERR( 'LPSD_FMATRIX' )
      LPSD_FMATRIX = 0d0

      ! LPSD_DIST
      ALLOCATE ( LPSD_DIST(5,3, NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'LPSD_DIST' )
      LPSD_DIST = 0d0  

      ! LRFE_BULK
      ALLOCATE ( LRFE_BULK(5,3,NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'LRFE_BULK' )
      LRFE_BULK = 0d0

      ! LRFE_ASYMM
      ALLOCATE ( LRFE_ASYMM(3,NMODE), STAT = AS )
      IF (AS/=0) CALL ALLOC_ERR( 'LRFE_BMIE_ASYMM' )
      LRFE_ASYMM = 0d0

      ! LRFE_EXPCOEF
      ALLOCATE ( LRFE_EXPCOEF(6,0:N_MOMENTS,3,NMODE), STAT=AS)
      IF (AS/=0) CALL ALLOC_ERR( 'LRFE_EXPCOEF' )
      LRFE_EXPCOEF = 0d0

      ! LRFE_FMATRIX
      ALLOCATE ( LRFE_FMATRIX(6,NPANGLE,3,NMODE), STAT=AS)
      IF (AS/=0) CALL ALLOC_ERR( 'LRFE_FMATRIX' )
      LRFE_FMATRIX = 0d0

      ! For multi-mode only
      IF ( NMODE >= 2 ) THEN

         ! LFRC_BULK
         ALLOCATE ( LFRC_BULK(5,NMODE), STAT = AS )
         IF (AS/=0) CALL ALLOC_ERR( 'LFRC_BULK' )
         LFRC_BULK = 0d0

         ! LFRC_ASYMM
         ALLOCATE ( LFRC_ASYMM(NMODE), STAT = AS )
         IF (AS/=0) CALL ALLOC_ERR( 'LFRC_ASYMM' )
         LFRC_ASYMM = 0d0

         ! LFRC_EXPCOEF
         ALLOCATE(LFRC_EXPCOEF(6,0:N_MOMENTS,NMODE),STAT=AS)
         IF (AS/=0) CALL ALLOC_ERR( 'LFRC_EXPCOEF' )
         LFRC_EXPCOEF = 0d0

         ! LFRC_FMATRIX
         !ALLOCATE (LFRC_FMATRIX(4,NPANGLE,NMODE), STAT=AS)
         !IF (AS/=0) CALL ALLOC_ERR( 'LFRC_FMATRIX' )
         !LFRC_FMATRIX = 0d0

      ENDIF

      ! Reset IS_INIT so we do not allocate arrays again
      IS_INIT = .TRUE.

      ! Return to calling program
      END SUBROUTINE INIT_AEROSOL
!
!----------------------------------------------------------------------
!
      SUBROUTINE CLEANUP_AEROSOL

      !=================================================================
      ! CLEANUP__AEROSOL begins here!
      !=================================================================

      ! Deallocates all module arrays
      IF ( ALLOCATED( COLUMN_MASS   ) ) DEALLOCATE( COLUMN_MASS   )
      IF ( ALLOCATED( COLUMN_AOD    ) ) DEALLOCATE( COLUMN_AOD    )
      IF ( ALLOCATED( COLUMN_NUM    ) ) DEALLOCATE( COLUMN_NUM    )
      IF ( ALLOCATED( PROF_MASS     ) ) DEALLOCATE( PROF_MASS     )
      IF ( ALLOCATED( PROF_AOD      ) ) DEALLOCATE( PROF_AOD      )
      IF ( ALLOCATED( PROF_NUM      ) ) DEALLOCATE( PROF_NUM      )
      IF ( ALLOCATED( LPROF_MASS_COL) ) DEALLOCATE( LPROF_MASS_COL)
      IF ( ALLOCATED( LPROF_MASS_PAR) ) DEALLOCATE( LPROF_MASS_PAR)
      IF ( ALLOCATED( LPROF_AOD_COL ) ) DEALLOCATE( LPROF_AOD_COL )
      IF ( ALLOCATED( LPROF_AOD_PAR ) ) DEALLOCATE( LPROF_AOD_PAR )
      IF ( ALLOCATED( MASS_EFF      ) ) DEALLOCATE( MASS_EFF      )
      IF ( ALLOCATED( KEXT_MASS     ) ) DEALLOCATE( KEXT_MASS     )
      IF ( ALLOCATED( KSCA_MASS     ) ) DEALLOCATE( KSCA_MASS     )

      IF ( ALLOCATED( ASYMM         ) ) DEALLOCATE( ASYMM         )
      IF ( ALLOCATED( NCOEFFS       ) ) DEALLOCATE( NCOEFFS       )
      IF ( ALLOCATED( BULK_AOP      ) ) DEALLOCATE( BULK_AOP      )
      IF ( ALLOCATED( EXPCOEFFS     ) ) DEALLOCATE( EXPCOEFFS     )
      IF ( ALLOCATED( FMATRIX       ) ) DEALLOCATE( FMATRIX       )
      IF ( ALLOCATED( DIST          ) ) DEALLOCATE( DIST          )
      IF ( ALLOCATED( LPSD_BULK     ) ) DEALLOCATE( LPSD_BULK     )
      IF ( ALLOCATED( LPSD_ASYMM    ) ) DEALLOCATE( LPSD_ASYMM    )
      IF ( ALLOCATED( LPSD_EXPCOEF  ) ) DEALLOCATE( LPSD_EXPCOEF  )
      IF ( ALLOCATED( LPSD_FMATRIX  ) ) DEALLOCATE( LPSD_FMATRIX  )
      IF ( ALLOCATED( LPSD_DIST     ) ) DEALLOCATE( LPSD_DIST     )
      IF ( ALLOCATED( LRFE_BULK     ) ) DEALLOCATE( LRFE_BULK     )
      IF ( ALLOCATED( LRFE_ASYMM    ) ) DEALLOCATE( LRFE_ASYMM    )
      IF ( ALLOCATED( LRFE_EXPCOEF  ) ) DEALLOCATE( LRFE_EXPCOEF  )
      IF ( ALLOCATED( LRFE_FMATRIX  ) ) DEALLOCATE( LRFE_FMATRIX  )
      IF ( ALLOCATED( LFRC_BULK     ) ) DEALLOCATE( LFRC_BULK     )
      IF ( ALLOCATED( LFRC_ASYMM    ) ) DEALLOCATE( LFRC_ASYMM    )
      IF ( ALLOCATED( LFRC_EXPCOEF  ) ) DEALLOCATE( LFRC_EXPCOEF  )
      !IF ( ALLOCATED( LFRC_FMATRIX  ) ) DEALLOCATE( LFRC_FMATRIX  )

      END SUBROUTINE CLEANUP_AEROSOL
!
!----------------------------------------------------------------------
!
      ! End of module
      END MODULE AEROSOL_MOD
