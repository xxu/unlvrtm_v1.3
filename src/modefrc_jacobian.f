! $id: modefrc_jacobian.f, unl-vrtm v1.1.2 2012/11/06 21:22:38 xxu 
      SUBROUTINE MODEFRC_JACOBIAN
!
!******************************************************************************
!  Routine MODEFRC_JACOBIAN derive the Jacobian of Stoke vector to the 
!   modal fraction, which originally calculated by VLIDORT directly.
!   As the weighting function with respect to the AOD (or mass) modal 
!   fraction is derivable from the weighting function with respect to 
!   AOD (or mass). Offline derivation saves memory for VLIDORT
!   simulation. 
!   (xxu, 11/6/12, 11/7/12)
!  
!  REVISION HISTORY:
!  (1 )
!******************************************************************************
!
      ! References to F90 modules
      USE LPAR_MOD,           ONLY : ID_AOD_M01,   ID_AOD_M02
      USE LPAR_MOD,           ONLY : ID_MASS_M01,  ID_MASS_M02      
      USE LPAR_MOD,           ONLY : LPAR_NAMES,   N_LPAR_TOTAL
      USE LPAR_MOD,           ONLY : N_LPAR_4VLD,  N_LPAR_EXTRA
      USE AEROSOL_MOD,        ONLY : COLUMN_MASS,  COLUMN_AOD
      USE AEROSOL_MOD,        ONLY : PROF_MASS,    PROF_AOD
      USE VLIDORT_ARRAY_MOD,  ONLY : JACKS_PROFLS, JACKS_COLUMN
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS
      USE NAMELIST_ARRAY_MOD, ONLY : N_GEOM,       N_STOKS
      USE NAMELIST_ARRAY_MOD, ONLY : NRECEP_LEV,   LPRT
      USE IOP_MOD,            ONLY : TAUAER_WFS

      IMPLICIT NONE

      ! Local variables
      REAL*8                     :: FRC
      INTEGER                    :: Q, UT, V, O1
      INTEGER                    :: N, REV_N

      ! If none extra Jacobian, return to the calling routine
      IF ( N_LPAR_EXTRA == 0 ) RETURN 
 
      ! Return if one of the mode has zero aerosol mass 
      IF ( COLUMN_MASS(1) * COLUMN_MASS(2) == 0 ) RETURN

      !================================================================
      ! MODEFRC_JACOBIAN begins here!
      !================================================================
      WRITE(6,'(A)') ' MODEFRC_JACOBIAN: Calculate Jacobian wtr MFRAC'

      ! Start the parameter loop
      DO Q = N_LPAR_4VLD+1, N_LPAR_4VLD+N_LPAR_EXTRA

         ! Case statement for linearization parameters
         SELECT CASE ( TRIM(LPAR_NAMES(Q)) )

         !=============================================================
         ! AOD fraction, 1st mode
         !=============================================================
         CASE ( 'AODFRC_M01' )

            ! modal fraction
            FRC = COLUMN_AOD(1) / SUM( COLUMN_AOD )

            IF ( LPRT ) WRITE(6,*) ' - AODFRC_M01:', ID_AOD_M01, FRC

            ! AOD weighting functions
            TAUAER_WFS(Q,:) = 0d0

            ! Column weighting function
            ! JACKS_COLUMN(Q,1:NRECEP_LEV,1N_GEOM:,1:N_STOKS)
            JACKS_COLUMN(Q,:,:,:) = JACKS_COLUMN(ID_AOD_M01,:,:,:)
     &                            - FRC / ( 1d0 - FRC )
     &                            * JACKS_COLUMN(ID_AOD_M02,:,:,:)

            ! Layer loop for aerosol layers
            DO N = 1, N_LAYERS
            REV_N = N_LAYERS - N + 1
            IF ( SUM( PROF_AOD(REV_N,:) ) /= 0
     &           .AND. PROF_AOD(REV_N,1)*PROF_AOD(REV_N,2) /= 0 ) THEN

               ! modal fraction over current layer 
               FRC = PROF_AOD(REV_N,1) / SUM( PROF_AOD(REV_N,:) )

               ! JACKS_PROFLS(Q,N,1:NRECEP_LEV,1N_GEOM:,1:N_STOKS)
               JACKS_PROFLS(Q,N,:,:,:)
     &                        = JACKS_PROFLS(ID_AOD_M01,N,:,:,:)
     &                        - FRC / ( 1d0 - FRC )
     &                        * JACKS_PROFLS(ID_AOD_M02,N,:,:,:)

            ENDIF
            ENDDO

         !=============================================================
         ! AOD fraction, 2nd mode
         !=============================================================
         CASE ( 'AODFRC_M02' )

            ! modal fraction
            FRC = COLUMN_AOD(2) / SUM( COLUMN_AOD )

            IF ( LPRT ) WRITE(6,*) ' - AODFRC_M02:', ID_AOD_M02, FRC

            ! AOD weighting functions
            TAUAER_WFS(Q,:) = 0d0

            ! Column weighting function
            ! JACKS_COLUMN(Q,1:NRECEP_LEV,1N_GEOM:,1:N_STOKS)
            JACKS_COLUMN(Q,:,:,:) = JACKS_COLUMN(ID_AOD_M02,:,:,:)
     &                            - FRC / ( 1d0 - FRC )
     &                            * JACKS_COLUMN(ID_AOD_M01,:,:,:)

            ! Layer loop for aerosol layers
            DO N = 1, N_LAYERS
            REV_N = N_LAYERS - N + 1
            IF ( SUM( PROF_AOD(REV_N,:) ) /= 0
     &           .AND. PROF_AOD(REV_N,1)*PROF_AOD(REV_N,2) /= 0 ) THEN
            
               ! modal fraction over current layer 
               FRC = PROF_AOD(REV_N,2) / SUM( PROF_AOD(REV_N,:) )

               ! JACKS_PROFLS(Q,N,1:NRECEP_LEV,1N_GEOM:,1:N_STOKS)
               JACKS_PROFLS(Q,N,:,:,:)
     &                        = JACKS_PROFLS(ID_AOD_M02,N,:,:,:)
     &                        - FRC / ( 1d0 - FRC )
     &                        * JACKS_PROFLS(ID_AOD_M01,N,:,:,:)

            ENDIF
            ENDDO

         !=============================================================
         ! Mass fraction, 1st mode
         !=============================================================
         CASE ( 'MASSFRC_M01' )

            ! modal fraction
            FRC = COLUMN_MASS(1) / SUM( COLUMN_MASS )

            IF ( LPRT ) WRITE(6,*) ' - MASSFRC_M01:', ID_MASS_M01, FRC

            ! Column weighting function
            ! JACKS_COLUMN(Q,1:NRECEP_LEV,1N_GEOM:,1:N_STOKS)
            JACKS_COLUMN(Q,:,:,:) = JACKS_COLUMN(ID_MASS_M01,:,:,:)
     &                            - FRC / ( 1d0 - FRC )
     &                            * JACKS_COLUMN(ID_MASS_M02,:,:,:)

            ! Layer loop for aerosol layers
            DO N = 1, N_LAYERS
            REV_N = N_LAYERS - N + 1
            IF ( SUM( PROF_MASS(REV_N,:) ) /= 0 
     &           .AND. PROF_MASS(REV_N,1)*PROF_MASS(REV_N,2) /= 0 ) THEN

               ! modal fraction over current layer 
               FRC = PROF_MASS(REV_N,1) / SUM( PROF_MASS(REV_N,:) )

               ! AOD weighting functions
               TAUAER_WFS(Q,N) = TAUAER_WFS(ID_MASS_M01,N) 
     &                         - FRC / ( 1d0 - FRC ) 
     &                         * TAUAER_WFS(ID_MASS_M02,N)
              
               ! JACKS_PROFLS(Q,N,1:NRECEP_LEV,1N_GEOM:,1:N_STOKS)
               JACKS_PROFLS(Q,N,:,:,:) 
     &                        = JACKS_PROFLS(ID_MASS_M01,N,:,:,:)
     &                        - FRC / ( 1d0 - FRC )
     &                        * JACKS_PROFLS(ID_MASS_M02,N,:,:,:)

            ENDIF
            ENDDO

         !=============================================================
         ! Mass fraction, 2nd mode
         !=============================================================
         CASE ( 'MASSFRC_M02' )

           ! modal fraction
            FRC = COLUMN_MASS(2) / SUM( COLUMN_MASS )

            IF ( LPRT ) WRITE(6,*) ' - MASSFRC_M02:', ID_MASS_M02, FRC

            ! Column weighting function
            ! JACKS_COLUMN(Q,1:NRECEP_LEV,1N_GEOM:,1:N_STOKS)
            JACKS_COLUMN(Q,:,:,:) = JACKS_COLUMN(ID_MASS_M02,:,:,:)
     &                            - FRC / ( 1d0 - FRC )
     &                            * JACKS_COLUMN(ID_MASS_M01,:,:,:)

            ! Layer loop for aerosol layers
            DO N = 1, N_LAYERS
            REV_N = N_LAYERS - N + 1
            IF ( SUM( PROF_MASS(REV_N,:) ) /= 0
     &           .AND. PROF_MASS(REV_N,1)*PROF_MASS(REV_N,2) /= 0 ) THEN

               ! modal fraction over current layer 
               FRC = PROF_MASS(REV_N,2) / SUM( PROF_MASS(REV_N,:) )

               ! AOD weighting functions
               TAUAER_WFS(Q,N) = TAUAER_WFS(ID_MASS_M02,N)
     &                         - FRC / ( 1d0 - FRC )
     &                         * TAUAER_WFS(ID_MASS_M01,N)

               ! JACKS_PROFLS(Q,N,1:NRECEP_LEV,1N_GEOM:,1:N_STOKS)
               JACKS_PROFLS(Q,N,:,:,:)
     &                        = JACKS_PROFLS(ID_MASS_M02,N,:,:,:)
     &                        - FRC / ( 1d0 - FRC )
     &                        * JACKS_PROFLS(ID_MASS_M01,N,:,:,:)

            ENDIF
            ENDDO

         END SELECT

      ENDDO

      ! Return to the calling routine
      RETURN

      ! End of routine
      END SUBROUTINE MODEFRC_JACOBIAN
