! $Id: sao_xsec_mod.f, v1.1 2010/12/24 09:46:43 xxu
      MODULE SAO_XSEC_MOD
!******************************************************************************
! uv_vis_xsec.f contains subroutines calculating some gases' cross  section. 
!  Molecule   Spectral-range [nm]   Temperature-Range 
!        O3:  196.06 - 660.18          200-300
!       NO2:  238.07 - 666.57          220, 294
!       NO3:  476.19 - 794.21          298
!       SO2:  227.35 - 416.75          295-358
! (xxu, 12/24/10, 7/18/12)
!
! Reference:
!   Orphal, J. and K. Chance, 2003: Ultraviolet and visible absorption 
!   cross-sections for HITRAN. Journal of Quantitative Spectroscopy and 
!   Radiative Transfer, 82, 491-504.   ( See Table 1 for further references )
!
! Module Routines:
! =============================================================================
! (1 ) O3_XSEC_195_830()     :
! (2 ) O4_XSEC_337_666()     :
! (3 ) NO2_XSEC_238_666()    :
! (4 ) SO2_XSEC_227_416()    :
! (5 ) H2CO_XSEC_300_385()   :
!
! NOTES:
! (1 ) All FUNCTIONS now use the format:
!      FUNCTION FUNCTION_NAME( VAR1, ..., VARn )  RESULT ( VAR ) 
!      (xxu, 5/24/11)
!******************************************************************************
!

      IMPLICIT NONE

      !=================================================================
      ! MODULE PRIVATE DECLARATIONS -- keep certain internal variables 
      ! and routines from being seen outside "spectra_mod.f"
      !=================================================================

      ! Make everything PRIVATE ...
      PRIVATE

      ! ... except these routines
      PUBLIC  :: O3_XSEC_195_830
      PUBLIC  :: O4_XSEC_337_666
      PUBLIC  :: NO2_XSEC_238_666
      PUBLIC  :: SO2_XSEC_227_416
      PUBLIC  :: H2CO_XSEC_300_385
      PUBLIC  :: SPLINE, SPLINT

      ! ... and these variables

      ! Module variables

      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !=================================================================
      CONTAINS

!------------------------------------------------------------------------------

      FUNCTION O3_XSEC_195_830( TK ) RESULT ( XSEC_VALUE )

      ! References to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : DIR_DATA
      USE SPECTRA_MOD,        ONLY : NLAMDA,   LAMDAS

      IMPLICIT NONE

      ! Arguments
      REAL*8, INTENT(IN)  :: TK

      ! Return
      REAL*8              :: XSEC_VALUE(NLAMDA)

      ! Constants
      INTEGER, PARAMETER  :: MAXDIM = 63501
      INTEGER, PARAMETER  :: IU_O3  = 21

      ! Local variables
      CHARACTER(LEN=255)  :: O3XSEC_FILE
      REAL*8              :: WL(MAXDIM)
      REAL*8              :: XSEC0(MAXDIM)
      REAL*8              :: XSEC1
      REAL*8              :: Y2(MAXDIM)
      REAL*8              :: LAMB1, LAMB2

      INTEGER             :: I
      INTEGER             :: NX, NLINE
      REAL*8              :: POS, C0, C1, C2
      REAL*8              :: TC, TC2, CONV

      !================================================================
      ! O3_XSEC_195_830 begins here!
      !================================================================


      ! Check if the window is covered
      IF ( LAMDAS(1) > 830.22 .OR. LAMDAS(NLAMDA) < 195 ) THEN
         XSEC_VALUE(:) = 0d0
         RETURN
      ENDIF

      ! Buffer control, initialization
      LAMB1 = LAMDAS(1)      - 2.0d0
      LAMB2 = LAMDAS(NLAMDA) + 2.0d0

      ! initialization
      WL    = 0d0
      XSEC0 = 0d0
      XSEC1 = 0d0
      CONV  = 1d20

      ! Temperature from Kelvin to C
      TC  = TK - 275.15d0
      TC2 = TC * TC

      ! Filename
      O3XSEC_FILE = TRIM(DIR_DATA)
     &            // 'xsec/o3abs_brion_195_830_vacfinal.dat'

      ! Open file
      OPEN(UNIT=IU_O3, FILE=O3XSEC_FILE, STATUS='OLD')

      ! Read header line
      READ(IU_O3, *)

      ! Start to read
      POS   = 0d0
      NLINE = 1
      NX    = 0

      DO WHILE( POS <= LAMB2 .AND. NLINE < MAXDIM )
      
         NLINE = NLINE+1
         READ(IU_O3, *) POS, C0, C1, C2

         IF ( POS >= LAMB1 ) THEN
            NX        = NX + 1
            WL(NX)    = POS
            XSEC0(NX) = 1d-20 * ( C0 + C1*TC + C2*TC2 ) * CONV
         ENDIF

      ENDDO 

      CLOSE(IU_O3)

      ! Now spline the data
      CALL SPLINE( WL(1:NX), XSEC0(1:NX), NX, 0d0, 0d0, Y2(1:NX) )

      DO I = 1, NLAMDA

         IF ( LAMDAS(I) >= WL(1) .AND. LAMDAS(I) <= WL(NX) ) THEN
            CALL SPLINT( WL(1:NX), XSEC0(1:NX), Y2(1:NX), 
     &                   NX, LAMDAS(I), XSEC1)
            XSEC_VALUE(I) = XSEC1 / CONV
            IF ( XSEC_VALUE(I) < 0d0 ) XSEC_VALUE(I) = 0d0
         ELSE
            XSEC_VALUE(I) = 0d0
         ENDIF

      ENDDO
     
      ! Return to the calling routine
      RETURN

      END FUNCTION O3_XSEC_195_830

!------------------------------------------------------------------------------

      FUNCTION O4_XSEC_337_666( ) RESULT ( XSEC_VALUE )

      ! References to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : DIR_DATA
      USE SPECTRA_MOD,        ONLY : NLAMDA,   LAMDAS

      IMPLICIT NONE

      ! Return
      REAL*8              :: XSEC_VALUE(NLAMDA)

      ! Constants
      INTEGER, PARAMETER  :: MAXDIM = 12118
      INTEGER, PARAMETER  :: IU_O4  = 22

      ! Local variables
      CHARACTER(LEN=255)  :: O4XSEC_FILE
      REAL*8              :: WL(MAXDIM)
      REAL*8              :: XSEC0(MAXDIM)
      REAL*8              :: XSEC1
      REAL*8              :: Y2(MAXDIM)
      REAL*8              :: LAMB1, LAMB2

      INTEGER             :: I
      INTEGER             :: NX, NLINE
      REAL*8              :: POS, C0, CONV

      !================================================================
      ! O4_XSEC_337_666 begins here!
      !================================================================


      ! Check if the window is covered
      IF ( LAMDAS(1) > 666.70 .OR. LAMDAS(NLAMDA) < 337.30 ) THEN
         XSEC_VALUE(:) = 0d0
         RETURN
      ENDIF

      ! Buffer control, initialization
      LAMB1 = LAMDAS(1)      - 2.0d0
      LAMB2 = LAMDAS(NLAMDA) + 2.0d0

      ! initialization
      WL    = 0d0
      XSEC0 = 0d0
      XSEC1 = 0d0
      CONV  = 1d40

      ! Filename
      O4XSEC_FILE = TRIM(DIR_DATA) // 'xsec/O4_294K_BISA_666.dat'

      ! Open file
      OPEN(UNIT=IU_O4, FILE=O4XSEC_FILE, STATUS='OLD')

      ! Read header line
      READ(IU_O4, *)

      ! Start to read
      POS   = 0d0
      NLINE = 1
      NX    = 0

      DO WHILE( POS <= LAMB2 .AND. NLINE < MAXDIM )

         NLINE = NLINE+1
         READ(IU_O4, *) POS, C0

         IF ( POS >= LAMB1 ) THEN
            NX        = NX + 1
            WL(NX)    = POS
            XSEC0(NX) = C0 * CONV
         ENDIF

      ENDDO

      CLOSE(IU_O4)

      ! Now spline the data
      CALL SPLINE( WL(1:NX), XSEC0(1:NX), NX, 0d0, 0d0, Y2(1:NX) )

      DO I = 1, NLAMDA

         IF ( LAMDAS(I) >= WL(1) .AND. LAMDAS(I) <= WL(NX) ) THEN
            CALL SPLINT( WL(1:NX), XSEC0(1:NX), Y2(1:NX),
     &                   NX, LAMDAS(I), XSEC1)
            XSEC_VALUE(I) = XSEC1 / CONV
            IF ( XSEC_VALUE(I) < 0d0 ) XSEC_VALUE(I) = 0d0 
         ELSE
            XSEC_VALUE(I) = 0d0
         ENDIF

      ENDDO

      ! Return to the calling routine
      RETURN

      END FUNCTION O4_XSEC_337_666

!------------------------------------------------------------------------------

      FUNCTION NO2_XSEC_238_666( TK ) RESULT ( XSEC_VALUE )

      ! References to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : DIR_DATA
      USE SPECTRA_MOD,        ONLY : NLAMDA,   LAMDAS

      IMPLICIT NONE

      ! Arguments
      REAL*8, INTENT(IN)  :: TK

      ! Return
      REAL*8              :: XSEC_VALUE(NLAMDA)

      ! Constants
      INTEGER, PARAMETER  :: MAXDIM  = 27992
      INTEGER, PARAMETER  :: IU_NO2  = 23

      ! Local variables
      CHARACTER(LEN=255)  :: NO2XSEC_FILE

      REAL*8              :: WL(MAXDIM)
      REAL*8              :: XSEC0(MAXDIM)
      REAL*8              :: XSEC1
      REAL*8              :: Y2(MAXDIM)
      REAL*8              :: LAMB1, LAMB2

      INTEGER             :: I
      INTEGER             :: NX, NLINE
      REAL*8              :: POS, VN, C1, C2
      REAL*8              :: FRC1, CONV

      !================================================================
      ! NO2_XSEC_238_666() begins here
      !================================================================

      ! Check if the window is covered
      IF ( LAMDAS(1) > 666.50 .OR. LAMDAS(NLAMDA) < 238.00 ) THEN
         XSEC_VALUE(:) = 0d0
         RETURN
      ENDIF

      ! Buffer control, initialization
      LAMB1 = LAMDAS(1)      - 2.0d0
      LAMB2 = LAMDAS(NLAMDA) + 2.0d0

      ! initialization
      WL    = 0d0
      XSEC0 = 0d0
      XSEC1 = 0d0
      CONV  = 1d20

      ! Weight based on the Temperature closeness
      IF ( TK <= 220 ) FRC1 = 1.0d0
      IF ( TK >= 294 ) FRC1 = 0.0d0
      IF ( TK > 220 .AND. TK < 294 ) FRC1 = (294.-TK) / (294.-220.)

      ! Name the NO2 XSEC file
      NO2XSEC_FILE = TRIM(DIR_DATA) // 'xsec/NO2_UV00.dat'

      ! Open file
      OPEN(UNIT=IU_NO2, FILE=NO2XSEC_FILE, STATUS='OLD')

      ! Read header line
      READ(IU_NO2, *)             

      ! Start to read
      POS   = 0d0
      NLINE = 1
      NX    = 0

      DO WHILE( POS <= LAMB2 .AND. NLINE < MAXDIM )

         NLINE = NLINE+1
         READ(IU_NO2, *) POS, VN, C1, C2

         IF ( POS >= LAMB1 ) THEN
            NX        = NX + 1
            WL(NX)    = POS
            XSEC0(NX) = ( FRC1 * C1 + ( 1.0d0 - FRC1 ) * C2 ) * CONV
         ENDIF

      ENDDO

      CLOSE(IU_NO2)

      ! Now spline the data
      CALL SPLINE( WL(1:NX), XSEC0(1:NX), NX, 0d0, 0d0, Y2(1:NX) )

      DO I = 1, NLAMDA

         IF ( LAMDAS(I) >= WL(1) .AND. LAMDAS(I) <= WL(NX) ) THEN
            CALL SPLINT( WL(1:NX), XSEC0(1:NX), Y2(1:NX),
     &                   NX, LAMDAS(I), XSEC1)
            XSEC_VALUE(I) = XSEC1 / CONV
            IF ( XSEC_VALUE(I) < 0d0 ) XSEC_VALUE(I) = 0d0
         ELSE
            XSEC_VALUE(I) = 0d0
         ENDIF

      ENDDO

      ! Return to the calling routine
      RETURN

      END FUNCTION NO2_XSEC_238_666

!------------------------------------------------------------------------------

      FUNCTION SO2_XSEC_227_416( TK ) RESULT ( XSEC_VALUE )

      ! References to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : DIR_DATA
      USE SPECTRA_MOD,        ONLY : NLAMDA,   LAMDAS

      IMPLICIT NONE

      ! Arguments
      REAL*8, INTENT(IN)  :: TK

      ! Return
      REAL*8              :: XSEC_VALUE(NLAMDA)

      ! Constants
      INTEGER, PARAMETER  :: MAXDIM  = 39981
      INTEGER, PARAMETER  :: IU_SO2  = 24

      ! Local variables
      CHARACTER(LEN=255)  :: SO2XSEC_FILE

      REAL*8              :: WL(MAXDIM)
      REAL*8              :: XSEC0(MAXDIM)
      REAL*8              :: XSEC1
      REAL*8              :: Y2(MAXDIM)
      REAL*8              :: LAMB1, LAMB2

      INTEGER             :: I
      INTEGER             :: NX, NLINE
      REAL*8              :: POS, VN, C1, C2, C3, C4
      REAL*8              :: FRC1, CONV

      !================================================================
      ! SO2_XSEC_227_416() begins here
      !================================================================

      ! Check if the window is covered
      IF ( LAMDAS(1) > 416.70 .OR. LAMDAS(NLAMDA) < 222.35 ) THEN
         XSEC_VALUE(:) = 0d0
         RETURN
      ENDIF

      ! Buffer control, initialization
      LAMB1 = LAMDAS(1)      - 2.0d0
      LAMB2 = LAMDAS(NLAMDA) + 2.0d0

      ! initialization
      WL    = 0d0
      XSEC0 = 0d0
      XSEC1 = 0d0
      CONV  = 1d20

      ! Weight based on the Temperature closeness
      IF ( TK <= 298 ) FRC1 = 1.0d0
      IF ( TK >= 318 ) FRC1 = 0.0d0
      IF ( TK > 298 .AND. TK < 318 ) FRC1 = (318.-TK) / (318.-298.)

      ! Name the NO2 XSEC file
      SO2XSEC_FILE = TRIM(DIR_DATA) // 'xsec/SO2_UV08.dat'

      ! Open file
      OPEN(UNIT=IU_SO2, FILE=SO2XSEC_FILE, STATUS='OLD')

      ! Read header line
      READ(IU_SO2, *)

      ! Start to read
      POS   = 0d0
      NLINE = 1
      NX    = 0

      DO WHILE( POS <= LAMB2 .AND. NLINE < MAXDIM )

         NLINE = NLINE+1
         READ(IU_SO2, *) POS, VN, C1, C2, C3, C4

         IF ( POS >= LAMB1 ) THEN
            NX        = NX + 1
            WL(NX)    = POS
            XSEC0(NX) = ( FRC1 * C1 + ( 1.0d0 - FRC1 ) * C2 ) * CONV
         ENDIF

      ENDDO

      CLOSE(IU_SO2)

      ! Now spline the data
      CALL SPLINE( WL(1:NX), XSEC0(1:NX), NX, 0d0, 0d0, Y2(1:NX) )

      DO I = 1, NLAMDA

         IF ( LAMDAS(I) >= WL(1) .AND. LAMDAS(I) <= WL(NX) ) THEN
            CALL SPLINT( WL(1:NX), XSEC0(1:NX), Y2(1:NX),
     &                   NX, LAMDAS(I), XSEC1)
            XSEC_VALUE(I) = XSEC1 / CONV
            IF ( XSEC_VALUE(I) < 0d0 ) XSEC_VALUE(I) = 0d0
         ELSE
            XSEC_VALUE(I) = 0d0
         ENDIF

      ENDDO

      ! Return to the calling routine
      RETURN

      END FUNCTION SO2_XSEC_227_416

!------------------------------------------------------------------------------

      FUNCTION H2CO_XSEC_300_385( ) RESULT ( XSEC_VALUE )

      ! References to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : DIR_DATA
      USE SPECTRA_MOD,        ONLY : NLAMDA,   LAMDAS

      IMPLICIT NONE

      ! Return
      REAL*8              :: XSEC_VALUE(NLAMDA)

      ! Constants
      INTEGER, PARAMETER  :: MAXDIM  = 12118
      INTEGER, PARAMETER  :: IU_H2CO = 25

      ! Local variables
      CHARACTER(LEN=255)  :: H2COXSEC_FILE
      REAL*8              :: WL(MAXDIM)
      REAL*8              :: XSEC0(MAXDIM)
      REAL*8              :: XSEC1
      REAL*8              :: Y2(MAXDIM)
      REAL*8              :: LAMB1, LAMB2

      INTEGER             :: I
      INTEGER             :: NX, NLINE
      REAL*8              :: POS, C0, CONV

      !================================================================
      ! H2CO_XSEC_300_385 begins here!
      !================================================================

      ! Check if the window is covered
      IF ( LAMDAS(1) > 385.70 .OR. LAMDAS(NLAMDA) < 300.50 ) THEN
         XSEC_VALUE(:) = 0d0
         RETURN
      ENDIF

      ! Buffer control, initialization
      LAMB1 = LAMDAS(1)      - 2.0d0
      LAMB2 = LAMDAS(NLAMDA) + 2.0d0

      ! initialization
      WL    = 0d0
      XSEC0 = 0d0
      XSEC1 = 0d0
      CONV  = 1d20

      ! Filename
      H2COXSEC_FILE = TRIM(DIR_DATA) // 'xsec/h2co_cc.300'

      ! Open file
      OPEN(UNIT=IU_H2CO, FILE=H2COXSEC_FILE, STATUS='OLD')

      ! Read header line
      READ(IU_H2CO, *)

      ! Start to read
      POS   = 0d0
      NLINE = 1
      NX    = 0

      DO WHILE( POS <= LAMB2 .AND. NLINE < MAXDIM )

         NLINE = NLINE+1
         READ(IU_H2CO, *) POS, C0

         IF ( POS >= LAMB1 ) THEN
            NX        = NX + 1
            WL(NX)    = POS
            XSEC0(NX) = C0 * CONV
         ENDIF

      ENDDO

      CLOSE(IU_H2CO)

      ! Now spline the data
      CALL SPLINE( WL(1:NX), XSEC0(1:NX), NX, 0d0, 0d0, Y2(1:NX) )

      DO I = 1, NLAMDA

         IF ( LAMDAS(I) >= WL(1) .AND. LAMDAS(I) <= WL(NX) ) THEN
            CALL SPLINT( WL(1:NX), XSEC0(1:NX), Y2(1:NX),
     &                   NX, LAMDAS(I), XSEC1)
            XSEC_VALUE(I) = XSEC1 / CONV
            IF ( XSEC_VALUE(I) < 0d0 ) XSEC_VALUE(I) = 0d0
         ELSE
            XSEC_VALUE(I) = 0d0
         ENDIF

      ENDDO

      ! Return to the calling routine
      RETURN

      END FUNCTION H2CO_XSEC_300_385

!------------------------------------------------------------------------------

      SUBROUTINE SPLINE( X, Y, N, YP1, YPN, Y2 )
!
!******************************************************************************
! Routine SPLINE calculates the cubic spline given arrays X(1:N) and Y(1:N) 
!  containing a tabulated function, i.e., Y(i) = f( X(i) ), with X(1) < X(2)
!  < ... < X(N), and given YP1 and YPN for the first derivative of the
!  interplotating function at points 1 and N, respectively. this routine
!  returns an array Y2(1:N) of length N which contains the second
!  derivatives of the interpolating function at the tabulated points X(i).
!  If YP1 and/or YPN are equal to 1E+30 or larger, the routine is
!  signaled to set the corresponding boundary condition for a natural
!  spline, with zero second derivative on that boundary.
!
!  Code is obtained from Dr. Xing Liu and updated to this model. 
!  (xxu, 7/16/2012)
!
! Reference:
!   Numerical Recipes in Fortran 77: The Art of Scientific Computing, Cambridge
!    University Press. page 107-110.  "The goal of cubic spline interpolation 
!    is to get an interpolation formula that is smooth in the first derivative,
!    and continuous in the second derivative, both within an interval and at
!    its boundaries."
!   
!******************************************************************************
!
      ! Arguments
      INTEGER, INTENT(IN)   :: N
      REAL*8,  INTENT(IN)   :: X(N), Y(N), YP1, YPN
      REAL*8,  INTENT(OUT)  :: Y2(N)

      ! Local variables
      INTEGER               :: I, K
      REAL*8                :: P, QN, SIG, UN, U(N)

      !================================================================
      ! SPLINE begins here!
      !================================================================

      ! The lower boundary condition is set either to be "natural"
      IF (YP1 > .99d30) THEN

         Y2(1) = 0d0
         U(1)  = 0d0

      ! or else to have a specified first derivative.
      ELSE

         Y2(1) = -0.5d0
         U(1)  = 3d0 / ( X(2) - X(1) )
     &         * ( ( Y(2) - Y(1) ) / ( X(2) - X(1 ) ) - YP1 )

      ENDIF

      ! This is the decomposition loop of the tridiagonal algorithm. Y2
      ! and U are used for temporary storage of the decomposed factors.
      DO I = 2, N-1

         SIG   = ( X(I) - X(I-1) ) / ( X(I+1) - X(I-1) )
         P     = SIG * Y2(I-1) + 2D0
         Y2(I) = ( SIG - 1D0 ) / P
         U(I)  = 1d0 / P 
     &         * ( 6.0D0 * ( ( Y(I+1) - Y(I) ) / ( X(I+1) - X(I) ) 
     &                     - ( Y(I) - Y(I-1) ) / ( X(I) - X(I-1) ) )
     &                   / ( X(I+1) - X(I-1) ) 
     &                   - SIG * U(I-1) ) 

      ENDDO

      ! The upper boundary condition is set either to be “natural”
      IF (YPN > .99D30) THEN

         QN = 0d0
         UN = 0d0

      ! or else to have a specified first derivative.
      ELSE

         QN = 0.5d0
         UN = 3d0 / ( X(N) - X(N-1) )
     &      * ( YPN - ( Y(N) - Y(N-1) ) / ( X(N) - X(N-1) ) )

      ENDIF

      Y2(N) = ( UN - QN * U(N-1) ) / ( QN * Y2(N-1) + 1d0 )

      ! This is the backsubstitution loop of the tridiago- nal algorithm.
      DO K = N-1, 1, -1

        Y2(K) = Y2(K) * Y2(K+1) + U(K)

      ENDDO

      ! Return to the calling routine
      END SUBROUTINE SPLINE     

!------------------------------------------------------------------------------

      SUBROUTINE SPLINT( XA, YA, Y2A, N, X, Y)
!
!******************************************************************************
!
!  Code is obtained from Dr. Xing Liu and updated to this model. 
!  (xxu, 7/16/2012)
!
! Reference:
!   Numerical Recipes in Fortran 77: The Art of Scientific Computing, Cambridge
!    University Press. page 107-110.  "The goal of cubic spline interpolation 
!    is to get an interpolation formula that is smooth in the first derivative,
!    and continuous in the second derivative, both within an interval and at
!    its boundaries."
!
!******************************************************************************
!
      ! Arguments
      INTEGER, INTENT(IN)   :: N
      REAL*8,  INTENT(IN)   :: X, XA(N), YA(N), Y2A(N)
      REAL*8,  INTENT(OUT)  :: Y

      ! Local variables
      INTEGER               :: K, KHI, KLO
      REAL*8                :: A, B, H

      !================================================================
      ! SPLINT begins here!
      !================================================================

      KLO = 1
      KHI = N

 1    IF ( KHI-KLO > 1 ) THEN

         K = ( KHI + KLO ) / 2

         IF( XA(K) > X ) THEN
            KHI = K
         ELSE
            KLO = K
         ENDIF

      GOTO 1
      ENDIF

      ! KLO and KHI now bracket at the value of X
      H = XA(KHI) - XA(KLO)

      ! The xa’s must be distinct. 
      IF ( H == 0d0 ) PAUSE

      ! Cubic spline polynomial is now evaluated. 
      A = ( XA(KHI) - X ) / H
      B = ( X - XA(KLO) ) / H
      Y = A * YA(KLO) + B * YA(KHI) 
     &  + ( (A**3-A) * Y2A(KLO) + (B**3-B) * Y2A(KHI) ) * (H**2) /6d0

      ! Return to the calling routine
      END SUBROUTINE SPLINT

!------------------------------------------------------------------------------
 
      ! ENd of module
      END MODULE SAO_XSEC_MOD
