! $Id: iop_mod.f, v1.1 2011/3/11 15:04:13 xxu Exp$
      MODULE IOP_MOD
!
!******************************************************************************
!  Module IOP_MOD prepares the Input-of-Optical-Properties for the Radiative
!  transfer model (VLIDORT), including layer optical thickness, single 
!  scattering albedo, and phase greek maxtrix, as well as their liearization
!  inputs for the Jacobian calculation.
!  (xxu, 3/11/11, 3/11/11)
! 
!  Module Variables;
!  ============================================================================
!  (1 ) IOP_TAU    (REAL*8 ) :      
!  (2 ) IOP_SSA    (REAL*8 ) :
!  (3 ) IOP_PHA    (REAL*8 ) :
!  (4 ) LIOP_TAU   (REAL*8 ) :
!  (5 ) LIOP_SSA   (REAL*8 ) :
!  (7 ) LIOP_PHA   (REAL*8 ) :
!  (8 ) LIOP_JCB   (INTEGER) : 
!  (9 ) TAUAER_WFS (REAL*8 ) : 
!
!  Module Routines:
!  ============================================================================
!  (1 ) AGGREGATE_IOP        :
!  (2 ) INIT_IOP             :
!  (4 ) CLEANUP_IOP          :
!  
!  NOTES:
!  (1 ) This module is originally one part of the model VLIDOR_MOD. The 
!       making of a single module is to reduce the complexity of the module
!       VLIDORT_MOD. (xxu, 3/11/11)
!******************************************************************************
!
      IMPLICIT NONE

      ! Make everything PRIVATE ...
      PRIVATE

      ! ... except these routines
      PUBLIC :: INIT_IOP
      PUBLIC :: AGGREGATE_IOP
      PUBLIC :: CLEANUP_IOP

      ! ... and these variables
      PUBLIC :: IOP_TAU,  IOP_SSA,  IOP_PHA
      PUBLIC :: LIOP_TAU, LIOP_SSA, LIOP_PHA
      PUBLIC :: LIOP_JCB, TAUAER_WFS

      !=================================================================
      ! MODULE VARIABLES
      !=================================================================
      REAL*8,  PARAMETER    :: PRS0  = 1d-10
      REAL*8,  ALLOCATABLE  :: IOP_TAU(:)
      REAL*8,  ALLOCATABLE  :: IOP_SSA(:)
      REAL*8,  ALLOCATABLE  :: IOP_PHA(:,:,:)
      REAL*8,  ALLOCATABLE  :: LIOP_TAU(:,:)
      REAL*8,  ALLOCATABLE  :: LIOP_SSA(:,:)
      REAL*8,  ALLOCATABLE  :: LIOP_PHA(:,:,:,:)
      INTEGER, ALLOCATABLE  :: LIOP_JCB(:,:)  
      REAL*8,  ALLOCATABLE  :: TAUAER_WFS(:,:)

      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !=================================================================       
      CONTAINS
!
!----------------------------------------------------------------------
!
      SUBROUTINE INIT_IOP

      ! References to F90 modules 
      USE ERROR_MOD,          ONLY : ALLOC_ERR
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS, N_MOMENTS, N_STOKS
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB
      USE LPAR_MOD,           ONLY : N_LPAR_TOTAL, N_LPAR_4VLD

      ! Local Variables
      INTEGER            :: AS
      REAL*8             :: PS
      INTEGER            :: ISS
      LOGICAL, SAVE      :: IS_INIT = .FALSE.

      !=================================================================
      ! INIT_IOP begins here!
      !=================================================================

      ! Return if we have already initialized
      IF ( IS_INIT ) RETURN

      !Controls number of non-zero entries in the Greek matrix
      ISS = 1
      IF ( N_STOKS > 1 ) ISS = 8

      ALLOCATE( IOP_TAU(N_LAYERS), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'IOP_TAU' )
      IOP_TAU = 0d0

      ALLOCATE( IOP_SSA(N_LAYERS), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'IOP_SSA' )
      IOP_SSA = 0d0

      ALLOCATE( IOP_PHA(0:N_MOMENTS-1,N_LAYERS,ISS), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'IOP_PHA' )
      IOP_PHA = 0d0

      ! Linearization IOP
      IF ( LJACOB ) THEN 

         ALLOCATE( LIOP_TAU(N_LPAR_TOTAL,N_LAYERS), STAT=AS )
         IF (AS/=0) CALL ALLOC_ERR( 'LIOP_TAU' )
         LIOP_TAU = 0d0

         ALLOCATE( LIOP_SSA(N_LPAR_TOTAL,N_LAYERS), STAT=AS )
         IF (AS/=0) CALL ALLOC_ERR( 'LIOP_SSA' )
         LIOP_SSA = 0d0

         ALLOCATE( LIOP_PHA(N_LPAR_TOTAL,0:N_MOMENTS-1,N_LAYERS,ISS), 
     &             STAT=AS )
         IF (AS/=0) CALL ALLOC_ERR( 'LIOP_PHA' )
         LIOP_PHA = 0d0

         ALLOCATE( LIOP_JCB(N_LPAR_TOTAL,N_LAYERS), STAT=AS )
         IF (AS/=0) CALL ALLOC_ERR( 'LIOP_JCB' )
         LIOP_JCB = 0

         ALLOCATE( TAUAER_WFS(N_LPAR_TOTAL,N_LAYERS), STAT=AS )
         IF (AS/=0) CALL ALLOC_ERR( 'TAUAER_WFS' )
         TAUAER_WFS = 0d0

      ENDIF

      ! Reset IS_INIT so we do not allocate arrays again
      IS_INIT = .TRUE.

      WRITE(6, 100)
 100  FORMAT( " - INIT_IOP: Initialize the IOP arrays" )

      END SUBROUTINE INIT_IOP
!
!----------------------------------------------------------------------
!
      SUBROUTINE AGGREGATE_IOP
!
!******************************************************************************
!  Subroutine AGGREGATE_IOP creates the necessary input of optical property
!  for VLIDORT. (xxu, 3/11/11)
!
!  NOTES:
!  (1 ) Addressed the indexing for phase function expansion by new LMIE 
!       code (2011 version by Dr. Robert Spurr). Note that, the value for 
!       element 12 and 21 is negative, and for index 43 is negative.
!       (xxu, 12/15/11)
!******************************************************************************
!
      ! Refereces to F90 modules
      USE AIROPTIC_MOD,       ONLY : TAUAER,        SSAAER
      USE AIROPTIC_MOD,       ONLY : TAUGAS,        PHAAER
      USE AIROPTIC_MOD,       ONLY : TAURAY,        PHARAY
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS,      N_MOMENTS
      USE NAMELIST_ARRAY_MOD, ONLY : N_STOKS,       RMRI
      USE NAMELIST_ARRAY_MOD, ONLY : DISTPAR,       PROF_PAR
      USE NAMELIST_ARRAY_MOD, ONLY : SHAPE_PAR
      USE NAMELIST_ARRAY_MOD, ONLY : NONVAR_AOD,    NONVAR_MASS
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB
      USE NAMELIST_ARRAY_MOD, ONLY : NMODE
      USE LPAR_MOD,           ONLY : LPAR_NAMES,    N_LPAR_4VLD
      USE AEROSOL_MOD,        ONLY : BULK_AOP,      EXPCOEFFS
      USE AEROSOL_MOD,        ONLY : DIST
      USE AEROSOL_MOD,        ONLY : LPSD_BULK,     LPSD_EXPCOEF
      USE AEROSOL_MOD,        ONLY : LPSD_DIST,     LRFE_BULK
      USE AEROSOL_MOD,        ONLY : LRFE_BULK,     LRFE_EXPCOEF
      USE AEROSOL_MOD,        ONLY : PROF_AOD,      LPROF_AOD_PAR
      USE AEROSOL_MOD,        ONLY : LFRC_BULK,     LFRC_EXPCOEF
      USE AEROSOL_MOD,        ONLY : PROF_MASS
      USE AEROSOL_MOD,        ONLY : KEXT_MASS,     KSCA_MASS

      ! Local variables
      REAL*8,  PARAMETER :: SFACTOR_PIXEL = 1.0
      INTEGER            :: GK, CK, NMASK, CMASK(8), SMASK(8)
      DATA CMASK  / 1,  2,  3,  4,  5,  5,  6,  6 /
      DATA SMASK  / 1,  1,  1,  1, -1, -1,  1, -1 / 
      INTEGER            :: I, J, K, L, N, Q, L_REV
      INTEGER            :: IM, IP
      REAL*8             :: RATIO,    BETA,   SK
      REAL*8             :: AEREXT,   AERSCA, AER_OMEGA
      REAL*8             :: MOLEXT,   MOLSCA, GASEXT
      REAL*8             :: TOTEXT,   TOTSCA
      REAL*8             :: OMEGA,    RAY_WT, AER_WT
      REAL*8             :: SUM_PHAAER(0:N_MOMENTS-1,6)

      REAL*8             :: BETA_AER, TERM01, TERM02, ALL_TERMS
      REAL*8             :: L_PAR00,  L_PAR01
      REAL*8             :: L_PAR02
      REAL*8             :: L_PAR03(0:N_MOMENTS-1,6)
      REAL*8             :: FRC(NMODE), FRC1(NMODE)
      REAL*8             :: FRC2(NMODE)

      !================================================================
      ! AGGREGATE_IOP begins here!
      !================================================================

      WRITE(6, 100)
 100  FORMAT( ' - AGGREGATE_IOP: make IOP for VLIDORT' )

      ! Controls number of non-zero entries in the Greek matrix
      NMASK = 1
      IF ( N_STOKS > 1 ) NMASK = 8

      ! Layer Loop
      DO L = 1, N_LAYERS

         ! Note that VLIDORT profile array is from up to down
         L_REV = N_LAYERS - L + 1

         ! Initializing quantity fraction 
         ! FRC = fraction of extinction optical depth
         ! FRC1 = fraction of scattering optical depth
         ! FRC2 = mass fraction
         FRC  = 0d0
         FRC1 = 0d0
         FRC2 = 0d0

         !=============================================================
         ! Bulk properties:
         ! Note, the variables here are named by (xxu, 1/12/11) :
         ! AEREXT:    aerosol extinction optical depth
         ! AER_OMEGA: aerosol single scattering albedo
         ! AERSCA:    aerosol scattering optical depth
         ! MOLEXT:    Rayleigh and Gas extinction optical depth
         ! MOLSCA:    Rayleigh scattering optical depth
         ! GASEXT:    gas extinction (absorption) optical depth
         ! TOTEXT:    ensemble extinction optical depth
         ! TOTSCA:    ensemble scattering optical depth
         ! OMEGA :    ensemble single scattering albedo
         !=============================================================

         ! Aerosol 
         AEREXT    = SUM( TAUAER(L_REV,1:NMODE) )
         AERSCA    = SUM( TAUAER(L_REV,1:NMODE)* SSAAER(L_REV,1:NMODE) )
         AER_OMEGA = AERSCA / AEREXT

         ! Calculate the optical fraction for each aerosol mode
         IF ( AEREXT > PRS0 ) THEN
            FRC(:)  = TAUAER(L_REV,1:NMODE) / AEREXT
            FRC1(:) = FRC(:) * SSAAER(L_REV,:) / AER_OMEGA
            FRC2(:) = PROF_MASS(L_REV,:) / SUM( PROF_MASS(L_REV,:) )
         ENDIF

         DO N = 0, N_MOMENTS - 1
         DO J = 1, 6
            SUM_PHAAER(N,J) = SUM( PHAAER(L,N,J,:) * FRC1(:) )
         ENDDO
         ENDDO

         AEREXT    = AEREXT * SFACTOR_PIXEL   ! Scaled by a factor
         AERSCA    = AERSCA * SFACTOR_PIXEL   ! Scaled by a factor

         ! Molecules ( including air molecules and trace gases )
         MOLSCA    = TAURAY(L_REV)
         GASEXT    = TAUGAS(L_REV)
         MOLEXT    = MOLSCA + GASEXT

         ! Total ensemble 
         TOTEXT    = AEREXT + MOLEXT
         TOTSCA    = AERSCA + MOLSCA
         OMEGA     = TOTSCA / TOTEXT
         IF ( OMEGA >= 0.999999d0 ) OMEGA = 1d0 - 1d-4
         IF ( OMEGA <= 0d0        ) OMEGA = 1d-4

         ! Scattering contribution ratios 
         RAY_WT = MOLSCA / TOTSCA
         AER_WT = AERSCA / TOTSCA

         !=============================================================
         ! Optical proerty inputs
         !=============================================================
         IOP_TAU(L) = TOTEXT
         IOP_SSA(L) = OMEGA

!         ! Debug only 
!         WRITE(6,'(A,2I4,1P2E12.3)') 'XXX', L, L_REV, 
!     &                   IOP_TAU(L),IOP_SSA(L) 

         DO K = 1, NMASK  
            SK = DBLE( SMASK(K) )          
            CK = CMASK(K)
            DO N = 0, 2
               IOP_PHA(N,L,K) = RAY_WT * PHARAY(L_REV,N,CK)
     &                        + AER_WT * SUM_PHAAER(N,CK) * SK
            ENDDO
            DO N = 3, N_MOMENTS-1
               IOP_PHA(N,L,K) = AER_WT * SUM_PHAAER(N,CK) * SK
            ENDDO
            ! Greek matrix, P11 set to 1.0 if not exactly 1 
            IF ( K ==1 ) IOP_PHA(0,L,K) = 1.0d0

         ENDDO ! K

         !=============================================================
         ! Linearized ptical proerty inputs
         !=============================================================
         IF ( LJACOB .AND. N_LPAR_4VLD >=1 ) THEN

            ! jacobian calculation only for aerosol layer
            ! Check if the layer has aerosol
            ! 1 for linearization calculation 
            !IF ( AEREXT > PRS0 ) LIOP_JCB(L) = 1

            !==========================================================
            ! Loop for linear parameters
            !==========================================================
            DO Q = 1, N_LPAR_4VLD

            ! Initialization
            L_PAR00 = 0d0
            L_PAR01 = 0d0
            L_PAR02 = 0d0
            L_PAR03 = 0d0

            ! Case statement for linearization parameters
            SELECT CASE ( TRIM(LPAR_NAMES(Q)) )


            !==========================================================
            ! Gas absorption optical thickness 
            !==========================================================
            CASE ( 'TOTAL_GAS' )

               IF ( GASEXT > PRS0 ) THEN
 
                  LIOP_JCB(Q,L)     = 1
                  LIOP_TAU(Q,L)     = GASEXT / TOTEXT
                  LIOP_SSA(Q,L)     = - LIOP_TAU(Q,L)
                  LIOP_PHA(Q,:,L,:) = 0d0        
                  TAUAER_WFS(Q,L)   = 0d0

               ENDIF

            !==========================================================
            ! Aerosol optical thickness 
            !==========================================================
            CASE ( 'AOD_M01', 'AOD_M02' )

               ! Mode #
               IM = 1
               IF ( TRIM( LPAR_NAMES(Q) ) == 'AOD_M02' ) IM = 2

               ! AOD of this mode
               L_PAR00 = AEREXT * FRC(IM)

               ! Prepare the three linear terms
               IF ( AEREXT * FRC1(IM) > PRS0 ) THEN

                  LIOP_JCB(Q,L) = 1 
                  L_PAR01 = L_PAR00
                  L_PAR02 = AERSCA * FRC1(IM)
                  DO J = 1, 6
                  DO N = 0, N_MOMENTS-1
                     L_PAR03(N,J) = L_PAR02 / AERSCA
     &                    * ( PHAAER(L_REV,N,J,IM) - SUM_PHAAER(N,J) )
                  ENDDO
                  ENDDO                  

               ENDIF

               ! non-variation of AOD 
               ! Needs to justified ... (xxu, 7/10/12) 
               IF ( NONVAR_AOD ) L_PAR02 = 0d0

            !==========================================================
            ! Single scattering albedo
            !==========================================================
            CASE ( 'SSA_M01', 'SSA_M02' )

               ! Mode #
               IM = 1
               IF ( TRIM( LPAR_NAMES(Q) ) == 'SSA_M02' ) IM = 2

               ! Single scattering albedo
               L_PAR00 = SSAAER(L_REV,IM)

               ! Prepare the three linear terms
               IF ( AEREXT * FRC1(IM) > PRS0  ) THEN

                  LIOP_JCB(Q,L) = 1
                  L_PAR01 = 0d0
                  L_PAR02 = AERSCA * FRC1(IM)
                  DO J = 1, 6
                  DO N = 0, N_MOMENTS-1
                     L_PAR03(N,J) = L_PAR02 / AERSCA
     &                    * ( PHAAER(L_REV,N,J,IM) - SUM_PHAAER(N,J) )
                  ENDDO
                  ENDDO

               ENDIF 

            !==========================================================
            ! Aerosol column mass concentration
            !==========================================================
            CASE ( 'MASS_M01', 'MASS_M02' )

               ! Mode #
               IM = 1
               IF ( TRIM( LPAR_NAMES(Q) ) == 'MASS_M02' ) IM = 2 

               ! Aerosol mass on this layer
               L_PAR00 = PROF_MASS(L_REV,IM)

               ! Prepare the three linear terms
               IF ( AEREXT * FRC1(IM) > PRS0 ) THEN

                  LIOP_JCB(Q,L) = 1
                  L_PAR01 = KEXT_MASS(IM) * L_PAR00
                  L_PAR02 = KSCA_MASS(IM) * L_PAR00
                  DO J = 1, 6
                  DO N = 0, N_MOMENTS-1
                     L_PAR03(N,J) = L_PAR02 / AERSCA 
     &                  * ( PHAAER(L_REV,N,J,IM) - SUM_PHAAER(N,J) )
                  ENDDO
                  ENDDO

               ENDIF

            !==========================================================
            ! Real and imaginary refractive index for single & 1st mode
            !==========================================================
            CASE ( 'NREAL_M01', 'NIMAG_M01', 
     &             'NREAL_M02', 'NIMAG_M02' )

               ! Other Mode # and Parameter #
               IF (      TRIM( LPAR_NAMES(Q) ) == 'NREAL_M01' ) THEN
                  IP = 1
                  IM = 1
               ELSE IF ( TRIM( LPAR_NAMES(Q) ) == 'NIMAG_M01' ) THEN
                  IP = 2
                  IM = 1 
               ELSE IF ( TRIM( LPAR_NAMES(Q) ) == 'NREAL_M02' ) THEN
                  IP = 1
                  IM = 2
               ELSE IF ( TRIM( LPAR_NAMES(Q) ) == 'NIMAG_M02' ) THEN
                  IP = 2
                  IM = 2
               ENDIF

               ! Parameter of refractive index
               L_PAR00 = RMRI(IP,IM)

               ! Prepare the three linear terms
               IF ( L_PAR00 /= 0d0 .AND. AEREXT > PRS0 ) THEN 

                  LIOP_JCB(Q,L) = 1
                  L_PAR01 = AEREXT * FRC(IM) / BULK_AOP(4,IM)
     &                    * LRFE_BULK(4,IP,IM)
                  L_PAR02 = AERSCA * FRC1(IM) / BULK_AOP(5,IM)
     &                    * LRFE_BULK(5,IP,IM)

                  DO J = 1, 6
                  DO N = 0, N_MOMENTS-1
                     L_PAR03(N,J) 
     &                = ( L_PAR02 * PHAAER(L_REV,N,J,IM) 
     &                  + AERSCA * FRC1(IM) * LRFE_EXPCOEF(J,N,IP,IM)
     &                  - SUM( PHAAER(L_REV,N,J,:)*FRC1(:) ) * L_PAR02 )
     &                / AERSCA

                  ENDDO
                  ENDDO
                  !PRINT*, L, FRC(IM), L_PAR00, L_PAR01, L_PAR02

               ENDIF

            !==========================================================
            ! Shape factor
            !==========================================================
            CASE ( 'SHAPE_M01', 'SHAPE_M02' )

               ! 3rd elements of refractive index variable
               IP = 3
               ! Mode #
               IM = 1
               IF ( TRIM( LPAR_NAMES(Q) ) == 'SHAPE_M02' ) IM = 2

               ! Shape factor
               L_PAR00 = SHAPE_PAR(IM)

               ! Prepare the three linear terms
               IF ( L_PAR00 /= 0d0 .AND. AEREXT * FRC1(IM) > PRS0 ) THEN

                  LIOP_JCB(Q,L) = 1
                  L_PAR01 = LRFE_BULK(4,IP,IM) * AEREXT * FRC(IM)
     &                    / BULK_AOP(4,IM)
                  L_PAR02 = LRFE_BULK(5,IP,IM) * AERSCA * FRC1(IM)
     &                    / BULK_AOP(5,IM)

                  DO J = 1, 6
                  DO N = 0, N_MOMENTS-1
                     L_PAR03(N,J)
     &                = ( L_PAR02 * PHAAER(L_REV,N,J,IM)
     &                  + AERSCA * FRC1(IM) * LRFE_EXPCOEF(J,N,IP,IM)
     &                  - SUM( PHAAER(L_REV,N,J,:)*FRC1(:) ) * L_PAR02 )
     &                / AERSCA

                  ENDDO
                  ENDDO

               ENDIF


            !==========================================================
            ! Size parameters for 1st and 2nd modes
            !==========================================================
            CASE ( 'SIZE01_M01', 'SIZE02_M01', 'SIZE03_M01', 
     &             'SIZE01_M02', 'SIZE02_M02', 'SIZE03_M02' )

               ! Mode # and Paramter # indices
               IF (      TRIM( LPAR_NAMES(Q) ) == 'SIZE01_M01' ) THEN
                  IP = 1
                  IM = 1
               ELSE IF ( TRIM( LPAR_NAMES(Q) ) == 'SIZE02_M01' ) THEN
                  IP = 2
                  IM = 1
               ELSE IF ( TRIM( LPAR_NAMES(Q) ) == 'SIZE03_M01' ) THEN
                  IP = 3
                  IM = 1
               ELSE IF ( TRIM( LPAR_NAMES(Q) ) == 'SIZE01_M02' ) THEN
                  IM = 2
                  IP = 1 
               ELSE IF ( TRIM( LPAR_NAMES(Q) ) == 'SIZE02_M02' ) THEN
                  IM = 2
                  IP = 2
               ELSE IF ( TRIM( LPAR_NAMES(Q) ) == 'SIZE03_M02' ) THEN
                  IM = 2
                  IP = 3
               ENDIF

               ! Parameter of aerosol size distribution
               L_PAR00 = DISTPAR(IP,IM)

               ! Prepare the three linear terms
               IF ( L_PAR00 /= 0d0 .AND. AEREXT * FRC1(IM) > PRS0 ) THEN

                  LIOP_JCB(Q,L) = 1
                  L_PAR01 = AEREXT * FRC(IM)
     &                    * ( LPSD_BULK(4,IP,IM) / BULK_AOP(4,IM)
     &                      - LPSD_DIST(4,IP,IM) / DIST(4,IM) )
                  L_PAR02 = AERSCA * FRC1(IM)
     &                    * ( LPSD_BULK(5,IP,IM) / BULK_AOP(5,IM)
     &                      - LPSD_DIST(4,IP,IM) / DIST(4,IM) )

                  DO J = 1, 6
                  DO N = 0, N_MOMENTS-1
                     L_PAR03(N,J)
     &                = ( L_PAR02 * PHAAER(L_REV,N,J,IM)
     &                  + AERSCA * FRC1(IM) * LPSD_EXPCOEF(J,N,IP,IM)
     &                  - SUM( PHAAER(L_REV,N,J,:)*FRC1(:) ) * L_PAR02 )
     &                / AERSCA

                  ENDDO
                  ENDDO

               ENDIF 
 
            !==========================================================       
            ! Profile parameters for each mode
            !==========================================================
            CASE ( 'PROF01_M01', 'PROF02_M01', 
     &             'PROF01_M02', 'PROF02_M02' )

               ! Other Mode # and Parameter #
               IF (      TRIM( LPAR_NAMES(Q) ) == 'PROF01_M01' ) THEN
                  IP = 1
                  IM = 1
               ELSE IF ( TRIM( LPAR_NAMES(Q) ) == 'PROF02_M01' ) THEN
                  IP = 2
                  IM = 1
               ELSE IF ( TRIM( LPAR_NAMES(Q) ) == 'PROF01_M02' ) THEN
                  IP = 1
                  IM = 2
               ELSE IF ( TRIM( LPAR_NAMES(Q) ) == 'PROF02_M02' ) THEN
                  IP = 2
                  IM = 2
               ENDIF

               ! Parameter of aerosol profile
               L_PAR00 = PROF_PAR(IP,IM)

               ! Prepare the three linear terms
               IF ( L_PAR00 /= 0d0 .AND. AEREXT * FRC1(IM) > PRS0 ) THEN

                  LIOP_JCB(Q,L) = 1
                  L_PAR01 = LPROF_AOD_PAR(L_REV,IP,IM) * L_PAR00
                  L_PAR02 = L_PAR01 * BULK_AOP(3,IM)

                  DO J = 1, 6
                  DO N = 0, N_MOMENTS-1
                     L_PAR03(N,J) = L_PAR02 / AERSCA
     &                  * ( PHAAER(L_REV,N,J,IM) - SUM_PHAAER(N,J) )
                  ENDDO
                  ENDDO


               ENDIF

!-----------------------------------------------------------------------
! prior to 11/6/12, xxu
! Now the Jacobians with respect mode fraction are dervied 
!  based on the VLIDORT calculated Jacobian of mass/AOD, which 
!  was moved to the vlidort_mod.f
!  (xxu, 11/5/12)
!            !==========================================================
!            ! Fraction of each mode
!            !==========================================================
!            CASE ( 'FRC_M01', 'FRC_M02' )
! 
!               ! Mode #1
!               IM = 1
!
!               ! Mode #2 
!               IF ( TRIM( LPAR_NAMES(Q) ) == 'FRC_M02' ) IM = 2
! 
!               ! Parameter of modal mass fraction 
!               L_PAR00 = FRC2(IM)
!
!               ! Prepare the three linear terms
!               IF ( L_PAR00 /= 1d0 .AND. L_PAR00 /= 0d0 .AND. 
!     &              AEREXT * FRC1(IM) > PRS0 ) THEN
!
!!                 L_PAR01 = L_PAR00 * AEREXT 
!!    &                    * ( FRC(IM)/FRC2(IM) - FRC(3-IM)/FRC2(3-IM)  )
!!                 L_PAR02 = L_PAR00 * AERSCA
!!    &                    * ( FRC1(IM)/FRC2(IM)- FRC1(3-IM)/FRC2(3-IM) )
!!
!!                  DO J = 1, 6
!!                  DO N = 0, N_MOMENTS-1
!!
!!                     L_PAR03(N,J) = L_PAR00 * FRC1(IM) * FRC1(3-IM)
!!     &                / ( FRC2(IM) * FRC2(3-IM) ) 
!!     &                * ( PHAAER(L_REV,N,J,IM) - PHAAER(L_REV,N,J,3-IM))
!!
!!                  ENDDO
!!                  ENDDO
!
!                  LIOP_JCB(Q,L) = 1
!                  L_PAR01 = PROF_MASS(L_REV,IM) 
!     &                    * ( KEXT_MASS(IM) - KEXT_MASS(3-IM) )
!                  L_PAR02 = PROF_MASS(L_REV,IM)  
!     &                    * ( KSCA_MASS(IM) - KSCA_MASS(3-IM) )
!
!                  DO J = 1, 6
!                  DO N = 0, N_MOMENTS-1
!                     
!                     L_PAR03(N,J) = PROF_MASS(L_REV,IM) * KSCA_MASS(IM) 
!     &                            * ( AERSCA - L_PAR02 )
!     &                            * ( PHAAER(L_REV,N,J,IM) 
!     &                              - PHAAER(L_REV,N,J,3-IM) )
!     &                            / AERSCA / AERSCA
!
!                  ENDDO
!                  ENDDO
!
!
!               ENDIF
!----------------------------------------------------------------------

            END SELECT

            !==========================================================
            ! Aggregate to the universal IOP for aerosol parameters
            !==========================================================
            IF ( TRIM( LPAR_NAMES(Q) ) /= 'TOTAL_GAS' ) THEN

               ! derivative of AOD = 0 for the non-variation of AOD
               IF ( NONVAR_AOD ) L_PAR01 = 0d0

               ! Save the AOD weighting functions
               TAUAER_WFS(Q,L) = L_PAR01

               ! Input to the universal expression (see my notes)
               LIOP_TAU(Q,L) = L_PAR01 / TOTEXT 
               LIOP_SSA(Q,L) = L_PAR02 / TOTSCA - LIOP_TAU(Q,L)

               DO K = 1, NMASK
                  SK = DBLE( SMASK(K) )
                  CK = CMASK(K)
                  DO N = 0, N_MOMENTS-1
                     BETA     = IOP_PHA(N,L,K)
                     BETA_AER = SUM_PHAAER(N,CK) * SK
                     TERM01   = L_PAR03(N,CK) * SK * AER_WT
                     TERM02   = L_PAR02 * (BETA_AER - BETA) / TOTSCA
                     IF ( BETA /= 0 ) THEN
                        ALL_TERMS =  (TERM01 + TERM02) / BETA
                        LIOP_PHA(Q,N,L,K) = ALL_TERMS
                     ENDIF
                  ENDDO ! LOOP:N
               ENDDO ! LOOP:K

               ! The first term of the P11 must be constant, i.e.,
               ! jacobian of zero
               LIOP_PHA(Q,0,L,1) = 0d0

            ENDIF

            !==========================================================
            ! End of loop for linearized parameters
            !==========================================================            
            ENDDO ! Q

         ENDIF

      ! End layer loop: L
      ENDDO

      ! Return to the calling routine 
      END SUBROUTINE AGGREGATE_IOP
!
!----------------------------------------------------------------------
!
      SUBROUTINE CLEANUP_IOP

      !  clean up the output arrays of this module
      IF (ALLOCATED (IOP_TAU     )) DEALLOCATE (IOP_TAU     )
      IF (ALLOCATED (IOP_SSA     )) DEALLOCATE (IOP_SSA     )
      IF (ALLOCATED (IOP_PHA     )) DEALLOCATE (IOP_PHA     )
      IF (ALLOCATED (LIOP_TAU    )) DEALLOCATE (LIOP_TAU    )
      IF (ALLOCATED (LIOP_SSA    )) DEALLOCATE (LIOP_SSA    )
      IF (ALLOCATED (LIOP_PHA    )) DEALLOCATE (LIOP_PHA    )
      IF (ALLOCATED (LIOP_JCB    )) DEALLOCATE (LIOP_JCB    )
      IF (ALLOCATED (TAUAER_WFS  )) DEALLOCATE (TAUAER_WFS  )

      END SUBROUTINE CLEANUP_IOP
!
!----------------------------------------------------------------------
!
      ! End of the module
      END MODULE IOP_MOD
