! $Id: lpar_mod.f, v1.2 2013/01/18 10:19:12 xxu Exp $
      MODULE LPAR_MOD 
!
!******************************************************************************
!  Module LPAR_MOD address the linearization (Jacobian) parameters. It 
!  associates the index for each linearization parameter, in terms of their 
!  names. 
!  (xxu, 3/7/11, 1/18/13)
! 
!  Module Variables:
!  ============================================================================
!  (1 ) N_LPAR_TOTAL ( INTEGER ) : # of total L parameters         [ none ]
!  (2 ) LPAR_NAMES   ( CH*30   ) : Names for the L parameters      [ none ] 
!   
!  Module Routines:
!  ============================================================================
!  (1 ) INIT_LPAR               : Define L parameters based on namelist 
!  (2 ) INIT_LPAR_ID            : Give ID for each parameter
!  (3 ) ALL_LPAR_NAMES          : specified parameters' name
!
!  REVISION HISTROY:
!  ============================================================================
!  Revision v1.2, xxu  1/18/13
!   - Add ID for all parameters (xxu, 1/18/13)
!  Revision v 1.1  
!   - Added the surface Linearization ( xxu, 5/17/11)
!
!******************************************************************************
! 
      IMPLICIT NONE

      ! Make everything PRIVATE ...
      PRIVATE

      ! ... except these routines
      PUBLIC :: INIT_LPAR

      ! ... and these variables
      PUBLIC :: N_LPAR_TOTAL 
      PUBLIC :: N_LPAR_4VLD
      PUBLIC :: N_LPAR_EXTRA
      PUBLIC :: N_LPAR_SURF
      PUBLIC :: LPAR_NAMES
      PUBLIC :: ID_AOD_M01,     ID_AOD_M02
      PUBLIC :: ID_MASS_M01,    ID_MASS_M02
      PUBLIC :: ID_SSA_M01,     ID_SSA_M02
      PUBLIC :: ID_NREAL_M01,   ID_NREAL_M02
      PUBLIC :: ID_NIMAG_M01,   ID_NIMAG_M02
      PUBLIC :: ID_SHAPE_M01,   ID_SHAPE_M02
      PUBLIC :: ID_SIZE01_M01,  ID_SIZE01_M02
      PUBLIC :: ID_SIZE02_M01,  ID_SIZE02_M02
      PUBLIC :: ID_SIZE03_M01,  ID_SIZE03_M02
      PUBLIC :: ID_PROF01_M01,  ID_PROF01_M02
      PUBLIC :: ID_PROF02_M01,  ID_PROF02_M02
      PUBLIC :: ID_TOTAL_GAS
      PUBLIC :: ID_AODFRC_M01,  ID_AODFRC_M02
      PUBLIC :: ID_MASSFRC_M01, ID_MASSFRC_M02

      !=================================================================
      ! MODULE VARIABLES
      !=================================================================
      INTEGER, PARAMETER :: MAX_LPAR       = 23
      INTEGER, PARAMETER :: MAX_LPAR_EXTRA = 4
      INTEGER            :: N_LPAR_TOTAL 
      INTEGER            :: N_LPAR_4VLD
      INTEGER            :: N_LPAR_EXTRA
      INTEGER            :: N_LPAR_SURF
      CHARACTER(LEN=30)  :: LPAR_NAMES(MAX_LPAR) 
      INTEGER            :: ID_AOD_M01,    ID_AOD_M02
      INTEGER            :: ID_MASS_M01,   ID_MASS_M02
      INTEGER            :: ID_SSA_M01,    ID_SSA_M02
      INTEGER            :: ID_NREAL_M01,  ID_NREAL_M02
      INTEGER            :: ID_NIMAG_M01,  ID_NIMAG_M02
      INTEGER            :: ID_SHAPE_M01,  ID_SHAPE_M02
      INTEGER            :: ID_SIZE01_M01, ID_SIZE01_M02
      INTEGER            :: ID_SIZE02_M01, ID_SIZE02_M02
      INTEGER            :: ID_SIZE03_M01, ID_SIZE03_M02
      INTEGER            :: ID_PROF01_M01, ID_PROF01_M02
      INTEGER            :: ID_PROF02_M01, ID_PROF02_M02
      INTEGER            :: ID_TOTAL_GAS
      INTEGER            :: ID_AODFRC_M01, ID_AODFRC_M02
      INTEGER            :: ID_MASSFRC_M01, ID_MASSFRC_M02

      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !=================================================================       
      CONTAINS

!------------------------------------------------------------------------------

      SUBROUTINE INIT_LPAR

      ! Referenced to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_GAS
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_AOD
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_SSA
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_MASS
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_REFR
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_SIZE
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_EPS
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_MFRC
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_SURF
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB_PROF
      USE NAMELIST_ARRAY_MOD, ONLY : IDIST
      USE NAMELIST_ARRAY_MOD, ONLY : IDPROF
      USE NAMELIST_ARRAY_MOD, ONLY : NMODE
      USE NAMELIST_ARRAY_MOD, ONLY : SSMODEL
      USE NAMELIST_ARRAY_MOD, ONLY : LPRT
      USE NAMELIST_ARRAY_MOD, ONLY : LLAMBERTIAN
      USE NAMELIST_ARRAY_MOD, ONLY : LBRDF
      USE NAMELIST_ARRAY_MOD, ONLY : N_BRDF

      ! Local variables
      INTEGER            :: N_LPAR_GAS
      INTEGER            :: N_LPAR_AOD,   N_LPAR_SSA
      INTEGER            :: N_LPAR_MASS,  N_LPAR_EPS 
      INTEGER            :: N_LPAR_REFR,  N_LPAR_SIZE
      INTEGER            :: N_LPAR_MFRC,  N_LPAR_PROF
      INTEGER            :: N1,           N2
      INTEGER            :: N,            I
      CHARACTER(LEN=30)  :: ALL_NAMES(MAX_LPAR+MAX_LPAR_EXTRA)

      !================================================================
      ! INIT_LPAR begins here!
      !================================================================

      ! Initializing
      I            = 0
      N_LPAR_GAS   = I
      N_LPAR_AOD   = I
      N_LPAR_SSA   = I
      N_LPAR_MASS  = I
      N_LPAR_REFR  = I
      N_LPAR_SIZE  = I
      N_LPAR_MFRC  = I
      N_LPAR_PROF  = I
      N_LPAR_EPS   = I
      N_LPAR_TOTAL = I
      N_LPAR_4VLD  = I
      N_LPAR_EXTRA = I
      N_LPAR_SURF  = I
 
      ID_AOD_M01     = I 
      ID_AOD_M02     = I
      ID_MASS_M01    = I
      ID_MASS_M02    = I
      ID_SSA_M01     = I
      ID_SSA_M02     = I
      ID_NREAL_M01   = I
      ID_NIMAG_M01   = I
      ID_NREAL_M02   = I
      ID_NIMAG_M02   = I
      ID_SHAPE_M01   = I
      ID_SHAPE_M02   = I
      ID_SIZE01_M01  = I
      ID_SIZE02_M01  = I
      ID_SIZE03_M01  = I
      ID_SIZE01_M02  = I
      ID_SIZE02_M02  = I
      ID_SIZE03_M02  = I
      ID_PROF01_M01  = I
      ID_PROF02_M01  = I
      ID_PROF01_M02  = I
      ID_PROF02_M02  = I
      ID_TOTAL_GAS   = I
      ID_AODFRC_M01  = I
      ID_AODFRC_M02  = I
      ID_MASSFRC_M01 = I
      ID_MASSFRC_M02 = I

      ! If no linearization
      IF ( .NOT. LJACOB ) RETURN

      ! Get all parameter names
      CALL ALL_LPAR_NAMES( ALL_NAMES )

      ! Assign each linear parameter
      ! Start with 0 for the total parameter number
      N = 0

      !================================================================ 
      ! Gas optical thickness
      !================================================================
      IF ( LJACOB_GAS ) THEN

         N_LPAR_GAS = 1
         LPAR_NAMES(N+1) = ALL_NAMES(23)
         N = N + N_LPAR_GAS

      ENDIF

      !================================================================ 
      ! Aerosol optical thickness 'AOD'
      !================================================================
      IF ( LJACOB_AOD ) THEN

         N_LPAR_AOD = NMODE
         LPAR_NAMES(N+1:N+NMODE) = ALL_NAMES(1:1+NMODE-1)

         N = N + N_LPAR_AOD

      ENDIF

      !================================================================
      ! Aerosol single scattering albedo 'SSA'
      !================================================================
      IF ( LJACOB_SSA ) THEN

         N_LPAR_SSA = NMODE
         LPAR_NAMES(N+1:N+NMODE) = ALL_NAMES(3:3+NMODE-1)
         N = N + N_LPAR_SSA

      ENDIF

      !================================================================
      ! Aerosol mass
      !================================================================
      IF ( LJACOB_MASS ) THEN

         N_LPAR_MASS = NMODE
         LPAR_NAMES(N+1:N+NMODE) = ALL_NAMES(5:5+NMODE-1)

         N = N + N_LPAR_MASS

      ENDIF

      ! Jacobian wrt refractive index, size, and profile:
      ! Treat differently for single or multi-mode
     
      !================================================================
      ! Refractive index (real and imag components) - Single mode
      !================================================================
      IF ( LJACOB_REFR ) THEN

         N_LPAR_REFR = 2 * NMODE
         LPAR_NAMES(N+1:N+N_LPAR_REFR) = ALL_NAMES(7:7+N_LPAR_REFR-1) 
         N = N + N_LPAR_REFR

      ENDIF
      !================================================================
      ! Aerosol particle shape factor, only for non-spherical (t-matrix)
      !================================================================
      IF ( LJACOB_EPS ) THEN
         
         DO I = 1, NMODE 
         IF ( SSMODEL(I) == 2 ) THEN

            N_LPAR_EPS = N_LPAR_EPS + 1
            LPAR_NAMES(N+N_LPAR_EPS) = ALL_NAMES(11+I-1)

         ENDIF
         ENDDO
         
         N = N + N_LPAR_EPS

      ENDIF

      !================================================================         
      ! Aerosol size paramters, depending on the size distribution
      !================================================================
      IF ( LJACOB_SIZE ) THEN

         ! Some dist have 2 parameter, while others have 3.

         ! First mode
         SELECT CASE ( IDIST(1) )
         CASE ( 1, 2, 3, 4, 5 )
            N1 = 2
         CASE ( 6, 7, 8 )
            N1 = 3 
         END SELECT

         ! Number of size parameters
         N_LPAR_SIZE = N1

         ! Parameter names
         LPAR_NAMES(N+1:N+N1) = ALL_NAMES(13:13+N1-1) 

         ! Add to the global index
         N = N + N1

         ! Second mode 
         IF ( NMODE == 2 ) THEN 

            SELECT CASE ( IDIST(2) )
            CASE ( 1, 2, 3, 4, 5 )
               N2 = 2
            CASE ( 6, 7, 8 )
               N2 = 3
            END SELECT

            ! Number of size parameters
            N_LPAR_SIZE = N_LPAR_SIZE + N2

            ! Parameter names
            LPAR_NAMES(N+1:N+N2) = ALL_NAMES(16:16+N2-1)

            ! Add to the global index
            N = N + N2

         ENDIF

      ENDIF

      !================================================================
      ! Aerosol vertical profile Jacobian - Single mode
      !================================================================
      IF ( LJACOB_PROF ) THEN

         ! Tread different profiles
         SELECT CASE ( IDPROF(1) )
         CASE ( 1 )
            N1 = 0
         CASE ( 2 )
            N1 = 1
         CASE ( 3 )
            N1 = 2
         END SELECT

         ! Only valid for profile #2 and #3 
         IF ( N1 > 0 ) THEN

            ! Number of paramters
            N_LPAR_PROF = N1

            ! Mode #1
            LPAR_NAMES(N+1:N+N1) = ALL_NAMES(19:19+N1-1)

            ! Add to the global index
            N = N + N1

         ENDIF 

         ! Mode #2 
         IF ( NMODE == 2 ) THEN

            ! Tread different profiles
            SELECT CASE ( IDPROF(2) )
            CASE ( 1 )
               N2 = 0
            CASE ( 2 )
               N2 = 1
            CASE ( 3 )
               N2 = 2
            END SELECT

            ! Only valid for profile #2 and #3 
            IF ( N2 > 0 ) THEN

               ! Number of parameters 
               N_LPAR_PROF = N_LPAR_PROF + N2 

               ! Parameter names for Mode #2
               LPAR_NAMES(N+1:N+N2) = ALL_NAMES(21:21+N2-1)

               ! Add to the global index
               N = N + N2

            ENDIF

         ENDIF

      ENDIF

      ! Number of Jacobians directly obtained from VLIDORT
      N_LPAR_4VLD = N

      ! Now start the derived Jacobians (EXTRA)

      !================================================================
      ! Mode fraction parameter - Derived Jacobian (EXTRA)
      !================================================================
      IF ( LJACOB_MFRC .AND. NMODE == 2 ) THEN

         ! AOD fractions
         IF ( LJACOB_AOD ) THEN 
            N_LPAR_MFRC = NMODE
            LPAR_NAMES(N+1:N+NMODE) = 
     &                        ALL_NAMES(MAX_LPAR+1:MAX_LPAR+NMODE)
            N = N + NMODE
         ENDIF

         ! Mass fractions
         !!!!!!!!!!!!!!!!!!!!!! Stoped here!!!!!
         IF ( LJACOB_MASS ) THEN
            N_LPAR_MFRC = NMODE + N_LPAR_MFRC
            LPAR_NAMES(N+1:N+NMODE) =
     &                       ALL_NAMES(MAX_LPAR+3:MAX_LPAR+2*NMODE)
            N = N + NMODE
         ENDIF

      ENDIF

      ! Total Number of Jacobian
      N_LPAR_TOTAL = N

      ! Number of extra Jacobians
      N_LPAR_EXTRA = N_LPAR_TOTAL - N_LPAR_4VLD

      ! Assign parameters' ID
      CALL INIT_LPAR_ID

      !================================================================
      ! Surface Jacobian
      !================================================================
      IF ( LJACOB_SURF ) THEN
         
         IF ( LLAMBERTIAN ) N_LPAR_SURF = 1
         IF ( LBRDF       ) N_LPAR_SURF = N_BRDF

      ENDIF     
 
      !================================================================
      ! Screen printing
      !================================================================
      WRITE(6,120) 
      WRITE(6,100) 'Number of Jacobian frim VLIDORT: ', N_LPAR_4VLD
      WRITE(6,100) 'Number of derived Jacobians    : ', N_LPAR_EXTRA 
      DO I = 1, N_LPAR_TOTAL
        WRITE(6,110) I, LPAR_NAMES(I)
      ENDDO
      WRITE(6,100) 'Number of surface paramters: ', N_LPAR_SURF
 100  FORMAT( A, I6)
 110  FORMAT( I6,':  ', A)
 120  FORMAT( ' - INIT_LPAR: initialize linearization parameters' )   
 130  FORMAT( ' - INIT_LPAR: User specified parameter is not support!' )

      ! Return to the calling routine
      RETURN

      END SUBROUTINE INIT_LPAR

!------------------------------------------------------------------------------
 
      SUBROUTINE INIT_LPAR_ID

      ! Local variables
      INTEGER  :: I

      !================================================================
      ! INIT_LPAR_ID begin here!
      !================================================================

      DO I = 1, N_LPAR_TOTAL

         ! Case statement for linearization parameters
         SELECT CASE ( TRIM(LPAR_NAMES(I)) )

         CASE( 'AOD_M01'     ); ID_AOD_M01     = I 
         CASE( 'AOD_M02'     ); ID_AOD_M02     = I
         CASE( 'SSA_M01'     ); ID_SSA_M01     = I
         CASE( 'SSA_M02'     ); ID_SSA_M02     = I
         CASE( 'MASS_M01'    ); ID_MASS_M01    = I
         CASE( 'MASS_M02'    ); ID_MASS_M02    = I
         CASE( 'NREAL_M01'   ); ID_NREAL_M01   = I
         CASE( 'NIMAG_M01'   ); ID_NIMAG_M01   = I
         CASE( 'NREAL_M02'   ); ID_NREAL_M02   = I
         CASE( 'NIMAG_M02'   ); ID_NIMAG_M02   = I
         CASE( 'SHAPE_M01'   ); ID_SHAPE_M01   = I
         CASE( 'SHAPE_M02'   ); ID_SHAPE_M02   = I
         CASE( 'SIZE01_M01'  ); ID_SIZE01_M01  = I
         CASE( 'SIZE02_M01'  ); ID_SIZE02_M01  = I
         CASE( 'SIZE03_M01'  ); ID_SIZE03_M01  = I
         CASE( 'SIZE01_M02'  ); ID_SIZE01_M02  = I
         CASE( 'SIZE02_M02'  ); ID_SIZE02_M02  = I
         CASE( 'SIZE03_M02'  ); ID_SIZE03_M02  = I
         CASE( 'PROF01_M01'  ); ID_PROF01_M01  = I
         CASE( 'PROF02_M01'  ); ID_PROF02_M01  = I
         CASE( 'PROF01_M02'  ); ID_PROF01_M02  = I
         CASE( 'PROF02_M02'  ); ID_PROF02_M02  = I
         CASE( 'TOTAL_GAS'   ); ID_TOTAL_GAS   = I
         CASE( 'AODFRC_M01'  ); ID_AODFRC_M01  = I
         CASE( 'AODFRC_M02'  ); ID_AODFRC_M02  = I
         CASE( 'MASSFRC_M01' ); ID_MASSFRC_M01 = I
         CASE( 'MASSFRC_M02' ); ID_MASSFRC_M02 = I

         END SELECT

      ENDDO

      END SUBROUTINE INIT_LPAR_ID

!------------------------------------------------------------------------------

      SUBROUTINE ALL_LPAR_NAMES( ALL_NAMES )
!
!******************************************************************************
!  Subroutine ALL_LPAR_NAMES define all parameters' name for Jacobian
!  calculation.
!  (xxu, 7/7/12)
!******************************************************************************
!
      ! Arguments
      CHARACTER(LEN=30),INTENT(OUT):: ALL_NAMES(MAX_LPAR+MAX_LPAR_EXTRA)

      !================================================================
      ! ALL_LPAR_NAMES begin here!
      !================================================================

      ! Bisic jacobians calculated by VLIDORT
      ALL_NAMES( 1) = 'AOD_M01'
      ALL_NAMES( 2) = 'AOD_M02'
      ALL_NAMES( 3) = 'SSA_M01'
      ALL_NAMES( 4) = 'SSA_M02'
      ALL_NAMES( 5) = 'MASS_M01'
      ALL_NAMES( 6) = 'MASS_M02'
      ALL_NAMES( 7) = 'NREAL_M01'
      ALL_NAMES( 8) = 'NIMAG_M01'
      ALL_NAMES( 9) = 'NREAL_M02'
      ALL_NAMES(10) = 'NIMAG_M02'
      ALL_NAMES(11) = 'SHAPE_M01'
      ALL_NAMES(12) = 'SHAPE_M02'
      ALL_NAMES(13) = 'SIZE01_M01'
      ALL_NAMES(14) = 'SIZE02_M01'
      ALL_NAMES(15) = 'SIZE03_M01'
      ALL_NAMES(16) = 'SIZE01_M02'
      ALL_NAMES(17) = 'SIZE02_M02'
      ALL_NAMES(18) = 'SIZE03_M02'
      ALL_NAMES(19) = 'PROF01_M01'
      ALL_NAMES(20) = 'PROF02_M01'
      ALL_NAMES(21) = 'PROF01_M02'
      ALL_NAMES(22) = 'PROF02_M02'
      ALL_NAMES(23) = 'TOTAL_GAS'

      ! Derived Jacobians 
      ALL_NAMES(MAX_LPAR+1) = 'AODFRC_M01'
      ALL_NAMES(MAX_LPAR+2) = 'AODFRC_M02'
      ALL_NAMES(MAX_LPAR+3) = 'MASSFRC_M01'
      ALL_NAMES(MAX_LPAR+4) = 'MASSFRC_M02'

      END SUBROUTINE ALL_LPAR_NAMES

!------------------------------------------------------------------------------

      ! End of module
      END MODULE LPAR_MOD
