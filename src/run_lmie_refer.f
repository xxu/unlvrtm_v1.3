! $Id: run_lmie.f, v 1.1 2011/11/29 14:13:41 xxu $Exp
      SUBROUTINE  RUN_LMIE_REFERENCE( REFER_DIST, REFER_BULK_AOP )
!
!******************************************************************************
!  Subourtine RUN_LMIE_REFERENCE runs the linearized mie code with the
!   referenced aerosol physical properties read in the namelist.ini
!   file. The calculated optical properties are used to calculate the
!   aerosl mass. Generally, if the referenced AOD (or mass) is used, the
!   aerosl mass will be fixed, and one is able to conduct simulations by
!   varying other aerosol parameters, like wavelength, refractive index,
!   size distribution, vertical profiles, etc.
!  The code is modified based on run_lmie.f.
!   (xxu, 07/12/12)
!  
!  Module referenced by run_lmie.f
!  ============================================================================
!  (1 ) namelist_array_mod.f
!  (2 ) aerosol_mod.f
!  (3 ) RTSMIE_parameters_m.f90 (from LMIE library)
!  (4 ) error_mod.f
!
!  NOTES:
! (1 ) The MIE_BULK is the old version (2009) of LMIE code has 4 elements, 
!      inlcuding extinction efficiency (Qext), scattering efficiency (Qsca), 
!      omega, and assymmetric factor. Now in the new version (2011), 
!      assymmetric factor is separated, and the first two elements are 
!      extinction (k_ext) and scattering coefficients (k_sca). They are related
!      by Qext = k_ext / G, where G is the geometric cross section. Hence,
!      in this code, what we need is Qext and its derivatives, G need to be 
!      applied. (xxu, 12/16/11)
!******************************************************************************
!
      ! References to F90 modules
      USE RTSMIE_PARAMETERS_M, ONLY : DP
      USE RTSMIE_PARAMETERS_M, ONLY : MAX_MIE_ANGLES, MAX_MIE_SIZES
      USE RTSMIE_SOURCECODE_PLUS_M, ONLY : RTSMIE_MAIN_PLUS 
      USE NAMELIST_ARRAY_MOD,  ONLY : LPRT
      USE NAMELIST_ARRAY_MOD,  ONLY : REFER_LAMDA,    REFER_RMRI
      USE NAMELIST_ARRAY_MOD,  ONLY : REFER_LMONODIS, REFER_MONOR
      USE NAMELIST_ARRAY_MOD,  ONLY : REFER_SIZERANGE,REFER_IDIST
      USE NAMELIST_ARRAY_MOD,  ONLY : REFER_DISTPAR
      USE ERROR_MOD,           ONLY : ERROR_STOP
      USE AEROSOL_MOD,         ONLY : NPANGLE

      IMPLICIT NONE

      !================================================================
      ! Varibles used for calling the Linear MIE routine
      !  See RTSMIE code for details ...
      !================================================================

      ! Arguments
      REAL*8,  INTENT(OUT) :: REFER_DIST(5)
      REAL*8,  INTENT(OUT) :: REFER_BULK_AOP(5)

      ! logicals
      LOGICAL       :: DO_EXPCOEFFS
      LOGICAL       :: DO_FMATRIX
      LOGICAL       :: DO_MONODISPERSE
      LOGICAL       :: DO_LINEARREF
      LOGICAL       :: DO_LINEARPSD

      ! Other inputs
      INTEGER       :: PSD_INDEX
      REAL(KIND=DP) :: PSD_PARS(3)
      REAL(KIND=DP) :: MONORADIUS
      REAL(KIND=DP) :: R1
      REAL(KIND=DP) :: R2
      LOGICAL       :: FIXR1R2
      INTEGER       :: NBLOCKS
      INTEGER       :: NWEIGHTS
      REAL(KIND=DP) :: XPARTICLE_LIMIT
      REAL(KIND=DP) :: R1R2_CUTOFF
      INTEGER       :: N_FMATRIX_ANGLES
      REAL(KIND=DP) :: FMATRIX_ANGLES(MAX_MIE_ANGLES)
      REAL(KIND=DP) :: WAVELENGTH
      REAL(KIND=DP) :: N_REAL
      REAL(KIND=DP) :: N_IMAG

      ! Outputs
      REAL(KIND=DP) :: MIE_BULK(3)
      REAL(KIND=DP) :: MIE_ASYMM
      INTEGER       :: MIE_NCOEFFS
      REAL(KIND=DP) :: MIE_EXPCOEFFS(6,0:MAX_MIE_ANGLES)
      REAL(KIND=DP) :: MIE_FMATRIX(4,MAX_MIE_ANGLES)
      REAL(KIND=DP) :: MIE_DIST(5)

      REAL(KIND=DP) :: LPSD_MIE_BULK(3,3)
      REAL(KIND=DP) :: LPSD_MIE_ASYMM(3)
      REAL(KIND=DP) :: LPSD_MIE_EXPCOEFFS(6,0:MAX_MIE_ANGLES,3)
      REAL(KIND=DP) :: LPSD_MIE_FMATRIX(4,MAX_MIE_ANGLES,3)
      REAL(KIND=DP) :: LPSD_MIE_DIST(5,3)
      REAL(KIND=DP) :: LRFE_MIE_BULK(3,3)
      REAL(KIND=DP) :: LRFE_MIE_ASYMM(2)
      REAL(KIND=DP) :: LRFE_MIE_EXPCOEFFS(6,0:MAX_MIE_ANGLES,2)
      REAL(KIND=DP) :: LRFE_MIE_FMATRIX(4,MAX_MIE_ANGLES,2)

      ! diagnostic outputs
      LOGICAL            :: FAIL
      INTEGER            :: ISTATUS
      CHARACTER(LEN=255) :: MESSAGE, TRACE, ACTION, ERRMSG

      !================================================================
      ! Additional local varibles
      !================================================================
      INTEGER       :: J, II, IU
      REAL*8        :: MF1, MF2
       

      WRITE(6, '(A)' ) ' RUN_LMIE_REFERENCE: Mie property for ref. AOD'

      !================================================================
      ! RUN_LMIE begins here!
      !================================================================

      ! Initializations... logicals
      DO_EXPCOEFFS     = .FALSE.
      DO_FMATRIX       = .FALSE.
      DO_LINEARREF     = .FALSE.    ! Ture for derivatives wrt refract
      DO_LINEARPSD     = .FALSE.    ! True for derivatives wrt size pars
      FIXR1R2          = .FALSE.    ! False for using the namelisted R1 R2
      
      ! Initializations... Some constants & dimensions 
      NBLOCKS          = 100
      NWEIGHTS         = 10
      XPARTICLE_LIMIT  = 1000.

      ! Initializations... Fmatrix angles are regular here between 0 and 180.
      N_FMATRIX_ANGLES = NPANGLE
      IF ( DO_FMATRIX ) THEN
         DO J = 1, N_FMATRIX_ANGLES 
            FMATRIX_ANGLES(j) = DBLE(J-1) * 180.0d0
     &                        / DBLE( N_FMATRIX_ANGLES - 1 )
         ENDDO
      ELSE
         FMATRIX_ANGLES = 0.0d0
      ENDIF

      ! End of Initializations...

      !================================================================
      ! Mono-mode OR 1st mode of bi-mode calculation
      !================================================================

      ! Assign variables for calling LMIE routine
      DO_MONODISPERSE = REFER_LMONODIS
      MONORADIUS      = REFER_MONOR
      PSD_INDEX       = REFER_IDIST
      PSD_PARS(1:3)   = REFER_DISTPAR(1:3)
      R1              = REFER_SIZERANGE(1)
      R2              = REFER_SIZERANGE(2)
      WAVELENGTH      = REFER_LAMDA * 1d-3
      N_REAL          = REFER_RMRI(1)
      N_IMAG          = ABS( REFER_RMRI(2) )


      ! Screen print debugging
      IF ( LPRT ) THEN
         WRITE(6,'(a)') '- For Reference AOD'
         WRITE(6,200)   'PSD_INDEX =', PSD_INDEX
         WRITE(6,200)   'PSD_PARS  =', PSD_PARS
         WRITE(6,200)   'R1 & R2   =', R1, R2
         WRITE(6,200)   'Wave      =', WAVELENGTH
         WRITE(6,200)   'Refractive=', N_REAL, N_IMAG
      ENDIF

      ! Call the RTSMIE routine
      CALL RTSMIE_MAIN_PLUS    
     &   ( DO_EXPCOEFFS, DO_FMATRIX, DO_MONODISPERSE,           ! I
     &     DO_LINEARREF, DO_LINEARPSD,                          ! I
     &     PSD_INDEX, PSD_PARS, MONORADIUS,                     ! I
     &     R1, R2,  FIXR1R2,                                    ! I
     &     NBLOCKS, NWEIGHTS, XPARTICLE_LIMIT, R1R2_CUTOFF,     ! I
     &     N_FMATRIX_ANGLES, FMATRIX_ANGLES,                    ! I
     &     WAVELENGTH, N_REAL, N_IMAG,                          ! I
     &     MIE_BULK, MIE_ASYMM, MIE_NCOEFFS,                    ! O
     &     MIE_EXPCOEFFS, MIE_FMATRIX, MIE_DIST,                ! O
     &     LPSD_MIE_BULK, LPSD_MIE_ASYMM,                       ! O
     &     LPSD_MIE_EXPCOEFFS, LPSD_MIE_FMATRIX, LPSD_MIE_DIST, ! O
     &     LRFE_MIE_BULK, LRFE_MIE_ASYMM,                       ! O
     &     LRFE_MIE_EXPCOEFFS, LRFE_MIE_FMATRIX,                ! O
     &     FAIL, ISTATUS, MESSAGE, TRACE, ACTION )             ! O
 
      ! Check the run status
      IF ( FAIL ) THEN
         WRITE(6,'(a)') TRIM(ADJUSTL(MESSAGE))
         WRITE(6,'(a)') TRIM(ADJUSTL(TRACE  ))
         WRITE(6,'(a)') TRIM(ADJUSTL(ACTION )) 
         ERRMSG = 'RTSMie calculation failed ' 
         CALL ERROR_STOP( ERRMSG, 'run_lmie.f' )
      ENDIF

      ! Screen print for debug
      IF ( LPRT ) THEN
         WRITE(6,200) 'MIE_BULK =', MIE_BULK
         WRITE(6,200) 'MIE_DIST =', MIE_DIST
         WRITE(6,200) 'R1 & R2  =', R1, R2
      ENDIF  

      !----------------------------------------------------------------
      ! Convert extinction coefficients to efficiency factor
      !           Qext = kext / G  
      ! And the factor here: == Qext / reff
      ! where Qext: ext efficiency
      !       kext: ext xsec
      !       G   : geometric Xsec
      !       reff: effective radius 
      !----------------------------------------------------------------
      !REFER_FACTOR = MIE_BULK(1) / MIE_DIST(2) / MIE_DIST(4)

      REFER_DIST(:)       = MIE_DIST(:)
      REFER_BULK_AOP(1:3) = MIE_BULK(1:3)  
      REFER_BULK_AOP(4:5) = MIE_BULK(1:2) / MIE_DIST(2)

 200  FORMAT(A,1P5E11.3)

      ! Return the calling routine
      END SUBROUTINE  RUN_LMIE_REFERENCE
