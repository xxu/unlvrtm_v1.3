! $Id: vlidort_array_mod.f, v beta 5 2011/12/08 11:40:54 xxu art-rtm$
      MODULE VLIDORT_ARRAY_MOD
!
!*****************************************************************************
!  Module VLIDORT_ARRAY_MOD contains variables used to store the VLIDORT 
!  outputs, including the normalized radiance and their Jacobians with 
!  respect to the aerosol physical and optical properties.
!  (xxu, 12/8/11)
!
!  Module Variables:
!  ============================================================================
!  (1 ) STOKES_COMPS   (REAL*8): Stokes parameters             [none]
!  (2 ) JACKS_PROFLS   (REAL*8): Atmos profil Jacobians        [none]
!  (3 ) JACKS_COLUMN   (REAL*8): Atmos column Jacobians        [none]
!  (4 ) JACKS_SURF     (REAL*8): Surface reflectance Jacobians [none]
!
!  Module Routines:
!  ============================================================================
!  (1 ) INIT_VLIDORT_ARRAY     : Allocate module arrays
!  (2 ) CLEANUP_VLIDORT_ARRAY  : Deallocate module arrays
!
!  NOTES:
!  (1 ) This module was orginally part of the module VLIDORT_MOD, but now 
!       serves as separated module for clearness. (xxu, 12/8/11)
!
!******************************************************************************
!
      IMPLICIT NONE

      ! Make everything PUBLIC ...
      PUBLIC

      ! ... except these routines

      ! ... and these variables

      !================================================================
      !  Module variables
      !================================================================
      REAL*8,ALLOCATABLE   :: STOKES_COMPS(:,:,:)
      REAL*8,ALLOCATABLE   :: JACKS_PROFLS(:,:,:,:,:)
      REAL*8,ALLOCATABLE   :: JACKS_COLUMN(:,:,:,:)
      REAL*8,ALLOCATABLE   :: JACKS_SURF  (:,:,:,:)

      !================================================================ 
      ! ROUTINES follow below the "CONTAINS" statement ...
      !================================================================
      CONTAINS

!------------------------------------------------------------------------------

      SUBROUTINE INIT_VLIDORT_ARRAY( RESET_IS_INIT )

      ! References to F90 modules 
      USE ERROR_MOD,          ONLY : ALLOC_ERR
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS,     N_STOKS
      USE NAMELIST_ARRAY_MOD, ONLY : N_GEOM,       NRECEP_LEV
      USE LPAR_MOD,           ONLY : N_LPAR_TOTAL, N_LPAR_SURF
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB
 
      ! Arguments
      LOGICAL, OPTIONAL      :: RESET_IS_INIT

      ! Local variables
      INTEGER            :: AS
      LOGICAL, SAVE      :: IS_INIT = .FALSE.

      ! Reset IS_INIT as false when clean up
      IF ( PRESENT( RESET_IS_INIT ) ) THEN
         IS_INIT = .FALSE.
         PRINT*, ' INIT_VLIDORT_ARRAY: IS_INIT is reset to FALSE'
         RETURN
      ENDIF

      !=================================================================
      ! INIT_VLIDORT_ARRAY begins here!
      !=================================================================     

      ! Return if we have already initialized
      IF ( IS_INIT ) RETURN

      ! STOKES_COMPS
      ALLOCATE ( STOKES_COMPS(NRECEP_LEV, N_GEOM, N_STOKS), STAT = AS )
      IF ( AS/=0 ) CALL ALLOC_ERR( 'STOKES_COMPS' )
      STOKES_COMPS = 0d0
 
      ! Jacobian variables
      IF ( LJACOB ) THEN

         ! JACKS_PROFLS
         ALLOCATE ( JACKS_PROFLS(N_LPAR_TOTAL, N_LAYERS, NRECEP_LEV, 
     &                           N_GEOM, N_STOKS), STAT = AS )
         IF ( AS/=0 ) CALL ALLOC_ERR( 'JACKS_PROFLS' )
         JACKS_PROFLS = 0

         ! JACKS_COLUMN
         ALLOCATE ( JACKS_COLUMN(N_LPAR_TOTAL, NRECEP_LEV,
     &                           N_GEOM, N_STOKS), STAT = AS )
         IF ( AS/=0 ) CALL ALLOC_ERR( 'JACKS_COLUMN' )
         JACKS_COLUMN = 0

         ! JACKS_SURF
         ALLOCATE ( JACKS_SURF(N_LPAR_SURF, NRECEP_LEV,
     &                         N_GEOM, N_STOKS), STAT = AS )
         IF ( AS/=0 ) CALL ALLOC_ERR( 'JACKS_SURF' )
         JACKS_SURF = 0

      ENDIF

      ! Reset IS_INIT so we do not allocate arrays again
      IS_INIT = .TRUE.

      WRITE(6, 100)
 100  FORMAT( " - INIT_VLIDORT_ARRAY: Initialize the VLIDORT arrays" )

      ! Return to the calling routine 
      END SUBROUTINE INIT_VLIDORT_ARRAY

!------------------------------------------------------------------------------

      SUBROUTINE CLEANUP_VLIDORT_ARRAY

      ! Local variables
      LOGICAL                :: RESET_IS_INIT = .TRUE.

      !  clean up the output arrays of this module
      IF ( ALLOCATED( STOKES_COMPS ) ) DEALLOCATE ( STOKES_COMPS )
      IF ( ALLOCATED( JACKS_PROFLS ) ) DEALLOCATE ( JACKS_PROFLS )
      IF ( ALLOCATED( JACKS_COLUMN ) ) DEALLOCATE ( JACKS_COLUMN ) 
      IF ( ALLOCATED( JACKS_SURF   ) ) DEALLOCATE ( JACKS_SURF   )

      ! Reset IS_INIT in routine INIT_VLIDORT_ARRAY
      CALL INIT_VLIDORT_ARRAY( RESET_IS_INIT )

      END SUBROUTINE CLEANUP_VLIDORT_ARRAY

!------------------------------------------------------------------------------

      ! End of module
      END MODULE VLIDORT_ARRAY_MOD

