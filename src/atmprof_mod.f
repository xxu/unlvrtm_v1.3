! $Id: atmprof_mod.f, v1.2 2010/11/23 14:53:21 xxu
      MODULE ATMPROF_MOD
!
!******************************************************************************
!  Module ATMPROF_MOD contains the build-in atmospheric profiles (from OPTICAL
!  PROPERTIES OF THE ATMOSPHERE, THIRD EDITION' AFCRL-72-0497 (AD 753 075),
!  'U.S. STANDARD ATMOSPHERE 1976' AND 'SUPPLEMENTS 1966'), plus collected 
!  constituent profiles (REF) and sets other constants related to the 
!  atmospheric profiles. 
!  (xxu, 11/23/10, 1/28/13)
!
!  Module Variables:
!  ============================================================================ 
!  (1 ) MAXMOL   ( INTEGER ) : max # of the trace gases
!  (2 ) STDL     ( INTEGER ) : # of levels for the build-in profiles
!  (3 ) ATMZ     ( REAL*8  ) : level height                     [ km    ]
!  (4 ) ATMP     ( REAL*8  ) : level pressure                   [ hPa   ]
!  (5 ) ATMT     ( REAL*8  ) : level temperature                [ K     ]
!  (6 ) INTENSITY( REAL*8  ) : level air molecular # density    [ #/cm3 ] **removed  
!  (7 ) AMOLS    ( REAL*8  ) : level trace gas mixing ratio     [ ppbm  ] **removed
!  (8 ) LAYZ     ( REAL*8  ) : layer height                     [ km    ] **removed
!  (9 ) BOXH     ( REAL*8  ) : layer box height                 [ m     ]
!  (10) LAYP     ( REAL*8  ) : layer pressure                   [ hPa   ]
!  (11) LAYT     ( REAL*8  ) : layer temperature                [ K     ]
!  (12) LAYINT   ( REAL*8  ) : layer air molecular # density    [ #/cm3 ]
!  (13) LAYMOLS  ( REAL*8  ) : layer trace gas mixing ratio     [ ppbm  ]
!  (14) STDATMZ  ( REAL*8  ) : build-in level height            [ km    ]
!  (15) STDATMP  ( REAL*8  ) : build-in level pressure          [ hPa   ]
!  (16) STDATMT  ( REAL*8  ) : build-in level temperature       [ K     ]
!  (17) STDINTENSITY( R*8  ) : build-in level air # density     [ #/cm3 ]
!  (18) STDAMOLS ( REAL*8  ) : build-in level gas mixing ratio  [ ppbm  ]
!  (19) STDBOXH
!  (20) STDLAYP
!  (21) STDLAYT
!  (22) STDLAYINT
!  (23) STDLAYMOLS
!   
!  Module Routines
!  ============================================================================
!  (1 ) LOAD_ATMOS           : load atmos profiles
!  (2 ) INIT_ATMOS           : allocate module variables 
!  (3 ) CLEANUP_ATMOS        : deallocate module variables
!  (4 ) STDATMOS             : build-in atmos profile databases
!
!  REVISION HISTORY
!  ============================================================================
!  Revision v1.2 (xxu, 1/28/13)
!    (1 ) Restore variable ATMT for 33-, 26-, and 3-layer atmosphere,
!         which will be used as input for VLIDORT (xxu, 1/28/13)
!  Revision v1.1 (xxu, 3/2/11)
!    (1 ) Added module variables for the bulk layer quantities (LAYZ, etc)the 
!         calculation of the bulk layer quantities are moved here from the main 
!         code. (xxu, 3/2/11)
!    (2 ) Remove variables ATMT, INTENISTY, AMOLS, and LAYZ, as they are useless;
!         and add variables STDLAYP, STDLAYT, STDBOXH, STDLAYINT, STDLAYMOLS, 
!         which are directly calculated from standard atmospheric profile.
!         (xxu, 10/20/11)
!    (3 ) Now add a scaling factor for gas mixing ratio: GASSF. User can
!         define the scaling factor in the input namelist.ini file.
!         (xxu, 8/22/13)
!******************************************************************************
! 
      IMPLICIT NONE
 
      !=================================================================
      ! MODULE PRIVATE DECLARATIONS -- keep certain internal variables 
      ! and routines from being seen outside "atmprof_mod.f"
      !=================================================================

      ! Make everything PRIVATE ...
      PRIVATE

      ! ... except these routines
      PUBLIC :: LOAD_ATMOS
      PUBLIC :: INIT_ATMOS
      PUBLIC :: CLEANUP_ATMOS

      ! ... and these variables
      PUBLIC :: MAXMOL,       STDL
      PUBLIC :: ATMZ,         ATMP
      PUBLIC :: ATMT
      !PUBLIC :: INTENSITY,    AMOLS
      !PUBLIC :: LAYZ,         LAYP
      PUBLIC :: LAYP
      PUBLIC :: LAYT,         BOXH
      PUBLIC :: LAYINT,       LAYMOLS
      !PUBLIC :: STDATMZ,      STDATMP 
      !PUBLIC :: STDATMT
      !PUBLIC :: STDINTENSITY, STDAMOLS

      ! Module Variables
      INTEGER,PARAMETER  :: MAXMOL = 29
      INTEGER,PARAMETER  :: STDL   = 50
      REAL*8,ALLOCATABLE :: ATMZ(:)
      REAL*8,ALLOCATABLE :: ATMP(:) 
      REAL*8,ALLOCATABLE :: ATMT(:)
      !REAL*8,ALLOCATABLE :: INTENSITY(:)
      !REAL*8,ALLOCATABLE :: AMOLS(:,:)
      !REAL*8,ALLOCATABLE :: LAYZ(:)
      REAL*8,ALLOCATABLE :: LAYP(:)
      REAL*8,ALLOCATABLE :: LAYT(:)
      REAL*8,ALLOCATABLE :: LAYINT(:)
      REAL*8,ALLOCATABLE :: LAYMOLS(:,:)
      REAL*8,ALLOCATABLE :: BOXH(:)
      REAL*8             :: STDATMZ(STDL)
      REAL*8             :: STDATMP(STDL)
      REAL*8             :: STDATMT(STDL)
      REAL*8             :: STDINTENSITY(STDL) 
      REAL*8             :: STDAMOLS(STDL,MAXMOL)
      REAL*8             :: STDBOXH(STDL-1)
      REAL*8             :: STDLAYP(STDL-1)
      REAL*8             :: STDLAYT(STDL-1)
      REAL*8             :: STDLAYMOLS(STDL-1,MAXMOL)
      REAL*8             :: STDLAYINT(STDL-1)

      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !=================================================================
      CONTAINS

!------------------------------------------------------------------------------

      SUBROUTINE LOAD_ATMOS ( IATM )

      ! Referenced to F90 modules
      USE ERROR_MOD,          ONLY : ERROR_STOP   
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS
      USE NAMELIST_ARRAY_MOD, ONLY : LATMOS_M1, LATMOS_M2
      USE NAMELIST_ARRAY_MOD, ONLY : LATMOS_M3
      USE NAMELIST_ARRAY_MOD, ONLY : NGAS,      GASNAME
      USE NAMELIST_ARRAY_MOD, ONLY : GASSF

      ! Arguments
      INTEGER, INTENT(IN) :: IATM

      ! Local variables
      INTEGER             :: L, I
      CHARACTER(LEN=255)  :: ERRMSG

      !================================================================
      !  LOAD_ATMOS begins here!
      !================================================================

      ! Print to screen
      !WRITE( 6, '(/a)') REPEAT( '-', 79 )
      WRITE( 6, '(/a)') ' - LOAD_ATMOS: load atmos profile'

      ! profile indices for the build-in databases
      IF ( IATM == 1 .OR. IATM == 2 .OR. IATM == 3 .OR.
     &     IATM == 4 .OR. IATM == 5 .OR. IATM == 6 ) THEN
         
         ! Assign standard atmosheric profile for certain type
         CALL STDATMOS ( IATM )

         ! Average variables from layer boundaries to layer for std atmpsheric profiles
         DO L = 1, STDL-1

            STDLAYP(L) = ( STDATMP(L+1) + STDATMP(L) ) / 2d0
            STDBOXH(L) = ( STDATMZ(L+1) - STDATMZ(L) ) * 1d3
            STDLAYT(L) = ( STDATMT(L+1) * STDINTENSITY(L+1) 
     &                   + STDATMT(L) * STDINTENSITY(L)      )
     &                 / ( STDINTENSITY(L+1) + STDINTENSITY(L) )
            STDLAYMOLS(L,:) = ( STDAMOLS(L+1,:) * STDINTENSITY(L+1)
     &                        + STDAMOLS(L,:) * STDINTENSITY(L)   )
     &                      / ( STDINTENSITY(L+1) + STDINTENSITY(L) )
            STDLAYINT(L) = ( STDATMP(L) - STDATMP(L+1) ) * 1d2 / 9.81d0 
     &                   / STDBOXH(L)*1d3 * 6.02252d23 / 28.966d0 * 1d-6

         ENDDO

         ! If the specified later number exceeds 33 then, use the
         ! 49-layer standard data
         IF ( N_LAYERS > 33 ) THEN
 
            CALL ATMOS_PROF_LXX
 
         ! Otherwise use the reduced 33-layer(deflat) profile
         !  Also two specific profile is 26- and 3-layer profiles.
         ELSE

            IF ( LATMOS_M1 ) THEN
               CALL ATMOS_PROF_L33
            ELSE IF ( LATMOS_M2 ) THEN
               CALL ATMOS_PROF_L26
            ELSE IF ( LATMOS_M3 ) THEN
               CALL ATMOS_PROF_L03
            ELSE 
               CALL ATMOS_PROF_L33
            ENDIF
            
         ENDIF 

         ! Apply scaling factor 
         WRITE(6,'(A)') '  TR# Name  Scaling-Factor' 
         DO I = 1, NGAS

            LAYMOLS(:,I) = LAYMOLS(:,I) * GASSF(I)
            WRITE(6,110) I, GASNAME(I), GASSF(I)

         ENDDO

      ! User specified atmos profiles
      ELSE IF ( IATM == -1 ) THEN
       
         WRITE (6, 120) ' - Load user specified atmos profiles ..'

         ! This part is under construction. 
         ERRMSG = 'Currently user atmos profile is not aviable!' 
         CALL ERROR_STOP( ERRMSG, 'atmprof_mod.f' )

      ELSE 

         WRITE(6,130) IATM
         ERRMSG = 'Stoped, please check in namelist.ini'
         CALL ERROR_STOP( ERRMSG, 'atmprof_mod.f' )

      ENDIF
 
 110  FORMAT(2X, I4, A10, F7.3) 
 120  FORMAT( A )
 130  FORMAT(2X, 'LOAD_ATMOS: IATM =', I4 ,' should be integer -1, 1~6')

      ! Return to the calling program
      END SUBROUTINE LOAD_ATMOS

!------------------------------------------------------------------------------

      SUBROUTINE INIT_ATMOS
!
!******************************************************************************
!  Subroutine INIT_ATMOS allocate module variables
!  (xxu, 3/2/11)
!******************************************************************************
!
      ! Refereced to F90 modules
      USE ERROR_MOD,          ONLY : ALLOC_ERR
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS
       
      ! Local variables
      INTEGER            :: AS
      INTEGER            :: N_LAYERS0 
      INTEGER            :: N_LEVELS
      LOGICAL, SAVE      :: IS_INIT = .FALSE.
     


      !=================================================================
      ! INIT_ATMOS begins here!
      !=================================================================

      ! Return if we have already initialized
      IF ( IS_INIT ) RETURN

      ! the larger one between 33 and N_LAYERS
      N_LAYERS0 = MAX( 33, N_LAYERS )

      ! # of levels = n_layers + 1
      N_LEVELS = N_LAYERS0 + 1

      ! ATMZ
      ALLOCATE( ATMZ(N_LEVELS), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'ATMZ' )
      ATMZ = 0d0

      ! ATMP
      ALLOCATE( ATMP(N_LEVELS), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'ATMP' )
      ATMP = 0d0

      ! ATMT
      ALLOCATE( ATMT(N_LEVELS), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'ATMT' )
      ATMT = 0d0

      ! INTENSITY
      !ALLOCATE( INTENSITY(N_LEVELS), STAT=AS )
      !IF (AS/=0) CALL ALLOC_ERR( 'INTENSITY' )
      !INTENSITY = 0d0

      ! AMOLS
      !ALLOCATE( AMOLS(N_LEVELS,MAXMOL), STAT=AS )
      !IF (AS/=0) CALL ALLOC_ERR( 'AMOLS' )
      !AMOLS = 0d0

      ! LAYZ
      !ALLOCATE( LAYZ(N_LAYERS), STAT=AS )
      !IF (AS/=0) CALL ALLOC_ERR( 'LAYZ' )
      !LAYZ = 0d0

      ! LAYP
      ALLOCATE( LAYP(N_LAYERS0), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'LAYP' )
      LAYP = 0d0

      ! LAYT
      ALLOCATE( LAYT(N_LAYERS0), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'LAYT' )
      LAYT = 0d0

      ! BOXH
      ALLOCATE( BOXH(N_LAYERS0), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'BOXH' )
      BOXH = 0d0

      ! LAYINT
      ALLOCATE( LAYINT(N_LAYERS0), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'LAYINT' )
      LAYINT = 0d0

      ! LAYMOLS
      ALLOCATE( LAYMOLS(N_LAYERS0,MAXMOL), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'LAYMOLS' )
      LAYMOLS = 0d0

      ! Reset IS_INIT so we do not allocate arrays again
      IS_INIT = .TRUE.

      WRITE(6,100)
 100  FORMAT(" - INIT_ATMOS: initialize atmos profile arrays" )

      ! Return to the calling routine
      RETURN

      END SUBROUTINE INIT_ATMOS

!------------------------------------------------------------------------------

      SUBROUTINE CLEANUP_ATMOS
 
      !=================================================================
      ! CLEANUP_ATMOS begins here!
      !=================================================================

      ! Deallocates all module arrays
      IF ( ALLOCATED( ATMZ      )  ) DEALLOCATE( ATMZ       )
      IF ( ALLOCATED( ATMP      )  ) DEALLOCATE( ATMP       )
      IF ( ALLOCATED( ATMT      )  ) DEALLOCATE( ATMT       )
      !IF ( ALLOCATED( INTENSITY )  ) DEALLOCATE( INTENSITY  )
      !IF ( ALLOCATED( AMOLS     )  ) DEALLOCATE( AMOLS      )
      !IF ( ALLOCATED( LAYZ      )  ) DEALLOCATE( LAYZ       )
      IF ( ALLOCATED( LAYP      )  ) DEALLOCATE( LAYP       )
      IF ( ALLOCATED( LAYT      )  ) DEALLOCATE( LAYT       )
      IF ( ALLOCATED( LAYINT    )  ) DEALLOCATE( LAYINT     )
      IF ( ALLOCATED( LAYMOLS   )  ) DEALLOCATE( LAYMOLS    )

      END SUBROUTINE CLEANUP_ATMOS

!------------------------------------------------------------------------------

      SUBROUTINE ATMOS_PROF_LXX
! 
!******************************************************************************
!  Routine ATMOS_PROF_M0 specifies the atmospheric profiles with the number 
!   'N_LAYERS' of lower layers.
!******************************************************************************
!
      ! Refereced to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS

      !================================================================
      ! ATMOS_PROF_M1 begins here!
      !================================================================

      ! Determine the # of atmospher layer 
      IF ( N_LAYERS <= STDL-1 ) THEN 

         ! Level properties
         ATMZ(1:N_LAYERS+1)    = STDATMZ(1:N_LAYERS+1)
         ATMP(1:N_LAYERS+1)    = STDATMP(1:N_LAYERS+1)
         ATMT(1:N_LAYERS+1)      = STDATMT(1:N_LAYERS+1)
         !INTENSITY(1:N_LAYERS+1) = STDINTENSITY(1:N_LAYERS+1)
         !AMOLS(1:N_LAYERS+1,:)   = STDAMOLS(1:N_LAYERS+1,:)

         ! Layer properties
         LAYP(1:N_LAYERS)      = STDLAYP(1:N_LAYERS)
         BOXH(1:N_LAYERS)      = STDBOXH(1:N_LAYERS)
         LAYINT(1:N_LAYERS)    = STDLAYINT(1:N_LAYERS)
         LAYT(1:N_LAYERS)      = STDLAYT(1:N_LAYERS)
         LAYMOLS(1:N_LAYERS,:) = STDLAYMOLS(1:N_LAYERS,:)
         
      ELSE

         ! Level properties
         ATMZ(1:STDL)      = STDATMZ(1:STDL)
         ATMP(1:STDL)      = STDATMP(1:STDL)
         ATMT(1:STDL)      = STDATMT(1:STDL)
         !INTENSITY(1:STDL) = STDINTENSITY(1:STDL)
         !AMOLS(1:STDL,:)   = STDAMOLS(1:STDL,:)

         ! Layer properties
         LAYP(1:STDL-1)      = STDLAYP(1:STDL-1)
         BOXH(1:STDL-1)      = STDBOXH(1:STDL-1)
         LAYINT(1:STDL-1)    = STDLAYINT(1:STDL-1)
         LAYT(1:STDL-1)      = STDLAYT(1:STDL-1)
         LAYMOLS(1:STDL-1,:) = STDLAYMOLS(1:STDL-1,:)

      ENDIF

      ! Return to the calling routine
      END SUBROUTINE ATMOS_PROF_LXX

!------------------------------------------------------------------------------

      SUBROUTINE ATMOS_PROF_L33
! 
!******************************************************************************
!  Routine ATMOS_PROF_M1 specifies the reduced atmospheric profiles with 
!   with following setting (reduced std profile M1):
!        level#  height     old-level
!           1 :    0 km       1  
!           | :    |          |
!          26 :   25 km      26 
!          27 :   30 km      28
!          28 :   35 km      30
!          29 :   40 km      32
!          30 :   50 km      36
!          31 :   60 km      38
!          32 :   80 km      42
!          33 :  100 km      46
!          34 :  120 km      50
!******************************************************************************

      ! Local variables
      INTEGER       :: I

      !================================================================
      ! ATMOS_PROF_M1 begins here!
      !================================================================

      ! Level height 
      ATMZ(1:26) = STDATMZ(1:26)
      ATMZ(27  ) = STDATMZ(28  )
      ATMZ(28  ) = STDATMZ(30  )
      ATMZ(29  ) = STDATMZ(32  )
      ATMZ(30  ) = STDATMZ(36  )
      ATMZ(31  ) = STDATMZ(38  )
      ATMZ(32  ) = STDATMZ(42  )
      ATMZ(33  ) = STDATMZ(46  )
      ATMZ(34  ) = STDATMZ(50  )

      ! Level pressure
      ATMP(1:26) = STDATMP(1:26)
      ATMP(27  ) = STDATMP(28  )
      ATMP(28  ) = STDATMP(30  )
      ATMP(29  ) = STDATMP(32  )
      ATMP(30  ) = STDATMP(36  )
      ATMP(31  ) = STDATMP(38  )
      ATMP(32  ) = STDATMP(42  )
      ATMP(33  ) = STDATMP(46  )
      ATMP(34  ) = STDATMP(50  )

      ! Level temperature
      ATMT(1:26) = STDATMT(1:26)
      ATMT(27  ) = STDATMT(28  )
      ATMT(28  ) = STDATMT(30  )
      ATMT(29  ) = STDATMT(32  )
      ATMT(30  ) = STDATMT(36  )
      ATMT(31  ) = STDATMT(38  )
      ATMT(32  ) = STDATMT(42  )
      ATMT(33  ) = STDATMT(46  )
      ATMT(34  ) = STDATMT(50  )

      ! Layer pressure & box height
      LAYP(1:25) = STDLAYP(1:25)
      BOXH(1:25) = STDBOXH(1:25)
      DO I = 26, 33
         LAYP(I) = ( ATMP(I) + ATMP(I+1) ) / 2d0
         BOXH(I) = ( ATMZ(I+1) - ATMZ(I) ) * 1d3
      ENDDO

      ! Layer air intensity
      LAYINT(1:25) = STDLAYINT(1:25)
      LAYINT(26  ) = SUM( STDLAYINT(26:27)*STDBOXH(26:27) ) / BOXH(26)
      LAYINT(27  ) = SUM( STDLAYINT(28:29)*STDBOXH(28:29) ) / BOXH(27)
      LAYINT(28  ) = SUM( STDLAYINT(30:31)*STDBOXH(30:31) ) / BOXH(28)
      LAYINT(29  ) = SUM( STDLAYINT(32:35)*STDBOXH(32:35) ) / BOXH(29)
      LAYINT(30  ) = SUM( STDLAYINT(36:37)*STDBOXH(36:37) ) / BOXH(30)
      LAYINT(31  ) = SUM( STDLAYINT(38:41)*STDBOXH(38:41) ) / BOXH(31)
      LAYINT(32  ) = SUM( STDLAYINT(42:45)*STDBOXH(42:45) ) / BOXH(32)
      LAYINT(33  ) = SUM( STDLAYINT(46:49)*STDBOXH(46:49) ) / BOXH(33)

      ! Layer temperature
      LAYT(1:25) = STDLAYT(1:25)
      LAYT(26  ) = SUM( STDLAYT(26:27)*STDLAYINT(26:27)*STDBOXH(26:27) )
     &           / ( LAYINT(26) * BOXH(26) )
      LAYT(27  ) = SUM( STDLAYT(28:29)*STDLAYINT(28:29)*STDBOXH(28:29) )
     &           / ( LAYINT(27) * BOXH(27) )
      LAYT(28  ) = SUM( STDLAYT(30:31)*STDLAYINT(30:31)*STDBOXH(30:31) )
     &           / ( LAYINT(28) * BOXH(28) )
      LAYT(29  ) = SUM( STDLAYT(32:35)*STDLAYINT(32:35)*STDBOXH(32:35) )
     &           / ( LAYINT(29) * BOXH(29) )
      LAYT(30  ) = SUM( STDLAYT(36:37)*STDLAYINT(36:37)*STDBOXH(36:37) )
     &           / ( LAYINT(30) * BOXH(30) )
      LAYT(31  ) = SUM( STDLAYT(38:41)*STDLAYINT(38:41)*STDBOXH(38:41) )
     &           / ( LAYINT(31) * BOXH(31) )
      LAYT(32  ) = SUM( STDLAYT(42:45)*STDLAYINT(42:45)*STDBOXH(42:45) )
     &           / ( LAYINT(32) * BOXH(32) )
      LAYT(33  ) = SUM( STDLAYT(46:49)*STDLAYINT(46:49)*STDBOXH(46:49) )
     &           / ( LAYINT(33) * BOXH(33) )

      ! Layer gas mixing ratio
      DO I = 1, MAXMOL

         LAYMOLS(1:25,I) = STDLAYMOLS(1:25,I)
         LAYMOLS(26,I)
     &      = SUM( STDLAYMOLS(26:27,I)*STDLAYINT(26:27)*STDBOXH(26:27) )
     &      / ( LAYINT(26) * BOXH(26) )
         LAYMOLS(27,I)
     &      = SUM( STDLAYMOLS(28:29,I)*STDLAYINT(28:29)*STDBOXH(28:29) )
     &      / ( LAYINT(27) * BOXH(27) )
         LAYMOLS(28,I)
     &      = SUM( STDLAYMOLS(30:31,I)*STDLAYINT(30:31)*STDBOXH(30:31) )
     &      / ( LAYINT(28) * BOXH(28) )
         LAYMOLS(29,I)
     &      = SUM( STDLAYMOLS(32:35,I)*STDLAYINT(32:35)*STDBOXH(32:35) )
     &      / ( LAYINT(29) * BOXH(29) )
         LAYMOLS(30,I)
     &      = SUM( STDLAYMOLS(36:37,I)*STDLAYINT(36:37)*STDBOXH(36:37) )
     &      / ( LAYINT(30) * BOXH(30) )
         LAYMOLS(31,I)
     &      = SUM( STDLAYMOLS(38:41,I)*STDLAYINT(38:41)*STDBOXH(38:41) )
     &      / ( LAYINT(31) * BOXH(31) )
         LAYMOLS(32,I)
     &      = SUM( STDLAYMOLS(42:45,I)*STDLAYINT(42:45)*STDBOXH(42:45) )
     &      / ( LAYINT(32) * BOXH(32) )
         LAYMOLS(33,I)
     &      = SUM( STDLAYMOLS(46:49,I)*STDLAYINT(46:49)*STDBOXH(46:49) )
     &      / ( LAYINT(33) * BOXH(33) )

      ENDDO

      ! Return to the calling routine
      END SUBROUTINE ATMOS_PROF_L33

!------------------------------------------------------------------------------

      SUBROUTINE ATMOS_PROF_L26
! 
!******************************************************************************
!  Routine ATMOS_PROF_M1 specifies the reduced atmospheric profiles with 
!   with following setting (reduced std profile M1):
!        level#  height
!           1 :    0 km
!           | :    | 
!          21 :   20 km
!          22 :   25 km
!          23 :   30 km
!          24 :   40 km
!          25 :   50 km
!          26 :   80 km
!          27 :  120 km
!******************************************************************************
!
      ! Local variable
      INTEGER      :: I 

      !================================================================
      ! ATMOS_PROF_M1 begins here!
      !================================================================

      ! Level height 
      ATMZ(1:21) = STDATMZ(1:21)
      ATMZ(22  ) = STDATMZ(26  )
      ATMZ(23  ) = STDATMZ(28  )
      ATMZ(24  ) = STDATMZ(32  )
      ATMZ(25  ) = STDATMZ(36  )
      ATMZ(26  ) = STDATMZ(42  )
      ATMZ(27  ) = STDATMZ(50  )

      ! Level pressure
      ATMP(1:21) = STDATMP(1:21)
      ATMP(22  ) = STDATMP(26  )
      ATMP(23  ) = STDATMP(28  )
      ATMP(24  ) = STDATMP(32  )
      ATMP(25  ) = STDATMP(36  )
      ATMP(26  ) = STDATMP(42  )
      ATMP(27  ) = STDATMP(50  )

      ! Level Temperature
      ATMT(1:21) = STDATMT(1:21)
      ATMT(22  ) = STDATMT(26  )
      ATMT(23  ) = STDATMT(28  )
      ATMT(24  ) = STDATMT(32  )
      ATMT(25  ) = STDATMT(36  )
      ATMT(26  ) = STDATMT(42  )
      ATMT(27  ) = STDATMT(50  )

      ! Layer pressure
      LAYP(1:20) = STDLAYP(1:20)
      LAYP(21  ) = ( ATMP(21) + ATMP(22) ) / 2d0
      LAYP(22  ) = ( ATMP(22) + ATMP(23) ) / 2d0
      LAYP(23  ) = ( ATMP(23) + ATMP(24) ) / 2d0
      LAYP(24  ) = ( ATMP(24) + ATMP(25) ) / 2d0
      LAYP(25  ) = ( ATMP(25) + ATMP(26) ) / 2d0
      LAYP(26  ) = ( ATMP(26) + ATMP(27) ) / 2d0

      ! Layer box height
      BOXH(1:20) = STDBOXH(1:20)
      BOXH(21  ) = ( ATMZ(22) - ATMZ(21) ) * 1d3
      BOXH(22  ) = ( ATMZ(23) - ATMZ(22) ) * 1d3
      BOXH(23  ) = ( ATMZ(24) - ATMZ(23) ) * 1d3
      BOXH(24  ) = ( ATMZ(25) - ATMZ(24) ) * 1d3
      BOXH(25  ) = ( ATMZ(26) - ATMZ(25) ) * 1d3 
      BOXH(26  ) = ( ATMZ(27) - ATMZ(26) ) * 1d3

      ! Layer air intensity
      LAYINT(1:20) = STDLAYINT(1:20)
      LAYINT(21  ) = SUM( STDLAYINT(21:25)*STDBOXH(21:25) ) / BOXH(21)
      LAYINT(22  ) = SUM( STDLAYINT(26:27)*STDBOXH(26:27) ) / BOXH(22)
      LAYINT(23  ) = SUM( STDLAYINT(28:31)*STDBOXH(28:31) ) / BOXH(23)
      LAYINT(24  ) = SUM( STDLAYINT(32:35)*STDBOXH(32:35) ) / BOXH(24)
      LAYINT(25  ) = SUM( STDLAYINT(36:41)*STDBOXH(36:41) ) / BOXH(25)
      LAYINT(26  ) = SUM( STDLAYINT(42:49)*STDBOXH(42:49) ) / BOXH(26)

      ! Layer temperature
      LAYT(1:20) = STDLAYT(1:20)
      LAYT(21  ) = SUM( STDLAYT(21:25)*STDLAYINT(21:25)*STDBOXH(21:25) )
     &           / ( LAYINT(21) * BOXH(21) )
      LAYT(22  ) = SUM( STDLAYT(26:27)*STDLAYINT(26:27)*STDBOXH(26:27) )
     &           / ( LAYINT(22) * BOXH(22) )
      LAYT(23  ) = SUM( STDLAYT(28:31)*STDLAYINT(28:31)*STDBOXH(28:31) )
     &           / ( LAYINT(23) * BOXH(23) )
      LAYT(24  ) = SUM( STDLAYT(32:35)*STDLAYINT(32:35)*STDBOXH(32:35) )
     &           / ( LAYINT(24) * BOXH(24) )
      LAYT(25  ) = SUM( STDLAYT(36:41)*STDLAYINT(36:41)*STDBOXH(36:41) )
     &           / ( LAYINT(25) * BOXH(25) )
      LAYT(26  ) = SUM( STDLAYT(42:49)*STDLAYINT(42:49)*STDBOXH(42:49) )
     &           / ( LAYINT(26) * BOXH(26) )

      ! Layer gas mixing ratio
      DO I = 1, MAXMOL
       
         LAYMOLS(1:20,I) = STDLAYMOLS(1:20,I)
         LAYMOLS(21,I)   
     &      = SUM( STDLAYMOLS(21:25,I)*STDLAYINT(21:25)*STDBOXH(21:25) )
     &      / ( LAYINT(21) * BOXH(21) )
         LAYMOLS(22,I)
     &      = SUM( STDLAYMOLS(26:27,I)*STDLAYINT(26:27)*STDBOXH(26:27) )
     &      / ( LAYINT(22) * BOXH(22) )
         LAYMOLS(23,I)
     &      = SUM( STDLAYMOLS(28:31,I)*STDLAYINT(28:31)*STDBOXH(28:31) )
     &      / ( LAYINT(23) * BOXH(23) )
         LAYMOLS(24,I)
     &      = SUM( STDLAYMOLS(32:35,I)*STDLAYINT(32:35)*STDBOXH(32:35) )
     &      / ( LAYINT(24) * BOXH(24) )
         LAYMOLS(25,I)
     &      = SUM( STDLAYMOLS(36:41,I)*STDLAYINT(36:41)*STDBOXH(36:41) )
     &      / ( LAYINT(25) * BOXH(25) )
         LAYMOLS(26,I)
     &      = SUM( STDLAYMOLS(42:49,I)*STDLAYINT(42:49)*STDBOXH(42:49) )
     &      / ( LAYINT(26) * BOXH(26) )

      ENDDO

      ! Return to the calling routine
      END SUBROUTINE ATMOS_PROF_L26

!------------------------------------------------------------------------------

      SUBROUTINE ATMOS_PROF_L03
! 
!******************************************************************************
!  Routine ATMOS_PROF_M2 specifies the reduced atmospheric profiles with 
!   with following setting (reduced std profile M2):
!        level#  height
!           1 :    0 km
!           2 :    5 km 
!           3 :   20 km
!           4 :  120 km
!******************************************************************************
!
      ! Local variable
      INTEGER      :: I 

      !================================================================
      ! ATMOS_PROF_M2 begins here!
      !================================================================

      ! Level height 
      ATMZ(1) = STDATMZ(1 )
      ATMZ(2) = STDATMZ(6 )
      ATMZ(3) = STDATMZ(21)
      ATMZ(4) = STDATMZ(50)

      ! Level pressure
      ATMP(1) = STDATMP(1 )
      ATMP(2) = STDATMP(6 )
      ATMP(3) = STDATMP(21)
      ATMP(4) = STDATMP(50)

      ! Level Temperature
      ATMT(1) = STDATMT(1 )
      ATMT(2) = STDATMT(6 )
      ATMT(3) = STDATMT(21)
      ATMT(4) = STDATMT(50)

      ! Layer pressure
      LAYP(1) = ( ATMP(1) + ATMP(2) ) / 2d0
      LAYP(2) = ( ATMP(2) + ATMP(3) ) / 2d0
      LAYP(3) = ( ATMP(3) + ATMP(4) ) / 2d0

      ! Layer box height
      BOXH(1) = ( ATMZ(2) - ATMZ(1) ) * 1d3
      BOXH(2) = ( ATMZ(3) - ATMZ(2) ) * 1d3
      BOXH(3) = ( ATMZ(4) - ATMZ(3) ) * 1d3
      !BOXH(1) = SUM( STDBOXH( 1: 5) )
      !BOXH(2) = SUM( STDBOXH( 6:15) )
      !BOXH(3) = SUM( STDBOXH(16:49) )

      ! Layer air intensity
      LAYINT(1) = SUM( STDLAYINT( 1: 5)*STDBOXH( 1: 5) ) / BOXH(1)
      LAYINT(2) = SUM( STDLAYINT( 6:20)*STDBOXH( 6:20) ) / BOXH(2)
      LAYINT(3) = SUM( STDLAYINT(21:49)*STDBOXH(21:49) ) / BOXH(3)

      ! Layer temperature
      LAYT(1) = SUM( STDLAYT( 1: 5)*STDLAYINT( 1: 5)*STDBOXH( 1: 5) )
     &        / ( LAYINT(1) * BOXH(1) )
      LAYT(2) = SUM( STDLAYT( 6:20)*STDLAYINT( 6:20)*STDBOXH( 6:20) )
     &        / ( LAYINT(2) * BOXH(2) )
      LAYT(3) = SUM( STDLAYT(21:49)*STDLAYINT(21:49)*STDBOXH(21:49) )
     &        / ( LAYINT(3) * BOXH(3) )

      ! Layer gas mixing ratio 
      DO I = 1, MAXMOL

         LAYMOLS(1,I)   
     &      = SUM( STDLAYMOLS( 1: 5,I)*STDLAYINT( 1: 5)*STDBOXH( 1: 5) )
     &      / ( LAYINT(1) * BOXH(1) )
         LAYMOLS(2,I) 
     &      = SUM( STDLAYMOLS( 6:20,I)*STDLAYINT( 6:20)*STDBOXH( 6:20) )
     &      / ( LAYINT(2) * BOXH(2) )
         LAYMOLS(3,I)
     &      = SUM( STDLAYMOLS(21:49,I)*STDLAYINT(21:49)*STDBOXH(21:49) )
     &      / ( LAYINT(3) * BOXH(3) )

      ENDDO

      ! Return to the calling routine
      END SUBROUTINE ATMOS_PROF_L03

!------------------------------------------------------------------------------

      SUBROUTINE STDATMOS ( IATM )
! 
!******************************************************************************
!  Some instruction on this code:
!  (1) This code is based on LBLRTM.
!  (2) 6 type of atmopshere
!        1       Tropical
!        2       Mid-Latitude Summer
!        3       Mid-Latitude Winter
!        4       High-LAT Summer
!        5       High-LAT Winter
!        6       US Standard
!       -1       User Specified
!
!******************************************************************************
!
      ! Arguments
      INTEGER, INTENT(IN) :: IATM
       
      ! Local Variables
      CHARACTER(LEN=8) :: CTMNA1(3),CTMNA2(3),CTMNA3(3)
      CHARACTER(LEN=8) :: CTMNA4(3),CTMNA5(3),CTMNA6(3)
      REAL*8           :: ALT(50)
      REAL*8           :: P1(50),P2(50),P3(50),P4(50),P5(50),P6(50)
      REAL*8           :: T1(50),T2(50),T3(50),T4(50),T5(50),T6(50)
      REAL*8           :: AMOL11(50),AMOL12(50),AMOL13(50),AMOL14(50)
      REAL*8           :: AMOL15(50),AMOL16(50),AMOL17(50),AMOL18(50)
      REAL*8           :: AMOL21(50),AMOL22(50),AMOL23(50),AMOL24(50)
      REAL*8           :: AMOL25(50),AMOL26(50),AMOL27(50),AMOL28(50)
      REAL*8           :: AMOL31(50),AMOL32(50),AMOL33(50),AMOL34(50)
      REAL*8           :: AMOL35(50),AMOL36(50),AMOL37(50),AMOL38(50)
      REAL*8           :: AMOL41(50),AMOL42(50),AMOL43(50),AMOL44(50)
      REAL*8           :: AMOL45(50),AMOL46(50),AMOL47(50),AMOL48(50)
      REAL*8           :: AMOL51(50),AMOL52(50),AMOL53(50),AMOL54(50)
      REAL*8           :: AMOL55(50),AMOL56(50),AMOL57(50),AMOL58(50)
      REAL*8           :: AMOL61(50),AMOL62(50),AMOL63(50),AMOL64(50)
      REAL*8           :: AMOL65(50),AMOL66(50),AMOL67(50),AMOL68(50)
      REAL*8           :: ANO(50),   SO2(50),   ANO2(50),  ANH3(50)
      REAL*8           :: HNO3(50),  OH(50),    HF(50),    HCL(50)
      REAL*8           :: HBR(50),   HI(50),    CLO(50),   OCS(50)
      REAL*8           :: H2CO(50),  HOCL(50),  AN2(50),   HCN(50)
      REAL*8           :: CH3CL(50), H2O2(50),  C2H2(50),  C2H6(50)
      REAL*8           :: PH3(50),   TDUM(50)

      !================================================================
      ! Load the build-in data base fisrt
      !================================================================

      ! DATA ATM TYPE                                                   
      DATA CTMNA1  /'TROPICAL','        ','        '/ 
      DATA CTMNA2  /'MIDLATIT','UDE SUMM','ER      '/                   
      DATA CTMNA3  /'MIDLATIT','UDE WINT','ER      '/                   
      DATA CTMNA4  /'SUBARCTI','C SUMMER','        '/                   
      DATA CTMNA5  /'SUBARCTI','C WINTER','        '/                   
      DATA CTMNA6  /'U. S. ST','ANDARD, ','1976    '/                   
                                                                        
      ! DATA ALT (KM)                                                   
      DATA ALT/                                                         
     *       0.0,       1.0,       2.0,       3.0,       4.0,           
     *       5.0,       6.0,       7.0,       8.0,       9.0,           
     *      10.0,      11.0,      12.0,      13.0,      14.0,           
     *      15.0,      16.0,      17.0,      18.0,      19.0,           
     *      20.0,      21.0,      22.0,      23.0,      24.0,           
     *      25.0,      27.5,      30.0,      32.5,      35.0,           
     *      37.5,      40.0,      42.5,      45.0,      47.5,           
     *      50.0,      55.0,      60.0,      65.0,      70.0,           
     *      75.0,      80.0,      85.0,      90.0,      95.0,           
     *     100.0,     105.0,     110.0,     115.0,     120.0/           
                                                                        
      ! DATA PRESSURE                                                   
      DATA P1/                                                          
     * 1.013E+03, 9.040E+02, 8.050E+02, 7.150E+02, 6.330E+02,           
     * 5.590E+02, 4.920E+02, 4.320E+02, 3.780E+02, 3.290E+02,           
     * 2.860E+02, 2.470E+02, 2.130E+02, 1.820E+02, 1.560E+02,           
     * 1.320E+02, 1.110E+02, 9.370E+01, 7.890E+01, 6.660E+01,           
     * 5.650E+01, 4.800E+01, 4.090E+01, 3.500E+01, 3.000E+01,           
     * 2.570E+01, 1.763E+01, 1.220E+01, 8.520E+00, 6.000E+00,           
     * 4.260E+00, 3.050E+00, 2.200E+00, 1.590E+00, 1.160E+00,           
     * 8.540E-01, 4.560E-01, 2.390E-01, 1.210E-01, 5.800E-02,           
     * 2.600E-02, 1.100E-02, 4.400E-03, 1.720E-03, 6.880E-04,           
     * 2.890E-04, 1.300E-04, 6.470E-05, 3.600E-05, 2.250E-05/           
      DATA P2/                                                          
     * 1.013E+03, 9.020E+02, 8.020E+02, 7.100E+02, 6.280E+02,           
     * 5.540E+02, 4.870E+02, 4.260E+02, 3.720E+02, 3.240E+02,           
     * 2.810E+02, 2.430E+02, 2.090E+02, 1.790E+02, 1.530E+02,           
     * 1.300E+02, 1.110E+02, 9.500E+01, 8.120E+01, 6.950E+01,           
     * 5.950E+01, 5.100E+01, 4.370E+01, 3.760E+01, 3.220E+01,           
     * 2.770E+01, 1.907E+01, 1.320E+01, 9.300E+00, 6.520E+00,           
     * 4.640E+00, 3.330E+00, 2.410E+00, 1.760E+00, 1.290E+00,           
     * 9.510E-01, 5.150E-01, 2.720E-01, 1.390E-01, 6.700E-02,           
     * 3.000E-02, 1.200E-02, 4.480E-03, 1.640E-03, 6.250E-04,           
     * 2.580E-04, 1.170E-04, 6.110E-05, 3.560E-05, 2.270E-05/           
      DATA P3/                                                          
     * 1.018E+03, 8.973E+02, 7.897E+02, 6.938E+02, 6.081E+02,           
     * 5.313E+02, 4.627E+02, 4.016E+02, 3.473E+02, 2.993E+02,           
     * 2.568E+02, 2.199E+02, 1.882E+02, 1.611E+02, 1.378E+02,           
     * 1.178E+02, 1.007E+02, 8.610E+01, 7.360E+01, 6.280E+01,           
     * 5.370E+01, 4.580E+01, 3.910E+01, 3.340E+01, 2.860E+01,           
     * 2.440E+01, 1.646E+01, 1.110E+01, 7.560E+00, 5.180E+00,           
     * 3.600E+00, 2.530E+00, 1.800E+00, 1.290E+00, 9.400E-01,           
     * 6.830E-01, 3.620E-01, 1.880E-01, 9.500E-02, 4.700E-02,           
     * 2.220E-02, 1.030E-02, 4.560E-03, 1.980E-03, 8.770E-04,           
     * 4.074E-04, 2.000E-04, 1.057E-04, 5.980E-05, 3.600E-05/           
      DATA P4/                                                          
     * 1.010E+03, 8.960E+02, 7.929E+02, 7.000E+02, 6.160E+02,           
     * 5.410E+02, 4.740E+02, 4.130E+02, 3.590E+02, 3.108E+02,           
     * 2.677E+02, 2.300E+02, 1.977E+02, 1.700E+02, 1.460E+02,           
     * 1.260E+02, 1.080E+02, 9.280E+01, 7.980E+01, 6.860E+01,           
     * 5.900E+01, 5.070E+01, 4.360E+01, 3.750E+01, 3.228E+01,           
     * 2.780E+01, 1.923E+01, 1.340E+01, 9.400E+00, 6.610E+00,           
     * 4.720E+00, 3.400E+00, 2.480E+00, 1.820E+00, 1.340E+00,           
     * 9.870E-01, 5.370E-01, 2.880E-01, 1.470E-01, 7.100E-02,           
     * 3.200E-02, 1.250E-02, 4.510E-03, 1.610E-03, 6.060E-04,           
     * 2.480E-04, 1.130E-04, 6.000E-05, 3.540E-05, 2.260E-05/           
      DATA P5/                                                          
     * 1.013E+03, 8.878E+02, 7.775E+02, 6.798E+02, 5.932E+02,           
     * 5.158E+02, 4.467E+02, 3.853E+02, 3.308E+02, 2.829E+02,           
     * 2.418E+02, 2.067E+02, 1.766E+02, 1.510E+02, 1.291E+02,           
     * 1.103E+02, 9.431E+01, 8.058E+01, 6.882E+01, 5.875E+01,           
     * 5.014E+01, 4.277E+01, 3.647E+01, 3.109E+01, 2.649E+01,           
     * 2.256E+01, 1.513E+01, 1.020E+01, 6.910E+00, 4.701E+00,           
     * 3.230E+00, 2.243E+00, 1.570E+00, 1.113E+00, 7.900E-01,           
     * 5.719E-01, 2.990E-01, 1.550E-01, 7.900E-02, 4.000E-02,           
     * 2.000E-02, 9.660E-03, 4.500E-03, 2.022E-03, 9.070E-04,           
     * 4.230E-04, 2.070E-04, 1.080E-04, 6.000E-05, 3.590E-05/           
      DATA P6/                                                          
     * 1.013E+03, 8.988E+02, 7.950E+02, 7.012E+02, 6.166E+02,           
     * 5.405E+02, 4.722E+02, 4.111E+02, 3.565E+02, 3.080E+02,           
     * 2.650E+02, 2.270E+02, 1.940E+02, 1.658E+02, 1.417E+02,           
     * 1.211E+02, 1.035E+02, 8.850E+01, 7.565E+01, 6.467E+01,           
     * 5.529E+01, 4.729E+01, 4.047E+01, 3.467E+01, 2.972E+01,           
     * 2.549E+01, 1.743E+01, 1.197E+01, 8.010E+00, 5.746E+00,           
     * 4.150E+00, 2.871E+00, 2.060E+00, 1.491E+00, 1.090E+00,           
     * 7.978E-01, 4.250E-01, 2.190E-01, 1.090E-01, 5.220E-02,           
     * 2.400E-02, 1.050E-02, 4.460E-03, 1.840E-03, 7.600E-04,           
     * 3.200E-04, 1.450E-04, 7.100E-05, 4.010E-05, 2.540E-05/           
                                                                        
      ! DATA TEMPERATUR                                                 
      DATA T1/                                                          
     *    299.70,    293.70,    287.70,    283.70,    277.00,           
     *    270.30,    263.60,    257.00,    250.30,    243.60,           
     *    237.00,    230.10,    223.60,    217.00,    210.30,           
     *    203.70,    197.00,    194.80,    198.80,    202.70,           
     *    206.70,    210.70,    214.60,    217.00,    219.20,           
     *    221.40,    227.00,    232.30,    237.70,    243.10,           
     *    248.50,    254.00,    259.40,    264.80,    269.60,           
     *    270.20,    263.40,    253.10,    236.00,    218.90,           
     *    201.80,    184.80,    177.10,    177.00,    184.30,           
     *    190.70,    212.00,    241.60,    299.70,    380.00/           
      DATA T2/                                                          
     *    294.20,    289.70,    285.20,    279.20,    273.20,           
     *    267.20,    261.20,    254.70,    248.20,    241.70,           
     *    235.30,    228.80,    222.30,    215.80,    215.70,           
     *    215.70,    215.70,    215.70,    216.80,    217.90,           
     *    219.20,    220.40,    221.60,    222.80,    223.90,           
     *    225.10,    228.45,    233.70,    239.00,    245.20,           
     *    251.30,    257.50,    263.70,    269.90,    275.20,           
     *    275.70,    269.30,    257.10,    240.10,    218.10,           
     *    196.10,    174.10,    165.10,    165.00,    178.30,           
     *    190.50,    222.20,    262.40,    316.80,    380.00/           
      DATA T3/                                                          
     *    272.20,    268.70,    265.20,    261.70,    255.70,           
     *    249.70,    243.70,    237.70,    231.70,    225.70,           
     *    219.70,    219.20,    218.70,    218.20,    217.70,           
     *    217.20,    216.70,    216.20,    215.70,    215.20,           
     *    215.20,    215.20,    215.20,    215.20,    215.20,           
     *    215.20,    215.50,    217.40,    220.40,    227.90,           
     *    235.50,    243.20,    250.80,    258.50,    265.10,           
     *    265.70,    260.60,    250.80,    240.90,    230.70,           
     *    220.40,    210.10,    199.80,    199.50,    208.30,           
     *    218.60,    237.10,    259.50,    293.00,    333.00/           
      DATA T4/                                                          
     *    287.20,    281.70,    276.30,    270.90,    265.50,           
     *    260.10,    253.10,    246.10,    239.20,    232.20,           
     *    225.20,    225.20,    225.20,    225.20,    225.20,           
     *    225.20,    225.20,    225.20,    225.20,    225.20,           
     *    225.20,    225.20,    225.20,    225.20,    226.60,           
     *    228.10,    231.00,    235.10,    240.00,    247.20,           
     *    254.60,    262.10,    269.50,    273.60,    276.20,           
     *    277.20,    274.00,    262.70,    239.70,    216.60,           
     *    193.60,    170.60,    161.70,    161.60,    176.80,           
     *    190.40,    226.00,    270.10,    322.70,    380.00/           
      DATA T5/                                                          
     *    257.20,    259.10,    255.90,    252.70,    247.70,           
     *    240.90,    234.10,    227.30,    220.60,    217.20,           
     *    217.20,    217.20,    217.20,    217.20,    217.20,           
     *    217.20,    216.60,    216.00,    215.40,    214.80,           
     *    214.20,    213.60,    213.00,    212.40,    211.80,           
     *    211.20,    213.60,    216.00,    218.50,    222.30,           
     *    228.50,    234.70,    240.80,    247.00,    253.20,           
     *    259.30,    259.10,    250.90,    248.40,    245.40,           
     *    234.70,    223.90,    213.10,    202.30,    211.00,           
     *    218.50,    234.00,    252.60,    288.50,    333.00/           
      DATA T6/                                                          
     *    288.20,    281.70,    275.20,    268.70,    262.20,           
     *    255.70,    249.20,    242.70,    236.20,    229.70,           
     *    223.30,    216.80,    216.70,    216.70,    216.70,           
     *    216.70,    216.70,    216.70,    216.70,    216.70,           
     *    216.70,    217.60,    218.60,    219.60,    220.60,           
     *    221.60,    224.00,    226.50,    230.00,    236.50,           
     *    242.90,    250.40,    257.30,    264.20,    270.60,           
     *    270.70,    260.80,    247.00,    233.30,    219.60,           
     *    208.40,    198.60,    188.90,    186.90,    188.40,           
     *    195.10,    208.80,    240.00,    300.00,    360.00/           
                                                                        
      ! DATA  H2O                                                        
      DATA AMOL11/                                                      
     * 2.593E+04, 1.949E+04, 1.534E+04, 8.600E+03, 4.441E+03,           
     * 3.346E+03, 2.101E+03, 1.289E+03, 7.637E+02, 4.098E+02,           
     * 1.912E+02, 7.306E+01, 2.905E+01, 9.900E+00, 6.220E+00,           
     * 4.000E+00, 3.000E+00, 2.900E+00, 2.750E+00, 2.600E+00,           
     * 2.600E+00, 2.650E+00, 2.800E+00, 2.900E+00, 3.200E+00,           
     * 3.250E+00, 3.600E+00, 4.000E+00, 4.300E+00, 4.600E+00,           
     * 4.900E+00, 5.200E+00, 5.500E+00, 5.700E+00, 5.900E+00,           
     * 6.000E+00, 6.000E+00, 6.000E+00, 5.400E+00, 4.500E+00,           
     * 3.300E+00, 2.100E+00, 1.300E+00, 8.500E-01, 5.400E-01,           
     * 4.000E-01, 3.400E-01, 2.800E-01, 2.400E-01, 2.000E-01/           
      DATA AMOL21/                                                      
     * 1.876E+04, 1.378E+04, 9.680E+03, 5.984E+03, 3.813E+03,           
     * 2.225E+03, 1.510E+03, 1.020E+03, 6.464E+02, 4.129E+02,           
     * 2.472E+02, 9.556E+01, 2.944E+01, 8.000E+00, 5.000E+00,           
     * 3.400E+00, 3.300E+00, 3.200E+00, 3.150E+00, 3.200E+00,           
     * 3.300E+00, 3.450E+00, 3.600E+00, 3.850E+00, 4.000E+00,           
     * 4.200E+00, 4.450E+00, 4.700E+00, 4.850E+00, 4.950E+00,           
     * 5.000E+00, 5.100E+00, 5.300E+00, 5.450E+00, 5.500E+00,           
     * 5.500E+00, 5.350E+00, 5.000E+00, 4.400E+00, 3.700E+00,           
     * 2.950E+00, 2.100E+00, 1.330E+00, 8.500E-01, 5.400E-01,           
     * 4.000E-01, 3.400E-01, 2.800E-01, 2.400E-01, 2.000E-01/           
      DATA AMOL31/                                                      
     * 4.316E+03, 3.454E+03, 2.788E+03, 2.088E+03, 1.280E+03,           
     * 8.241E+02, 5.103E+02, 2.321E+02, 1.077E+02, 5.566E+01,           
     * 2.960E+01, 1.000E+01, 6.000E+00, 5.000E+00, 4.800E+00,           
     * 4.700E+00, 4.600E+00, 4.500E+00, 4.500E+00, 4.500E+00,           
     * 4.500E+00, 4.500E+00, 4.530E+00, 4.550E+00, 4.600E+00,           
     * 4.650E+00, 4.700E+00, 4.750E+00, 4.800E+00, 4.850E+00,           
     * 4.900E+00, 4.950E+00, 5.000E+00, 5.000E+00, 5.000E+00,           
     * 4.950E+00, 4.850E+00, 4.500E+00, 4.000E+00, 3.300E+00,           
     * 2.700E+00, 2.000E+00, 1.330E+00, 8.500E-01, 5.400E-01,           
     * 4.000E-01, 3.400E-01, 2.800E-01, 2.400E-01, 2.000E-01/           
      DATA AMOL41/                                                      
     * 1.194E+04, 8.701E+03, 6.750E+03, 4.820E+03, 3.380E+03,           
     * 2.218E+03, 1.330E+03, 7.971E+02, 3.996E+02, 1.300E+02,           
     * 4.240E+01, 1.330E+01, 6.000E+00, 4.450E+00, 4.000E+00,           
     * 4.000E+00, 4.000E+00, 4.050E+00, 4.300E+00, 4.500E+00,           
     * 4.600E+00, 4.700E+00, 4.800E+00, 4.830E+00, 4.850E+00,           
     * 4.900E+00, 4.950E+00, 5.000E+00, 5.000E+00, 5.000E+00,           
     * 5.000E+00, 5.000E+00, 5.000E+00, 5.000E+00, 5.000E+00,           
     * 4.950E+00, 4.850E+00, 4.500E+00, 4.000E+00, 3.300E+00,           
     * 2.700E+00, 2.000E+00, 1.330E+00, 8.500E-01, 5.400E-01,           
     * 4.000E-01, 3.400E-01, 2.800E-01, 2.400E-01, 2.000E-01/           
      DATA AMOL51/                                                      
     * 1.405E+03, 1.615E+03, 1.427E+03, 1.166E+03, 7.898E+02,           
     * 4.309E+02, 2.369E+02, 1.470E+02, 3.384E+01, 2.976E+01,           
     * 2.000E+01, 1.000E+01, 6.000E+00, 4.450E+00, 4.500E+00,           
     * 4.550E+00, 4.600E+00, 4.650E+00, 4.700E+00, 4.750E+00,           
     * 4.800E+00, 4.850E+00, 4.900E+00, 4.950E+00, 5.000E+00,           
     * 5.000E+00, 5.000E+00, 5.000E+00, 5.000E+00, 5.000E+00,           
     * 5.000E+00, 5.000E+00, 5.000E+00, 5.000E+00, 5.000E+00,           
     * 4.950E+00, 4.850E+00, 4.500E+00, 4.000E+00, 3.300E+00,           
     * 2.700E+00, 2.000E+00, 1.330E+00, 8.500E-01, 5.400E-01,           
     * 4.000E-01, 3.400E-01, 2.800E-01, 2.400E-01, 2.000E-01/           
      DATA AMOL61/                                                      
     * 7.745E+03, 6.071E+03, 4.631E+03, 3.182E+03, 2.158E+03,           
     * 1.397E+03, 9.254E+02, 5.720E+02, 3.667E+02, 1.583E+02,           
     * 6.996E+01, 3.613E+01, 1.906E+01, 1.085E+01, 5.927E+00,           
     * 5.000E+00, 3.950E+00, 3.850E+00, 3.825E+00, 3.850E+00,           
     * 3.900E+00, 3.975E+00, 4.065E+00, 4.200E+00, 4.300E+00,           
     * 4.425E+00, 4.575E+00, 4.725E+00, 4.825E+00, 4.900E+00,           
     * 4.950E+00, 5.025E+00, 5.150E+00, 5.225E+00, 5.250E+00,           
     * 5.225E+00, 5.100E+00, 4.750E+00, 4.200E+00, 3.500E+00,           
     * 2.825E+00, 2.050E+00, 1.330E+00, 8.500E-01, 5.400E-01,           
     * 4.000E-01, 3.400E-01, 2.800E-01, 2.400E-01, 2.000E-01/           

      ! DATA CO2
      DATA AMOL12/                                                      
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.280E+02, 3.200E+02, 3.100E+02, 2.700E+02,           
     * 1.950E+02, 1.100E+02, 6.000E+01, 4.000E+01, 3.500E+01/           
      DATA AMOL22/                                                      
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.280E+02, 3.200E+02, 3.100E+02, 2.700E+02,           
     * 1.950E+02, 1.100E+02, 6.000E+01, 4.000E+01, 3.500E+01/           
      DATA AMOL32/                                                      
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.280E+02, 3.200E+02, 3.100E+02, 2.700E+02,           
     * 1.950E+02, 1.100E+02, 6.000E+01, 4.000E+01, 3.500E+01/           
      DATA AMOL42/                                                      
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.280E+02, 3.200E+02, 3.100E+02, 2.700E+02,           
     * 1.950E+02, 1.100E+02, 6.000E+01, 4.000E+01, 3.500E+01/           
      DATA AMOL52/                                                      
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.280E+02, 3.200E+02, 3.100E+02, 2.700E+02,           
     * 1.950E+02, 1.100E+02, 6.000E+01, 4.000E+01, 3.500E+01/           
      DATA AMOL62/                                                      
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02, 3.300E+02,           
     * 3.300E+02, 3.280E+02, 3.200E+02, 3.100E+02, 2.700E+02,           
     * 1.950E+02, 1.100E+02, 6.000E+01, 4.000E+01, 3.500E+01/           

      ! DATA OZONE
      DATA AMOL13/                                                      
     * 2.869E-02, 3.150E-02, 3.342E-02, 3.504E-02, 3.561E-02,           
     * 3.767E-02, 3.989E-02, 4.223E-02, 4.471E-02, 5.000E-02,           
     * 5.595E-02, 6.613E-02, 7.815E-02, 9.289E-02, 1.050E-01,           
     * 1.256E-01, 1.444E-01, 2.500E-01, 5.000E-01, 9.500E-01,           
     * 1.400E+00, 1.800E+00, 2.400E+00, 3.400E+00, 4.300E+00,           
     * 5.400E+00, 7.800E+00, 9.300E+00, 9.850E+00, 9.700E+00,           
     * 8.800E+00, 7.500E+00, 5.900E+00, 4.500E+00, 3.450E+00,           
     * 2.800E+00, 1.800E+00, 1.100E+00, 6.500E-01, 3.000E-01,           
     * 1.800E-01, 3.300E-01, 5.000E-01, 5.200E-01, 5.000E-01,           
     * 4.000E-01, 2.000E-01, 5.000E-02, 5.000E-03, 5.000E-04/           
      DATA AMOL23/                                                      
     * 3.017E-02, 3.337E-02, 3.694E-02, 4.222E-02, 4.821E-02,           
     * 5.512E-02, 6.408E-02, 7.764E-02, 9.126E-02, 1.111E-01,           
     * 1.304E-01, 1.793E-01, 2.230E-01, 3.000E-01, 4.400E-01,           
     * 5.000E-01, 6.000E-01, 7.000E-01, 1.000E+00, 1.500E+00,           
     * 2.000E+00, 2.400E+00, 2.900E+00, 3.400E+00, 4.000E+00,           
     * 4.800E+00, 6.000E+00, 7.000E+00, 8.100E+00, 8.900E+00,           
     * 8.700E+00, 7.550E+00, 5.900E+00, 4.500E+00, 3.500E+00,           
     * 2.800E+00, 1.800E+00, 1.300E+00, 8.000E-01, 4.000E-01,           
     * 1.900E-01, 2.000E-01, 5.700E-01, 7.500E-01, 7.000E-01,           
     * 4.000E-01, 2.000E-01, 5.000E-02, 5.000E-03, 5.000E-04/           
      DATA AMOL33/                                                      
     * 2.778E-02, 2.800E-02, 2.849E-02, 3.200E-02, 3.567E-02,           
     * 4.720E-02, 5.837E-02, 7.891E-02, 1.039E-01, 1.567E-01,           
     * 2.370E-01, 3.624E-01, 5.232E-01, 7.036E-01, 8.000E-01,           
     * 9.000E-01, 1.100E+00, 1.400E+00, 1.800E+00, 2.300E+00,           
     * 2.900E+00, 3.500E+00, 3.900E+00, 4.300E+00, 4.700E+00,           
     * 5.100E+00, 5.600E+00, 6.100E+00, 6.800E+00, 7.100E+00,           
     * 7.200E+00, 6.900E+00, 5.900E+00, 4.600E+00, 3.700E+00,           
     * 2.750E+00, 1.700E+00, 1.000E-00, 5.500E-01, 3.200E-01,           
     * 2.500E-01, 2.300E-01, 5.500E-01, 8.000E-01, 8.000E-01,           
     * 4.000E-01, 2.000E-01, 5.000E-02, 5.000E-03, 5.000E-04/           
      DATA AMOL43/                                                      
     * 2.412E-02, 2.940E-02, 3.379E-02, 3.887E-02, 4.478E-02,           
     * 5.328E-02, 6.564E-02, 7.738E-02, 9.114E-02, 1.420E-01,           
     * 1.890E-01, 3.050E-01, 4.100E-01, 5.000E-01, 6.000E-01,           
     * 7.000E-01, 8.500E-01, 1.000E+00, 1.300E+00, 1.700E+00,           
     * 2.100E+00, 2.700E+00, 3.300E+00, 3.700E+00, 4.200E+00,           
     * 4.500E+00, 5.300E+00, 5.700E+00, 6.900E+00, 7.700E+00,           
     * 7.800E+00, 7.000E+00, 5.400E+00, 4.200E+00, 3.200E+00,           
     * 2.500E+00, 1.700E+00, 1.200E+00, 8.000E-01, 4.000E-01,           
     * 2.000E-01, 1.800E-01, 6.500E-01, 9.000E-01, 8.000E-01,           
     * 4.000E-01, 2.000E-01, 5.000E-02, 5.000E-03, 5.000E-04/           
      DATA AMOL53/                                                      
     * 1.802E-02, 2.072E-02, 2.336E-02, 2.767E-02, 3.253E-02,           
     * 3.801E-02, 4.446E-02, 7.252E-02, 1.040E-01, 2.100E-01,           
     * 3.000E-01, 3.500E-01, 4.000E-01, 6.500E-01, 9.000E-01,           
     * 1.200E+00, 1.500E+00, 1.900E+00, 2.450E+00, 3.100E+00,           
     * 3.700E+00, 4.000E+00, 4.200E+00, 4.500E+00, 4.600E+00,           
     * 4.700E+00, 4.900E+00, 5.400E+00, 5.900E+00, 6.200E+00,           
     * 6.250E+00, 5.900E+00, 5.100E+00, 4.100E+00, 3.000E+00,           
     * 2.600E+00, 1.600E+00, 9.500E-01, 6.500E-01, 5.000E-01,           
     * 3.300E-01, 1.300E-01, 7.500E-01, 8.000E-01, 8.000E-01,           
     * 4.000E-01, 2.000E-01, 5.000E-02, 5.000E-03, 5.000E-04/           
      DATA AMOL63/                                                      
     * 2.660E-02, 2.931E-02, 3.237E-02, 3.318E-02, 3.387E-02,           
     * 3.768E-02, 4.112E-02, 5.009E-02, 5.966E-02, 9.168E-02,           
     * 1.313E-01, 2.149E-01, 3.095E-01, 3.846E-01, 5.030E-01,           
     * 6.505E-01, 8.701E-01, 1.187E+00, 1.587E+00, 2.030E+00,           
     * 2.579E+00, 3.028E+00, 3.647E+00, 4.168E+00, 4.627E+00,           
     * 5.118E+00, 5.803E+00, 6.553E+00, 7.373E+00, 7.837E+00,           
     * 7.800E+00, 7.300E+00, 6.200E+00, 5.250E+00, 4.100E+00,           
     * 3.100E+00, 1.800E+00, 1.100E+00, 7.000E-01, 3.000E-01,           
     * 2.500E-01, 3.000E-01, 5.000E-01, 7.000E-01, 7.000E-01,           
     * 4.000E-01, 2.000E-01, 5.000E-02, 5.000E-03, 5.000E-04/           

      ! DATA  N2O
      DATA AMOL14/                                                      
     * 3.200E-01, 3.200E-01, 3.200E-01, 3.200E-01, 3.200E-01,           
     * 3.200E-01, 3.200E-01, 3.200E-01, 3.200E-01, 3.195E-01,           
     * 3.179E-01, 3.140E-01, 3.095E-01, 3.048E-01, 2.999E-01,           
     * 2.944E-01, 2.877E-01, 2.783E-01, 2.671E-01, 2.527E-01,           
     * 2.365E-01, 2.194E-01, 2.051E-01, 1.967E-01, 1.875E-01,           
     * 1.756E-01, 1.588E-01, 1.416E-01, 1.165E-01, 9.275E-02,           
     * 6.693E-02, 4.513E-02, 2.751E-02, 1.591E-02, 9.378E-03,           
     * 4.752E-03, 3.000E-03, 2.065E-03, 1.507E-03, 1.149E-03,           
     * 8.890E-04, 7.056E-04, 5.716E-04, 4.708E-04, 3.932E-04,           
     * 3.323E-04, 2.837E-04, 2.443E-04, 2.120E-04, 1.851E-04/           
      DATA AMOL24/                                                      
     * 3.200E-01, 3.200E-01, 3.200E-01, 3.200E-01, 3.200E-01,           
     * 3.200E-01, 3.200E-01, 3.200E-01, 3.195E-01, 3.163E-01,           
     * 3.096E-01, 2.989E-01, 2.936E-01, 2.860E-01, 2.800E-01,           
     * 2.724E-01, 2.611E-01, 2.421E-01, 2.174E-01, 1.843E-01,           
     * 1.607E-01, 1.323E-01, 1.146E-01, 1.035E-01, 9.622E-02,           
     * 8.958E-02, 8.006E-02, 6.698E-02, 4.958E-02, 3.695E-02,           
     * 2.519E-02, 1.736E-02, 1.158E-02, 7.665E-03, 5.321E-03,           
     * 3.215E-03, 2.030E-03, 1.397E-03, 1.020E-03, 7.772E-04,           
     * 6.257E-04, 5.166E-04, 4.352E-04, 3.727E-04, 3.237E-04,           
     * 2.844E-04, 2.524E-04, 2.260E-04, 2.039E-04, 1.851E-04/           
      DATA AMOL34/                                                      
     * 3.200E-01, 3.200E-01, 3.200E-01, 3.200E-01, 3.200E-01,           
     * 3.200E-01, 3.200E-01, 3.200E-01, 3.195E-01, 3.163E-01,           
     * 3.096E-01, 2.989E-01, 2.936E-01, 2.860E-01, 2.800E-01,           
     * 2.724E-01, 2.611E-01, 2.421E-01, 2.174E-01, 1.843E-01,           
     * 1.621E-01, 1.362E-01, 1.230E-01, 1.124E-01, 1.048E-01,           
     * 9.661E-02, 8.693E-02, 7.524E-02, 6.126E-02, 5.116E-02,           
     * 3.968E-02, 2.995E-02, 2.080E-02, 1.311E-02, 8.071E-03,           
     * 4.164E-03, 2.629E-03, 1.809E-03, 1.321E-03, 1.007E-03,           
     * 7.883E-04, 6.333E-04, 5.194E-04, 4.333E-04, 3.666E-04,           
     * 3.140E-04, 2.717E-04, 2.373E-04, 2.089E-04, 1.851E-04/           
      DATA AMOL44/                                                      
     * 3.100E-01, 3.100E-01, 3.100E-01, 3.100E-01, 3.079E-01,           
     * 3.024E-01, 2.906E-01, 2.822E-01, 2.759E-01, 2.703E-01,           
     * 2.651E-01, 2.600E-01, 2.549E-01, 2.494E-01, 2.433E-01,           
     * 2.355E-01, 2.282E-01, 2.179E-01, 2.035E-01, 1.817E-01,           
     * 1.567E-01, 1.350E-01, 1.218E-01, 1.102E-01, 9.893E-02,
     * 8.775E-02, 7.327E-02, 5.941E-02, 4.154E-02, 3.032E-02,
     * 1.949E-02, 1.274E-02, 9.001E-03, 6.286E-03, 4.558E-03,
     * 2.795E-03, 1.765E-03, 1.214E-03, 8.866E-04, 6.756E-04,           
     * 5.538E-04, 4.649E-04, 3.979E-04, 3.459E-04, 3.047E-04,           
     * 2.713E-04, 2.439E-04, 2.210E-04, 2.017E-04, 1.851E-04/           
      DATA AMOL54/                                                      
     * 3.200E-01, 3.200E-01, 3.200E-01, 3.200E-01, 3.200E-01,           
     * 3.200E-01, 3.200E-01, 3.200E-01, 3.195E-01, 3.163E-01,           
     * 3.096E-01, 2.989E-01, 2.936E-01, 2.860E-01, 2.800E-01,           
     * 2.724E-01, 2.611E-01, 2.421E-01, 2.174E-01, 1.843E-01,           
     * 1.621E-01, 1.362E-01, 1.230E-01, 1.122E-01, 1.043E-01,           
     * 9.570E-02, 8.598E-02, 7.314E-02, 5.710E-02, 4.670E-02,           
     * 3.439E-02, 2.471E-02, 1.631E-02, 1.066E-02, 7.064E-03,           
     * 3.972E-03, 2.508E-03, 1.726E-03, 1.260E-03, 9.602E-04,           
     * 7.554E-04, 6.097E-04, 5.024E-04, 4.210E-04, 3.579E-04,           
     * 3.080E-04, 2.678E-04, 2.350E-04, 2.079E-04, 1.851E-04/           
      DATA AMOL64/                                                      
     * 3.200E-01, 3.200E-01, 3.200E-01, 3.200E-01, 3.200E-01,           
     * 3.200E-01, 3.200E-01, 3.200E-01, 3.200E-01, 3.195E-01,           
     * 3.179E-01, 3.140E-01, 3.095E-01, 3.048E-01, 2.999E-01,           
     * 2.944E-01, 2.877E-01, 2.783E-01, 2.671E-01, 2.527E-01,           
     * 2.365E-01, 2.194E-01, 2.051E-01, 1.967E-01, 1.875E-01,           
     * 1.756E-01, 1.588E-01, 1.416E-01, 1.165E-01, 9.275E-02,           
     * 6.693E-02, 4.513E-02, 2.751E-02, 1.591E-02, 9.378E-03,           
     * 4.752E-03, 3.000E-03, 2.065E-03, 1.507E-03, 1.149E-03,           
     * 8.890E-04, 7.056E-04, 5.716E-04, 4.708E-04, 3.932E-04,           
     * 3.323E-04, 2.837E-04, 2.443E-04, 2.120E-04, 1.851E-04/           

      ! DATA CO
      DATA AMOL15/                                                      
     * 1.500E-01, 1.450E-01, 1.399E-01, 1.349E-01, 1.312E-01,           
     * 1.303E-01, 1.288E-01, 1.247E-01, 1.185E-01, 1.094E-01,           
     * 9.962E-02, 8.964E-02, 7.814E-02, 6.374E-02, 5.025E-02,           
     * 3.941E-02, 3.069E-02, 2.489E-02, 1.966E-02, 1.549E-02,           
     * 1.331E-02, 1.232E-02, 1.232E-02, 1.307E-02, 1.400E-02,           
     * 1.521E-02, 1.722E-02, 1.995E-02, 2.266E-02, 2.487E-02,           
     * 2.738E-02, 3.098E-02, 3.510E-02, 3.987E-02, 4.482E-02,           
     * 5.092E-02, 5.985E-02, 6.960E-02, 9.188E-02, 1.938E-01,           
     * 5.688E-01, 1.549E+00, 3.849E+00, 6.590E+00, 1.044E+01,           
     * 1.705E+01, 2.471E+01, 3.358E+01, 4.148E+01, 5.000E+01/           
      DATA AMOL25/
     * 1.500E-01, 1.450E-01, 1.399E-01, 1.349E-01, 1.312E-01, 
     * 1.303E-01, 1.288E-01, 1.247E-01, 1.185E-01, 1.094E-01,
     * 9.962E-02, 8.964E-02, 7.814E-02, 6.374E-02, 5.025E-02,
     * 3.941E-02, 3.069E-02, 2.489E-02, 1.966E-02, 1.549E-02,
     * 1.331E-02, 1.232E-02, 1.232E-02, 1.307E-02, 1.400E-02,
     * 1.521E-02, 1.722E-02, 1.995E-02, 2.266E-02, 2.487E-02,
     * 2.716E-02, 2.962E-02, 3.138E-02, 3.307E-02, 3.487E-02,
     * 3.645E-02, 3.923E-02, 4.673E-02, 6.404E-02, 1.177E-01,
     * 2.935E-01, 6.815E-01, 1.465E+00, 2.849E+00, 5.166E+00,
     * 1.008E+01, 1.865E+01, 2.863E+01, 3.890E+01, 5.000E+01/
      DATA AMOL55/                                                      
     * 1.500E-01, 1.450E-01, 1.399E-01, 1.349E-01, 1.312E-01,           
     * 1.303E-01, 1.288E-01, 1.247E-01, 1.185E-01, 1.094E-01,           
     * 9.962E-02, 8.964E-02, 7.814E-02, 6.374E-02, 5.025E-02,           
     * 3.941E-02, 3.069E-02, 2.489E-02, 1.966E-02, 1.549E-02,           
     * 1.331E-02, 1.232E-02, 1.232E-02, 1.307E-02, 1.400E-02,           
     * 1.521E-02, 1.722E-02, 2.037E-02, 2.486E-02, 3.168E-02,           
     * 4.429E-02, 6.472E-02, 1.041E-01, 1.507E-01, 2.163E-01,           
     * 3.141E-01, 4.842E-01, 7.147E-01, 1.067E+00, 1.516E+00,           
     * 2.166E+00, 3.060E+00, 4.564E+00, 6.877E+00, 1.055E+01,           
     * 1.710E+01, 2.473E+01, 3.359E+01, 4.149E+01, 5.000E+01/           
      DATA AMOL65/                                                      
     * 1.500E-01, 1.450E-01, 1.399E-01, 1.349E-01, 1.312E-01,           
     * 1.303E-01, 1.288E-01, 1.247E-01, 1.185E-01, 1.094E-01,           
     * 9.962E-02, 8.964E-02, 7.814E-02, 6.374E-02, 5.025E-02,           
     * 3.941E-02, 3.069E-02, 2.489E-02, 1.966E-02, 1.549E-02,           
     * 1.331E-02, 1.232E-02, 1.232E-02, 1.307E-02, 1.400E-02,           
     * 1.498E-02, 1.598E-02, 1.710E-02, 1.850E-02, 2.009E-02,           
     * 2.220E-02, 2.497E-02, 2.824E-02, 3.241E-02, 3.717E-02,           
     * 4.597E-02, 6.639E-02, 1.073E-01, 1.862E-01, 3.059E-01,           
     * 6.375E-01, 1.497E+00, 3.239E+00, 5.843E+00, 1.013E+01,           
     * 1.692E+01, 2.467E+01, 3.356E+01, 4.148E+01, 5.000E+01/           
      DATA AMOL16/                                                      
     * 1.700E+00, 1.700E+00, 1.700E+00, 1.700E+00, 1.700E+00,           
     * 1.700E+00, 1.700E+00, 1.699E+00, 1.697E+00, 1.693E+00,           
     * 1.685E+00, 1.675E+00, 1.662E+00, 1.645E+00, 1.626E+00,           
     * 1.605E+00, 1.582E+00, 1.553E+00, 1.521E+00, 1.480E+00,           
     * 1.424E+00, 1.355E+00, 1.272E+00, 1.191E+00, 1.118E+00,           
     * 1.055E+00, 9.870E-01, 9.136E-01, 8.300E-01, 7.460E-01,           
     * 6.618E-01, 5.638E-01, 4.614E-01, 3.631E-01, 2.773E-01,           
     * 2.100E-01, 1.651E-01, 1.500E-01, 1.500E-01, 1.500E-01,           
     * 1.500E-01, 1.500E-01, 1.500E-01, 1.400E-01, 1.300E-01,           
     * 1.200E-01, 1.100E-01, 9.500E-02, 6.000E-02, 3.000E-02/           
      DATA AMOL26/                                                      
     * 1.700E+00, 1.700E+00, 1.700E+00, 1.700E+00, 1.697E+00,           
     * 1.687E+00, 1.672E+00, 1.649E+00, 1.629E+00, 1.615E+00,           
     * 1.579E+00, 1.542E+00, 1.508E+00, 1.479E+00, 1.451E+00,           
     * 1.422E+00, 1.390E+00, 1.356E+00, 1.323E+00, 1.281E+00,           
     * 1.224E+00, 1.154E+00, 1.066E+00, 9.730E-01, 8.800E-01,           
     * 7.888E-01, 7.046E-01, 6.315E-01, 5.592E-01, 5.008E-01,           
     * 4.453E-01, 3.916E-01, 3.389E-01, 2.873E-01, 2.384E-01,           
     * 1.944E-01, 1.574E-01, 1.500E-01, 1.500E-01, 1.500E-01,           
     * 1.500E-01, 1.500E-01, 1.500E-01, 1.400E-01, 1.300E-01,           
     * 1.200E-01, 1.100E-01, 9.500E-02, 6.000E-02, 3.000E-02/           
      DATA AMOL36/                                                      
     * 1.700E+00, 1.700E+00, 1.700E+00, 1.700E+00, 1.697E+00,           
     * 1.687E+00, 1.672E+00, 1.649E+00, 1.629E+00, 1.615E+00,           
     * 1.579E+00, 1.542E+00, 1.508E+00, 1.479E+00, 1.451E+00,           
     * 1.422E+00, 1.390E+00, 1.356E+00, 1.323E+00, 1.281E+00,           
     * 1.224E+00, 1.154E+00, 1.066E+00, 9.730E-01, 8.800E-01,           
     * 7.931E-01, 7.130E-01, 6.438E-01, 5.746E-01, 5.050E-01,           
     * 4.481E-01, 3.931E-01, 3.395E-01, 2.876E-01, 2.386E-01,           
     * 1.944E-01, 1.574E-01, 1.500E-01, 1.500E-01, 1.500E-01,           
     * 1.500E-01, 1.500E-01, 1.500E-01, 1.400E-01, 1.300E-01,           
     * 1.200E-01, 1.100E-01, 9.500E-02, 6.000E-02, 3.000E-02/           
      DATA AMOL46/                                                      
     * 1.700E+00, 1.700E+00, 1.700E+00, 1.700E+00, 1.697E+00,           
     * 1.687E+00, 1.672E+00, 1.649E+00, 1.629E+00, 1.615E+00,           
     * 1.579E+00, 1.542E+00, 1.506E+00, 1.471E+00, 1.434E+00,           
     * 1.389E+00, 1.342E+00, 1.290E+00, 1.230E+00, 1.157E+00,           
     * 1.072E+00, 9.903E-01, 9.170E-01, 8.574E-01, 8.013E-01,           
     * 7.477E-01, 6.956E-01, 6.442E-01, 5.888E-01, 5.240E-01,           
     * 4.506E-01, 3.708E-01, 2.992E-01, 2.445E-01, 2.000E-01,           
     * 1.660E-01, 1.500E-01, 1.500E-01, 1.500E-01, 1.500E-01,           
     * 1.500E-01, 1.500E-01, 1.500E-01, 1.400E-01, 1.300E-01,           
     * 1.200E-01, 1.100E-01, 9.500E-02, 6.000E-02, 3.000E-02/           
      DATA AMOL56/                                                      
     * 1.700E+00, 1.700E+00, 1.700E+00, 1.700E+00, 1.697E+00,           
     * 1.687E+00, 1.672E+00, 1.649E+00, 1.629E+00, 1.615E+00,           
     * 1.579E+00, 1.542E+00, 1.506E+00, 1.471E+00, 1.434E+00,           
     * 1.389E+00, 1.342E+00, 1.290E+00, 1.230E+00, 1.161E+00,           
     * 1.084E+00, 1.014E+00, 9.561E-01, 9.009E-01, 8.479E-01,           
     * 7.961E-01, 7.449E-01, 6.941E-01, 6.434E-01, 5.883E-01,           
     * 5.238E-01, 4.505E-01, 3.708E-01, 3.004E-01, 2.453E-01,           
     * 1.980E-01, 1.590E-01, 1.500E-01, 1.500E-01, 1.500E-01,           
     * 1.500E-01, 1.500E-01, 1.500E-01, 1.400E-01, 1.300E-01,           
     * 1.200E-01, 1.100E-01, 9.500E-02, 6.000E-02, 3.000E-02/           
      DATA AMOL66/                                                      
     * 1.700E+00, 1.700E+00, 1.700E+00, 1.700E+00, 1.700E+00,           
     * 1.700E+00, 1.700E+00, 1.699E+00, 1.697E+00, 1.693E+00,           
     * 1.685E+00, 1.675E+00, 1.662E+00, 1.645E+00, 1.626E+00,           
     * 1.605E+00, 1.582E+00, 1.553E+00, 1.521E+00, 1.480E+00,           
     * 1.424E+00, 1.355E+00, 1.272E+00, 1.191E+00, 1.118E+00,           
     * 1.055E+00, 9.870E-01, 9.136E-01, 8.300E-01, 7.460E-01,           
     * 6.618E-01, 5.638E-01, 4.614E-01, 3.631E-01, 2.773E-01,           
     * 2.100E-01, 1.650E-01, 1.500E-01, 1.500E-01, 1.500E-01,           
     * 1.500E-01, 1.500E-01, 1.500E-01, 1.400E-01, 1.300E-01,           
     * 1.200E-01, 1.100E-01, 9.500E-02, 6.000E-02, 3.000E-02/           

      ! DATA O2 
      DATA AMOL17/                                                      
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.000E+05, 1.900E+05, 1.800E+05,           
     * 1.600E+05, 1.400E+05, 1.200E+05, 9.400E+04, 7.250E+04/           
      DATA AMOL27/                                                      
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.000E+05, 1.900E+05, 1.800E+05,           
     * 1.600E+05, 1.400E+05, 1.200E+05, 9.400E+04, 7.250E+04/           
      DATA AMOL37/                                                      
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.000E+05, 1.900E+05, 1.800E+05,           
     * 1.600E+05, 1.400E+05, 1.200E+05, 9.400E+04, 7.250E+04/           
      DATA AMOL47/                                                      
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.000E+05, 1.900E+05, 1.800E+05,           
     * 1.600E+05, 1.400E+05, 1.200E+05, 9.400E+04, 7.250E+04/           
      DATA AMOL57/                                                      
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.000E+05, 1.900E+05, 1.800E+05,           
     * 1.600E+05, 1.400E+05, 1.200E+05, 9.400E+04, 7.250E+04/           
      DATA AMOL67/                                                      
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05, 2.090E+05,           
     * 2.090E+05, 2.090E+05, 2.000E+05, 1.900E+05, 1.800E+05,           
     * 1.600E+05, 1.400E+05, 1.200E+05, 9.400E+04, 7.250E+04/           

      ! DATA DENSITY 
      DATA AMOL18/                                                      
     * 2.450E+19, 2.231E+19, 2.028E+19, 1.827E+19, 1.656E+19,           
     * 1.499E+19, 1.353E+19, 1.218E+19, 1.095E+19, 9.789E+18,           
     * 8.747E+18, 7.780E+18, 6.904E+18, 6.079E+18, 5.377E+18,           
     * 4.697E+18, 4.084E+18, 3.486E+18, 2.877E+18, 2.381E+18,           
     * 1.981E+18, 1.651E+18, 1.381E+18, 1.169E+18, 9.920E+17,           
     * 8.413E+17, 5.629E+17, 3.807E+17, 2.598E+17, 1.789E+17,           
     * 1.243E+17, 8.703E+16, 6.147E+16, 4.352E+16, 3.119E+16,           
     * 2.291E+16, 1.255E+16, 6.844E+15, 3.716E+15, 1.920E+15,           
     * 9.338E+14, 4.314E+14, 1.801E+14, 7.043E+13, 2.706E+13,           
     * 1.098E+13, 4.445E+12, 1.941E+12, 8.706E+11, 4.225E+11/           
      DATA AMOL28/                                                      
     * 2.496E+19, 2.257E+19, 2.038E+19, 1.843E+19, 1.666E+19,           
     * 1.503E+19, 1.351E+19, 1.212E+19, 1.086E+19, 9.716E+18,           
     * 8.656E+18, 7.698E+18, 6.814E+18, 6.012E+18, 5.141E+18,           
     * 4.368E+18, 3.730E+18, 3.192E+18, 2.715E+18, 2.312E+18,           
     * 1.967E+18, 1.677E+18, 1.429E+18, 1.223E+18, 1.042E+18,           
     * 8.919E+17, 6.050E+17, 4.094E+17, 2.820E+17, 1.927E+17,           
     * 1.338E+17, 9.373E+16, 6.624E+16, 4.726E+16, 3.398E+16,           
     * 2.500E+16, 1.386E+16, 7.668E+15, 4.196E+15, 2.227E+15,           
     * 1.109E+15, 4.996E+14, 1.967E+14, 7.204E+13, 2.541E+13,           
     * 9.816E+12, 3.816E+12, 1.688E+12, 8.145E+11, 4.330E+11/           
      DATA AMOL38/                                                      
     * 2.711E+19, 2.420E+19, 2.158E+19, 1.922E+19, 1.724E+19,           
     * 1.542E+19, 1.376E+19, 1.225E+19, 1.086E+19, 9.612E+18,           
     * 8.472E+18, 7.271E+18, 6.237E+18, 5.351E+18, 4.588E+18,           
     * 3.931E+18, 3.368E+18, 2.886E+18, 2.473E+18, 2.115E+18,           
     * 1.809E+18, 1.543E+18, 1.317E+18, 1.125E+18, 9.633E+17,           
     * 8.218E+17, 5.536E+17, 3.701E+17, 2.486E+17, 1.647E+17,           
     * 1.108E+17, 7.540E+16, 5.202E+16, 3.617E+16, 2.570E+16,           
     * 1.863E+16, 1.007E+16, 5.433E+15, 2.858E+15, 1.477E+15,           
     * 7.301E+14, 3.553E+14, 1.654E+14, 7.194E+13, 3.052E+13,           
     * 1.351E+13, 6.114E+12, 2.952E+12, 1.479E+12, 7.836E+11/           
      DATA AMOL48/                                                      
     * 2.549E+19, 2.305E+19, 2.080E+19, 1.873E+19, 1.682E+19,           
     * 1.508E+19, 1.357E+19, 1.216E+19, 1.088E+19, 9.701E+18,           
     * 8.616E+18, 7.402E+18, 6.363E+18, 5.471E+18, 4.699E+18,           
     * 4.055E+18, 3.476E+18, 2.987E+18, 2.568E+18, 2.208E+18,           
     * 1.899E+18, 1.632E+18, 1.403E+18, 1.207E+18, 1.033E+18,           
     * 8.834E+17, 6.034E+17, 4.131E+17, 2.839E+17, 1.938E+17,           
     * 1.344E+17, 9.402E+16, 6.670E+16, 4.821E+16, 3.516E+16,           
     * 2.581E+16, 1.421E+16, 7.946E+15, 4.445E+15, 2.376E+15,           
     * 1.198E+15, 5.311E+14, 2.022E+14, 7.221E+13, 2.484E+13,           
     * 9.441E+12, 3.624E+12, 1.610E+12, 7.951E+11, 4.311E+11/           
      DATA AMOL58/                                                      
     * 2.855E+19, 2.484E+19, 2.202E+19, 1.950E+19, 1.736E+19,           
     * 1.552E+19, 1.383E+19, 1.229E+19, 1.087E+19, 9.440E+18,           
     * 8.069E+18, 6.898E+18, 5.893E+18, 5.039E+18, 4.308E+18,           
     * 3.681E+18, 3.156E+18, 2.704E+18, 2.316E+18, 1.982E+18,           
     * 1.697E+18, 1.451E+18, 1.241E+18, 1.061E+18, 9.065E+17,           
     * 7.742E+17, 5.134E+17, 3.423E+17, 2.292E+17, 1.533E+17,           
     * 1.025E+17, 6.927E+16, 4.726E+16, 3.266E+16, 2.261E+16,           
     * 1.599E+16, 8.364E+15, 4.478E+15, 2.305E+15, 1.181E+15,           
     * 6.176E+14, 3.127E+14, 1.531E+14, 7.244E+13, 3.116E+13,           
     * 1.403E+13, 6.412E+12, 3.099E+12, 1.507E+12, 7.814E+11/           
      DATA AMOL68/                                                      
     * 2.548E+19, 2.313E+19, 2.094E+19, 1.891E+19, 1.704E+19,           
     * 1.532E+19, 1.373E+19, 1.228E+19, 1.094E+19, 9.719E+18,           
     * 8.602E+18, 7.589E+18, 6.489E+18, 5.546E+18, 4.739E+18,           
     * 4.050E+18, 3.462E+18, 2.960E+18, 2.530E+18, 2.163E+18,           
     * 1.849E+18, 1.575E+18, 1.342E+18, 1.144E+18, 9.765E+17,           
     * 8.337E+17, 5.640E+17, 3.830E+17, 2.524E+17, 1.761E+17,           
     * 1.238E+17, 8.310E+16, 5.803E+16, 4.090E+16, 2.920E+16,           
     * 2.136E+16, 1.181E+16, 6.426E+15, 3.386E+15, 1.723E+15,           
     * 8.347E+14, 3.832E+14, 1.711E+14, 7.136E+13, 2.924E+13,           
     * 1.189E+13, 5.033E+12, 2.144E+12, 9.688E+11, 5.114E+11/           
                                                                        
      DATA ANO        /                                                 
     *  3.00E-04,  3.00E-04,  3.00E-04,  3.00E-04,  3.00E-04,           
     *  3.00E-04,  3.00E-04,  3.00E-04,  3.00E-04,  3.00E-04,           
     *  3.00E-04,  3.00E-04,  3.00E-04,  2.99E-04,  2.95E-04,           
     *  2.83E-04,  2.68E-04,  2.52E-04,  2.40E-04,  2.44E-04,           
     *  2.55E-04,  2.77E-04,  3.07E-04,  3.60E-04,  4.51E-04,           
     *  6.85E-04,  1.28E-03,  2.45E-03,  4.53E-03,  7.14E-03,           
     *  9.34E-03,  1.12E-02,  1.19E-02,  1.17E-02,  1.10E-02,           
     *  1.03E-02,  1.01E-02,  1.01E-02,  1.03E-02,  1.15E-02,           
     *  1.61E-02,  2.68E-02,  7.01E-02,  2.13E-01,  7.12E-01,           
     *  2.08E+00,  4.50E+00,  7.98E+00,  1.00E+01,  1.00E+01/           
      DATA SO2       /                                                  
     *  3.00E-04,  2.74E-04,  2.36E-04,  1.90E-04,  1.46E-04,           
     *  1.18E-04,  9.71E-05,  8.30E-05,  7.21E-05,  6.56E-05,           
     *  6.08E-05,  5.79E-05,  5.60E-05,  5.59E-05,  5.64E-05,           
     *  5.75E-05,  5.75E-05,  5.37E-05,  4.78E-05,  3.97E-05,           
     *  3.19E-05,  2.67E-05,  2.28E-05,  2.07E-05,  1.90E-05,           
     *  1.75E-05,  1.54E-05,  1.34E-05,  1.21E-05,  1.16E-05,           
     *  1.21E-05,  1.36E-05,  1.65E-05,  2.10E-05,  2.77E-05,           
     *  3.56E-05,  4.59E-05,  5.15E-05,  5.11E-05,  4.32E-05,           
     *  2.83E-05,  1.33E-05,  5.56E-06,  2.24E-06,  8.96E-07,           
     *  3.58E-07,  1.43E-07,  5.73E-08,  2.29E-08,  9.17E-09/           
      DATA ANO2       /                                                 
     *  2.30E-05,  2.30E-05,  2.30E-05,  2.30E-05,  2.30E-05,           
     *  2.30E-05,  2.30E-05,  2.30E-05,  2.30E-05,  2.32E-05,           
     *  2.38E-05,  2.62E-05,  3.15E-05,  4.45E-05,  7.48E-05,           
     *  1.71E-04,  3.19E-04,  5.19E-04,  7.71E-04,  1.06E-03,           
     *  1.39E-03,  1.76E-03,  2.16E-03,  2.58E-03,  3.06E-03,           
     *  3.74E-03,  4.81E-03,  6.16E-03,  7.21E-03,  7.28E-03,           
     *  6.26E-03,  4.03E-03,  2.17E-03,  1.15E-03,  6.66E-04,           
     *  4.43E-04,  3.39E-04,  2.85E-04,  2.53E-04,  2.31E-04,           
     *  2.15E-04,  2.02E-04,  1.92E-04,  1.83E-04,  1.76E-04,           
     *  1.70E-04,  1.64E-04,  1.59E-04,  1.55E-04,  1.51E-04/           
      DATA ANH3       /                                                 
     *  5.00E-04,  5.00E-04,  4.63E-04,  3.80E-04,  2.88E-04,           
     *  2.04E-04,  1.46E-04,  9.88E-05,  6.48E-05,  3.77E-05,           
     *  2.03E-05,  1.09E-05,  6.30E-06,  3.12E-06,  1.11E-06,           
     *  4.47E-07,  2.11E-07,  1.10E-07,  6.70E-08,  3.97E-08,           
     *  2.41E-08,  1.92E-08,  1.72E-08,  1.59E-08,  1.44E-08,           
     *  1.23E-08,  9.37E-09,  6.35E-09,  3.68E-09,  1.82E-09,           
     *  9.26E-10,  2.94E-10,  8.72E-11,  2.98E-11,  1.30E-11,           
     *  7.13E-12,  4.80E-12,  3.66E-12,  3.00E-12,  2.57E-12,           
     *  2.27E-12,  2.04E-12,  1.85E-12,  1.71E-12,  1.59E-12,           
     *  1.48E-12,  1.40E-12,  1.32E-12,  1.25E-12,  1.19E-12/           
      DATA HNO3      /                                                  
     *  5.00E-05,  5.96E-05,  6.93E-05,  7.91E-05,  8.87E-05,           
     *  9.75E-05,  1.11E-04,  1.26E-04,  1.39E-04,  1.53E-04,           
     *  1.74E-04,  2.02E-04,  2.41E-04,  2.76E-04,  3.33E-04,           
     *  4.52E-04,  7.37E-04,  1.31E-03,  2.11E-03,  3.17E-03,           
     *  4.20E-03,  4.94E-03,  5.46E-03,  5.74E-03,  5.84E-03,           
     *  5.61E-03,  4.82E-03,  3.74E-03,  2.59E-03,  1.64E-03,           
     *  9.68E-04,  5.33E-04,  2.52E-04,  1.21E-04,  7.70E-05,           
     *  5.55E-05,  4.45E-05,  3.84E-05,  3.49E-05,  3.27E-05,           
     *  3.12E-05,  3.01E-05,  2.92E-05,  2.84E-05,  2.78E-05,           
     *  2.73E-05,  2.68E-05,  2.64E-05,  2.60E-05,  2.57E-05/           
      DATA OH        /                                                  
     *  4.40E-08,  4.40E-08,  4.40E-08,  4.40E-08,  4.40E-08,           
     *  4.40E-08,  4.40E-08,  4.41E-08,  4.45E-08,  4.56E-08,           
     *  4.68E-08,  4.80E-08,  4.94E-08,  5.19E-08,  5.65E-08,           
     *  6.75E-08,  8.25E-08,  1.04E-07,  1.30E-07,  1.64E-07,           
     *  2.16E-07,  3.40E-07,  5.09E-07,  7.59E-07,  1.16E-06,           
     *  2.18E-06,  5.00E-06,  1.17E-05,  3.40E-05,  8.35E-05,           
     *  1.70E-04,  2.85E-04,  4.06E-04,  5.11E-04,  5.79E-04,           
     *  6.75E-04,  9.53E-04,  1.76E-03,  3.74E-03,  7.19E-03,           
     *  1.12E-02,  1.13E-02,  6.10E-03,  1.51E-03,  2.42E-04,           
     *  4.47E-05,  1.77E-05,  1.19E-05,  1.35E-05,  2.20E-05/           
      DATA HF        /                                                  
     *  1.00E-08,  1.00E-08,  1.23E-08,  1.97E-08,  3.18E-08,           
     *  5.63E-08,  9.18E-08,  1.53E-07,  2.41E-07,  4.04E-07,           
     *  6.57E-07,  1.20E-06,  1.96E-06,  3.12E-06,  4.62E-06,           
     *  7.09E-06,  1.05E-05,  1.69E-05,  2.57E-05,  4.02E-05,           
     *  5.77E-05,  7.77E-05,  9.90E-05,  1.23E-04,  1.50E-04,           
     *  1.82E-04,  2.30E-04,  2.83E-04,  3.20E-04,  3.48E-04,           
     *  3.72E-04,  3.95E-04,  4.10E-04,  4.21E-04,  4.24E-04,           
     *  4.25E-04,  4.25E-04,  4.25E-04,  4.25E-04,  4.25E-04,           
     *  4.25E-04,  4.25E-04,  4.25E-04,  4.25E-04,  4.25E-04,           
     *  4.25E-04,  4.25E-04,  4.25E-04,  4.25E-04,  4.25E-04/           
      DATA HCL       /                                                  
     *  1.00E-03,  7.49E-04,  5.61E-04,  4.22E-04,  3.19E-04,           
     *  2.39E-04,  1.79E-04,  1.32E-04,  9.96E-05,  7.48E-05,           
     *  5.68E-05,  4.59E-05,  4.36E-05,  6.51E-05,  1.01E-04,           
     *  1.63E-04,  2.37E-04,  3.13E-04,  3.85E-04,  4.42E-04,           
     *  4.89E-04,  5.22E-04,  5.49E-04,  5.75E-04,  6.04E-04,           
     *  6.51E-04,  7.51E-04,  9.88E-04,  1.28E-03,  1.57E-03,           
     *  1.69E-03,  1.74E-03,  1.76E-03,  1.79E-03,  1.80E-03,           
     *  1.80E-03,  1.80E-03,  1.80E-03,  1.80E-03,  1.80E-03,           
     *  1.80E-03,  1.80E-03,  1.80E-03,  1.80E-03,  1.80E-03,           
     *  1.80E-03,  1.80E-03,  1.80E-03,  1.80E-03,  1.80E-03/           
      DATA HBR       /                                                  
     *  1.70E-06,  1.70E-06,  1.70E-06,  1.70E-06,  1.70E-06,           
     *  1.70E-06,  1.70E-06,  1.70E-06,  1.70E-06,  1.70E-06,           
     *  1.70E-06,  1.70E-06,  1.70E-06,  1.70E-06,  1.70E-06,           
     *  1.70E-06,  1.70E-06,  1.70E-06,  1.70E-06,  1.70E-06,           
     *  1.70E-06,  1.70E-06,  1.70E-06,  1.70E-06,  1.70E-06,           
     *  1.71E-06,  1.76E-06,  1.90E-06,  2.26E-06,  2.82E-06,           
     *  3.69E-06,  4.91E-06,  6.13E-06,  6.85E-06,  7.08E-06,           
     *  7.14E-06,  7.15E-06,  7.15E-06,  7.15E-06,  7.15E-06,           
     *  7.15E-06,  7.15E-06,  7.15E-06,  7.15E-06,  7.15E-06,           
     *  7.15E-06,  7.15E-06,  7.15E-06,  7.15E-06,  7.15E-06/           
      DATA HI        /                                                  
     *  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,           
     *  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,           
     *  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,           
     *  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,           
     *  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,           
     *  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,           
     *  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,           
     *  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,           
     *  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,           
     *  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06,  3.00E-06/           
      DATA CLO       /                                                  
     *  1.00E-08,  1.00E-08,  1.00E-08,  1.00E-08,  1.00E-08,           
     *  1.00E-08,  1.00E-08,  1.00E-08,  1.01E-08,  1.05E-08,           
     *  1.21E-08,  1.87E-08,  3.18E-08,  5.61E-08,  9.99E-08,           
     *  1.78E-07,  3.16E-07,  5.65E-07,  1.04E-06,  2.04E-06,           
     *  4.64E-06,  8.15E-06,  1.07E-05,  1.52E-05,  2.24E-05,           
     *  3.97E-05,  8.48E-05,  1.85E-04,  3.57E-04,  5.08E-04,           
     *  6.07E-04,  5.95E-04,  4.33E-04,  2.51E-04,  1.56E-04,           
     *  1.04E-04,  7.69E-05,  6.30E-05,  5.52E-05,  5.04E-05,           
     *  4.72E-05,  4.49E-05,  4.30E-05,  4.16E-05,  4.03E-05,           
     *  3.93E-05,  3.83E-05,  3.75E-05,  3.68E-05,  3.61E-05/           
      DATA OCS       /                                                  
     *  6.00E-04,  5.90E-04,  5.80E-04,  5.70E-04,  5.62E-04,           
     *  5.55E-04,  5.48E-04,  5.40E-04,  5.32E-04,  5.25E-04,           
     *  5.18E-04,  5.09E-04,  4.98E-04,  4.82E-04,  4.60E-04,           
     *  4.26E-04,  3.88E-04,  3.48E-04,  3.09E-04,  2.74E-04,           
     *  2.41E-04,  2.14E-04,  1.88E-04,  1.64E-04,  1.37E-04,           
     *  1.08E-04,  6.70E-05,  2.96E-05,  1.21E-05,  4.31E-06,           
     *  1.60E-06,  6.71E-07,  4.35E-07,  3.34E-07,  2.80E-07,           
     *  2.47E-07,  2.28E-07,  2.16E-07,  2.08E-07,  2.03E-07,           
     *  1.98E-07,  1.95E-07,  1.92E-07,  1.89E-07,  1.87E-07,           
     *  1.85E-07,  1.83E-07,  1.81E-07,  1.80E-07,  1.78E-07/           
      DATA H2CO      /                                                  
     *  2.40E-03,  1.07E-03,  4.04E-04,  2.27E-04,  1.40E-04,           
     *  1.00E-04,  7.44E-05,  6.04E-05,  5.01E-05,  4.22E-05,           
     *  3.63E-05,  3.43E-05,  3.39E-05,  3.50E-05,  3.62E-05,           
     *  3.62E-05,  3.58E-05,  3.50E-05,  3.42E-05,  3.39E-05,           
     *  3.43E-05,  3.68E-05,  4.03E-05,  4.50E-05,  5.06E-05,           
     *  5.82E-05,  7.21E-05,  8.73E-05,  1.01E-04,  1.11E-04,           
     *  1.13E-04,  1.03E-04,  7.95E-05,  4.82E-05,  1.63E-05,           
     *  5.10E-06,  2.00E-06,  1.05E-06,  6.86E-07,  5.14E-07,           
     *  4.16E-07,  3.53E-07,  3.09E-07,  2.76E-07,  2.50E-07,           
     *  2.30E-07,  2.13E-07,  1.98E-07,  1.86E-07,  1.75E-07/           
      DATA HOCL      /                                                  
     *  7.70E-06,  1.06E-05,  1.22E-05,  1.14E-05,  9.80E-06,           
     *  8.01E-06,  6.42E-06,  5.42E-06,  4.70E-06,  4.41E-06,           
     *  4.34E-06,  4.65E-06,  5.01E-06,  5.22E-06,  5.60E-06,           
     *  6.86E-06,  8.77E-06,  1.20E-05,  1.63E-05,  2.26E-05,           
     *  3.07E-05,  4.29E-05,  5.76E-05,  7.65E-05,  9.92E-05,           
     *  1.31E-04,  1.84E-04,  2.45E-04,  2.96E-04,  3.21E-04,           
     *  3.04E-04,  2.48E-04,  1.64E-04,  9.74E-05,  4.92E-05,           
     *  2.53E-05,  1.50E-05,  1.05E-05,  8.34E-06,  7.11E-06,           
     *  6.33E-06,  5.78E-06,  5.37E-06,  5.05E-06,  4.78E-06,           
     *  4.56E-06,  4.37E-06,  4.21E-06,  4.06E-06,  3.93E-06/           
      DATA AN2        /                                                 
     *  7.81E+05,  7.81E+05,  7.81E+05,  7.81E+05,  7.81E+05,           
     *  7.81E+05,  7.81E+05,  7.81E+05,  7.81E+05,  7.81E+05,           
     *  7.81E+05,  7.81E+05,  7.81E+05,  7.81E+05,  7.81E+05,           
     *  7.81E+05,  7.81E+05,  7.81E+05,  7.81E+05,  7.81E+05,           
     *  7.81E+05,  7.81E+05,  7.81E+05,  7.81E+05,  7.81E+05,           
     *  7.81E+05,  7.81E+05,  7.81E+05,  7.81E+05,  7.81E+05,           
     *  7.81E+05,  7.81E+05,  7.81E+05,  7.81E+05,  7.81E+05,           
     *  7.81E+05,  7.81E+05,  7.81E+05,  7.81E+05,  7.81E+05,           
     *  7.81E+05,  7.81E+05,  7.81E+05,  7.80E+05,  7.79E+05,           
     *  7.77E+05,  7.74E+05,  7.70E+05,  7.65E+05,  7.60E+05/           
      DATA HCN       /                                                  
     *  1.70E-04,  1.65E-04,  1.63E-04,  1.61E-04,  1.60E-04,           
     *  1.60E-04,  1.60E-04,  1.60E-04,  1.60E-04,  1.60E-04,           
     *  1.60E-04,  1.60E-04,  1.60E-04,  1.59E-04,  1.57E-04,           
     *  1.55E-04,  1.52E-04,  1.49E-04,  1.45E-04,  1.41E-04,           
     *  1.37E-04,  1.34E-04,  1.30E-04,  1.25E-04,  1.19E-04,           
     *  1.13E-04,  1.05E-04,  9.73E-05,  9.04E-05,  8.46E-05,           
     *  8.02E-05,  7.63E-05,  7.30E-05,  7.00E-05,  6.70E-05,           
     *  6.43E-05,  6.21E-05,  6.02E-05,  5.88E-05,  5.75E-05,           
     *  5.62E-05,  5.50E-05,  5.37E-05,  5.25E-05,  5.12E-05,           
     *  5.00E-05,  4.87E-05,  4.75E-05,  4.62E-05,  4.50E-05/           
      DATA CH3CL     /                                                  
     *  7.00E-04,  6.70E-04,  6.43E-04,  6.22E-04,  6.07E-04,           
     *  6.02E-04,  6.00E-04,  6.00E-04,  5.98E-04,  5.94E-04,           
     *  5.88E-04,  5.79E-04,  5.66E-04,  5.48E-04,  5.28E-04,           
     *  5.03E-04,  4.77E-04,  4.49E-04,  4.21E-04,  3.95E-04,           
     *  3.69E-04,  3.43E-04,  3.17E-04,  2.86E-04,  2.48E-04,           
     *  1.91E-04,  1.10E-04,  4.72E-05,  1.79E-05,  7.35E-06,           
     *  3.03E-06,  1.32E-06,  8.69E-07,  6.68E-07,  5.60E-07,           
     *  4.94E-07,  4.56E-07,  4.32E-07,  4.17E-07,  4.05E-07,           
     *  3.96E-07,  3.89E-07,  3.83E-07,  3.78E-07,  3.73E-07,           
     *  3.69E-07,  3.66E-07,  3.62E-07,  3.59E-07,  3.56E-07/           
      DATA H2O2      /                                                  
     *  2.00E-04,  1.95E-04,  1.92E-04,  1.89E-04,  1.84E-04,           
     *  1.77E-04,  1.66E-04,  1.49E-04,  1.23E-04,  9.09E-05,           
     *  5.79E-05,  3.43E-05,  1.95E-05,  1.08E-05,  6.59E-06,           
     *  4.20E-06,  2.94E-06,  2.30E-06,  2.24E-06,  2.68E-06,           
     *  3.68E-06,  5.62E-06,  1.03E-05,  1.97E-05,  3.70E-05,           
     *  6.20E-05,  1.03E-04,  1.36E-04,  1.36E-04,  1.13E-04,           
     *  8.51E-05,  6.37E-05,  5.17E-05,  4.44E-05,  3.80E-05,           
     *  3.48E-05,  3.62E-05,  5.25E-05,  1.26E-04,  3.77E-04,           
     *  1.12E-03,  2.00E-03,  1.68E-03,  4.31E-04,  4.98E-05,           
     *  6.76E-06,  8.38E-07,  9.56E-08,  1.00E-08,  1.00E-09/           
      DATA C2H2      /                                                  
     *  3.00E-04,  1.72E-04,  9.57E-05,  6.74E-05,  5.07E-05,           
     *  3.99E-05,  3.19E-05,  2.80E-05,  2.55E-05,  2.40E-05,           
     *  2.27E-05,  2.08E-05,  1.76E-05,  1.23E-05,  7.32E-06,           
     *  4.52E-06,  2.59E-06,  1.55E-06,  8.63E-07,  5.30E-07,           
     *  3.10E-07,  1.89E-07,  1.04E-07,  5.75E-08,  2.23E-08,           
     *  8.51E-09,  4.09E-09,  2.52E-09,  1.86E-09,  1.52E-09,           
     *  1.32E-09,  1.18E-09,  1.08E-09,  9.97E-10,  9.34E-10,           
     *  8.83E-10,  8.43E-10,  8.10E-10,  7.83E-10,  7.60E-10,           
     *  7.40E-10,  7.23E-10,  7.07E-10,  6.94E-10,  6.81E-10,           
     *  6.70E-10,  6.59E-10,  6.49E-10,  6.40E-10,  6.32E-10/           
      DATA C2H6      /                                                  
     *  2.00E-03,  2.00E-03,  2.00E-03,  2.00E-03,  1.98E-03,           
     *  1.95E-03,  1.90E-03,  1.85E-03,  1.79E-03,  1.72E-03,           
     *  1.58E-03,  1.30E-03,  9.86E-04,  7.22E-04,  4.96E-04,           
     *  3.35E-04,  2.14E-04,  1.49E-04,  1.05E-04,  7.96E-05,           
     *  6.01E-05,  4.57E-05,  3.40E-05,  2.60E-05,  1.89E-05,           
     *  1.22E-05,  5.74E-06,  2.14E-06,  8.49E-07,  3.42E-07,           
     *  1.34E-07,  5.39E-08,  2.25E-08,  1.04E-08,  6.57E-09,           
     *  4.74E-09,  3.79E-09,  3.28E-09,  2.98E-09,  2.79E-09,           
     *  2.66E-09,  2.56E-09,  2.49E-09,  2.43E-09,  2.37E-09,           
     *  2.33E-09,  2.29E-09,  2.25E-09,  2.22E-09,  2.19E-09/           
      DATA PH3       /                                                  
     *  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,           
     *  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,           
     *  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,           
     *  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,           
     *  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,           
     *  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,           
     *  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,           
     *  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,           
     *  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,           
     *  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14,  1.00E-14/           

      !================================================================
      ! ASSIGN to module arrays
      !================================================================

      ! Zerolize  arrays
      STDATMZ(:) = 0d0
      STDATMP(:) = 0d0
      STDATMT(:) = 0d0
      STDINTENSITY(:) = 0d0
      STDAMOLS(:,:) = 0d0
      
      IF ( IATM == 1 ) THEN
      
         WRITE(6,100) CTMNA1(1:3)
         STDATMP(1:50) = P1(1:50)
         STDATMT(1:50) = T1(1:50)
         STDAMOLS(1:50,1) = AMOL11(1:50)
         STDAMOLS(1:50,2) = AMOL12(1:50)
         STDAMOLS(1:50,3) = AMOL13(1:50)
         STDAMOLS(1:50,4) = AMOL14(1:50)
         STDAMOLS(1:50,5) = AMOL15(1:50)
         STDAMOLS(1:50,6) = AMOL16(1:50)
         STDAMOLS(1:50,7) = AMOL17(1:50)
         STDINTENSITY(1:50) = AMOL18(1:50)

      ELSE IF ( IATM == 2 ) THEN

         WRITE(6,100) CTMNA2(1:3)
         STDATMP(1:50) = P2(1:50)
         STDATMT(1:50) = T2(1:50)
         STDAMOLS(1:50,1) = AMOL21(1:50)
         STDAMOLS(1:50,2) = AMOL22(1:50)
         STDAMOLS(1:50,3) = AMOL23(1:50)
         STDAMOLS(1:50,4) = AMOL24(1:50)
         STDAMOLS(1:50,5) = AMOL25(1:50)
         STDAMOLS(1:50,6) = AMOL26(1:50)
         STDAMOLS(1:50,7) = AMOL27(1:50)
         STDINTENSITY(1:50) = AMOL28(1:50)

      ELSE IF ( IATM == 3 ) THEN

         WRITE(6,100) CTMNA3(1:3)
         STDATMP(1:50) = P3(1:50)
         STDATMT(1:50) = T3(1:50)
         STDAMOLS(1:50,1) = AMOL31(1:50)
         STDAMOLS(1:50,2) = AMOL32(1:50)
         STDAMOLS(1:50,3) = AMOL33(1:50)
         STDAMOLS(1:50,4) = AMOL34(1:50)
         STDAMOLS(1:50,5) = AMOL35(1:50)
         STDAMOLS(1:50,6) = AMOL36(1:50)
         STDAMOLS(1:50,7) = AMOL37(1:50)
         STDINTENSITY(1:50) = AMOL38(1:50)
      
      ELSE IF ( IATM == 4 ) THEN
         
         WRITE(6,100) CTMNA4(1:3)
         STDATMP(1:50) = P4(1:50)
         STDATMT(1:50) = T4(1:50)
         STDAMOLS(1:50,1) = AMOL41(1:50)
         STDAMOLS(1:50,2) = AMOL42(1:50)
         STDAMOLS(1:50,3) = AMOL43(1:50)
         STDAMOLS(1:50,4) = AMOL44(1:50)
         STDAMOLS(1:50,5) = AMOL45(1:50)
         STDAMOLS(1:50,6) = AMOL46(1:50)
         STDAMOLS(1:50,7) = AMOL47(1:50)
         STDINTENSITY(1:50) = AMOL48(1:50)
      
      ELSE IF ( IATM == 5 ) THEN
         
         WRITE(6,100) CTMNA5(1:3)
         STDATMP(1:50) = P5(1:50)
         STDATMT(1:50) = T5(1:50)
         STDAMOLS(1:50,1) = AMOL51(1:50)
         STDAMOLS(1:50,2) = AMOL52(1:50)
         STDAMOLS(1:50,3) = AMOL53(1:50)
         STDAMOLS(1:50,4) = AMOL54(1:50)
         STDAMOLS(1:50,5) = AMOL55(1:50)
         STDAMOLS(1:50,6) = AMOL56(1:50)
         STDAMOLS(1:50,7) = AMOL57(1:50)
         STDINTENSITY(1:50) = AMOL58(1:50)
      
      ELSE IF ( IATM == 6 ) THEN
         
         WRITE(6,100) CTMNA6(1:3)
         STDATMP(1:50) = P6(1:50)
         STDATMT(1:50) = T6(1:50)
         STDAMOLS(1:50,1) = AMOL61(1:50)
         STDAMOLS(1:50,2) = AMOL62(1:50)
         STDAMOLS(1:50,3) = AMOL63(1:50)
         STDAMOLS(1:50,4) = AMOL64(1:50)
         STDAMOLS(1:50,5) = AMOL65(1:50)
         STDAMOLS(1:50,6) = AMOL66(1:50)
         STDAMOLS(1:50,7) = AMOL67(1:50)
         STDINTENSITY(1:50) = AMOL68(1:50)

      ENDIF
 100  FORMAT( '  Used atmos profile: ', 3A8 )

      STDATMZ(1:50)     = ALT(1:50)
      STDAMOLS(1:50, 8) = ANO(1:50)
      STDAMOLS(1:50, 9) = SO2(1:50)
      STDAMOLS(1:50,10) = ANO2(1:50)
      STDAMOLS(1:50,11) = ANH3(1:50)
      STDAMOLS(1:50,12) = HNO3(1:50)
      STDAMOLS(1:50,13) = OH(1:50)
      STDAMOLS(1:50,14) = HF(1:50)
      STDAMOLS(1:50,15) = HCL(1:50)
      STDAMOLS(1:50,16) = HBR(1:50)
      STDAMOLS(1:50,17) = HI(1:50)
      STDAMOLS(1:50,18) = CLO(1:50)
      STDAMOLS(1:50,19) = OCS(1:50)
      STDAMOLS(1:50,20) = H2CO(1:50)
      STDAMOLS(1:50,21) = HOCL(1:50)
      STDAMOLS(1:50,22) = AN2(1:50)
      STDAMOLS(1:50,23) = HCN(1:50)
      STDAMOLS(1:50,24) = CH3CL(1:50)
      STDAMOLS(1:50,25) = H2O2(1:50)
      STDAMOLS(1:50,26) = C2H2(1:50)
      STDAMOLS(1:50,27) = C2H6(1:50)
      STDAMOLS(1:50,28) = PH3(1:50)
      STDAMOLS(1:50,29) = TDUM(1:50)

      END SUBROUTINE STDATMOS
          
!------------------------------------------------------------------------------

      END MODULE ATMPROF_MOD
