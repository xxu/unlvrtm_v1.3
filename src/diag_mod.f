! $Id: diag_mod.f,v 1.1 2010/08/29 23:51:27 xxu Exp $
      MODULE DIAG_MOD
!
!******************************************************************************
!  Module DIAG_MOD contains arrays and routines for archiving the output
!  diagnostic varaibles based on the setting of DIAGNOSTIC MENU in the
!  file namelist.ini.
!  (xxu, 8/29/10, 3/17/11)
!
!  Module Variables:
!  ============================================================================
!  (1 ) FID      ( INTEGER ) : Unit # of the netCDF file  
!  (2 ) DIM01    ( INTEGER ) : Dimenison ID for n_sza 
!  (3 ) DIM02    ( INTEGER ) : Dimenison ID for n_vza
!  (4 ) DIM03    ( INTEGER ) : Dimenison ID for n_raz
!  (5 ) DIM04    ( INTEGER ) : Dimenison ID for n_level
!  (6 ) DIM05    ( INTEGER ) : Dimenison ID for n_layer
!  (7 ) DIM06    ( INTEGER ) : Dimenison ID for maxmol
!  (8 ) DIM07    ( INTEGER ) : Dimenison ID for n_geom
!  (9 ) DIM08    ( INTEGER ) : Dimenison ID for n_lout
!  (10) DIM09    ( INTEGER ) : Dimenison ID for n_stok
!  (11) DIM10    ( INTEGER ) : Dimenison ID for n_jacb
!  (12) DIM11    ( INTEGER ) : Dimenison ID for n_gas
!  (13) DIM12    ( INTEGER ) : Dimenison ID for nval2
!  (14) DIM13    ( INTEGER ) : Dimenison ID for nval3
!  (15) DIM14    ( INTEGER ) : Dimenison ID for n_momt
!  (16) DIM15    ( INTEGER ) : Dimenison ID for len_gas
!  (17) DIM16    ( INTEGER ) : Dimenison ID for len_lpar
!  (18) DIM17    ( INTEGER ) : Dimenison ID for nval4
!  (19) DIM18    ( INTEGER ) : Dimenison ID for nval5
!  (20) DIM19    ( INTEGER ) : Dimenison ID for nval6
!  (12) DIM20    ( INTEGER ) : Dimenison ID for nangle
!  (13) DIM21    ( INTEGER ) : Dimenison ID for n_jacb_s
!  Module Routines:
!  ============================================================================
!  (1 ) OUTPUT_DIAG          : Output diagnostic variables to netCDF file
!  (2 ) SAVE_DIAG01          : Save variables for DIAG01
!  (3 ) SAVE_DIAG02          : Save variables for DIAG02
!  (4 ) SAVE_DIAG03          : Save variables for DIAG03
!  (5 ) SAVE_DIAG04          : Save variables for DIAG04
!  (6 ) SAVE_DIAG05          : Save variables for DIAG05
!  (7 ) SAVE_DIAG06          : Save variables for DIAG06
!  (8 ) SAVE_DIAG07          : Save variables for DIAG07
!  (10) SAVE_DIAG08          : Save variables for DIAG08
!  (11) CHECK                : Check the status of calling netCDF libraries
!  (12) NCIO_0D              : Save 1-D variables in to the netCDF file
!  (13) NCIO_0D_INT          : Save 1-D variables in to the netCDF file
!  (14) NCIO_1D              : Save 1-D variables in to the netCDF file
!  (15) NCIO_1DA             : Save 1-D variables in to the netCDF file
!  (16) NCIO_2D              : Save 2-D variables in to the netCDF file
!  (17) NCIO_3D              : Save 3-D variables in to the netCDF file
!  (18) NCIO_4D              : Save 4-D variables in to the netCDF file
!  (19) NCIO_5D              : Save 5-D variables in to the netCDF file
!
!  NOTES:
!  (1 ) Now this diagnostic code is completely associated to the DIAGNOSTIC
!      MENU in the file namelist.ini. (xxu, 3/17/11)
!  (2 ) Added the surface Jacobian variabes. (xxu, 5/17/11)
!******************************************************************************
!
      IMPLICIT NONE

      ! Make everything PRIVATE ...
      PRIVATE

      ! ... except these routines
      PUBLIC :: OUTPUT_DIAG
      PUBLIC :: CHECK
      PUBLIC :: NCIO_0D
      PUBLIC :: NCIO_1D
      PUBLIC :: NCIO_1DA
      PUBLIC :: NCIO_2D
      PUBLIC :: NCIO_3D

      ! ..., and these variables

      !=================================================================
      ! Module variables
      !=================================================================
      INTEGER :: FID
      INTEGER :: DIM01, DIM02, DIM03, DIM04, DIM05, DIM06 
      INTEGER :: DIM07, DIM08, DIM09, DIM10, DIM11, DIM12 
      INTEGER :: DIM13, DIM14, DIM15, DIM16, DIM17, DIM18
      INTEGER :: DIM19, DIM20, DIM21, DIM22, DIM23

      LOGICAL :: LDIAGINFO  = .TRUE.
      INTEGER, PARAMETER :: IU_DIAGINFO = 35

      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !================================================================= 
      CONTAINS 
 
!------------------------------------------------------------------------------
 
      SUBROUTINE OUTPUT_DIAG( IDLAMDA, LAMDA )

      ! References to F90 modules
      USE NETCDF
      USE NAMELIST_ARRAY_MOD, ONLY : DIR_OUTPUT
      USE NAMELIST_ARRAY_MOD, ONLY : DIAG_PREFIX
      USE NAMELIST_ARRAY_MOD, ONLY : LAEROSOL, LTRACEGAS, LRAYLEIGH
      USE NAMELIST_ARRAY_MOD, ONLY : LVLIDORT, LJACOB, LJACOB_SURF
      USE NAMELIST_ARRAY_MOD, ONLY : LDIAG01, LDIAG02, LDIAG03, LDIAG04
      USE NAMELIST_ARRAY_MOD, ONLY : LDIAG05, LDIAG06, LDIAG07, LDIAG08
      USE ERROR_MOD,          ONLY : ERROR_STOP
      USE TIME_MOD,           ONLY : SYSTEM_TIMESTAMP  

      ! Arguments
      INTEGER, INTENT(IN)   :: IDLAMDA
      REAL*8,  INTENT(IN)   :: LAMDA

      ! Local variables
      CHARACTER(LEN=255)    :: FILENAME
      CHARACTER(LEN=255)    :: DIAGINFO_FILE
      CHARACTER(LEN=4)      :: LABEL_LAMDA
      CHARACTER(LEN=16)     :: STAMP
      INTEGER               :: IOS

      ! Screen print
      WRITE(6,*) '-OUTPUT_DIAG: archiving the diagnostic variables'

      !================================================================
      ! OUTPUT_DIAG begins here!
      !================================================================

      ! Make the lamda label
      WRITE( LABEL_LAMDA,'(I4.4)') IDLAMDA
 
      ! Output filename
      FILENAME = TRIM(DIR_OUTPUT) //TRIM(DIAG_PREFIX) 
     &        // '_LAMDA.' // LABEL_LAMDA // '.nc'

      IF ( IDLAMDA == 1 ) THEN    
         DIAGINFO_FILE = TRIM(DIR_OUTPUT) // TRIM(DIAG_PREFIX)
     &           // '_LAMDA.' // LABEL_LAMDA // '.diaginfo'
         LDIAGINFO = .TRUE.
      ELSE IF ( IDLAMDA == 2 ) THEN
         DIAGINFO_FILE = TRIM(DIR_OUTPUT) // TRIM(DIAG_PREFIX) 
     &          // '.diaginfo'
      ELSE
         LDIAGINFO = .FALSE.
      ENDIF

      ! Create the netcdf file
      !CALL CHECK( NF90_CREATE( FILENAME, NF90_CLOBBER, FID ), 100 )
      CALL CHECK( NF90_CREATE( FILENAME, 0, FID ), 100 )

      ! Define the dimensions
      CALL DEFINE_DIMS

      ! End define mode.
      CALL CHECK ( NF90_ENDDEF(FID), 200 )

      !================================================================
      ! Save the variables to the NetCDF file
      !================================================================

      ! Write the information file: diaginfo.dat
      IF ( LDIAGINFO ) THEN

         ! Open "diaginfo.dat" file for output 
         OPEN( IU_DIAGINFO, FILE=DIAGINFO_FILE, IOSTAT=IOS )
         IF ( IOS /= 0 ) CALL ERROR_STOP("Open *.info",
     &                                   "diag_mod.f")
 
         ! Write file header
         WRITE( IU_DIAGINFO, '(A)' ) '#' // REPEAT( '=', 78 )
        
         ! Create a timestamp with the system date & time
         STAMP = SYSTEM_TIMESTAMP()
         WRITE( IU_DIAGINFO,  100  ) STAMP
         WRITE( IU_DIAGINFO,  105  ) TRIM(DIAG_PREFIX)
         WRITE( IU_DIAGINFO,  110  ) 
         WRITE( IU_DIAGINFO,  115  )
         WRITE( IU_DIAGINFO,  120  )
         WRITE( IU_DIAGINFO, '(a)' ) '# File Format:'
         WRITE( IU_DIAGINFO,  125  )
         WRITE( IU_DIAGINFO, '(A)' ) '#' // REPEAT( '=', 78 )

      ENDIF

      ! DIAG01: Model inputs
      IF ( LDIAG01 ) CALL SAVE_DIAG01( LAMDA )

      ! DIAG02: Atmospheric profiles 
      IF ( LDIAG02 ) CALL SAVE_DIAG02
 
      ! DIAG03: Mie properties and their linearization, aerosol loadings
      IF ( LDIAG03 .AND. LAEROSOL ) CALL SAVE_DIAG03

      ! DIAG04: Gas absorptions and Rayleigh scattering spectrum
      IF ( IDLAMDA > 1 ) LDIAG04 = .FALSE.
      IF ( LDIAG04 .AND. ( LTRACEGAS .OR. LRAYLEIGH ) ) CALL SAVE_DIAG04

      ! DIAG05: Atmospheric optical properties
      IF ( LDIAG05 ) CALL SAVE_DIAG05

      ! DIAG06: IOP
      IF ( LDIAG06 .AND. LVLIDORT ) CALL SAVE_DIAG06

      ! DIAG07: Radiances
      IF ( LDIAG07 .AND. LVLIDORT ) CALL SAVE_DIAG07

      ! DIAG08: Jacobians
      IF ( LDIAG08 .AND. LVLIDORT .AND. (LJACOB .OR. LJACOB_SURF) ) 
     &   CALL SAVE_DIAG08
 
      ! Close the file.
      CALL CHECK( NF90_CLOSE(FID), 1000 )

      ! CLose file and Reset LDIAGINFO so we do not write diaginfo.dat again
      IF ( LDIAGINFO ) THEN
         CLOSE( IU_DIAGINFO )
      ENDIF 

      ! Screen printing
      WRITE(6,'(A)') ' => results saved to ' // TRIM(FILENAME)

      ! Format strings
 100  FORMAT( '# diaginfo.dat: Created by ART-RTM at ', a,       /,'#' )
 105  FORMAT( '# ****** CUSTOMIZED FOR ', a, ' SIMULATION *****',/,'#' )
 110  FORMAT( '# This file contains names of the variables which are',
     &        ' stored in the     '                                    )
 115  FORMAT( '# output NetCDF file. This file contains their index,' 
     &        ' variable name,    '                                    )
 120  FORMAT( '# units, and longname.', / '#'                          )
 125  FORMAT( '# INDICES    (I4 ): index for each variable with',      
     &        ' OFFSET of 100 in order ', /, '#', 18x, 
     &        ' to distinguish diagnostic category.', / 
     &        '# --         (2X ): 2-character spacer', / 
     &        '# NAMES      (A18): Variable name.', /
     &        '# --         (2X ): 2-character spacer.', /
     &        '# UNITS      (A12): Variable name.', /
     &        '# --         (2X ): 2-character spacer.', /
     &        '# NAMES      (A40): Variable long-name.'                )
 
      ! Return to the calling routine
      END SUBROUTINE OUTPUT_DIAG

!------------------------------------------------------------------------------

      SUBROUTINE DEFINE_DIMS
!
!******************************************************************************
!  Subroutine DEFINE_DIMS define dimenison for the netCDF file:
!  (1 ) FID      ( INTEGER ) : Unit # of the netCDF file  
!  (2 ) DIM01    ( INTEGER ) : Dimenison ID for n_sza 
!  (3 ) DIM02    ( INTEGER ) : Dimenison ID for n_vza
!  (4 ) DIM03    ( INTEGER ) : Dimenison ID for n_raz
!  (5 ) DIM04    ( INTEGER ) : Dimenison ID for n_level
!  (6 ) DIM05    ( INTEGER ) : Dimenison ID for n_layer
!  (7 ) DIM06    ( INTEGER ) : Dimenison ID for maxmol
!  (8 ) DIM07    ( INTEGER ) : Dimenison ID for n_geom
!  (9 ) DIM08    ( INTEGER ) : Dimenison ID for n_lout
!  (10) DIM09    ( INTEGER ) : Dimenison ID for n_stok
!  (11) DIM10    ( INTEGER ) : Dimenison ID for n_jacb
!  (12) DIM11    ( INTEGER ) : Dimenison ID for n_gas
!  (13) DIM12    ( INTEGER ) : Dimenison ID for nval2
!  (14) DIM13    ( INTEGER ) : Dimenison ID for nval3
!  (15) DIM14    ( INTEGER ) : Dimenison ID for n_momt
!  (16) DIM15    ( INTEGER ) : Dimenison ID for len_gas
!  (17) DIM16    ( INTEGER ) : Dimenison ID for len_lpar
!  (18) DIM17    ( INTEGER ) : Dimenison ID for nval4
!  (19) DIM18    ( INTEGER ) : Dimenison ID for nval5
!  (20) DIM19    ( INTEGER ) : Dimenison ID for nval6
!  (12) DIM20    ( INTEGER ) : Dimenison ID for nangle
!  (13) DIM21    ( INTEGER ) : Dimenison ID for n_jacb_s
!******************************************************************************
!
      ! References to F90 modules
      USE NETCDF
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS,   N_MOMENTS,  NGAS
      USE NAMELIST_ARRAY_MOD, ONLY : N_THETA0,   N_THETA,    N_PHI
      USE NAMELIST_ARRAY_MOD, ONLY : N_GEOM,     NRECEP_LEV, N_STOKS
      USE NAMELIST_ARRAY_MOD, ONLY : NMODE
      USE ATMPROF_MOD,        ONLY : MAXMOL
      USE LPAR_MOD,           ONLY : N_LPAR_TOTAL, N_LPAR_SURF
      USE AEROSOL_MOD,        ONLY : NPANGLE
      USE NAMELIST_ARRAY_MOD, ONLY : LDIAG01, LDIAG02, LDIAG03, LDIAG04
      USE NAMELIST_ARRAY_MOD, ONLY : LDIAG05, LDIAG06, LDIAG07, LDIAG08
      USE NAMELIST_ARRAY_MOD, ONLY : LAEROSOL, LJACOB, LJACOB_SURF
      USE SPECTRA_MOD,        ONLY : NLAMDA

      !================================================================
      ! DEFINE_DIMS begins here!
      !================================================================

      CALL CHECK( NF90_DEF_DIM(FID,'n_sza',   N_THETA0,    DIM01), 001 )
      CALL CHECK( NF90_DEF_DIM(FID,'n_vza',   N_THETA,     DIM02), 002 )
      CALL CHECK( NF90_DEF_DIM(FID,'n_raz',   N_PHI,       DIM03), 003 )
      CALL CHECK( NF90_DEF_DIM(FID,'n_level', N_LAYERS+1,  DIM04), 004 )
      CALL CHECK( NF90_DEF_DIM(FID,'n_layer', N_LAYERS,    DIM05), 005 )
      CALL CHECK( NF90_DEF_DIM(FID,'maxmol',  MAXMOL,      DIM06), 006 )
      CALL CHECK( NF90_DEF_DIM(FID,'n_geom',  N_GEOM,      DIM07), 007 )
      CALL CHECK( NF90_DEF_DIM(FID,'n_lout',  NRECEP_LEV,  DIM08), 008 )
      CALL CHECK( NF90_DEF_DIM(FID,'n_stok',  N_STOKS,     DIM09), 009 )
      IF ( LJACOB ) 
     &CALL CHECK( NF90_DEF_DIM(FID,'n_jacb',  N_LPAR_TOTAL,DIM10), 010 )
      CALL CHECK( NF90_DEF_DIM(FID,'n_gas',   NGAS,        DIM11), 011 )
      CALL CHECK( NF90_DEF_DIM(FID,'nval2',   2,           DIM12), 012 )
      CALL CHECK( NF90_DEF_DIM(FID,'nval3',   3,           DIM13), 013 )
      CALL CHECK( NF90_DEF_DIM(FID,'n_momt',  N_MOMENTS,   DIM14), 014 )
      CALL CHECK( NF90_DEF_DIM(FID,'len_gas', 6,           DIM15), 015 )
      CALL CHECK( NF90_DEF_DIM(FID,'len_lpar',30,          DIM16), 016 )
      CALL CHECK( NF90_DEF_DIM(FID,'nval4',   4,           DIM17), 017 )
      CALL CHECK( NF90_DEF_DIM(FID,'nval5',   5,           DIM18), 018 )
      CALL CHECK( NF90_DEF_DIM(FID,'nval6',   6,           DIM19), 019 )
      IF ( LAEROSOL ) 
     &CALL CHECK( NF90_DEF_DIM(FID,'nangle',  NPANGLE,     DIM20), 020 )
      IF ( LJACOB .AND. LJACOB_SURF ) 
     &CALL CHECK( NF90_DEF_DIM(FID,'n_jacb_s',N_LPAR_SURF, DIM21), 021 )
      CALL CHECK( NF90_DEF_DIM(FID,'nmode',   NMODE,       DIM22), 022 )
      CALL CHECK( NF90_DEF_DIM(FID,'nlamda',  NLAMDA,      DIM23), 023 )
 
      ! Return to the calling routine
      END SUBROUTINE DEFINE_DIMS

!------------------------------------------------------------------------------

      SUBROUTINE SAVE_DIAG01( LAMDA )

      ! Subroutine SAVE_DIAG01 save the DIAG01 variables

      ! References to F90 modules
      USE NETCDF
      USE NAMELIST_ARRAY_MOD, ONLY : N_THETA0,   N_THETA,    N_PHI
      USE NAMELIST_ARRAY_MOD, ONLY : NRECEP_LEV, NGAS
      USE NAMELIST_ARRAY_MOD, ONLY : THETA0,     THETA,      PHI
      USE NAMELIST_ARRAY_MOD, ONLY : IATMOS,     SURF_REFL
      USE NAMELIST_ARRAY_MOD, ONLY : RECEP_LEV,  RECEP_DIR,  I_SCENARIO
      USE NAMELIST_ARRAY_MOD, ONLY : GASNAME,    GASMU,     COLUMN_MASS0
      USE NAMELIST_ARRAY_MOD, ONLY : COLUMN_AOD0,RMRI,       SIZERANGE
      USE NAMELIST_ARRAY_MOD, ONLY : IDIST,      DISTPAR,    L_REFER_AOD
      USE NAMELIST_ARRAY_MOD, ONLY : REFER_RMRI, REFER_LAMDA,REFER_IDIST
      USE NAMELIST_ARRAY_MOD, ONLY : REFER_SIZERANGE         
      USE NAMELIST_ARRAY_MOD, ONLY : REFER_DISTPAR
      USE NAMELIST_ARRAY_MOD, ONLY : NMODE,      MODEFRC
      USE NAMELIST_ARRAY_MOD, ONLY : MONOR,      REFER_MONOR
      USE NAMELIST_ARRAY_MOD, ONLY : IDPROF,     PROF_RANGE, PROF_PAR 
      USE NAMELIST_ARRAY_MOD, ONLY : SSMODEL,    AERDEN,     GAS_FWHM
      USE NAMELIST_ARRAY_MOD, ONLY : LAOD,       L_REFER_AOD
      USE NAMELIST_ARRAY_MOD, ONLY : GASSF

      ! ARgument
      REAL*8, INTENT(IN)    :: LAMDA

      ! Local parameters and variables
      INTEGER, PARAMETER    :: NDIAG = 32
      INTEGER               :: INDICES(NDIAG)
      CHARACTER(LEN=18)     :: NAMES(NDIAG)
      CHARACTER(LEN=40)     :: LNAMES(NDIAG)
      CHARACTER(LEN=12)     :: UNITS(NDIAG)
      INTEGER               :: I

      !================================================================
      ! SAVE_DIAG01 begins here!
      !===============================================================

      ! Define the variables for this diagnostic category
      DO I = 1, NDIAG
         INDICES(I) = I + 100
      ENDDO

      NAMES( 1)  = "IATM"
      NAMES( 2)  = "LAMDA"
      NAMES( 3)  = "SZA"
      NAMES( 4)  = "VZA"
      NAMES( 5)  = "RAZ"
      NAMES( 6)  = "R_SFC"
      NAMES( 7)  = "LOUTS"
      NAMES( 8)  = "DIR"
      NAMES( 9)  = "I_SCENARIO"
      NAMES(10)  = "GASNAMES"
      NAMES(11)  = "GASMU"
      NAMES(12)  = "FWHM"
      NAMES(13)  = "COLUMN_AOD0"
      NAMES(14)  = "COLUMN_MASS0"
      NAMES(15)  = "RMRI"
      NAMES(16)  = "SIZERANGE"
      NAMES(17)  = "IDIST"
      NAMES(18)  = "DISTPAR"
      NAMES(19)  = "MODEFRC"
      NAMES(20)  = "AERODEN"
      NAMES(21)  = "MONOR"
      NAMES(22)  = "SSMODEL"
      NAMES(23)  = "IDPROF"
      NAMES(24)  = "PROF_RANGE"
      NAMES(25)  = "PROF_PAR"
      NAMES(26)  = "REFER_LAMDA"
      NAMES(27)  = "REFER_RMRI"
      NAMES(28)  = "REFER_SIZERANGE"
      NAMES(29)  = "REFER_IDIST"
      NAMES(30)  = "REFER_DISTPAR"
      NAMES(31)  = "REFER_MONOR"
      NAMES(32)  = "GASSF"

      LNAMES( 1) = "Atmos Type"
      LNAMES( 2) = "Current Wavelength"
      LNAMES( 3) = "Solar Zenith Angle"
      LNAMES( 4) = "Viewer Zenith Angle"
      LNAMES( 5) = "Relative Azimuth Angle"
      LNAMES( 6) = "Surface Reflectance"
      LNAMES( 7) = "Recptor Levels"
      LNAMES( 8) = "Recptor Direction"
      LNAMES( 9) = "Index of SCC Scenario"
      LNAMES(10) = "Gas Names"
      LNAMES(11) = "Gas Molecular Weight"
      LNAMES(12) = "Full width of half maximum"
      LNAMES(13) = "Specified Column AOD"
      LNAMES(14) = "Specified Column Mass"
      LNAMES(15) = "Refractive Index"
      LNAMES(16) = "Size Range"
      LNAMES(17) = "Index of Size Dist Function"
      LNAMES(18) = "Distribution Parameters"
      LNAMES(19) = "Aerosol modal mass fraction"
      IF ( LAOD .AND. .NOT. L_REFER_AOD ) 
     & LNAMES(19) = "Aerosol modal AOD fraction"
      LNAMES(20) = "Aerosol density"
      LNAMES(21) = "Mono-disperse radius"
      LNAMES(22) = "Single scattering model"
      LNAMES(23) = "Index of vertical profile"
      LNAMES(24) = "Vertical range"
      LNAMES(25) = "Profile parameters"

      LNAMES(26) = "Referenced Wavelength"
      LNAMES(27) = "Referenced Refractive Index"
      LNAMES(28) = "Referenced Aerosol Size Range"
      LNAMES(29) = "Index Ref. Size Distribution"
      LNAMES(30) = "Ref. Distribution Parameters"
      LNAMES(31) = "Ref. Mono-disperse radius"
      LNAMES(32) = "Trace gas scalling factor"

      UNITS( :)  = "unitless"
      UNITS( 2)  = "nm"
      UNITS(3:5) = "degree"
      UNITS(11)  = "g mole^-1"
      UNITS(12)  = "nm"
      UNITS(14)  = "kg m^-2"
      UNITS(16)  = "um"
      UNITS(18)  = "um or none"
      UNITS(20)  = "kg m^-3"
      UNITS(21)  = "um"
      UNITS(24)  = "km"
      UNITS(25)  = "km^-1 or km" 
      UNITS(26)  = "nm"
      UNITS(28)  = "um"
      UNITS(30)  = "um or none"
      UNITS(31)  = "um"  


      ! Print to diaginfo.dat
      IF ( LDIAGINFO ) THEN
      DO I = 1, NDIAG
         WRITE(IU_DIAGINFO,100)INDICES(I), NAMES(I), UNITS(I), LNAMES(I)
      ENDDO
      ENDIF

      ! #1 Atmospheric profile type
      I = 1
      CALL NCIO_0D_INT (FID, IATMOS, NAMES(I), LNAMES(I), UNITS(I),
     &                  INDICES(I) )

      ! #2 Wavelength
      I = 2
      CALL NCIO_0D (FID, LAMDA, NAMES(I), LNAMES(I), UNITS(I),
     &                  INDICES(I) )

      ! #3 Solar zenith angle 
      I = 3
      CALL NCIO_1D ( FID, THETA0, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM01, N_THETA0, INDICES(I) )

      ! #4 Viewing zenith angle
      I = 4
      CALL NCIO_1D ( FID, THETA, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM02, N_THETA, INDICES(I) )

      ! #5 Relative azimuth angle
      I = 5
      CALL NCIO_1D ( FID, PHI, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM03, N_PHI, INDICES(I) )

      ! #6 Surface reflectance 
      I = 6 
      CALL NCIO_0D (FID, SURF_REFL, NAMES(I), LNAMES(I), UNITS(I),
     &              INDICES(I) )
 
      ! #7 Recptor levels
      I = 7 
      CALL NCIO_1D ( FID, RECEP_LEV(1:NRECEP_LEV), NAMES(I), LNAMES(I),
     &               UNITS(I), DIM08, NRECEP_LEV, INDICES(I) )

      ! #8 Recptor Direction (upward or downward)
      I = 8 
      CALL NCIO_0D_INT (FID, RECEP_DIR, NAMES(I), LNAMES(I), UNITS(I),
     &                  INDICES(I) )

      ! #9 Index of single scattering correction scenario for VLIDORT
      I = 9
      CALL NCIO_0D_INT (FID, I_SCENARIO, NAMES(I), LNAMES(I), UNITS(I),
     &                  INDICES(I) )

      ! #10 Gas Names
      I = 10
      CALL NCIO_1DA ( FID, GASNAME, NAMES(I), LNAMES(I), UNITS(I),
     &                DIM11, DIM15, NGAS, 6, INDICES(I) )

      ! #11 Gas molecular weight 
      I = 11
      CALL NCIO_1D ( FID, GASMU, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM11, NGAS, INDICES(I) )
     
      ! # 12 Full width of half maximum
      I = 12
      CALL NCIO_0D (FID, GAS_FWHM, NAMES(I), LNAMES(I), UNITS(I),
     &              INDICES(I) )

      ! # 13 Specified AOD
      I = 13
      CALL NCIO_0D (FID, COLUMN_AOD0, NAMES(I), LNAMES(I), UNITS(I),
     &              INDICES(I) ) 

      ! # 14 Fine-mode mass frc for Specified AOD
      I = 14
      CALL NCIO_0D (FID, COLUMN_MASS0, NAMES(I), LNAMES(I), UNITS(I),
     &              INDICES(I) )

      ! #15 Aerosol refractive indx
      I = 15 
      CALL NCIO_2D ( FID, RMRI(:,1:NMODE), 
     &               NAMES(I), LNAMES(I), UNITS(I),
     &               DIM12, DIM22, 2, NMODE, INDICES(I) )
   
      ! #16 Size range
      I = 16
      CALL NCIO_2D ( FID, SIZERANGE(:,1:NMODE),
     &               NAMES(I), LNAMES(I), UNITS(I), 
     &               DIM12, DIM22, 2, NMODE, INDICES(I) )
   
      ! #17 Size Distribution function
      I = 17  
      CALL NCIO_1D_INT (FID, IDIST(1:NMODE), 
     &                  NAMES(I), LNAMES(I), UNITS(I),
     &                  DIM22, NMODE, INDICES(I) )
     
      ! #18 Size Distribution parameters
      I = 18
      CALL NCIO_2D ( FID, DISTPAR(:,1:NMODE), 
     &               NAMES(I), LNAMES(I), UNITS(I),
     &               DIM13, DIM22, 3, NMODE, INDICES(I) )

      ! #19 Fractional modes
      I = 19
      CALL NCIO_1D ( FID, MODEFRC(1:NMODE),
     &               NAMES(I), LNAMES(I), UNITS(I),
     &               DIM22, NMODE, INDICES(I) )
   
      ! # 20 Aerosol density
      I = 20
      CALL NCIO_1D ( FID, AERDEN(1:NMODE),
     &               NAMES(I), LNAMES(I), UNITS(I),
     &               DIM22, NMODE, INDICES(I) ) 

      ! # 21 Mono-disperse radius
      I = 21
      CALL NCIO_1D ( FID, MONOR(1:NMODE),
     &               NAMES(I), LNAMES(I), UNITS(I),
     &               DIM22, NMODE, INDICES(I) )

      ! # 22 Single scattering model (MIE or T-Matrix)
      I = 22
      CALL NCIO_1D_INT (FID, SSMODEL(1:NMODE),
     &                  NAMES(I), LNAMES(I), UNITS(I),
     &                  DIM22, NMODE, INDICES(I) )

      ! # 23 Index of vertical profile
      I = 23
      CALL NCIO_1D_INT (FID, IDPROF(1:NMODE),
     &                  NAMES(I), LNAMES(I), UNITS(I),
     &                  DIM22, NMODE, INDICES(I) )

      ! # 24 Vertical range
      I = 24 
      CALL NCIO_2D ( FID, PROF_RANGE(:,1:NMODE),
     &               NAMES(I), LNAMES(I), UNITS(I),
     &               DIM12, DIM22, 2, NMODE, INDICES(I) )

      ! # 25 Profile parameters
      I = 25 
      CALL NCIO_2D ( FID, PROF_PAR(:,1:NMODE),
     &               NAMES(I), LNAMES(I), UNITS(I),
     &               DIM12, DIM22, 2, NMODE, INDICES(I) )

      IF ( L_REFER_AOD ) THEN

      ! #26 Referenced wavelength
      I = 26
      CALL NCIO_0D ( FID, REFER_LAMDA, NAMES(I), LNAMES(I), UNITS(I),
     &               INDICES(I) )

      ! #27 Referenced Aerosol refractive indx
      I = 27
      CALL NCIO_1D ( FID, REFER_RMRI, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM12, 2, INDICES(I) )

      ! #28 Referenced Size range
      I = 28
      CALL NCIO_1D ( FID, REFER_SIZERANGE, NAMES(I), LNAMES(I), 
     &               UNITS(I), DIM12, 2, INDICES(I) )

      ! #29 Size Distribution function
      I = 29
      CALL NCIO_0D_INT ( FID, REFER_IDIST, NAMES(I), LNAMES(I), 
     &                   UNITS(I), INDICES(I) )

      ! #30 Size Distribution parameters
      I = 30
      CALL NCIO_1D ( FID, REFER_DISTPAR, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM13, 3, INDICES(I) )

      ! # 31 Ref. Mono-disperse radius
      I = 31
      CALL NCIO_0D ( FID, REFER_MONOR, NAMES(I), LNAMES(I), UNITS(I),
     &               INDICES(I) )

      ! #32 Trace gas scalling factor
      I = 32
      CALL NCIO_1D ( FID, GASSF, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM11, NGAS, INDICES(I) )

      ENDIF

      ! Format strings
 100  FORMAT(I4,2x,A18,2x,A12,2x,A40)

      END SUBROUTINE SAVE_DIAG01

!------------------------------------------------------------------------------

      SUBROUTINE SAVE_DIAG02

      ! Subroutine SAVE_DIAG02 save the DIAG02 variables

      ! References to F90 modules
      USE NETCDF
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS, NGAS
      !USE ATMPROF_MOD,        ONLY : MAXMOL
      USE ATMPROF_MOD,        ONLY : ATMZ, ATMP, LAYT
      USE ATMPROF_MOD,        ONLY : LAYINT, LAYMOLS

      ! Local parameters and variables
      INTEGER, PARAMETER    :: NDIAG = 5
      INTEGER               :: INDICES(NDIAG)
      CHARACTER(LEN=18)     :: NAMES(NDIAG)
      CHARACTER(LEN=40)     :: LNAMES(NDIAG)
      CHARACTER(LEN=12)     :: UNITS(NDIAG)
      INTEGER               :: I

      !================================================================
      ! SAVE_DIAG02 begins here!
      !================================================================

      ! Define the variables for this diagnostic category
      DO I = 1, NDIAG
         INDICES(I) = I + 200
      ENDDO

      NAMES( 1) = "Z"
      NAMES( 2) = "P"
      NAMES( 3) = "T"
      NAMES( 4) = "LAYINT"
      NAMES( 5) = "LAYMOLS"

      LNAMES( 1) = "Level Height"
      LNAMES( 2) = "Level Pressure"
      LNAMES( 3) = "Layer Temperature"
      LNAMES( 4) = "Layer Air Number Density"
      LNAMES( 5) = "Air Molecular Mixing Ratio"

      UNITS( 1) = "km"
      UNITS( 2) = "hPa"
      UNITS( 3) = "K" 
      UNITS( 4) = "cm^-3"
      UNITS( 5) = "ppm"

      ! Print to diaginfo.dat
      IF ( LDIAGINFO ) THEN
      DO I = 1, NDIAG
         WRITE(IU_DIAGINFO,100)INDICES(I), NAMES(I), UNITS(I), LNAMES(I)
      ENDDO
      ENDIF

      ! #1 Level height
      CALL NCIO_1D ( FID, ATMZ(1:N_LAYERS+1),  NAMES(1), LNAMES(1),
     &               UNITS(1), DIM04, N_LAYERS+1, INDICES(1) )

      ! #2 Level pressure
      CALL NCIO_1D ( FID, ATMP(1:N_LAYERS+1), NAMES(2), LNAMES(2),
     &               UNITS(2), DIM04, N_LAYERS+1, INDICES(2) )

      ! #3 Layer temperature
      CALL NCIO_1D ( FID, LAYT(1:N_LAYERS), NAMES(3), LNAMES(3),
     &               UNITS(3), DIM05, N_LAYERS, INDICES(3) )

      ! #4 Layer air density
      CALL NCIO_1D ( FID, LAYINT(1:N_LAYERS), NAMES(4), LNAMES(4),
     &               UNITS(4), DIM05, N_LAYERS, INDICES(4) )

      ! #5 Air Molecular Number Fraction
      CALL NCIO_2D ( FID, LAYMOLS(1:N_LAYERS,1:NGAS), 
     &               NAMES(5), LNAMES(5), UNITS(5), 
     &               DIM05, DIM11, N_LAYERS, NGAS,
     &               INDICES(5) )

      ! Format strings
 100  FORMAT(I4,2x,A18,2x,A12,2x,A40)

      ! Return to the calling routine
      END SUBROUTINE SAVE_DIAG02

!------------------------------------------------------------------------------

      SUBROUTINE SAVE_DIAG03

      ! Subroutine SAVE_DIAG03 save the DIAG03 variables

      ! References to F90 modules
      USE NETCDF
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS
      USE NAMELIST_ARRAY_MOD, ONLY : N_MOMENTS,     NMODE
      USE AEROSOL_MOD,        ONLY : ASYMM,         BULK_AOP
      USE AEROSOL_MOD,        ONLY : EXPCOEFFS,     FMATRIX
      USE AEROSOL_MOD,        ONLY : DIST,          NPANGLE
      USE AEROSOL_MOD,        ONLY : LPSD_BULK,     LPSD_DIST
      USE AEROSOL_MOD,        ONLY : LRFE_BULK,     LFRC_BULK
      USE AEROSOL_MOD,        ONLY : LPSD_FMATRIX,  LRFE_FMATRIX
      USE AEROSOL_MOD,        ONLY : PROF_MASS,     PROF_AOD
      USE AEROSOL_MOD,        ONLY : PROF_NUM


      ! Local parameters and variables
      INTEGER, PARAMETER    :: NDIAG = 14
      INTEGER               :: INDICES(NDIAG)
      CHARACTER(LEN=18)     :: NAMES(NDIAG)
      CHARACTER(LEN=40)     :: LNAMES(NDIAG)
      CHARACTER(LEN=12)     :: UNITS(NDIAG)
      INTEGER               :: I

      !================================================================
      ! SAVE_DIAG03 begins here!
      !================================================================

      ! Define the variables for this diagnostic category
      DO I = 1, NDIAG
         INDICES(I) = I + 300
      ENDDO

      NAMES( 1) = "BULK_AOP"
      NAMES( 2) = "ASYMM"
      NAMES( 3) = "DIST"
      NAMES( 4) = "EXPCOEFFS"
      NAMES( 5) = "FMATRIX"
      NAMES( 6) = "LPSD_BULK"
      NAMES( 7) = "LPSD_DIST"
      NAMES( 8) = "LRFE_BULK"
      NAMES( 9) = "LFRC_BULK"
      NAMES(10) = "PROF_MASS"
      NAMES(11) = "PROF_AOD"
      NAMES(12) = "PROF_NUM"
      NAMES(13) = "LPSD_FMATRIX"
      NAMES(14) = "LRFE_FMATRIX"

      LNAMES( 1) = "Mie bulk parameters"
      LNAMES( 2) = "Asymmetric factor"
      LNAMES( 3) = "Distribution Parameters"
      LNAMES( 4) = "Greek Matrix Expansion Coefficients"
      LNAMES( 5) = "Scattering F-Matrix"
      LNAMES( 6) = "Linearized Mie Parameters wrt PSD"
      LNAMES( 7) = "Linearized Size Parameters wrt PSD"
      LNAMES( 8) = "Linearized Mie Parameters wrt RFE"
      LNAMES( 9) = "Linearized Mie Parameters wrt MFRC"
      LNAMES(10) = "Aerosol mass profile of each mode"
      LNAMES(11) = "Aerosol AOD profile of each mode"
      LNAMES(12) = "Aerosol number profile of each mode"
      LNAMES(13) = "Linearized F-Matrix wrt PSD"
      LNAMES(14) = "Linearized F-Matrix wrt RFE"

      UNITS( :) = "unitless"
      UNITS(10) = "kg m^-2"
      UNITS(12) = "m^-2"

      ! Print to diaginfo.dat
      IF ( LDIAGINFO ) THEN
      DO I = 1, NDIAG
         WRITE(IU_DIAGINFO,100)INDICES(I), NAMES(I), UNITS(I), LNAMES(I)
      ENDDO
      ENDIF

      ! #1 BULK_AOP(5,NMODE)
      I = 1
      CALL NCIO_2D ( FID, BULK_AOP, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM18, DIM22, 5, NMODE, INDICES(I) )

      ! #2 ASYMM(NMODE)
      I = 2
      CALL NCIO_1D ( FID, ASYMM, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM22, NMODE, INDICES(I) )
 
      ! #3 DIST(5,NMODE)
      I = 3
      CALL NCIO_2D ( FID, DIST, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM18, DIM22, 5, NMODE, INDICES(I) )

      ! #4 EXPCOEFFS(6,0:MAX_MIE_ANGLES,NMODE)
      I = 4
      CALL NCIO_3D ( FID, EXPCOEFFS(:,0:N_MOMENTS-1,:), 
     &               NAMES(I), LNAMES(I), UNITS(I),
     &               DIM19, DIM20, DIM14, 6, N_MOMENTS, NMODE,
     &               INDICES(I) ) 

      ! #5 FMATRIX(4,MAX_MIE_ANGLES,NMODE)
      I = 5
      CALL NCIO_3D ( FID, FMATRIX(:,1:NPANGLE,:),
     &               NAMES(I), LNAMES(I), UNITS(I),
     &               DIM19, DIM20, DIM22, 6, NPANGLE, NMODE, 
     &               INDICES(I) )

      ! #6 LPSD_BULK(5,3,NMODE)
      I = 6
      CALL NCIO_3D ( FID, LPSD_BULK, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM18, DIM13, DIM22, 5, 3, NMODE, INDICES(I) )

      ! #7 LPSD_DIST(5,3,NMODE)
      I = 7
      CALL NCIO_3D ( FID, LPSD_DIST, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM18, DIM13, DIM22, 5, 3, NMODE, INDICES(I) )


      ! #8 LRFE_BULK(5,3,NMODE)
      I = 8
      CALL NCIO_3D ( FID, LRFE_BULK, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM18, DIM13, DIM22, 5, 3, NMODE, INDICES(I) )
    
      ! #9 For multi-mode only
      IF ( NMODE >= 2 ) THEN

         ! LFRC_BULK(5,NMODE) 
         I = 9
         CALL NCIO_2D ( FID, LFRC_BULK, NAMES(I), LNAMES(I), UNITS(I),
     &                  DIM18, DIM22, 5, NMODE, INDICES(I) )

      ENDIF

      ! #10 Aerosol mass profile of each mode
      I = 10
      CALL NCIO_2D ( FID, PROF_MASS, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM05, DIM12, N_LAYERS, NMODE, INDICES(I) ) 

      ! #11 Aerosol AOD profile of each mode
      I = 11
      CALL NCIO_2D ( FID, PROF_AOD, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM05, DIM12, N_LAYERS, NMODE, INDICES(I) )

      ! #12 Aerosol number profile of each mode 
      I = 12
      CALL NCIO_2D ( FID, PROF_NUM, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM05, DIM12, N_LAYERS, NMODE, INDICES(I) )

      ! #13
      I = 13
      CALL NCIO_4D ( FID, LPSD_FMATRIX,
     &               NAMES(I), LNAMES(I), UNITS(I),
     &               DIM19, DIM20, DIM13, DIM22,
     &               6, NPANGLE, 3, NMODE, INDICES(I) )

      ! #14
      I = 14
      CALL NCIO_4D ( FID, LRFE_FMATRIX,
     &               NAMES(I), LNAMES(I), UNITS(I),
     &               DIM19, DIM20, DIM13, DIM22,
     &               6, NPANGLE, 3, NMODE, INDICES(I) )

      ! Format strings
 100  FORMAT(I4,2x,A18,2x,A12,2x,A40)

      ! Return to the calling routine
      END SUBROUTINE SAVE_DIAG03

!------------------------------------------------------------------------------

      SUBROUTINE SAVE_DIAG04

      ! Subroutine SAVE_DIAG04 save the DIAG04 variables

      ! References to F90 modules
      USE NETCDF
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS,      NGAS
      USE NAMELIST_ARRAY_MOD, ONLY : LTRACEGAS,     LRAYLEIGH
      USE SPECTRA_MOD,        ONLY : NLAMDA,        LAMDAS
      USE GAS_MOD,            ONLY : XSEC_SAO,      XSEC_HIT
      USE GAS_MOD,            ONLY : XABS_SAO,      XABS_HIT
      USE GAS_MOD,            ONLY : GAS_XSEC,      GAS_XABS
      USE RAYLEIGH_MOD,       ONLY : RAYLEIGH_XSCA, RAYLEIGH_PHAS

      ! Local parameters and variables
      INTEGER, PARAMETER    :: NDIAG = 9
      INTEGER               :: INDICES(NDIAG)
      CHARACTER(LEN=18)     :: NAMES(NDIAG)
      CHARACTER(LEN=40)     :: LNAMES(NDIAG)
      CHARACTER(LEN=12)     :: UNITS(NDIAG)
      INTEGER               :: I

      !================================================================
      ! SAVE_DIAG04 begins here!
      !================================================================

      ! Define the variables for this diagnostic category
      INDICES = (/1, 2, 3, 4, 5, 6, 7, 8, 9/) + 400

      NAMES   = (/ "LAMDAS", 
     &             "XSEC_SAO", 
     &             "XSEC_HIT",
     &             "GAS_XSEC",
     &             "XABS_SAO", 
     &             "XABS_HIT",
     &             "GAS_XABS",
     &             "RAYLEIGH_XSCA",
     &             "RAYLEIGH_PHAS" /)

      LNAMES  = (/ "Spectral wavelenth",
     &             "Absorption Xsec by SAO-Xsec",
     &             "Absorption Xsec by HITRAN",
     &             "Combined Gas Absorption Xsec", 
     &             "Absorption OD by SAO-Xsec",
     &             "Absorption OD by HITRAN",
     &             "Combined Gas Absorption OD", 
     &             "Rayleigh SCA OD",
     &             "Rayleigh phase function" /)

      UNITS(1)   = "nm"
      UNITS(2:4) = "cm^2/molec"
      UNITS(5:9) = "unitless"

      ! Print to diaginfo.dat
      IF ( LDIAGINFO ) THEN
      DO I = 1, NDIAG
         WRITE(IU_DIAGINFO,100)INDICES(I), NAMES(I), UNITS(I), LNAMES(I)
      ENDDO
      ENDIF

      ! LAMDAS
      CALL NCIO_1D ( FID, LAMDAS, NAMES(1), LNAMES(1), UNITS(1),
     &               DIM23, NLAMDA, INDICES(1) ) 

      ! Only do the output if the Trace gas calculation is turned on
      IF ( LTRACEGAS ) THEN

         ! XSEC_SAO(NLAMDA,N_LAYERS,NGAS)
         CALL NCIO_3D ( FID, XSEC_SAO, NAMES(2), LNAMES(2), UNITS(2),
     &                  DIM23, DIM05, DIM11, 
     &                  NLAMDA, N_LAYERS, NGAS, INDICES(2) )

         ! XSEC_HIT(NLAMDA,N_LAYERS,NGAS)
         CALL NCIO_3D ( FID, XSEC_HIT, NAMES(3), LNAMES(3), UNITS(3),
     &                  DIM23, DIM05, DIM11, 
     &                  NLAMDA, N_LAYERS, NGAS, INDICES(3) )

         ! GAS_XSEC(NLAMDA,N_LAYERS,NGAS)
         CALL NCIO_3D ( FID, GAS_XSEC, NAMES(4), LNAMES(4), UNITS(4),
     &                  DIM23, DIM05, DIM11,
     &                  NLAMDA, N_LAYERS, NGAS, INDICES(4) )

         ! XABS_SAO(NLAMDA,N_LAYERS,NGAS)
         CALL NCIO_3D ( FID, XABS_SAO, NAMES(5), LNAMES(5), UNITS(5),
     &                  DIM23, DIM05, DIM11,
     &                  NLAMDA, N_LAYERS, NGAS, INDICES(5) )

         ! XABS_HIT(NLAMDA,N_LAYERS,NGAS)
         CALL NCIO_3D ( FID, XABS_HIT, NAMES(6), LNAMES(6), UNITS(6),
     &                  DIM23, DIM05, DIM11,
     &                  NLAMDA, N_LAYERS, NGAS, INDICES(6) )

         ! GAS_XABS(NLAMDA,N_LAYERS,NGAS)
         CALL NCIO_3D ( FID, GAS_XABS, NAMES(7), LNAMES(7), UNITS(7),
     &                  DIM23, DIM05, DIM11,
     &                  NLAMDA, N_LAYERS, NGAS, INDICES(8) )
 
      ENDIF

      ! Only do the output if the Rayleigh calculation is turned on
      IF ( LRAYLEIGH ) THEN

         ! RAYLEIGH_XSCA(NLAMDA,N_LAYERS)
         CALL NCIO_2D ( FID, RAYLEIGH_XSCA, 
     &                  NAMES(8), LNAMES(8), UNITS(8),
     &                  DIM23, DIM05,
     &                  NLAMDA, N_LAYERS, INDICES(8) )

         ! RAYLEIGH_PHAS(NLAMDA,0:2,6)
         CALL NCIO_3D ( FID, RAYLEIGH_PHAS, 
     &                  NAMES(9), LNAMES(9), UNITS(9),
     &                  DIM23, DIM13, DIM19, 
     &                  NLAMDA, 3, 6, INDICES(9) )  

      ENDIF

      ! Format strings
 100  FORMAT(I4,2x,A18,2x,A12,2x,A40)

      END SUBROUTINE SAVE_DIAG04

!------------------------------------------------------------------------------

      SUBROUTINE SAVE_DIAG05

      ! Subroutine SAVE_DIAG05 save the DIAG05 variables

      ! References to F90 modules
      USE NETCDF
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS, NMODE, NGAS
      USE NAMELIST_ARRAY_MOD, ONLY : N_THETA, N_THETA0, N_PHI
      USE NAMELIST_ARRAY_MOD, ONLY : N_STOKS, LBRDF
      USE AIROPTIC_MOD,       ONLY : TAUAER, TAURAY, TAUGAS, SSAAER
      USE AIROPTIC_MOD,       ONLY : TAUGAS_A
      USE BRDF_MOD,           ONLY : BRDFVAL

      ! Local parameters and variables
      INTEGER, PARAMETER    :: NDIAG = 6
      INTEGER               :: INDICES(NDIAG)
      CHARACTER(LEN=18)     :: NAMES(NDIAG)
      CHARACTER(LEN=40)     :: LNAMES(NDIAG)
      CHARACTER(LEN=12)     :: UNITS(NDIAG)
      INTEGER               :: I

      !================================================================
      ! SAVE_DIAG05 begins here!
      !================================================================

      ! Define the variables for this diagnostic category
      DO I = 1, NDIAG
         INDICES(I) = I + 500
         UNITS(I)   = "unitless"
      ENDDO

      NAMES(1)  = "TAUAER"
      NAMES(2)  = "TAURAY"
      NAMES(3)  = "TAUGAS"
      NAMES(4)  = "SSAAER"
      NAMES(5)  = "TAUGAS_A"
      NAMES(6)  = "BRDFVAL"

      LNAMES(1) = "Aerosol optical depth"
      LNAMES(2) = "Rayleigh scattering OD"
      LNAMES(3) = "Gas optical depth"
      LNAMES(4) = "Aerosol single scattering albedo"
      LNAMES(5) = "Gas Absorption OD"    
      LNAMES(6) = "Exact direct BRDF"

      ! Print to diaginfo.dat
      IF ( LDIAGINFO ) THEN
      DO I = 1, NDIAG
         WRITE(IU_DIAGINFO,100)INDICES(I), NAMES(I), UNITS(I), LNAMES(I)
      ENDDO
      ENDIF

      ! Layer AOT
      I = 1
      CALL NCIO_2D ( FID, TAUAER, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM05, DIM22, N_LAYERS, NMODE, INDICES(I) )

      ! Layer Rayleigh scattering
      I = 2
      CALL NCIO_1D ( FID, TAURAY, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM05, N_LAYERS, INDICES(I) )

      ! Layer Gas Absorption
      I = 3
      CALL NCIO_1D ( FID, TAUGAS, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM05, N_LAYERS, INDICES(I) )

      ! Layer aerosol single scattering albedo (SSA)
      I = 4
      CALL NCIO_2D ( FID, SSAAER, NAMES(I), LNAMES(I), UNITS(I),
     &               DIM05, DIM22, N_LAYERS, NMODE, INDICES(I) )

      ! Trace gas absorption opcatial depth for all gases
      I = 5
      CALL NCIO_2D ( FID, TAUGAS_A(1:N_LAYERS,1:NGAS),
     &               NAMES(I), LNAMES(I), UNITS(I),
     &               DIM05, DIM11, N_LAYERS, NGAS, INDICES(I) )
 
      ! Exact direct BRDF calculated by VBRDF_SUP
      ! BRDFVAL(N_STOKS,N_THETA,N_PHI,N_THETA0)
      I = 6
      IF ( LBRDF ) THEN 
         CALL NCIO_4D ( FID, BRDFVAL,
     &                  NAMES(I), LNAMES(I), UNITS(I),
     &                  DIM09, DIM02, DIM03, DIM01,
     &                  N_STOKS, N_THETA, N_PHI, N_THETA0, INDICES(I) ) 
      ENDIF

      ! Format string
 100  FORMAT(I4,2x,A18,2x,A12,2x,A40)

      END SUBROUTINE SAVE_DIAG05

!------------------------------------------------------------------------------

      SUBROUTINE SAVE_DIAG06

      ! Subroutine SAVE_DIAG06 save the DIAG06 variables

      ! References to F90 modules
      USE NETCDF
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB
      USE NAMELIST_ARRAY_MOD, ONLY : N_STOKS,  N_LAYERS, N_MOMENTS
      USE IOP_MOD,            ONLY : IOP_TAU,  IOP_SSA,  IOP_PHA
      USE IOP_MOD,            ONLY : LIOP_TAU, LIOP_SSA, LIOP_PHA
      USE LPAR_MOD,           ONLY : N_LPAR_TOTAL

      ! Local Variables
      INTEGER               :: ISS, DIMISS

      INTEGER, PARAMETER    :: NDIAG = 4
      INTEGER               :: INDICES(NDIAG)
      CHARACTER(LEN=18)     :: NAMES(NDIAG)
      CHARACTER(LEN=40)     :: LNAMES(NDIAG)
      CHARACTER(LEN=12)     :: UNITS(NDIAG)
      INTEGER               :: I

      !================================================================
      ! SAVE_DIAG06 begins here!
      !================================================================

      ! Define the variables for this diagnostic category
      DO I = 1, NDIAG
         INDICES(I) = I + 600
         UNITS(I)   = "unitless"
      ENDDO

      NAMES(1)  = "IOP_TAU"
      NAMES(2)  = "IOP_SSA"
      NAMES(3)  = "LIOP_TAU"
      NAMES(4)  = "LIOP_SSA"

      LNAMES(1) = "Ensemble Optical Depth"
      LNAMES(2) = "Ensemble SSA"
      LNAMES(3) = "Linearied Ensemble Optical Depth"
      LNAMES(4) = "Linearied Ensemble SSA"

      ! Print to diaginfo.dat
      IF ( LDIAGINFO ) THEN
      DO I = 1, NDIAG
         WRITE(IU_DIAGINFO,100)INDICES(I), NAMES(I), UNITS(I), LNAMES(I)
      ENDDO
      ENDIF

      !Controls number of non-zero entries in the Greek matrix
      ISS = 1
      IF ( N_STOKS > 1 ) ISS = 8

      ! IOP_TAU
      CALL NCIO_1D ( FID, IOP_TAU, NAMES(1), LNAMES(1), UNITS(1),
     &               DIM05, N_LAYERS, INDICES(1) )

      ! IOP_SSA
      CALL NCIO_1D ( FID, IOP_SSA, NAMES(2), LNAMES(2), UNITS(2),
     &               DIM05, N_LAYERS, INDICES(2) )

      !================================================================
      ! Linearized IOPs
      !================================================================
      IF ( LJACOB ) THEN

         ! LIOP_TAU
         CALL NCIO_2D ( FID, LIOP_TAU, NAMES(3), LNAMES(3), UNITS(3),
     &               DIM10, DIM05, N_LPAR_TOTAL, N_LAYERS, INDICES(3) )

         ! LIOP_SSA
         CALL NCIO_2D ( FID, LIOP_SSA, NAMES(4), LNAMES(4), UNITS(4),
     &               DIM10, DIM05, N_LPAR_TOTAL, N_LAYERS, INDICES(4) )

      ENDIF
      
      ! Format strings
 100  FORMAT(I4,2x,A18,2x,A12,2x,A40)

      END SUBROUTINE SAVE_DIAG06

!------------------------------------------------------------------------------

      SUBROUTINE SAVE_DIAG07

      ! Subroutine SAVE_DIAG07 save the DIAG07 variables

      ! References to F90 modules
      USE NETCDF
      USE NAMELIST_ARRAY_MOD, ONLY : N_STOKS, NRECEP_LEV, N_GEOM
      USE VLIDORT_ARRAY_MOD,  ONLY : STOKES_COMPS 

      ! Local vriables 
      INTEGER, PARAMETER    :: NDIAG = 1
      INTEGER               :: INDICES(NDIAG)
      CHARACTER(LEN=18)     :: NAMES(NDIAG)
      CHARACTER(LEN=40)     :: LNAMES(NDIAG)
      CHARACTER(LEN=12)     :: UNITS(NDIAG)
      INTEGER               :: I

      !================================================================
      ! SAVE_DIAG07 begins here!
      !================================================================

      ! Define the variables for this diagnostic category
      DO I = 1, NDIAG
         INDICES(I) = I + 700
         UNITS(I)   = "unitless"
      ENDDO

      NAMES(1)  = "STOKES"
      LNAMES(1) = "Stokes Vector"

      ! Print to diaginfo.dat
      IF ( LDIAGINFO ) THEN
      DO I = 1, NDIAG
         WRITE(IU_DIAGINFO,100)INDICES(I), NAMES(I), UNITS(I), LNAMES(I)
      ENDDO
      ENDIF
          
      ! Stokes vector
      CALL NCIO_3D ( FID, STOKES_COMPS, NAMES(1), LNAMES(1), UNITS(1),
     &               DIM08, DIM07, DIM09,
     &               NRECEP_LEV, N_GEOM, N_STOKS, INDICES(1) )

      ! Format strings
 100  FORMAT(I4,2x,A18,2x,A12,2x,A40)

      END SUBROUTINE SAVE_DIAG07

!------------------------------------------------------------------------------

      SUBROUTINE SAVE_DIAG08

      ! Subroutine SAVE_DIAG08 save the DIAG08 variables

      ! References to F90 modules
      USE NETCDF
      USE NAMELIST_ARRAY_MOD, ONLY : N_STOKS,      NRECEP_LEV 
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS,     N_GEOM
      USE NAMELIST_ARRAY_MOD, ONLY : LJACOB,       LJACOB_SURF
      USE LPAR_MOD,           ONLY : N_LPAR_TOTAL, LPAR_NAMES
      USE LPAR_MOD,           ONLY : N_LPAR_SURF
      USE VLIDORT_ARRAY_MOD,  ONLY : JACKS_PROFLS, JACKS_COLUMN
      USE VLIDORT_ARRAY_MOD,  ONLY : JACKS_SURF
      USE IOP_MOD,            ONLY : TAUAER_WFS

      ! Local vriables 
      INTEGER, PARAMETER    :: NDIAG = 5
      INTEGER               :: INDICES(NDIAG)
      CHARACTER(LEN=18)     :: NAMES(NDIAG)
      CHARACTER(LEN=40)     :: LNAMES(NDIAG)
      CHARACTER(LEN=12)     :: UNITS(NDIAG)
      INTEGER               :: I

      !================================================================
      ! SAVE_DIAG08 begins here!
      !================================================================

      ! Define the variables for this diagnostic category
      DO I = 1, NDIAG
         INDICES(I) = I + 800
         UNITS(I)   = "unitless"
      ENDDO

      NAMES(1)  = "LPAR_NAMES"
      NAMES(2)  = "JCOBIANS_COLUMN"
      NAMES(3)  = "JCOBIANS"
      NAMES(4)  = "JCOBIANS_SURF"
      NAMES(5)  = "TAUAER_WFS"

      LNAMES(1) = "Jacobian Names"
      LNAMES(2) = "Column Jacobians"
      LNAMES(3) = "Profile Jacobians"
      LNAMES(4) = "Surface Jacobians"
      LNAMES(5) = "AOD Weighting functions"

      ! Print to diaginfo.dat
      IF ( LDIAGINFO ) THEN
      DO I = 1, NDIAG
         WRITE(IU_DIAGINFO,100)INDICES(I), NAMES(I), UNITS(I), LNAMES(I)
      ENDDO
      ENDIF

      ! Atmospheric Jacobians
      IF (LJACOB) THEN

         ! Jacobian parameter names
         I = 1
         CALL NCIO_1DA ( FID, LPAR_NAMES, NAMES(I), LNAMES(I), UNITS(I),
     &                  DIM10, DIM16, N_LPAR_TOTAL, 30, INDICES(I) )

         ! Column Jacobians 
         I = 2
         CALL NCIO_4D ( FID, JACKS_COLUMN,
     &                  NAMES(I), LNAMES(I), UNITS(I),
     &                  DIM10, DIM08, DIM07, DIM09, 
     &                  N_LPAR_TOTAL, NRECEP_LEV,
     &                  N_GEOM, N_STOKS, INDICES(I) )

         ! Profule Jacobians 
         I = 3
         CALL NCIO_5D ( FID, JACKS_PROFLS,
     &                  NAMES(I), LNAMES(I), UNITS(I),
     &                  DIM10, DIM05, DIM08, DIM07, DIM09,
     &                  N_LPAR_TOTAL, N_LAYERS, NRECEP_LEV,
     &                  N_GEOM, N_STOKS, INDICES(I) )
      ENDIF

      ! Surface Jacobians
      IF ( LJACOB_SURF .AND. LJACOB ) THEN

         I = 4
         CALL NCIO_4D ( FID, JACKS_SURF, NAMES(I), LNAMES(I), UNITS(I),
     &                  DIM21, DIM08, DIM07, DIM09,
     &                  N_LPAR_SURF, NRECEP_LEV,
     &                  N_GEOM, N_STOKS, INDICES(I) )

      ENDIF


      ! AOD Jacobians with respect to aerosol parameters
      IF (LJACOB) THEN
     
         I = 5
         CALL NCIO_2D ( FID, TAUAER_WFS, NAMES(I), LNAMES(I), UNITS(I),
     &                  DIM10, DIM05, 
     &                  N_LPAR_TOTAL, N_LAYERS, INDICES(I) )

      ENDIF

      ! Format strings
 100  FORMAT(I4,2x,A18,2x,A12,2x,A40)

      ! Return to the calling routine
      END SUBROUTINE SAVE_DIAG08

!------------------------------------------------------------------------------

      SUBROUTINE NCIO_0D(NCID, VAR0D, VARNAME, LONGNAME, VARUNIT, INDEX)

      ! References to F90 modules
      USE NETCDF
 
      INTEGER, INTENT(IN)          :: NCID, INDEX
      REAL*8,  INTENT(IN)          :: VAR0D
      CHARACTER(LEN=*), INTENT(IN) :: VARNAME, LONGNAME, VARUNIT

      ! Local variables
      INTEGER                      :: VAR_ID

      CALL CHECK( NF90_REDEF(NCID), INDEX )

      CALL CHECK( NF90_DEF_VAR(NCID, TRIM(VARNAME), NF90_DOUBLE, VAR_ID)
     &            , INDEX ) 
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"name",TRIM(LONGNAME)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"units",TRIM(VARUNIT)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"index",INDEX), INDEX )
      CALL CHECK( NF90_ENDDEF(NCID), INDEX )
      CALL CHECK( NF90_PUT_VAR(NCID, VAR_ID, VAR0D ), INDEX )

      END SUBROUTINE NCIO_0D

!------------------------------------------------------------------------------

      SUBROUTINE NCIO_0D_INT (NCID, VAR0D, VARNAME, LONGNAME, 
     &                        VARUNIT, INDEX)

      ! References to F90 modules
      USE NETCDF

      INTEGER, INTENT(IN)          :: NCID, INDEX
      INTEGER, INTENT(IN)          :: VAR0D
      CHARACTER(LEN=*), INTENT(IN) :: VARNAME, LONGNAME, VARUNIT

      ! Local variables
      INTEGER                      :: VAR_ID

      CALL CHECK( NF90_REDEF(NCID), INDEX )

      CALL CHECK( NF90_DEF_VAR(NCID, TRIM(VARNAME), NF90_INT, VAR_ID)
     &            , INDEX )
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"name",TRIM(LONGNAME)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"units",TRIM(VARUNIT)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"index",INDEX), INDEX )
      CALL CHECK( NF90_ENDDEF(NCID), INDEX )
      CALL CHECK( NF90_PUT_VAR(NCID, VAR_ID, VAR0D ), INDEX )

      END SUBROUTINE NCIO_0D_INT

!------------------------------------------------------------------------------

      SUBROUTINE NCIO_1D (NCID, VAR1D, VARNAME, LONGNAME, VARUNIT,
     &                    DIMID, DIMV, INDEX)

      ! References to F90 modules
      USE NETCDF

      ! Arguments
      INTEGER, INTENT(IN)          :: NCID, DIMID, DIMV, INDEX
      REAL*8,  INTENT(IN)          :: VAR1D(DIMV)
      CHARACTER(LEN=*), INTENT(IN) :: VARNAME, LONGNAME, VARUNIT

      ! Local variable
      INTEGER                     :: DIMS(1) 
      INTEGER                     :: VAR_ID
 
      CALL CHECK( NF90_REDEF(NCID), INDEX )     

      DIMS(1) = DIMID
      CALL CHECK( NF90_DEF_VAR(NCID, TRIM(VARNAME),
     &            NF90_DOUBLE, DIMS, VAR_ID), INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"name",TRIM(LONGNAME)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"units",TRIM(VARUNIT)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"index",INDEX), INDEX )
      CALL CHECK( NF90_ENDDEF(NCID), INDEX )
      CALL CHECK( NF90_PUT_VAR(NCID, VAR_ID, VAR1D ), INDEX )

      END SUBROUTINE NCIO_1D

!------------------------------------------------------------------------------

      SUBROUTINE NCIO_1DA (NCID, VAR1D, VARNAME, LONGNAME, VARUNIT,
     &                    DIMID, LENID, DIMV, VARLEN, INDEX)

      ! References to F90 modules
      USE NETCDF

      ! Arguments
      INTEGER, INTENT(IN)          :: NCID, DIMID, DIMV, INDEX
      INTEGER, INTENT(IN)          :: LENID, VARLEN
      CHARACTER(LEN=*), INTENT(IN) :: VAR1D(DIMV)
      CHARACTER(LEN=*), INTENT(IN) :: VARNAME, LONGNAME, VARUNIT

      ! Local variable
      INTEGER                     :: DIMS(2)
      INTEGER                     :: VAR_ID

      CALL CHECK( NF90_REDEF(NCID), INDEX )

      DIMS(1) = LENID
      DIMS(2) = DIMID
      CALL CHECK( NF90_DEF_VAR(NCID, TRIM(VARNAME),
     &            NF90_CHAR, DIMS, VAR_ID), INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"name",TRIM(LONGNAME)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"units",TRIM(VARUNIT)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"index",INDEX), INDEX )
      CALL CHECK( NF90_ENDDEF(NCID), INDEX )
      CALL CHECK( NF90_PUT_VAR(NCID, VAR_ID, VAR1D, START=(/1,1/), 
     &            COUNT=(/VARLEN, DIMV/)), INDEX )

      END SUBROUTINE NCIO_1DA

!------------------------------------------------------------------------------

      SUBROUTINE NCIO_1D_INT (NCID, VAR1D, VARNAME, LONGNAME, VARUNIT,
     &                        DIMID, DIMV, INDEX)

      ! References to F90 modules
      USE NETCDF

      ! Arguments
      INTEGER, INTENT(IN)          :: NCID, DIMID, DIMV, INDEX
      INTEGER, INTENT(IN)          :: VAR1D(DIMV)
      CHARACTER(LEN=*), INTENT(IN) :: VARNAME, LONGNAME, VARUNIT

      ! Local variable            
      INTEGER                     :: DIMS(1)
      INTEGER                     :: VAR_ID

      CALL CHECK( NF90_REDEF(NCID), INDEX )

      DIMS(1) = DIMID
      CALL CHECK( NF90_DEF_VAR(NCID, TRIM(VARNAME),
     &            NF90_INT, DIMS, VAR_ID), INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"name",TRIM(LONGNAME)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"units",TRIM(VARUNIT)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"index",INDEX), INDEX )
      CALL CHECK( NF90_ENDDEF(NCID), INDEX )
      CALL CHECK( NF90_PUT_VAR(NCID, VAR_ID, VAR1D ), INDEX )

      END SUBROUTINE NCIO_1D_INT

!------------------------------------------------------------------------------

      SUBROUTINE NCIO_2D (NCID, VAR2D, VARNAME, LONGNAME, VARUNIT,
     &                    DIMID01, DIMID02, 
     &                    DIMV01,  DIMV02,  INDEX)

      ! References to F90 modules
      USE NETCDF

      ! Arguments
      INTEGER, INTENT(IN)          :: NCID, DIMID01, DIMID02
      INTEGER, INTENT(IN)          :: DIMV01, DIMV02, INDEX
      REAL*8,  INTENT(IN)          :: VAR2D(DIMV01,DIMV02)
      CHARACTER(LEN=*), INTENT(IN) :: VARNAME, LONGNAME, VARUNIT

      ! Local variable
      INTEGER                     :: DIMS(2)
      INTEGER                     :: VAR_ID

      CALL CHECK( NF90_REDEF(NCID), INDEX )

      DIMS(1) = DIMID01
      DIMS(2) = DIMID02
      
      CALL CHECK( NF90_DEF_VAR(NCID, TRIM(VARNAME),
     &            NF90_DOUBLE, DIMS, VAR_ID), INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"name",TRIM(LONGNAME)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"units",TRIM(VARUNIT)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"index",INDEX), INDEX )
      CALL CHECK( NF90_ENDDEF(NCID), INDEX )
      CALL CHECK( NF90_PUT_VAR(NCID, VAR_ID, VAR2D ), INDEX )

      END SUBROUTINE NCIO_2D

!------------------------------------------------------------------------------

      SUBROUTINE NCIO_3D (NCID, VAR, VARNAME, LONGNAME, VARUNIT,
     &                    DIMID01, DIMID02, DIMID03,
     &                    DIMV01,  DIMV02,  DIMV03, INDEX)

      ! References to F90 modules
      USE NETCDF

      ! Arguments
      INTEGER, INTENT(IN)          :: NCID, INDEX
      INTEGER, INTENT(IN)          :: DIMID01, DIMID02, DIMID03
      INTEGER, INTENT(IN)          :: DIMV01,  DIMV02,  DIMV03
      REAL*8,  INTENT(IN)          :: VAR(DIMV01,DIMV02,DIMV03)
      CHARACTER(LEN=*), INTENT(IN) :: VARNAME, LONGNAME, VARUNIT

      ! Local variable
      INTEGER                     :: DIMS(3)
      INTEGER                     :: VAR_ID

      CALL CHECK( NF90_REDEF(NCID), INDEX )

      DIMS(1) = DIMID01
      DIMS(2) = DIMID02
      DIMS(3) = DIMID03

      CALL CHECK( NF90_DEF_VAR(NCID, TRIM(VARNAME),
     &            NF90_DOUBLE, DIMS, VAR_ID), INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"name",TRIM(LONGNAME)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"units",TRIM(VARUNIT)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"index",INDEX), INDEX )
      CALL CHECK( NF90_ENDDEF(NCID), INDEX )
      CALL CHECK( NF90_PUT_VAR(NCID, VAR_ID, VAR), INDEX )

      END SUBROUTINE NCIO_3D

!------------------------------------------------------------------------------

      SUBROUTINE NCIO_4D (NCID, VAR, VARNAME, LONGNAME, VARUNIT,
     &                    DIMID01, DIMID02, DIMID03, DIMID04, 
     &                    DIMV01,  DIMV02,  DIMV03,  DIMV04,  INDEX)

      ! References to F90 modules
      USE NETCDF

      ! Arguments
      INTEGER, INTENT(IN)          :: NCID, INDEX
      INTEGER, INTENT(IN)          :: DIMID01, DIMID02, DIMID03, DIMID04
      INTEGER, INTENT(IN)          :: DIMV01,  DIMV02,  DIMV03,  DIMV04
      REAL*8,  INTENT(IN)          :: VAR(DIMV01,DIMV02,DIMV03,DIMV04)
      CHARACTER(LEN=*), INTENT(IN) :: VARNAME, LONGNAME, VARUNIT

      ! Local variable
      INTEGER                     :: DIMS(4)
      INTEGER                     :: VAR_ID

      CALL CHECK( NF90_REDEF(NCID), INDEX )

      DIMS(1) = DIMID01
      DIMS(2) = DIMID02
      DIMS(3) = DIMID03
      DIMS(4) = DIMID04

      CALL CHECK( NF90_DEF_VAR(NCID, TRIM(VARNAME),
     &            NF90_DOUBLE, DIMS, VAR_ID), INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"name",TRIM(LONGNAME)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"units",TRIM(VARUNIT)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"index",INDEX), INDEX )
      CALL CHECK( NF90_ENDDEF(NCID), INDEX )
      CALL CHECK( NF90_PUT_VAR(NCID, VAR_ID, VAR), INDEX )

      END SUBROUTINE NCIO_4D

!------------------------------------------------------------------------------

      SUBROUTINE NCIO_5D (NCID, VAR, VARNAME, LONGNAME, VARUNIT,
     &                    DIMID01, DIMID02, DIMID03, DIMID04, DIMID05,
     &                    DIMV01,  DIMV02,  DIMV03,  DIMV04,  DIMV05, 
     &                    INDEX)

      ! References to F90 modules
      USE NETCDF

      ! Arguments
      INTEGER, INTENT(IN) :: NCID, INDEX
      INTEGER, INTENT(IN) :: DIMID01, DIMID02, DIMID03, DIMID04, DIMID05
      INTEGER, INTENT(IN) :: DIMV01,  DIMV02,  DIMV03,  DIMV04,  DIMV05
      REAL*8,  INTENT(IN) :: VAR(DIMV01,DIMV02,DIMV03,DIMV04,DIMV05)
      CHARACTER(LEN=*), INTENT(IN) :: VARNAME, LONGNAME, VARUNIT

      ! Local variable
      INTEGER                     :: DIMS(5)
      INTEGER                     :: VAR_ID

      CALL CHECK( NF90_REDEF(NCID), INDEX )

      DIMS(1) = DIMID01
      DIMS(2) = DIMID02
      DIMS(3) = DIMID03
      DIMS(4) = DIMID04
      DIMS(5) = DIMID05

      CALL CHECK( NF90_DEF_VAR(NCID, TRIM(VARNAME),
     &            NF90_DOUBLE, DIMS, VAR_ID), INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"name",TRIM(LONGNAME)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"units",TRIM(VARUNIT)),INDEX)
      CALL CHECK( NF90_PUT_ATT(NCID,VAR_ID,"index",INDEX), INDEX )
      CALL CHECK( NF90_ENDDEF(NCID), INDEX )
      CALL CHECK( NF90_PUT_VAR(NCID, VAR_ID, VAR), INDEX )

      END SUBROUTINE NCIO_5D

!------------------------------------------------------------------------------

      SUBROUTINE CHECK( STATUS, LOCATION )
!
!******************************************************************************
!  Subroutine CHECK checks the status of calls to netCDF libraries routines
!  (dkh, 02/15/09) 
!
!  Arguments as Input:
!  ============================================================================
!  (1 ) STATUS    (INTEGER) : Completion status of netCDF library call    
!  (2 ) LOCATION  (INTEGER) : Location at which netCDF library call was made   
!     
!  NOTES:
!
!******************************************************************************
!
      ! Reference to f90 modules 
      USE ERROR_MOD,    ONLY  : ERROR_STOP
      USE NETCDF

      ! Arguments
      INTEGER, INTENT(IN)    :: STATUS
      INTEGER, INTENT(IN)    :: LOCATION

      !=================================================================
      ! CHECK begins here!
      !=================================================================

      IF ( STATUS /= NF90_NOERR ) THEN
         WRITE(6,*) TRIM( NF90_STRERROR( STATUS ) )
         WRITE(6,*) 'At location = ', LOCATION
         CALL ERROR_STOP('netCDF error', 'diag_mod.f')
      ENDIF

      ! Return to calling program
      END SUBROUTINE CHECK

!------------------------------------------------------------------------------

      ! End of module
      END MODULE DIAG_MOD

