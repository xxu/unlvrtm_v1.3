!  $Id: gas_mod.f, v1.1 2010/12/01 15:24:01 xxu
      MODULE GAS_MOD
!
!******************************************************************************
! Module GAS_MOD contains routine that calculates the gas absorption
! optical depth from the very high resolution HITRAN-2008 data based
! with line-by-line approach and SAO cross-section database. Below is
! the availabe spectral range for each individual gases. 
!   - CO2: 782.2 ~ 4000
!   - CO : 1181.4 ~ 2572.6
!   - CH4: 1087.0 ~ 4000
!   - OH : 519 ~ 4000
! (xxu, 12/1/10, 7/17/12 ) 
!
!  Module Variables:
!  ============================================================================
!  (1 ) MAXMOLS      (INTEGER): Max # of molecules   !! revmoved
!  (1 ) GAS_XABS     (REAL*8 ): Gas spectra absorption over each layer 
!  (  ) XSEC_SAO
!  (  ) XSEC_HIT  
!  Module Routines:
!  ============================================================================ 
!  (1 ) GET_GASTAU 
!******************************************************************************
! 
      IMPLICIT NONE

      !=================================================================
      ! MODULE PRIVATE DECLARATIONS -- keep certain internal variables 
      ! and routines from being seen outside "gas_mod.f"
      !=================================================================

      ! Make everything PRIVATE ...
      PRIVATE 

      ! ... except these routines
      PUBLIC :: GET_GAS_OPTICS
      PUBLIC :: CLEANUP_GAS

      ! ... and these variables
      PUBLIC :: XSEC_HIT
      PUBLIC :: XSEC_SAO
      PUBLIC :: XABS_HIT
      PUBLIC :: XABS_SAO
      PUBLIC :: GAS_XSEC
      PUBLIC :: GAS_XABS

      ! Module variables
      !INTEGER, PARAMETER   :: MAXMOLS   = 39   
      REAL*8,  ALLOCATABLE :: XSEC_SAO(:,:,:)
      REAL*8,  ALLOCATABLE :: XSEC_HIT(:,:,:)
      REAL*8,  ALLOCATABLE :: GAS_XSEC(:,:,:)
      REAL*8,  ALLOCATABLE :: XABS_SAO(:,:,:)
      REAL*8,  ALLOCATABLE :: XABS_HIT(:,:,:)
      REAL*8,  ALLOCATABLE :: GAS_XABS(:,:,:)


      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !=================================================================
      CONTAINS

!------------------------------------------------------------------------------

      SUBROUTINE GET_GAS_OPTICS
!
!******************************************************************************
! Routine GET_GAS_OPTICS calculate gas absorption optical depth of each
! gas molecule over each atmospheric layers. Two different database are
! used here. One is HITRAN-2008 database, and the other is SAO cross
! section database.
! (xxu, 7/17/12)
!
!******************************************************************************
!
      ! References to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS
      USE NAMELIST_ARRAY_MOD, ONLY : NGAS,        GASID
      USE NAMELIST_ARRAY_MOD, ONLY : GASNAME,     LGAS
      USE NAMELIST_ARRAY_MOD, ONLY : GAS_FWHM
      USE NAMELIST_ARRAY_MOD, ONLY : LPRT
      !USE ATMPROF_MOD,        ONLY : ATMZ,        ATMP
      USE ATMPROF_MOD,        ONLY : LAYP,        BOXH
      USE ATMPROF_MOD,        ONLY : LAYT,        LAYINT
      USE ATMPROF_MOD,        ONLY : LAYMOLS
      USE SPECTRA_MOD,        ONLY : NLAMDA
      USE SPECTRA_MOD,        ONLY : LAMDAS,      WAVENUMS
      USE SPECTRA_MOD,        ONLY : LINDIVIDUAL_SPECTRA
      USE SAO_XSEC_MOD,       ONLY : O3_XSEC_195_830
      USE SAO_XSEC_MOD,       ONLY : O4_XSEC_337_666
      USE SAO_XSEC_MOD,       ONLY : NO2_XSEC_238_666
      USE SAO_XSEC_MOD,       ONLY : SO2_XSEC_227_416
      USE SAO_XSEC_MOD,       ONLY : H2CO_XSEC_300_385
      USE ERROR_MOD,          ONLY : ERROR_STOP

      ! Local variables
      REAL*8                :: LAM1, LAM2
      REAL*8                :: T
      REAL*8                :: XSEC(NLAMDA)
      INTEGER               :: I, J, L
      LOGICAL               :: DO_HITRAN
      LOGICAL               :: IS_WAVENUM
      REAL*8                :: FWHM
      INTEGER               :: IERR
      REAL*8                :: XSEC2D(NLAMDA,N_LAYERS)
      REAL*8                :: XSEC1D(1,N_LAYERS)
      CHARACTER(LEN=255)    :: ERRMSG

      ! Screen printing
      WRITE(6,'(A)') ' - GET_GAS_OPTICS: Prepare gas absorption '

      !================================================================
      ! GET_GAS_OPTICS begins here!
      !================================================================

      ! Initialization ...
      CALL INIT_GAS  
      IS_WAVENUM = .FALSE.
      FWHM       = GAS_FWHM

      ! Wavelength range
      LAM1 = LAMDAS(1)
      LAM2 = LAMDAS(NLAMDA) 

      ! Gas molecule loop 
      DO I = 1, NGAS

      ! Gas is not included
      IF ( .NOT. LGAS(I) ) THEN

         ! Absorption is none 
         XSEC_SAO(:,:,I) = 0d0
         XSEC_HIT(:,:,I) = 0d0
         XABS_SAO(:,:,I) = 0d0
         XABS_HIT(:,:,I) = 0d0

         WRITE(6,110) I, TRIM( GASNAME(I) )
       
      ! Otherwise included
      ELSE      
 
         ! Initalization hitran is false
         DO_HITRAN = .FALSE.
     
         ! CASE statement for gas names
         SELECT CASE ( TRIM( GASNAME(I) ) )

         !=============================================================
         ! #1 H2O
         !=============================================================
         CASE ( 'H2O' )
 
            ! If the specified spectra is not covered by HITRAN
            IF ( LAMDAS(1) > 4000.0 .OR. LAMDAS(NLAMDA) < 369.30 ) THEN
 
               XSEC_HIT(:,:,I) = 0d0
               XABS_HIT(:,:,I) = 0d0

            ! Otherwise get the xsec from HITRAN
            ELSE

               DO_HITRAN = .TRUE.
           
            ENDIF

         !=============================================================
         ! #2 CO2
         !=============================================================
         CASE ( 'CO2' )

            ! If the specified spectra is not covered by HITRAN
            IF ( LAMDAS(1) > 4000.0 .OR. LAMDAS(NLAMDA) < 782.2 ) THEN

               XSEC_HIT(:,:,I) = 0d0
               XABS_HIT(:,:,I) = 0d0

            ! Otherwise get the xsec from HITRAN
            ELSE

               DO_HITRAN = .TRUE.

            ENDIF

         !=============================================================
         ! $3 O3
         !=============================================================
         CASE ( 'O3' ) 

            ! Get the xsec from SAO-XSEC database
            DO L = 1, N_LAYERS
               XSEC = O3_XSEC_195_830( LAYT(L) ) 
               XSEC_SAO(:,L,I) = XSEC(:)
               XABS_SAO(:,L,I) = LAYINT(L) * ( LAYMOLS(L,I) * 1d-6 )
     &                         * ( BOXH(L) * 1d2 ) * XSEC(:)  
            ENDDO

            ! Screen print
            WRITE(6,100) I, TRIM( GASNAME(I) )

            ! No HITRAN data is used for O3
            XSEC_HIT(:,:,I) = 0d0
            XABS_HIT(:,:,I) = 0d0
            
         !=============================================================
         ! #5 CO
         !=============================================================
         CASE ( 'CO' )

            ! If the specified spectra is not covered by HITRAN
            IF ( LAMDAS(1) > 2572.6 .OR. LAMDAS(NLAMDA) < 1181.4) THEN

               XSEC_HIT(:,:,I) = 0d0
               XABS_HIT(:,:,I) = 0d0

            ! Otherwise get the xsec from HITRAN
            ELSE

               DO_HITRAN = .TRUE.

            ENDIF

         !=============================================================
         ! #6 CH4
         !=============================================================
         CASE ( 'CH4' )

            ! If the specified spectra is not covered by HITRAN
            IF ( LAMDAS(1) > 4000.0 .OR. LAMDAS(NLAMDA) < 1087.0 ) THEN

               XSEC_HIT(:,:,I) = 0d0
               XABS_HIT(:,:,I) = 0d0

            ! Otherwise get the xsec from HITRAN
            ELSE

               DO_HITRAN = .TRUE.

            ENDIF

         !=============================================================
         ! #7 O2
         !=============================================================
         CASE ( 'O2' )

            ! Get the xsec from SAO-XSEC database
            XSEC = O4_XSEC_337_666()
            DO L = 1, N_LAYERS
               XSEC_SAO(:,L,I) = XSEC(:)
               XABS_SAO(:,L,I) = ( LAYINT(L) * LAYMOLS(L,I) * 1d-6 )
     &                         ** 2   
     &                         * ( BOXH(L) * 1d2 ) * XSEC(:)
            ENDDO
      
            ! Screen print
            WRITE(6,100) I, TRIM( GASNAME(I) )

            ! Get the xsec from HITRAN databsed
            ! If the specified spectra is not covered by HITRAN
            IF ( LAMDAS(1) > 1000.0 .OR. LAMDAS(NLAMDA) < 627.8 ) THEN

               XSEC_HIT(:,:,I) = 0d0
               XABS_HIT(:,:,I) = 0d0
  
            ! Otherwise get the xsec from HITRAN
            ELSE

               DO_HITRAN = .TRUE.

            ENDIF

         !=============================================================
         ! #9 SO2
         !=============================================================
         CASE ( 'SO2' )

            ! Get the xsec from SAO-XSEC database
            DO L = 1, N_LAYERS
               XSEC = SO2_XSEC_227_416( LAYT(L) )
               XSEC_SAO(:,L,I) = XSEC(:)
               XABS_SAO(:,L,I) = LAYINT(L) * ( LAYMOLS(L,I) * 1d-6 )
     &                         * ( BOXH(L) * 1d2 ) * XSEC(:)
            ENDDO

            ! Screen print
            WRITE(6,100) I, TRIM( GASNAME(I) )

            ! No HITRAN data is used for SO2
            XSEC_HIT(:,:,I) = 0d0
            XABS_HIT(:,:,I) = 0d0

         !=============================================================
         ! #10 NO2
         !=============================================================
         CASE ( 'NO2' )

            ! Get the xsec from SAO-XSEC database
            DO L = 1, N_LAYERS
               XSEC = NO2_XSEC_238_666( LAYT(L) )
               XSEC_SAO(:,L,I) = XSEC(:)
               XABS_SAO(:,L,I) = LAYINT(L) * ( LAYMOLS(L,I) * 1d-6 )
     &                         * ( BOXH(L) * 1d2 ) * XSEC(:)
            ENDDO

            ! Screen print
            WRITE(6,100) I, TRIM( GASNAME(I) )

            ! No HITRAN data is used for NO2
            XSEC_HIT(:,:,I) = 0d0
            XABS_HIT(:,:,I) = 0d0

         !=============================================================
         ! #13 OH
         !=============================================================
         CASE ( 'OH' )

            ! If the specified spectra is not covered by HITRAN
            IF ( LAMDAS(1) > 4000.0 .OR. LAMDAS(NLAMDA) < 509.0 ) THEN

               XSEC_HIT(:,:,I) = 0d0
               XABS_HIT(:,:,I) = 0d0

            ! Otherwise get the xsec from HITRAN
            ELSE

               DO_HITRAN = .TRUE.

            ENDIF

         !=============================================================
         ! #20 H2CO
         !=============================================================
         CASE ( 'H2CO' )

            ! Get the xsec from SAO-XSEC database
            XSEC = H2CO_XSEC_300_385()
            DO L = 1, N_LAYERS
               XSEC_SAO(:,L,I) = XSEC(:)
               XABS_SAO(:,L,I) = ( LAYINT(L) * LAYMOLS(L,I) * 1d-6 )
     &                         * ( BOXH(L) * 1d2 ) * XSEC(:)
            ENDDO

            ! Screen print
            WRITE(6,100) I, TRIM( GASNAME(I) )
 
            ! No HITRAN data is used for H2CO
            XSEC_HIT(:,:,I) = 0d0
            XABS_HIT(:,:,I) = 0d0

         !=============================================================
         ! Species that not currently included for uv-vis spectra
         !=============================================================
         CASE ( 'N2O',  'NO',  'NH3',
     &          'HNO3', 'HF',  'HCL',  'HBR',  'HI',
     &          'CLO',  'OCS', 'HOCL', 'N2')

             ! none absorption for these gases
             XSEC_SAO(:,:,I) = 0d0
             XSEC_HIT(:,:,I) = 0d0
             XABS_SAO(:,:,I) = 0d0
             XABS_HIT(:,:,I) = 0d0

             ! Screen print
             WRITE(6,120 ) I, TRIM( GASNAME(I) )

         END SELECT

         ! Get the xsec from HITRAN if DO_HITRAN is true
         IF ( DO_HITRAN ) THEN

            IF ( LINDIVIDUAL_SPECTRA ) THEN

               ! Calculate XSEC for each band
               DO J = 1, NLAMDA
 
                  ! Call the HITRAN routine
                  CALL GET_HITRAN_CRS( I, 1,  LAMDAS(J),
     &                                 IS_WAVENUM, N_LAYERS,
     &                                 LAYP(1:N_LAYERS)/1013.25,
     &                                 LAYT(1:N_LAYERS), FWHM,
     &                                 XSEC1D, IERR ) 

                  ! Error checking  
                  IF ( IERR == 1 ) THEN
                     ERRMSG = "Error in getting HITRAN xsec for "
     &                     // TRIM( GASNAME(I) )
                     CALL ERROR_STOP( ERRMSG, "gas_mod.f" )
                  ENDIF

                  ! Layer loop
                  DO L = 1, N_LAYERS
                     XSEC_HIT(J,L,I) = XSEC1D(1,L)
                     XABS_HIT(J,L,I) = LAYINT(L) 
     &                               * ( LAYMOLS(L,I) * 1d-6 )
     &                               * ( BOXH(L) * 1d2 ) 
     &                               * XSEC_HIT(J,L,I)
!       WRITE(6,315) I, TRIM( GASNAME(I) ), LAMDAS(J), L, 
!     &              XSEC_HIT(J,L,I), XABS_HIT(J,L,I)

                  ENDDO ! L                 

               ENDDO ! J

            ELSE

               ! Call the HITRAN routine
               CALL GET_HITRAN_CRS( I, NLAMDA,  LAMDAS, 
     &                              IS_WAVENUM, N_LAYERS,
     &                              LAYP(1:N_LAYERS)/1013.25,
     &                              LAYT(1:N_LAYERS), FWHM,
     &                              XSEC2D, IERR )

               ! Error checking  
               IF ( IERR == 1 ) THEN
                  ERRMSG = "Error in getting HITRAN xsec for "
     &                  // TRIM( GASNAME(I) )
                  CALL ERROR_STOP( ERRMSG, "gas_mod.f" )
               ENDIF

               ! Layer loop
               DO L = 1, N_LAYERS
                  XSEC_HIT(:,L,I) = XSEC2D(:,L)
                  XABS_HIT(:,L,I) = LAYINT(L) * ( LAYMOLS(L,I) * 1d-6 )
     &                            * ( BOXH(L) * 1d2 ) * XSEC_HIT(:,L,I)
               ENDDO

            ENDIF ! NOT LINDIVIDUAL_SPECTRA

            ! Screen print
            WRITE(6,105) I, TRIM( GASNAME(I) )

         ENDIF

         ! Combine the SAO and HITRAN XSEC to GAS_XABS
         DO L = 1, N_LAYERS
         DO J = 1, NLAMDA
            IF (  XSEC_HIT(J,L,I) * XSEC_SAO(J,L,I) == 0 ) THEN
               GAS_XSEC(J,L,I) = MAX( XSEC_HIT(J,L,I), XSEC_SAO(J,L,I) )
               GAS_XABS(J,L,I) = MAX( XABS_HIT(J,L,I), XABS_SAO(J,L,I) )
            ELSE
               GAS_XSEC(J,L,I) = ( XSEC_HIT(J,L,I)+XSEC_SAO(J,L,I) )/ 2.
               GAS_XABS(J,L,I) = ( XABS_HIT(J,L,I)+XABS_SAO(J,L,I) )/ 2.
            ENDIF
         ENDDO
         ENDDO  

      ENDIF ! .NOT. LGAS(I)

      ! End of gas molecule loop: I
      ENDDO

      ! Formates
 100  FORMAT(4X,"T#",I2," XSEC for ", A4, " is got from SAO-XSEC" )
 105  FORMAT(4X,"T#",I2," XSEC for ", A4, " is got from HITRAN" )
 110  FORMAT(4X,"F#",I2," XSEC for ", A4, " is NOT included!" )
 120  FORMAT(4X,"F#",I2," XSEC for ", A4, " is NOT bulit-in fir SW!" )
 315  FORMAT('AAA', I4, A6, 2X, F8.1, I4, 1P2E11.3 )
 
      ! Return to the calling routine
      END SUBROUTINE GET_GAS_OPTICS

!-------------------------------------------------------------------------------

      SUBROUTINE INIT_GAS

      ! Refereced to F90 modules
      USE NAMELIST_ARRAY_MOD, ONLY : N_LAYERS
      USE NAMELIST_ARRAY_MOD, ONLY : NGAS
      USE SPECTRA_MOD,        ONLY : NLAMDA
      USE ERROR_MOD,          ONLY : ALLOC_ERR

      ! Local variables
      INTEGER            :: AS
      LOGICAL, SAVE      :: IS_INIT = .FALSE.

      ! Return if we have already initialized
      IF ( IS_INIT ) RETURN

      !=================================================================
      ! INIT_GAS begins here!
      !=================================================================

      ! GAS_XSEC
      ALLOCATE( GAS_XSEC(NLAMDA,N_LAYERS,NGAS), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'GAS_XSEC' )
      GAS_XSEC = 0d0

      ! XSEC_SAO
      ALLOCATE( XSEC_SAO(NLAMDA,N_LAYERS,NGAS), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'XSEC_SAO' )
      XSEC_SAO = 0d0

      ! XSEC_HIT
      ALLOCATE( XSEC_HIT(NLAMDA,N_LAYERS,NGAS), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'XSEC_HIT' )
      XSEC_HIT = 0d0

      ! GAS_XABS
      ALLOCATE( GAS_XABS(NLAMDA,N_LAYERS,NGAS), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'GAS_XABS' )
      GAS_XABS = 0d0

      ! XABS_SAO
      ALLOCATE( XABS_SAO(NLAMDA,N_LAYERS,NGAS), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'XABS_SAO' )
      XABS_SAO = 0d0

      ! XABS_HIT
      ALLOCATE( XABS_HIT(NLAMDA,N_LAYERS,NGAS), STAT=AS )
      IF (AS/=0) CALL ALLOC_ERR( 'XABS_HIT' )
      XABS_HIT = 0d0

      ! Reset IS_INIT so we do not allocate arrays again
      IS_INIT = .TRUE.

      ! Return to the calling routine
      RETURN

      END SUBROUTINE INIT_GAS

!-------------------------------------------------------------------------------

      SUBROUTINE CLEANUP_GAS

      !=================================================================
      ! CLEANUP_SPCETRA begins here!
      !=================================================================

      ! Deallocates all module arrays
      IF ( ALLOCATED( GAS_XSEC    ) ) DEALLOCATE( GAS_XSEC    )
      IF ( ALLOCATED( XSEC_SAO    ) ) DEALLOCATE( XSEC_SAO    )
      IF ( ALLOCATED( XSEC_HIT    ) ) DEALLOCATE( XSEC_HIT    )
      IF ( ALLOCATED( GAS_XABS    ) ) DEALLOCATE( GAS_XABS    )
      IF ( ALLOCATED( XABS_SAO    ) ) DEALLOCATE( XABS_SAO    )
      IF ( ALLOCATED( XABS_HIT    ) ) DEALLOCATE( XABS_HIT    )

      END SUBROUTINE CLEANUP_GAS

!-------------------------------------------------------------------------------

      ! End of module
      END MODULE GAS_MOD
