! $Id: vlidort_mod.f, 1.1 2012/07/24 10:55:00 CST 
      SUBROUTINE RUN_VLIDORT_4FD
!
!******************************************************************************
!  Subroutine RUN_VLIDORT_4FD call the VLIDORT model to calculate the
!  radiative radiance, polarization, as well as their Jacobian
!  sensitivty with respect to the specified aerosol parameters. 
!  (xxu, 7/24/12, 7/24/12)
!
! 
!******************************************************************************
!
      !================================================================
      ! Referenced F90 modules
      !================================================================

      ! Modules from VLIDORT
      USE VLIDORT_PARS
      USE VLIDORT_BRDFSup_def
      USE VLIDORT_LSBRDFSup_def
      USE VLIDORT_Inputs_def
      USE VLIDORT_LinInputs_def
      USE VLIDORT_Outputs_def
      USE VLIDORT_LCOutputs_def
      USE VLIDORT_LPOutputs_def
      USE VLIDORT_LSOutputs_def
      USE VLIDORT_LinOutputs_def
      USE VLIDORT_AUX
      USE VLIDORT_L_INPUTS
      USE VLIDORT_L_MASTERS
      USE VLIDORT_ARRAY_MOD,  ONLY : STOKES_COMPS,    JACKS_PROFLS
      USE VLIDORT_ARRAY_MOD,  ONLY : JACKS_COLUMN,    JACKS_SURF
      USE ERROR_MOD,          ONLY : ERROR_STOP
      
      ! Modules from  
       
      IMPLICIT NONE

      !================================================================
      ! Routine variables
      !================================================================

      ! VLIDORT input structures
      TYPE(Fixed_Inputs_Boolean_def      ) :: VLD_FBoolean_IN
      TYPE(Fixed_Inputs_Control_def      ) :: VLD_FControl_IN
      TYPE(Modified_Inputs_Boolean_def   ) :: VLD_MBoolean_IN
      TYPE(Modified_Inputs_Control_def   ) :: VLD_MControl_IN
      TYPE(Fixed_Inputs_Sunrays_def      ) :: VLD_Beam_IN
      TYPE(Fixed_Inputs_UserVAL_def      ) :: VLD_User_IN
      TYPE(Fixed_Inputs_Chapman_def      ) :: VLD_Chapman_IN
      TYPE(Fixed_Inputs_Optical_def      ) :: VLD_Optical_IN
      TYPE(Fixed_Inputs_Write_def        ) :: VLD_Write_IN

      ! VLIDORT linearized input structures
      TYPE(Fixed_Inputs_LinControl_def   ) :: VLD_FLinControl_IN
      TYPE(Modified_Inputs_LinControl_def) :: VLD_MLinControl_IN
      TYPE(Inputs_LinIOPs_Atmos_def      ) :: VLD_LinOptical_IN

      ! VLIDORT BRDF input structures
      TYPE(BRDF_Surface_def              ) :: VLD_BRDF_IN
      TYPE(LSBRDF_Surface_def            ) :: VLD_LSBRDF_IN

      ! VLIDORT output structures
      TYPE(Input_Exception_Handling_def  ) :: VLD_INSTATUS
      TYPE(Outputs_Main_def              ) :: VLD_OUT
      TYPE(Exception_Handling_def        ) :: VLD_STATUS

      ! VLIDORT linearized output structures
      TYPE(LCOutputs_Main_def            ) :: VLD_LCOut
      TYPE(LPOutputs_Main_def            ) :: VLD_LPOut
      TYPE(LinOutputs_Main_def           ) :: VLD_LinOut
      TYPE(LSOutputs_Main_def            ) :: VLD_LSOut

      ! Local vriables
      INTEGER                              :: NSTOKES, NLAYERS
      INTEGER                              :: STATUS_INPUTCHECK
      INTEGER                              :: STATUS_CALCULATION
      CHARACTER(LEN=80)                    :: MESSAGE, TRACE
      LOGICAL                              :: OPENFILEFLAG

      INTEGER                              :: K, N, DIR, O1, IWF
      INTEGER                              :: IB, UM, AZ, V, UT

      !=================================================================
      ! VLIDORT_RUN begins here!
      !=================================================================

      ! Print to screen
      WRITE( 6, '(/a )' ) REPEAT( '=', 79 )
      WRITE( 6, '( a )' ) ' RUN_VLIDORT: calculate TOA reflectance '

      !================================================================
      ! Prepare inputs for VLIDORT
      !================================================================

      ! 
      CALL GET_VLD_FBOOLEAN_IN( VLD_FBOOLEAN_IN ) 

      CALL GET_VLD_FCONTROL_IN( VLD_FCONTROL_IN )

      CALL GET_VLD_MBOOLEAN_IN( VLD_MBOOLEAN_IN )

      CALL GET_VLD_MCONTROL_IN( VLD_MCONTROL_IN )

      CALL GET_VLD_BEAM_IN( VLD_BEAM_IN )

      CALL GET_VLD_USER_IN( VLD_USER_IN )
   
      CALL GET_VLD_CHAPMAN_IN( VLD_CHAPMAN_IN )

      CALL GET_VLD_WRITE_IN( VLD_WRITE_IN )

      CALL GET_VLD_BRDF_IN( VLD_BRDF_IN )

      CALL GET_VLD_LSBRDF_IN(VLD_LSBRDF_IN )

      CALL GET_LCONTROL_AND_OPTICS
     &          ( VLD_FLINCONTROL_IN, VLD_MLINCONTROL_IN, 
     &            VLD_OPTICAL_IN, VLD_LINOPTICAL_IN ) 

      !================================================================
      ! all inputs are prepared, start to run VLIDORT
      !================================================================

      ! call VLIDORT master routine
      CALL VLIDORT_L_MASTER 
     &        ( VLD_FBOOLEAN_IN, VLD_MBOOLEAN_IN, 
     &          VLD_FCONTROL_IN, VLD_MCONTROL_IN,
     &          VLD_BEAM_IN,     VLD_USER_IN, 
     &          VLD_CHAPMAN_IN,  VLD_OPTICAL_IN, 
     &          VLD_WRITE_IN, 
     &          VLD_FLINcONTROL_IN, VLD_MLINcONTROL_IN, 
     &          VLD_LINOPTICAL_IN,  
     &          VLD_BRDF_IN,     VLD_LSBRDF_IN,
     &          VLD_OUT,         VLD_LCOut,
     &          VLD_LPOut,       VLD_LSOut,
     &          VLD_LinOut,      VLD_STATUS )
   
      ! Check the running status  
      !mick fix - added "OPENFILEFLAG = .false."
      OPENFILEFLAG = .false.
      CALL VLIDORT_WRITE_STATUS
     &        ( 'VLIDORT_Execution.log', 
     &          35, OPENFILEFLAG,
     &          VLD_STATUS%TS_STATUS_INPUTCHECK,
     &          VLD_STATUS%TS_NCHECKMESSAGES,
     &          VLD_STATUS%TS_CHECKMESSAGES,
     &          VLD_STATUS%TS_ACTIONS,
     &          VLD_STATUS%TS_STATUS_CALCULATION, 
     &          VLD_STATUS%TS_MESSAGE,
     &          VLD_STATUS%TS_TRACE_1,
     &          VLD_STATUS%TS_TRACE_2,
     &          VLD_STATUS%TS_TRACE_3 )

      ! ERROR handling
      IF ( VLD_STATUS%TS_STATUS_INPUTCHECK == VLIDORT_SERIOUS ) THEN

         MESSAGE  = ' inputcheck in VLIDORT_V24S_MASTER failed'
         CALL ERROR_STOP( MESSAGE, 'vlidort_mod.f' )  
      
      ENDIF

      IF ( VLD_STATUS%TS_STATUS_CALCULATION == VLIDORT_SERIOUS ) THEN

         MESSAGE = ' calculation in LIDORT_V24S_MASTER failed'
         CALL ERROR_STOP( MESSAGE, 'vlidort_mod.f' )

      ENDIF

      !================================================================
      ! Store the model output into Module Variables
      !================================================================

      ! The light direction:
      IF ( VLD_FBOOLEAN_IN%TS_DO_UPWELLING ) THEN
         DIR = 1
      ELSE IF ( VLD_FBOOLEAN_IN%TS_DO_DNWELLING ) THEN
         DIR = 2
      ENDIF

      NSTOKES = VLD_FCONTROL_IN%TS_NSTOKES
      NLAYERS = VLD_FCONTROL_IN%TS_NLAYERS 
     

      ! Store model outputs
      DO O1 = 1, NSTOKES
      DO V  = 1, VLD_OUT%TS_N_GEOMETRIES
      DO UT = 1, VLD_USER_IN%TS_N_USER_LEVELS

         ! Stokes components
         STOKES_COMPS(UT,V,O1) = VLD_OUT%TS_STOKES(UT,V,O1,DIR)

         ! Atmosphere profile weighting function
         IF ( VLD_MLINCONTROL_IN%TS_DO_PROFILE_LINEARIZATION ) THEN
            DO IWF = 1, VLD_FLINCONTROL_IN%TS_N_TOTALPROFILE_WFS
               DO N   = 1, NLAYERS
                  JACKS_PROFLS(IWF,N,UT,V,O1) = 
     &                       VLD_LPOUT%TS_PROFILEWF(IWF,N,UT,V,O1,DIR)
               ENDDO
               JACKS_COLUMN(IWF,UT,V,O1)
     &                       = SUM(JACKS_PROFLS(IWF,1:NLAYERS,UT,V,O1))
            ENDDO
         ENDIF

         ! Atmosphere column weighting function
         IF ( VLD_MLINCONTROL_IN%TS_DO_COLUMN_LINEARIZATION ) THEN
            DO IWF = 1, VLD_FLINCONTROL_IN%TS_N_TOTALCOLUMN_WFS
               JACKS_COLUMN(IWF,UT,V,O1) = 
     &                         VLD_LCOUT%TS_COLUMNWF(IWF,UT,V,O1,DIR)
            ENDDO
         ENDIF

         ! Surface Jacobian
         IF ( VLD_MLINCONTROL_IN%TS_DO_SURFACE_LINEARIZATION ) THEN
                JACKS_SURF(1,UT,V,O1) = 
     &                          VLD_LSOUT%TS_SURFACEWF(1,UT,V,O1,DIR)
         ENDIF

      ENDDO
      ENDDO
      ENDDO

      !================================================================
      ! Print to screen
      !================================================================

      ! Print Stokes components
      WRITE(6,'(A)') ' -LOUT, SZA, VZA, RAZ, Stokes components:'
      DO UT = 1, VLD_USER_IN%TS_N_USER_LEVELS
      DO IB = 1, VLD_BEAM_IN%TS_N_SZANGLES
      DO UM = 1, VLD_USER_IN%TS_N_USER_VZANGLES
      DO AZ = 1, VLD_USER_IN%TS_N_USER_RELAZMS
         V  = VLD_OUT%TS_VZA_OFFSETS(IB, UM) + AZ
         WRITE(6,100) UT,
     &                VLD_BEAM_IN%TS_SZANGLES(IB),
     &                VLD_USER_IN%TS_USER_VZANGLES_INPUT(UM),
     &                VLD_USER_IN%TS_USER_RELAZMS(AZ),
     &                STOKES_COMPS(UT,V,1:NSTOKES)
      ENDDO ! AZ
      ENDDO ! UM
      ENDDO ! IB
      ENDDO ! UT

      ! Print column jacobian
      IF ( VLD_MLINCONTROL_IN%TS_DO_PROFILE_LINEARIZATION ) THEN
      WRITE(6,'(A)') ' -IWF, LOUT, SZA, VZA, RAZ, Column Jacobian:'
      DO IWF= 1, VLD_FLINCONTROL_IN%TS_N_TOTALPROFILE_WFS
      DO UT = 1, VLD_USER_IN%TS_N_USER_LEVELS
      DO IB = 1, VLD_BEAM_IN%TS_N_SZANGLES
      DO UM = 1, VLD_USER_IN%TS_N_USER_VZANGLES
      DO AZ = 1, VLD_USER_IN%TS_N_USER_RELAZMS
         V  = VLD_OUT%TS_VZA_OFFSETS(IB, UM) + AZ
         WRITE(6,110) IWF, UT, 
     &                VLD_BEAM_IN%TS_SZANGLES(IB),
     &                VLD_USER_IN%TS_USER_VZANGLES_INPUT(UM),
     &                VLD_USER_IN%TS_USER_RELAZMS(AZ), 
     &                JACKS_COLUMN(IWF,UT,V,1:NSTOKES) 
      ENDDO ! AZ
      ENDDO ! UM
      ENDDO ! IB
      ENDDO ! UT
      ENDDO ! IWF
      ENDIF

      IF ( VLD_MLINCONTROL_IN%TS_DO_SURFACE_LINEARIZATION )THEN
      WRITE(6,'(A)') ' LOUT, SZA, VZA, RAZ, SULFACE Jacobian:'
      DO UT = 1, VLD_USER_IN%TS_N_USER_LEVELS
      DO IB = 1, VLD_BEAM_IN%TS_N_SZANGLES
      DO UM = 1, VLD_USER_IN%TS_N_USER_VZANGLES
      DO AZ = 1, VLD_USER_IN%TS_N_USER_RELAZMS
         V  = VLD_OUT%TS_VZA_OFFSETS(IB, UM) + AZ
         WRITE(6,110) 1, UT,
     &                VLD_BEAM_IN%TS_SZANGLES(IB),
     &                VLD_USER_IN%TS_USER_VZANGLES_INPUT(UM),
     &                VLD_USER_IN%TS_USER_RELAZMS(AZ),
     &                JACKS_SURF(1,UT,V,1:NSTOKES) 
      ENDDO ! AZ
      ENDDO ! UM
      ENDDO ! IB
      ENDDO ! UT
      ENDIF

 100  FORMAT(I4,  3F7.2, 1P4E14.6)
 110  FORMAT(2I4, 3F7.2, 1P4E14.6)

      ! Return to calling program
      RETURN

      END SUBROUTINE RUN_VLIDORT

!------------------------------------------------------------------------------

      SUBROUTINE GET_VLD_FBOOLEAN_IN( VLD_FBOOLEAN_IN )
!
!******************************************************************************
! Function GET_VLD_FBOOLEAN_IN return the initialized TYPE variable for
! calling VLIDORT_L_MASTER.
! 
! Variables:
! =============================================================================
! TS_DO_FULLRAD_MODE        : Full Stokes vector calculation 
! TS_DO_SSCORR_TRUNCATION   : Flag for SSCORR truncation ( delta-M scaling)
! TS_DO_SSFULL              : Full-up single scatter calculation
! TS_DO_THERMAL_EMISSION    : Thermal emission flags
! TS_DO_SURFACE_EMISSION    : Surface emission flags
! TS_DO_PLANE_PARALLEL      : Plane-parallel treatment of direct beam
! TS_DO_UPWELLING           : upwelling output
! TS_DO_DNWELLING           : downwelling output
! TS_DO_QUAD_OUTPUT         : stream angle (quadrature outpu)
! TS_DO_TOA_CONTRIBS        : top of the atmosphere contributions
! TS_DO_LAMBERTIAN_SURFACE  : Lambertian surface
! TS_DO_SPECIALIST_OPTION_1 : Special Options
! TS_DO_SPECIALIST_OPTION_2 : Special Options
! TS_DO_SPECIALIST_OPTION_3 : Special Options
!
!******************************************************************************
!
      ! Referenced F90 modules
      USE VLIDORT_INPUTS_DEF, ONLY : FIXED_INPUTS_BOOLEAN_DEF
      USE NAMELIST_ARRAY_MOD, ONLY : RECEP_DIR      

      ! Arguments
      TYPE(FIXED_INPUTS_BOOLEAN_DEF) :: VLD_FBOOLEAN_IN

      VLD_FBOOLEAN_IN%TS_DO_FULLRAD_MODE      = .TRUE.
      VLD_FBOOLEAN_IN%TS_DO_SSCORR_TRUNCATION = .FALSE.
      VLD_FBOOLEAN_IN%TS_DO_SSFULL            = .FALSE.
      VLD_FBOOLEAN_IN%TS_DO_THERMAL_EMISSION  = .FALSE.
      VLD_FBOOLEAN_IN%TS_DO_SURFACE_EMISSION  = .FALSE.
      VLD_FBOOLEAN_IN%TS_DO_PLANE_PARALLEL    = .TRUE.

      ! Set user receptor direction
      IF ( RECEP_DIR == -1 ) THEN 
         VLD_FBOOLEAN_IN%TS_DO_UPWELLING = .TRUE.
         VLD_FBOOLEAN_IN%TS_DO_DNWELLING = .FALSE.
      ELSE IF ( RECEP_DIR == 1 ) THEN
         VLD_FBOOLEAN_IN%TS_DO_UPWELLING = .FALSE.
         VLD_FBOOLEAN_IN%TS_DO_DNWELLING = .TRUE.
      ENDIF 

      VLD_FBOOLEAN_IN%TS_DO_QUAD_OUTPUT         = .FALSE.
      VLD_FBOOLEAN_IN%TS_DO_TOA_CONTRIBS        = .FALSE.
      VLD_FBOOLEAN_IN%TS_DO_LAMBERTIAN_SURFACE  = .TRUE.
      VLD_FBOOLEAN_IN%TS_DO_SPECIALIST_OPTION_1 = .FALSE.
      VLD_FBOOLEAN_IN%TS_DO_SPECIALIST_OPTION_2 = .FALSE.
      VLD_FBOOLEAN_IN%TS_DO_SPECIALIST_OPTION_3 = .FALSE.

      ! Return to the calling routine  
      RETURN

      END SUBROUTINE GET_VLD_FBOOLEAN_IN

!------------------------------------------------------------------------------

      SUBROUTINE GET_VLD_FCONTROL_IN( VLD_FCONTROL_IN )
!
!******************************************************************************
! Function GET_VLD_FCONTROL_IN return the initialized TYPE variable for
! calling VLIDORT_L_MASTER.
! 
! Variables:
! =============================================================================
! TS_NSTOKES                 : Number of Stokes parameters
! TS_NSTREAMS                : Number of discrete ordinate streams
! TS_NLAYERS                 : Number of computational layers
! TS_NFINELAYERS             : Number of fine layers (spherical corr. only)
! TS_N_THERMAL_COEFFS        : Number of thermal coefficients (default=2)
! TS_VLIDORT_ACCURACY        : Accuracy for convergence of Fourier series
! TS_NLAYERS_NOMS            : Special Options
! TS_NLAYERS_CUTOFF          : Special Options
!******************************************************************************
!
      ! Referenced F90 modules
      USE VLIDORT_INPUTS_DEF,    ONLY : FIXED_INPUTS_CONTROL_DEF
      USE NAMELIST_ARRAY_MOD,    ONLY : N_STOKS, N_STRMS, N_LAYERS 
 
      ! Arguments
      TYPE(FIXED_INPUTS_CONTROL_DEF) :: VLD_FCONTROL_IN   

      VLD_FCONTROL_IN%TS_NSTOKES           = N_STOKS
      VLD_FCONTROL_IN%TS_NSTREAMS          = N_STRMS
      VLD_FCONTROL_IN%TS_NLAYERS           = N_LAYERS
      VLD_FCONTROL_IN%TS_NFINELAYERS       = 2 
      VLD_FCONTROL_IN%TS_N_THERMAL_COEFFS  = 0
      VLD_FCONTROL_IN%TS_VLIDORT_ACCURACY  = 1d-4
      VLD_FCONTROL_IN%TS_NLAYERS_NOMS      = 0d0
      VLD_FCONTROL_IN%TS_NLAYERS_CUTOFF    = 0d0

      ! Return to the calling routine  
      RETURN

      END SUBROUTINE GET_VLD_FCONTROL_IN

!------------------------------------------------------------------------------

      SUBROUTINE GET_VLD_MBOOLEAN_IN( VLD_MBOOLEAN_IN )
!
!******************************************************************************
! Function GET_VLD_MBOOLEAN_IN return the initialized TYPE variable for
! calling VLIDORT_L_MASTER.
! 
! Variables:
! =============================================================================
! TS_DO_SSCORR_NADIR        : nadir single scatter correction
! TS_DO_SSCORR_OUTGOING     : outgoing single scatter correction
! TS_DO_DOUBLE_CONVTEST     : double convergence test
! TS_DO_SOLAR_SOURCES       : Use solar sources
! TS_DO_REFRACTIVE_GEOMETRY : Do refractive geometry
! TS_DO_CHAPMAN_FUNCTION    : internal Chapman function calculation
! TS_DO_RAYLEIGH_ONLY       : Rayleigh atmosphere only
! TS_DO_DELTAM_SCALING      : delta-M scaling
! TS_DO_SOLUTION_SAVING     : solution saving
! TS_DO_BVP_TELESCOPING     : boundary-value telescoping
! TS_DO_USER_VZANGLES       : Use user-defined viewing zenith angles
! TS_DO_ADDITIONAL_MVOUT    : mean-value output additionally
! TS_DO_MVOUT_ONLY          : only mean-value output
! TS_DO_THERMAL_TRANSONLY   : thermal emission, transmittance only
!******************************************************************************
!
      ! Referenced F90 modules
      USE VLIDORT_INPUTS_DEF,    ONLY : MODIFIED_INPUTS_BOOLEAN_DEF

      ! Return value
      TYPE(MODIFIED_INPUTS_BOOLEAN_DEF) :: VLD_MBOOLEAN_IN

      VLD_MBOOLEAN_IN%TS_DO_SSCORR_NADIR        = .FALSE.
      VLD_MBOOLEAN_IN%TS_DO_SSCORR_OUTGOING     = .FALSE.
      VLD_MBOOLEAN_IN%TS_DO_DOUBLE_CONVTEST     = .TRUE.
      VLD_MBOOLEAN_IN%TS_DO_SOLAR_SOURCES       = .TRUE.
      VLD_MBOOLEAN_IN%TS_DO_REFRACTIVE_GEOMETRY = .FALSE.
      VLD_MBOOLEAN_IN%TS_DO_CHAPMAN_FUNCTION    = .FALSE.
      VLD_MBOOLEAN_IN%TS_DO_RAYLEIGH_ONLY       = .FALSE.
      VLD_MBOOLEAN_IN%TS_DO_DELTAM_SCALING      = .TRUE.
      VLD_MBOOLEAN_IN%TS_DO_SOLUTION_SAVING     = .FALSE.
      VLD_MBOOLEAN_IN%TS_DO_BVP_TELESCOPING     = .FALSE.
      VLD_MBOOLEAN_IN%TS_DO_USER_VZANGLES       = .TRUE.
      VLD_MBOOLEAN_IN%TS_DO_ADDITIONAL_MVOUT    = .TRUE.
      VLD_MBOOLEAN_IN%TS_DO_MVOUT_ONLY          = .FALSE.
      VLD_MBOOLEAN_IN%TS_DO_THERMAL_TRANSONLY   = .FALSE.

      END SUBROUTINE GET_VLD_MBOOLEAN_IN

!------------------------------------------------------------------------------

      SUBROUTINE GET_VLD_MCONTROL_IN( VLD_MCONTROL_IN )
!
!******************************************************************************
! Function GET_VLD_MCONTROL_IN return the initialized TYPE variable for
! calling VLIDORT_L_MASTER.
! 
! Variables:
! =============================================================================
! TS_NGREEK_MOMENTS_INPUT   : number of Legendre phase f expansion moments
!******************************************************************************
!
      ! Referenced F90 modules
      USE VLIDORT_INPUTS_DEF,    ONLY : MODIFIED_INPUTS_CONTROL_DEF
      USE NAMELIST_ARRAY_MOD,    ONLY : N_MOMENTS

      ! Arguments
      TYPE(MODIFIED_INPUTS_CONTROL_DEF      ) :: VLD_MCONTROL_IN

      VLD_MCONTROL_IN%TS_NGREEK_MOMENTS_INPUT = N_MOMENTS-1

      ! Return to the calling routine  
      RETURN

      END SUBROUTINE GET_VLD_MCONTROL_IN

!------------------------------------------------------------------------------

      SUBROUTINE GET_VLD_BEAM_IN( VLD_BEAM_IN )
!
!******************************************************************************
! Function GET_VLD_BEAM_IN return the initialized TYPE variable for
! calling VLIDORT_L_MASTER.
! 
! Variables:
! =============================================================================
! TS_FLUX_FACTOR            : Flux factor ( should be 1 or pi )
! TS_N_SZANGLES             : number of solar beams
! TS_SZANGLES               : Bottom-of-atmosphere solar zenith angles (degree)
!******************************************************************************
!
      ! Referenced F90 modules
      USE VLIDORT_INPUTS_DEF,   ONLY : FIXED_INPUTS_SUNRAYS_DEF
      USE NAMELIST_ARRAY_MOD,   ONLY : N_THETA0
      USE NAMELIST_ARRAY_MOD,   ONLY : THETA0

      ! Arguments
      TYPE(FIXED_INPUTS_SUNRAYS_DEF) :: VLD_BEAM_IN

      VLD_BEAM_IN%TS_FLUX_FACTOR          = 1d0
      VLD_BEAM_IN%TS_N_SZANGLES           = N_THETA0
      VLD_BEAM_IN%TS_SZANGLES(1:N_THETA0) = THETA0(1:N_THETA0)

      ! Return to the calling routine
      END SUBROUTINE GET_VLD_BEAM_IN

!------------------------------------------------------------------------------

      SUBROUTINE GET_VLD_USER_IN( VLD_USER_IN )
!
!******************************************************************************
! Function GET_VLD_USER_IN return the initialized TYPE variable for
! calling VLIDORT_L_MASTER.
! 
! Variables:
! =============================================================================
! TS_N_USER_RELAZMS         : # of User-defined relative azimuths
! TS_USER_RELAZMS           : User-defined relative azimuths
! TS_N_USER_VZANGLES        : # of User-defined zenith angle
! TS_USER_VZANGLES_INPUT    : User-defined zenith angle
! TS_N_USER_LEVELS          : # of User-defined vertical level output
! TS_USER_LEVELS            : User-defined vertical level output
! TS_GEOMETRY_SPECHEIGHT    : Geometry specification height
!
! NOTES:
! User-defined vertical level output. From Top-of-atmosphere. 
! Example for 4 outputs:
!     USER_LEVELS(1) = 0.0           --> Top-of-atmosphere
!     USER_LEVELS(2) = 1.1           --> One tenth of the way down into
!     Layer 2
!     USER_LEVELS(3) = 3.5           --> One half  of the way down into
!     Layer 4
!     USER_LEVELS(4) = dble(NLAYERS) --> Bottom of atmosphere
!******************************************************************************
!
      ! Referenced F90 modules
      USE VLIDORT_INPUTS_DEF,   ONLY : FIXED_INPUTS_USERVAL_DEF
      USE NAMELIST_ARRAY_MOD,   ONLY : N_THETA, N_PHI
      USE NAMELIST_ARRAY_MOD,   ONLY : THETA,   PHI
      USE NAMELIST_ARRAY_MOD,   ONLY : NRECEP_LEV,   RECEP_LEV
      USE ATMPROF_MOD,          ONLY : ATMZ

      ! Arguments
      TYPE(FIXED_INPUTS_USERVAL_DEF) :: VLD_USER_IN
     
      VLD_USER_IN%TS_N_USER_RELAZMS        = N_PHI
      VLD_USER_IN%TS_USER_RELAZMS(1:N_PHI) = PHI(1:N_PHI)
      VLD_USER_IN%TS_N_USER_VZANGLES       = N_THETA
      VLD_USER_IN%TS_USER_VZANGLES_INPUT(1:N_THETA) 
     &                                     = THETA(1:N_THETA) 
      VLD_USER_IN%TS_N_USER_LEVELS         = NRECEP_LEV
      VLD_USER_IN%TS_USER_LEVELS(1:NRECEP_LEV) = RECEP_LEV(1:NRECEP_LEV)
      VLD_USER_IN%TS_GEOMETRY_SPECHEIGHT   = ATMZ(1) ! surface
      
      ! Return to the calling routine
      END SUBROUTINE GET_VLD_USER_IN
 
!------------------------------------------------------------------------------

      SUBROUTINE GET_VLD_CHAPMAN_IN( VLD_CHAPMAN_IN )
!
!******************************************************************************
! Function GET_VLD_CHAPMAN_IN return the initialized TYPE variable for
! calling VLIDORT_L_MASTER.
! 
! Variables:
! =============================================================================
! TS_HEIGHT_GRID      : multilayer Height [km]
! TS_PRESSURE_GRID    : Pressures in [mb], Required for the Chapman function 
! TS_TEMPERATURE_GRID : temperatures in [K], Required for the Chapman
! TS_FINEGRID         : Number of fine layer gradations
!                       Required for the Chapman function, refractive
!                       geometry
! TS_EARTH_RADIUS     : Earth radius in [km] for Chapman function
! TS_RFINDEX_PARAMETER: Refractive index parameter (only for Chapman 
!                       function calculations with refractive index)
!******************************************************************************
!
      ! Referenced F90 modules
      USE VLIDORT_INPUTS_DEF,   ONLY : FIXED_INPUTS_CHAPMAN_DEF
      USE ATMPROF_MOD,          ONLY : ATMZ, ATMP, ATMT
      USE NAMELIST_ARRAY_MOD,   ONLY : N_LAYERS

      ! Arguments
      TYPE(FIXED_INPUTS_CHAPMAN_DEF) :: VLD_CHAPMAN_IN
 
      ! Local variables
      INTEGER                        :: L, REVSE_L

      DO L = 0, N_LAYERS

         REVSE_L = N_LAYERS - L + 1
         VLD_CHAPMAN_IN%TS_HEIGHT_GRID(L)      = ATMZ(REVSE_L)
         VLD_CHAPMAN_IN%TS_PRESSURE_GRID(L)    = ATMP(REVSE_L)
         VLD_CHAPMAN_IN%TS_TEMPERATURE_GRID(L) = ATMT(REVSE_L)
      ENDDO

      VLD_CHAPMAN_IN%TS_FINEGRID           = 2
      VLD_CHAPMAN_IN%TS_EARTH_RADIUS       = 6371.0d0
      VLD_CHAPMAN_IN%TS_RFINDEX_PARAMETER  = 0.000288d0

      ! Return to the calling routine
      END SUBROUTINE GET_VLD_CHAPMAN_IN

!------------------------------------------------------------------------------

      SUBROUTINE GET_VLD_WRITE_IN( VLD_WRITE_IN )

      ! Referenced F90 modules
      USE VLIDORT_INPUTS_DEF,   ONLY : FIXED_INPUTS_WRITE_DEF

      ! Arguments
      TYPE(FIXED_INPUTS_WRITE_DEF        ) :: VLD_WRITE_IN

      VLD_WRITE_IN%TS_DO_DEBUG_WRITE         = .FALSE.
      VLD_WRITE_IN%TS_DO_WRITE_INPUT         = .FALSE.
      VLD_WRITE_IN%TS_INPUT_WRITE_FILENAME   = 'vlidort_std_input'
      VLD_WRITE_IN%TS_DO_WRITE_SCENARIO      = .FALSE.
      VLD_WRITE_IN%TS_SCENARIO_WRITE_FILENAME= 'vlidort_std_scenario'
      VLD_WRITE_IN%TS_DO_WRITE_FOURIER       = .FALSE.
      VLD_WRITE_IN%TS_FOURIER_WRITE_FILENAME = 'vlidort_std_fourier'
      VLD_WRITE_IN%TS_DO_WRITE_RESULTS       = .FALSE.
      VLD_WRITE_IN%TS_RESULTS_WRITE_FILENAME = 'vlidort_std_result'

      ! Return to the calling routine
      END SUBROUTINE GET_VLD_WRITE_IN

!------------------------------------------------------------------------------

      SUBROUTINE GET_LCONTROL_AND_OPTICS
     &             ( FLCONTR, MLCONTR, OPTICAL, L_OPTIC )

      ! Combine this one with the following one.......
!
!******************************************************************************
! 
! Vairables
! =============================================================================
! TS_DO_SIMULATION_ONLY      : Simulation only (No Jacobian calculation)
! TS_DO_LTE_LINEARIZATION    : LTE temperature weighting functions
! TS_DO_SURFBB_LINEARIZATION : Surface blackbody weighting functions
! TS_LAYER_VARY_FLAG         : 
! TS_LAYER_VARY_NUMBER       : 
! TS_N_TOTALCOLUMN_WFS       :  Number of atmospheric column weighting f
! TS_N_TOTALPROFILE_WFS      : Number of atmospheric profile weighting f
! TS_N_SURFACE_WFS           : Number of surface property weighting f
! TS_COLUMNWF_NAMES          : Atmospheric column Jacobian names
! TS_PROFILEWF_NAMES         : Atmospheric profile Jacobian name
!******************************************************************************
!
      ! Referenced F90 modules
      USE VLIDORT_INPUTS_DEF,    ONLY : FIXED_INPUTS_OPTICAL_DEF
      USE VLIDORT_LINiNPUTS_DEF, ONLY : INPUTS_LINIOPS_ATMOS_DEF
      USE VLIDORT_LINiNPUTS_DEF, ONLY : FIXED_INPUTS_LINCONTROL_DEF
      USE VLIDORT_LINiNPUTS_DEF, ONLY : MODIFIED_INPUTS_LINCONTROL_DEF
      USE NAMELIST_ARRAY_MOD,    ONLY : N_LAYERS,     N_MOMENTS
      USE NAMELIST_ARRAY_MOD,    ONLY : N_STOKS
      USE NAMELIST_ARRAY_MOD,    ONLY : LJACOB
      USE NAMELIST_ARRAY_MOD,    ONLY : LJACOB_SURF,  SURF_REFL
      USE LPAR_MOD,              ONLY : N_LPAR_TOTAL, LPAR_NAMES
      USE LPAR_MOD,              ONLY : N_LPAR_SURF
      USE IOP_MOD,               ONLY : IOP_TAU,  IOP_SSA,  IOP_PHA
      USE IOP_MOD,               ONLY : LIOP_TAU, LIOP_SSA, LIOP_PHA
      USE IOP_MOD,               ONLY : LIOP_JCB

      ! Arguments (types)
      TYPE(FIXED_INPUTS_LINCONTROL_DEF   ) :: FLCONTR
      TYPE(MODIFIED_INPUTS_LINCONTROL_DEF) :: MLCONTR
      TYPE(FIXED_INPUTS_OPTICAL_DEF      ) :: OPTICAL
      TYPE(INPUTS_LINIOPS_ATMOS_DEF      ) :: L_OPTIC

      ! Local variables
      INTEGER                              :: GK, GMMASK(8), NMASK
      INTEGER                              :: I, L, N, K, Q
      !DATA GMMASK / 1, 2, 5, 6, 11, 12, 15, 16 /
      DATA GMMASK / 1, 6, 11, 16, 2, 5, 12, 15 /

      ! Surpress the Emission/Temperature Jacobian
      FLCONTR%TS_DO_LTE_LINEARIZATION    = .FALSE.
      FLCONTR%TS_DO_SURFBB_LINEARIZATION = .FALSE.
      OPTICAL%TS_THERMAL_BB_INPUT        = 0d0
      OPTICAL%TS_SURFACE_BB_INPUT        = 0d0
      OPTICAL%TS_LTE_DELTAU_VERT_INPUT   = 0d0
      OPTICAL%TS_LTE_THERMAL_BB_INPUT    = 0d0

      !================================================================
      ! Atmospheric Jacobian calculation scenario
      !================================================================
      IF ( LJACOB ) THEN

         FLCONTR%TS_DO_SIMULATION_ONLY       = .FALSE.
         MLCONTR%TS_DO_LINEARIZATION         = .TRUE.
         MLCONTR%TS_DO_ATMOS_LINEARIZATION   = .TRUE.
         MLCONTR%TS_DO_PROFILE_LINEARIZATION = .TRUE.
         MLCONTR%TS_DO_COLUMN_LINEARIZATION  = .FALSE.

         FLCONTR%TS_N_TOTALCOLUMN_WFS        = 0
         FLCONTR%TS_N_TOTALPROFILE_WFS       = N_LPAR_TOTAL
         FLCONTR%TS_PROFILEWF_NAMES(1:N_LPAR_TOTAL) 
     &           = LPAR_NAMES(1:N_LPAR_TOTAL)       

      ELSE

         FLCONTR%TS_DO_SIMULATION_ONLY       = .TRUE.
         MLCONTR%TS_DO_LINEARIZATION         = .FALSE.
         MLCONTR%TS_DO_ATMOS_LINEARIZATION   = .FALSE.
         MLCONTR%TS_DO_PROFILE_LINEARIZATION = .FALSE.
         MLCONTR%TS_DO_COLUMN_LINEARIZATION  = .FALSE.
         FLCONTR%TS_N_TOTALCOLUMN_WFS        = 0
         FLCONTR%TS_N_TOTALPROFILE_WFS       = 0

      ENDIF

      !================================================================ 
      ! VLIDORT inputs of optical properties
      !================================================================

      ! Controls number of non-zero entries in the Greek matrix
      NMASK = 1
      IF ( N_STOKS > 1 ) NMASK = 8 

      ! Layer loop
      DO L = 1, N_LAYERS

         ! Bulk optical properties
         OPTICAL%TS_DELTAU_VERT_INPUT(L) = IOP_TAU(L)
         OPTICAL%TS_OMEGA_TOTAL_INPUT(L) = IOP_SSA(L) 

         DO K = 1, NMASK
            GK = GMMASK(K)
            DO N = 0, N_MOMENTS-1
               OPTICAL%TS_GREEKMAT_TOTAL_INPUT(N,L,GK) = IOP_PHA(N,L,K)
            ENDDO
         ENDDO

         ! Linearized optical properties for Jacobian calculation
         IF ( LJACOB .AND. N_LPAR_TOTAL >=1 .AND. LIOP_JCB(L) == 1) THEN

            FLCONTR%TS_LAYER_VARY_FLAG(L)   = .TRUE.
            FLCONTR%TS_LAYER_VARY_NUMBER(L) = N_LPAR_TOTAL

            ! Prepare the linearized IOP
            DO Q = 1, N_LPAR_TOTAL

               L_OPTIC%TS_L_DELTAU_VERT_INPUT(Q,L) = LIOP_TAU(Q,L)
               L_OPTIC%TS_L_OMEGA_TOTAL_INPUT(Q,L) = LIOP_SSA(Q,L)

               DO K = 1, NMASK

                  GK = GMMASK(K)
                  DO N = 0, N_MOMENTS-1
                   L_OPTIC%TS_L_GREEKMAT_TOTAL_INPUT(Q,N,L,GK)
     &                                          = LIOP_PHA(Q,N,L,K)
                  ENDDO ! N
 
               ENDDO ! K

            ENDDO ! Q

         ENDIF

      ! End layer loop 
      ENDDO

      !================================================================ 
      ! Surface Jacobian Configuration
      !================================================================ 
      IF ( LJACOB_SURF ) THEN

         MLCONTR%TS_DO_SURFACE_LINEARIZATION = .TRUE.
         FLCONTR%TS_N_SURFACE_WFS            = N_LPAR_SURF

      ELSE

         MLCONTR%TS_DO_SURFACE_LINEARIZATION = .FALSE.
         FLCONTR%TS_N_SURFACE_WFS            = 0

      ENDIF

      !================================================================ 
      ! Surface Reflectance
      !================================================================ 
      OPTICAL%TS_LAMBERTIAN_ALBEDO = SURF_REFL 

      ! Return to the calling routine
      END SUBROUTINE GET_LCONTROL_AND_OPTICS

!------------------------------------------------------------------------------

      SUBROUTINE GET_VLD_BRDF_IN( VLD_BRDF_IN )
!
!******************************************************************************
! Original comments from vlidort_brdf_sup_def.f90:
! !  Fourier components of BRDF, in the following order (same all
! threads)
!    incident solar directions,   reflected quadrature streams
!    incident quadrature streams, reflected quadrature streams
!    incident solar directions,   reflected user streams
!    incident quadrature streams, reflected user streams
!******************************************************************************
!
      USE VLIDORT_BRDFSUP_DEF, ONLY : BRDF_SURFACE_DEF

      ! Arguments
      TYPE(BRDF_SURFACE_DEF) :: VLD_BRDF_IN

      VLD_BRDF_IN%TS_EXACTDB_BRDFUNC = 0d0
      VLD_BRDF_IN%TS_BRDF_F_0        = 0d0
      VLD_BRDF_IN%TS_BRDF_F          = 0d0
      VLD_BRDF_IN%TS_USER_BRDF_F_0   = 0d0
      VLD_BRDF_IN%TS_USER_BRDF_F     = 0d0
      VLD_BRDF_IN%TS_EMISSIVITY      = 0d0
      VLD_BRDF_IN%TS_USER_EMISSIVITY = 0d0

      ! Return to the calling routine
      END SUBROUTINE GET_VLD_BRDF_IN 

!------------------------------------------------------------------------------

      SUBROUTINE GET_VLD_LSBRDF_IN( VLD_LSBRDF_IN )

      USE VLIDORT_LSBRDFSUP_DEF,  ONLY : LSBRDF_SURFACE_DEF

      ! Arguments
      TYPE(LSBRDF_SURFACE_DEF ) :: VLD_LSBRDF_IN

      VLD_LSBRDF_IN%TS_LS_EXACTDB_BRDFUNC = 0d0
      VLD_LSBRDF_IN%TS_LS_BRDF_F_0        = 0d0
      VLD_LSBRDF_IN%TS_LS_BRDF_F          = 0d0
      VLD_LSBRDF_IN%TS_LS_USER_BRDF_F_0   = 0d0
      VLD_LSBRDF_IN%TS_LS_USER_BRDF_F     = 0d0
      VLD_LSBRDF_IN%TS_LS_EMISSIVITY      = 0d0
      VLD_LSBRDF_IN%TS_LS_USER_EMISSIVITY = 0d0

      ! Return to the calling routine
      END SUBROUTINE GET_VLD_LSBRDF_IN

