! $Id: run_lmie.f, v 1.1 2011/11/29 14:13:41 xxu $Exp
      SUBROUTINE  RUN_LMIE( LAMDA, IMODE )
!
!******************************************************************************
!  Subourtine RUN_LMIE runs the linearized mie code with the input from
!   reading namelist.ini. The results are saved in the module variables from
!   AEROSOL_MOD (xxu, 11/29/11)
!  
!  Module referenced by run_lmie.f
!  ============================================================================
!  (1 ) namelist_array_mod.f
!  (2 ) lmie_array_mod.f
!  (3 ) RTSMIE_parameters_m.f90
!
!  REVISION HISTORY:
! (1 ) The MIE_BULK is the old version (2009) of LMIE code has 4 elements, 
!      inlcuding extinction efficiency (Qext), scattering efficiency (Qsca), 
!      omega, and assymmetric factor. Now in the new version (2011), 
!      assymmetric factor is separated, and the first two elements are 
!      extinction (k_ext) and scattering coefficients (k_sca). They are related
!      by Qext = k_ext / G, where G is the geometric cross section. Hence,
!      in this code, what we need is Qext and its derivatives, G need to be 
!      applied. (xxu, 12/16/11)
! (2 ) Now all the Jacobians here are normalized to the denomators.
!      (xxu, 8/13/12)
!******************************************************************************
!
      ! References to F90 modules
      USE RTSMIE_PARAMETERS_M, ONLY : DP
      USE RTSMIE_PARAMETERS_M, ONLY : MAX_MIE_ANGLES, MAX_MIE_SIZES
      USE RTSMIE_SOURCECODE_PLUS_M, ONLY : RTSMIE_MAIN_PLUS 
      USE NAMELIST_ARRAY_MOD,  ONLY : LMONODIS,       MONOR
      USE NAMELIST_ARRAY_MOD,  ONLY : IDIST,          DISTPAR
      USE NAMELIST_ARRAY_MOD,  ONLY : SIZERANGE
      USE NAMELIST_ARRAY_MOD,  ONLY : RMRI,           N_MOMENTS
      USE NAMELIST_ARRAY_MOD,  ONLY : LPRT,           LDEBUG_MIE_CALC
      USE NAMELIST_ARRAY_MOD,  ONLY : LJACOB_REFR,    LJACOB_SIZE 
      USE ERROR_MOD,           ONLY : ERROR_STOP
      USE AEROSOL_MOD,         ONLY : NPANGLE,  ASYMM
      USE AEROSOL_MOD,         ONLY : NCOEFFS,        BULK_AOP      
      USE AEROSOL_MOD,         ONLY : EXPCOEFFS,      FMATRIX
      USE AEROSOL_MOD,         ONLY : DIST  
      USE AEROSOL_MOD,         ONLY : LPSD_BULK,      LPSD_ASYMM
      USE AEROSOL_MOD,         ONLY : LPSD_EXPCOEF,   LPSD_FMATRIX
      USE AEROSOL_MOD,         ONLY : LPSD_DIST,      LRFE_BULK 
      USE AEROSOL_MOD,         ONLY : LRFE_ASYMM,     LRFE_EXPCOEF 
      USE AEROSOL_MOD,         ONLY : LRFE_FMATRIX


      IMPLICIT NONE

      !================================================================
      ! Varibles used for calling the Linear MIE routine
      !  See RTSMIE code for details ...
      !================================================================

      ! Arguments
      REAL*8,  INTENT(IN)            :: LAMDA
      INTEGER, INTENT(IN)            :: IMODE 

      ! logicals
      LOGICAL       :: DO_EXPCOEFFS
      LOGICAL       :: DO_FMATRIX
      LOGICAL       :: DO_MONODISPERSE
      LOGICAL       :: DO_LINEARREF
      LOGICAL       :: DO_LINEARPSD

      ! Other inputs
      INTEGER       :: PSD_INDEX
      REAL(KIND=DP) :: PSD_PARS(3)
      REAL(KIND=DP) :: MONORADIUS
      REAL(KIND=DP) :: R1
      REAL(KIND=DP) :: R2
      LOGICAL       :: FIXR1R2
      INTEGER       :: NBLOCKS
      INTEGER       :: NWEIGHTS
      REAL(KIND=DP) :: XPARTICLE_LIMIT
      REAL(KIND=DP) :: R1R2_CUTOFF
      INTEGER       :: N_FMATRIX_ANGLES
      REAL(KIND=DP) :: FMATRIX_ANGLES(MAX_MIE_ANGLES)
      REAL(KIND=DP) :: WAVELENGTH
      REAL(KIND=DP) :: N_REAL
      REAL(KIND=DP) :: N_IMAG

      ! Outputs
      REAL(KIND=DP) :: MIE_BULK(3)
      REAL(KIND=DP) :: MIE_ASYMM
      INTEGER       :: MIE_NCOEFFS
      REAL(KIND=DP) :: MIE_EXPCOEFFS(6,0:MAX_MIE_ANGLES)
      REAL(KIND=DP) :: MIE_FMATRIX(4,MAX_MIE_ANGLES)
      REAL(KIND=DP) :: MIE_DIST(5)

      REAL(KIND=DP) :: LPSD_MIE_BULK(3,3)
      REAL(KIND=DP) :: LPSD_MIE_ASYMM(3)
      REAL(KIND=DP) :: LPSD_MIE_EXPCOEFFS(6,0:MAX_MIE_ANGLES,3)
      REAL(KIND=DP) :: LPSD_MIE_FMATRIX(4,MAX_MIE_ANGLES,3)
      REAL(KIND=DP) :: LPSD_MIE_DIST(5,3)
      REAL(KIND=DP) :: LRFE_MIE_BULK(3,3)
      REAL(KIND=DP) :: LRFE_MIE_ASYMM(2)
      REAL(KIND=DP) :: LRFE_MIE_EXPCOEFFS(6,0:MAX_MIE_ANGLES,2)
      REAL(KIND=DP) :: LRFE_MIE_FMATRIX(4,MAX_MIE_ANGLES,2)

      ! diagnostic outputs
      LOGICAL            :: FAIL
      INTEGER            :: ISTATUS
      CHARACTER(LEN=255) :: MESSAGE, TRACE, ACTION, ERRMSG

      !================================================================
      ! Additional local varibles
      !================================================================
      INTEGER       :: J, II, IU, K
      INTEGER       :: MIN_N
      REAL*8        :: MF1, MF2
       
      !================================================================
      ! RUN_LMIE begins here!
      !================================================================
      WRITE(6, * ) ' RUN_LMIE: calculate the Mie property'
       
      !================================================================
      ! Prepare inputs for the mie code
      !================================================================ 

      ! Initializations... logicals
      DO_EXPCOEFFS     = .TRUE.
      DO_FMATRIX       = .TRUE.
      DO_LINEARREF     =  LJACOB_REFR ! Ture for derivatives wrt refract
      DO_LINEARPSD     =  LJACOB_SIZE ! True for derivatives wrt size pars
      FIXR1R2          = .FALSE.    ! False for using the namelisted R1 R2
      
      ! Initializations... Some constants & dimensions 
      NBLOCKS          = 100
      NWEIGHTS         = 10
      XPARTICLE_LIMIT  = 1000.

      ! Initializations... Fmatrix angles are regular here between 0 and 180.
      N_FMATRIX_ANGLES = NPANGLE
      IF ( DO_FMATRIX ) THEN
         DO J = 1, N_FMATRIX_ANGLES 
            FMATRIX_ANGLES(j) = DBLE(J-1) * 180.0d0
     &                        / DBLE( N_FMATRIX_ANGLES - 1 )
         ENDDO
      ELSE
         FMATRIX_ANGLES = 0.0d0
      ENDIF

      ! Assign variables read from namelist.ini
      DO_MONODISPERSE = LMONODIS(IMODE)
      MONORADIUS      = MONOR(IMODE)
      PSD_INDEX       = IDIST(IMODE)
      PSD_PARS(1:3)   = DISTPAR(1:3,IMODE)
      R1              = SIZERANGE(1,IMODE)
      R2              = SIZERANGE(2,IMODE)
      WAVELENGTH      = LAMDA * 1d-3
      N_REAL          = RMRI(1,IMODE)
      N_IMAG          = ABS( RMRI(2,IMODE) )

      ! Screen print debugging
      IF ( LPRT ) THEN
         WRITE(6,100) 'IMODE     =', IMODE
         WRITE(6,100) 'PSD_INDEX =', PSD_INDEX
         WRITE(6,200) 'PSD_PARS  =', PSD_PARS
         WRITE(6,200) 'R1 & R2   =', R1, R2
         WRITE(6,200) 'Wave      =', WAVELENGTH
         WRITE(6,200) 'Refractive=', N_REAL, N_IMAG
         WRITE(6,100) 'N_F-ANGLES=', N_FMATRIX_ANGLES
      ENDIF
     
      !================================================================
      ! Call the RTSMIE routine
      !================================================================
      CALL RTSMIE_MAIN_PLUS    
     &   ( DO_EXPCOEFFS, DO_FMATRIX, DO_MONODISPERSE,           ! I
     &     DO_LINEARREF, DO_LINEARPSD,                          ! I
     &     PSD_INDEX, PSD_PARS, MONORADIUS,                     ! I
     &     R1, R2,  FIXR1R2,                                    ! I
     &     NBLOCKS, NWEIGHTS, XPARTICLE_LIMIT, R1R2_CUTOFF,     ! I
     &     N_FMATRIX_ANGLES, FMATRIX_ANGLES,                    ! I
     &     WAVELENGTH, N_REAL, N_IMAG,                          ! I
     &     MIE_BULK, MIE_ASYMM, MIE_NCOEFFS,                    ! O
     &     MIE_EXPCOEFFS, MIE_FMATRIX, MIE_DIST,                ! O
     &     LPSD_MIE_BULK, LPSD_MIE_ASYMM,                       ! O
     &     LPSD_MIE_EXPCOEFFS, LPSD_MIE_FMATRIX, LPSD_MIE_DIST, ! O
     &     LRFE_MIE_BULK, LRFE_MIE_ASYMM,                       ! O
     &     LRFE_MIE_EXPCOEFFS, LRFE_MIE_FMATRIX,                ! O
     &     FAIL, ISTATUS, MESSAGE, TRACE, ACTION )             ! O
 
      ! Check the run status
      IF ( FAIL ) THEN
         WRITE(6,'(a)') TRIM(ADJUSTL(MESSAGE))
         WRITE(6,'(a)') TRIM(ADJUSTL(TRACE  ))
         WRITE(6,'(a)') TRIM(ADJUSTL(ACTION )) 
         ERRMSG = 'RTSMie calculation failed ' 
         CALL ERROR_STOP( ERRMSG, 'run_lmie.f' )
      ENDIF

      ! Screen print for debug
      IF ( LPRT ) THEN
         WRITE(6,200) 'MIE_BULK =', MIE_BULK
         WRITE(6,200) 'MIE_DIST =', MIE_DIST
         WRITE(6,200) 'R1 & R2  =', R1, R2
      ENDIF  

      !================================================================  
      ! Now save the output to the module varaibles in AEROSOL_MOD
      !================================================================

      ! Bulk properties
      NCOEFFS(IMODE)      = MIN( MIE_NCOEFFS, N_MOMENTS )
      MIN_N               = NCOEFFS(IMODE)

      ! Ext Xsec, Scat Xsec, and single scattering albedo 
      BULK_AOP(1:3,IMODE) = MIE_BULK(1:3)

      ! Convert extinction coefficients to efficiency factor
      ! Qext = kext / G
      BULK_AOP(4:5,IMODE) = MIE_BULK(1:2) / MIE_DIST(2) 

      ! Size and distribution paramters
      DIST(:,IMODE)       = MIE_DIST 

      ! Assymetric factor and phase function expansions
      IF ( DO_EXPCOEFFS ) THEN 
         ASYMM(IMODE)     = MIE_ASYMM
         EXPCOEFFS(:,0:MIN_N,IMODE) = MIE_EXPCOEFFS(:,0:MIN_N)
      ENDIF

      ! F-Matrix 
      IF ( DO_FMATRIX ) FMATRIX(1:4,1:NPANGLE,IMODE)
     &                    = MIE_FMATRIX(1:4,1:NPANGLE)
 
      ! Linearizations ... wrt size parameters
      IF ( DO_LINEARPSD ) THEN

         ! Start a loop for size parameters
         DO K = 1, 3

            ! Copy variables to aerosol_mod module variables
            LPSD_BULK(1:3,K,IMODE) = LPSD_MIE_BULK(1:3,K) * PSD_PARS(K)
            LPSD_DIST(1:5,K,IMODE) = LPSD_MIE_DIST(1:5,K) * PSD_PARS(K)

            !----------------------------------------------------------
            ! Convert d_kext/d_rg to d_Qext/d_rg (xxu, 12/15/11):
            ! Here Qext = kext / G, then
            !  d_Qext / d_rg = ( d_kext/d_rg * G - d_G/d_rg * kext )
            !                / G^2
            !----------------------------------------------------------
            LPSD_BULK(4,K,IMODE) = ( LPSD_MIE_BULK(1,K) * MIE_DIST(2)
     &                             - LPSD_MIE_DIST(2,K) * MIE_BULK(1) )
     &                           / MIE_DIST(2) / MIE_DIST(2) 
     &                           * PSD_PARS(K)
            LPSD_BULK(5,K,IMODE) = ( LPSD_MIE_BULK(2,K) * MIE_DIST(2) 
     &                             - LPSD_MIE_DIST(2,K) * MIE_BULK(2) )
     &                           / MIE_DIST(2) / MIE_DIST(2)
     &                           * PSD_PARS(K)

            IF ( DO_EXPCOEFFS ) THEN
               LPSD_ASYMM(K,IMODE) = LPSD_MIE_ASYMM(K) * PSD_PARS(K)
               LPSD_EXPCOEF(:,0:MIN_N,K,IMODE) 
     &                             = LPSD_MIE_EXPCOEFFS(:,0:MIN_N,K)
     &                             * PSD_PARS(K)
            ENDIF

            IF ( DO_FMATRIX ) THEN 
               LPSD_FMATRIX(1:4,1:NPANGLE,K,IMODE) 
     &                             = LPSD_MIE_FMATRIX(1:4,1:NPANGLE,K)
     &                             * PSD_PARS(K)
            ENDIF

         ! End of loop K
         ENDDO 

      ENDIF

      ! Linearizations ... wrt refractive index
      IF ( DO_LINEARREF ) THEN

         LRFE_BULK(1:3,1,IMODE) = LRFE_MIE_BULK(1:3,1) * N_REAL
         LRFE_BULK(1:3,2,IMODE) = LRFE_MIE_BULK(1:3,2) * N_IMAG
         LRFE_BULK(4:5,1,IMODE) = LRFE_MIE_BULK(1:2,1) * N_REAL
     &                          / MIE_DIST(2) 
         LRFE_BULK(4:5,2,IMODE) = LRFE_MIE_BULK(1:2,2) * N_IMAG
     &                          / MIE_DIST(2)

         IF ( DO_EXPCOEFFS ) THEN 
            LRFE_ASYMM(1,IMODE) = LRFE_MIE_ASYMM(1) * N_REAL
            LRFE_ASYMM(2,IMODE) = LRFE_MIE_ASYMM(2) * N_IMAG
            LRFE_EXPCOEF(:,0:MIN_N,1,IMODE) 
     &                          = LRFE_MIE_EXPCOEFFS(:,0:MIN_N,1)
     &                          * N_REAL
            LRFE_EXPCOEF(:,0:MIN_N,2,IMODE)
     &                          = LRFE_MIE_EXPCOEFFS(:,0:MIN_N,2)
     &                          * N_IMAG
         ENDIF

         IF ( DO_FMATRIX ) THEN
               LRFE_FMATRIX(1:4,1:NPANGLE,1,IMODE)
     &                          = LRFE_MIE_FMATRIX(1:4,1:NPANGLE,1) 
     &                          * N_REAL
               LRFE_FMATRIX(:,1:NPANGLE,2,IMODE)
     &                          = LRFE_MIE_FMATRIX(:,1:NPANGLE,2)
     &                          * N_IMAG
         ENDIF


      ENDIF

      !================================================================
      ! End of LMIE model calculation,
      !================================================================

      ! Return to the calling routine if no debugging print
      IF ( .NOT. LDEBUG_MIE_CALC )  RETURN

      !================================================================
      ! Debug output of mie calculation
      !================================================================
      IU = 6 
      WRITE(IU,*) ' '
      WRITE(IU,*) '**************************************************'
      WRITE(IU,*) '********** L I N E A R I Z E D     M I E *********'
      WRITE(IU,*) '**************************************************'
      WRITE(IU,*)  '   MODE# = ', IMODE
      WRITE(IU,110)'  ==> Ext Xsec     :', BULK_AOP(1,IMODE)
      WRITE(IU,110)'  ==> Sca Xsec     :', BULK_AOP(2,IMODE)
      WRITE(IU,110)'  ==> SS Albedo    :', BULK_AOP(3,IMODE)
      WRITE(IU,110)'  ==> Ext Coeff.   :', BULK_AOP(4,IMODE)
      WRITE(IU,110)'  ==> Sca Coeff.   :', BULK_AOP(5,IMODE)
      WRITE(IU,110)'  ==> Asym factor  :', ASYMM(IMODE)
      WRITE(IU,110)'  ==> Total Density:', DIST(1,IMODE)
      WRITE(IU,110)'  ==> Geomet XSec  :', DIST(2,IMODE)
      WRITE(IU,110)'  ==> Dist. Volume :', DIST(3,IMODE)
      WRITE(IU,110)'  ==> Eff Radius   :', DIST(4,IMODE)
      WRITE(IU,110)'  ==> Eff Variance :', DIST(5,IMODE)
      WRITE(IU,130)'  ==> LRFE_BULK(1) :', LRFE_BULK(1,:,IMODE)
      WRITE(IU,130)'  ==> LRFE_BULK(2) :', LRFE_BULK(2,:,IMODE)
      WRITE(IU,130)'  ==> LRFE_BULK(3) :', LRFE_BULK(3,:,IMODE)
      WRITE(IU,130)'  ==> LRFE_BULK(4) :', LRFE_BULK(4,:,IMODE)
      WRITE(IU,130)'  ==> LRFE_BULK(5) :', LRFE_BULK(5,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_BULK(1) :', LPSD_BULK(1,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_BULK(2) :', LPSD_BULK(2,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_BULK(3) :', LPSD_BULK(3,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_BULK(4) :', LPSD_BULK(4,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_BULK(5) :', LPSD_BULK(5,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_DIST(1) :', LPSD_DIST(1,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_DIST(2) :', LPSD_DIST(2,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_DIST(3) :', LPSD_DIST(3,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_DIST(4) :', LPSD_DIST(4,:,IMODE)
      WRITE(IU,130)'  ==> LPSD_DIST(5) :', LPSD_DIST(5,:,IMODE)
      WRITE(IU,*) '********** EXPANSION COEFFICIENTS ***********'
      DO II = 0, NCOEFFS(IMODE) - 1
         WRITE(IU,160) II, EXPCOEFFS(:,II,IMODE)
      ENDDO

 100  FORMAT(A, I12  )
 110  FORMAT(A, F12.7)
 120  FORMAT(A, 2F12.7)
 130  FORMAT(A, 1P3E11.3)
 150  FORMAT(A, 1P5E11.3)
 160  FORMAT(I4, 6F11.7)
 170  FORMAT(I4, 5F11.7)
 180  FORMAT(I4, 6F12.5)  
 200  FORMAT(A,1P5E11.3) 


      ! Return the calling routine
      END SUBROUTINE  RUN_LMIE
