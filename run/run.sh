#!/bin/sh
#SBATCH --job-name=r.AR0
#SBATCH --time=10:00:00
#SBATCH --partition=jwang7
#SBATCH --error=$rundir/job.e
#SBATCH --output=$rundir/job.o
 
# unlimited memory and enable large stack size
ulimit -s unlimited
export KMP_STACKSIZE=209715200
 
# define your run & out directory
rundir=your_run_directory
outdir=output_folder_name

# now go to the run directory and start to run
cd ${rundir} 
mkdir ${outdir}
time ./unlvrtm.exe > log

# finially delete the executable file
rm ./unlvrtm.exe

